package com.gogoplay;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.londatiga.android.ActionItem;
import net.londatiga.android.QuickAction;
import net.londatiga.android.QuickAction.OnActionItemClickListener;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBar.TabListener;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gogoplay.data.Item;
import com.gogoplay.data.Item.Status;
import com.gogoplay.data.Notification.Action;
import com.gogoplay.data.User;
import com.gogoplay.ui.ShopActionBarActivity;
import com.gogoplay.ui.ShopActivity;
import com.gogoplay.ui.ShopApplication;
import com.gogoplay.ui.widget.ProgressDisplay;
import com.gogoplay.ui.widget.RecyclingImageView;
import com.gogoplay.ui.widget.ShopGridView;
import com.gogoplay.ui.widget.StaggeredGridView;
import com.gogoplay.util.Utils;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;

public class OrderHistory extends ShopActionBarActivity implements TabListener, OnRefreshListener<StaggeredGridView>, OnLastItemVisibleListener {
	//
	public static final int DISPLAY_DETAILS = 99;
	private static final String SELLING_TAB = "selling";
	private static final String BUYING_TAB = "buying";
	private static final String WATCHING_TAB = "watching";
	
	private ProductAdapter adapter;
	private Tab currentTab;
	private Tab sellingTab;
	private Tab buyingTab;
	private Tab watchingTab;
	private ProgressDisplay progress;
	private Item lastItem;

	private ShopGridView ptrGrid;

	private StaggeredGridView grid;

	private Handler itemDetailsHandler;
	
	private ProductsHandler handler;
	
	private Handler refreshHandler;

	private TextView footerView;
	
	private boolean loadOnServiceConnected;
	private int currentOrientation;
	private ViewGroup emptyArea;
	private TextView emptyTitle;
	private TextView empty;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// 
		super.onCreate(savedInstanceState);
		setContentView(R.layout.order_history);
		
		createUI(savedInstanceState);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.order_history, menu);

		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		switch (item.getItemId()) {
		case R.id.refresh:
			//
			refresh();
			return true;
		default:
			break;
		}

		return super.onOptionsItemSelected(item);
	}
	
	@SuppressLint("HandlerLeak")
	private void createUI(Bundle savedInstanceState) {
		//
		progress = (ProgressDisplay) findViewById(R.id.progress);
		
		handler = new ProductsHandler();
		
		refreshHandler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				// 
				hideMessage();
				refresh();
			}
		};
		
		itemDetailsHandler = new ItemDetailsHandler(); 
		adapter = new ProductAdapter(this, this, null);
		
		ptrGrid = (ShopGridView) findViewById(R.id.list);
		//ptrGrid.setEmptyView(emptyArea);
		ptrGrid.setOnRefreshListener(this);
		ptrGrid.setOnLastItemVisibleListener(this);
		
		grid = ptrGrid.getRefreshableView();
		
		footerView = new TextView(this);
		footerView.setGravity(Gravity.CENTER);
		footerView.setText(getString(R.string.end_of_list));
		footerView.setTextColor(getResources().getColor(R.color.light_blue));
		footerView.setTextSize(16);
		footerView.setTypeface(footerView.getTypeface(), Typeface.BOLD);
		int padding = (int) getResources().getDimensionPixelSize(R.dimen.padding);
		footerView.setPadding(padding, padding, padding, padding);
		footerView.setBackgroundResource(R.drawable.solid_rectangle);
		footerView.setVisibility(View.GONE);
		
		emptyArea = (ViewGroup) findViewById(R.id.emptyArea);
		
		emptyTitle = (TextView) findViewById(R.id.emptyTextTitle);
		emptyTitle.setVisibility(View.GONE);
		emptyTitle.setText(getEmptyTitle());
		
		empty = (TextView) findViewById(R.id.emptyText);
		empty.setVisibility(View.GONE);
		empty.setText(getEmptyText());
		
		/**
		 * <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:orientation="vertical" >
		 */
		
		LinearLayout footerLayout = new LinearLayout(this);
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		footerLayout.setLayoutParams(params);
		params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		footerLayout.addView(footerView, params);
		
		grid.addFooterView(footerLayout);
		
		grid.setAdapter(adapter);
		
		lastItem = null;

		// SET UP TABS
	    ActionBar actionBar = getSupportActionBar();
	    actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

	    // First Tab of the Activity
	    sellingTab = actionBar.newTab().setText("Selling").setTabListener(this);
	    sellingTab.setTag(SELLING_TAB);
	    actionBar.addTab(sellingTab);
	    actionBar.selectTab(sellingTab);
	    currentTab = sellingTab;

	    // Second Tab of the Activity
	    buyingTab = actionBar.newTab().setText("Buying").setTabListener(this);
	    buyingTab.setTag(BUYING_TAB);
	    actionBar.addTab(buyingTab);
	    
	    // Third Tab of the Activity
	    watchingTab = actionBar.newTab().setText("Watching").setTabListener(this);
	    watchingTab.setTag(WATCHING_TAB);
	    actionBar.addTab(watchingTab);
	}
	
	protected String getEmptyTitle() {
		//
		if (currentTab == sellingTab) {
			return getString(R.string.emptySellingTitle);
		} else if (currentTab == buyingTab) {
			
		} else if (currentTab == watchingTab) {
			return getString(R.string.emptyWishListTitle);
		}
		
		return getString(R.string.emptyProductsTitle);
	}
	
	protected String getEmptyText() {
		//
		if (currentTab == sellingTab) {
			return getString(R.string.emptySelling);
		} else if (currentTab == buyingTab) {
			
		} else if (currentTab == watchingTab) {
			return getString(R.string.emptyWishList);
		}
		return getString(R.string.emptyProducts);
	}
	
	@Override
	protected void onResumeFragments() {
		// 
		super.onResumeFragments();
		refresh();
	}
	
	@Override
	public String getViewFooter() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void dataServiceConnected(boolean connected) {
		// 
		if (loadOnServiceConnected && connected && adapter.getCount() <= 0) {
			loadOnServiceConnected = false;
			load(true, null);
		}
	}

	@Override
	public String getViewTitle() {
		// 
		return "My Listings";
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction arg1) {
		// 
		currentTab = tab;
		clear();
		refresh();
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction arg1) {
		// TODO Auto-generated method stub
		
	}
	
	public void displayProductDetails(Item item) {
		// 
		showMessage("");
		getDataService().getItem(this, item.getId(), null, itemDetailsHandler);
	}
	
	@SuppressLint("HandlerLeak")
	private class ItemDetailsHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			hideMessage();
			Item item = (Item) msg.getData().get("item");
			goToDetails(item);
		}
	}
	
	public void hideMessage() {
		progress.setVisibility(View.GONE);
		ptrGrid.onRefreshComplete();
	}
	
	public void showMessage(String msg) {
		//
		progress.setVisibility(View.VISIBLE);
	}
	
	@SuppressLint("HandlerLeak")
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// 
		if (data != null && resultCode == ShopActivity.RESULT_OK) {
			if (requestCode == DISPLAY_DETAILS) {
				// update current item
				Item updatedItem = (Item) data.getExtras().get("item");
				
				if (updatedItem != null) {
					//
					updateGrid(updatedItem);
				}
			}
		}
		
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	private void updateGrid(Item newItem) {
		if (newItem == null || grid.getCount() <= 0) 
			return;
		
		int start = grid.getFirstVisiblePosition();
		
		// Update the current adapter
		for (int i = start, j = grid.getLastVisiblePosition(); i <= j; i++) {
			Item oldItem = (Item) grid.getItemAtPosition(i);
			if (oldItem != null && Utils.equals(newItem.getId(),oldItem.getId())) {
				// store the new changes
				oldItem.merge(newItem);
				View view = grid.getChildAt(i - start);
				grid.getAdapter().getView(i, view, grid);
				break;
			}
		}
	}
	
	private void goToDetails(Item item) {
		//
		try {
			Intent intent = new Intent(this, ProductDetail.class);
			intent.setAction(ProductDetail.ACTION_DETAILS);
			
			intent.putExtra("params", item);
			
			startActivityForResult(intent, DISPLAY_DETAILS);
		} catch (Exception ex) {
			//
			
		}
	}
	
	private void refresh() {
		//
		load(true, null);
	}
	
	@SuppressLint("HandlerLeak")
	private void load(boolean refresh, String lastItemId) {
		//
		if (isDataServiceConnected()) {
			loadOnServiceConnected = false;
			showMessage("Loading...");
			
			if (!Utils.isEmpty(lastItemId)) {
				//
				footerView.setText(getString(R.string.loading));
				footerView.setVisibility(View.VISIBLE);
			}
			
			if (Utils.equals(currentTab.getTag(), SELLING_TAB)) {
				//
				Log.i(getTag(), "Fetching selling items " + (refresh ? " - for refresh" : ""));
				getDataService().getUserItems(this, lastItemId, getUser().getId(), handler, refresh);
			} else if (Utils.equals(currentTab.getTag(), BUYING_TAB)) {
				//
				Log.i(getTag(), "Fetching buying items " + (refresh ? " - for refresh" : ""));
				getDataService().getMyOffers(this, handler);
			} else {
				//
				Log.i(getTag(), "Fetching watched items " + (refresh ? " - for refresh" : ""));
				getDataService().getWatchedItems(this, lastItemId, handler, refresh);
			}
		} else {
			//
			loadOnServiceConnected = true;
		}
	}
	
	private void loadNextPage() {
		// load the next page of products
		
		if (isDataServiceConnected() && lastItem != null) {
			// make sure reload is only called once as needed
			showMessage("Loading next page...");
			
			String bottom = lastItem.getId();
			
			load(false, bottom);
			lastItem = null;
		}
	}
	
	@SuppressLint("HandlerLeak")
	private class ProductsHandler extends Handler {
		//
		@SuppressWarnings("unchecked")
		@Override
		public void handleMessage(Message msg) {
			//
			ArrayList<Item> items = (ArrayList<Item>) msg.getData().get("items");
			boolean refresh = msg.getData().getBoolean("refresh");
			String lastItemId = msg.getData().getString("lastItemId");
			
			displayItems(items, lastItemId, refresh);
			grid.setColumnCount((int)adapter.getNumColumns());
			adapter.notifyDataSetChanged();
		}
			
		private void displayItems(List<Item> items, String lastItemId, boolean refresh) {
			//
			hideMessage();
			
			//redraw();
			//grid.setNumColumns((int)adapter.getNumColumns());

			footerView.setVisibility(View.GONE);
			//grid.setColumnCount((int)adapter.getNumColumns(), false);
			
			if (!Utils.isEmpty(items)) {
				//
				emptyTitle.setVisibility(View.GONE);
				empty.setVisibility(View.GONE);
				lastItem = items.get(items.size() - 1);
					
				if (refresh) {
					// refresh data
					adapter.refresh(items);
					
				} else if (lastItemId == null) {
					//
					adapter.refresh(items);
				}
				else {
					adapter.append(items);
				}
			} else {
				//
				lastItem = null;
				
				if (refresh) {
					//
					// refresh data
					clear();
					
					emptyTitle.setVisibility(View.VISIBLE);
					empty.setVisibility(View.VISIBLE);
				}

				footerView.setText(getString(R.string.end_of_list));
			}
			
			toggleEmptyMessage(refresh);
			//grid.invalidateViews();
			
			//footerView.setVisibility(adapter.getCount() == 1 ? View.GONE : View.VISIBLE);
		}
	}
	
	public void clear() {
		//
		if (adapter != null) {
			adapter.refresh(new ArrayList<Item>());
		}
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// 
		super.onConfigurationChanged(newConfig);
		
		if (newConfig.orientation != currentOrientation) {
			//
			currentOrientation = newConfig.orientation;
			//scroller.setVisibility(View.GONE);
			redraw();
			//refresh();
		}
	}
	
	public void redraw() {
		//
		//grid.setColumnWidth(adapter.getColumnWidth());
		//grid.setNumColumns((int)adapter.getNumColumns());
		grid.setColumnCount((int) adapter.getNumColumns(), false);
		adapter.redraw();
		//grid.invalidateViews();
		toggleEmptyMessage(false);
	}
	
	private void toggleEmptyMessage(boolean refreshed) {
		//
		emptyTitle.setText(getEmptyTitle());
		empty.setText(getEmptyText());
		
		if (adapter.getCount() <= 0) {
			//
			emptyArea.setVisibility(View.VISIBLE);
			grid.setVisibility(View.GONE);
		} else {
			//
			emptyArea.setVisibility(View.GONE);
			grid.setVisibility(View.VISIBLE);
		}
	}
	
	static class ImageViewHolder {
		//
		ViewGroup root;
		RecyclingImageView imgView;
		ViewGroup itemDescLayout;
		TextView nameView;
		TextView descView;
		TextView priceView;
		TextView condView;
		View likes;
		ImageView likeIcon;
		ViewGroup itemOfferLayout;
		TextView itemOffer;
	}

	private class ProductAdapter extends BaseAdapter {
		//
		private static final String TAG = "MyListingsAdapter";

		private static final int ID_DETAILS = 1;

		private static final int ID_SHARE = 2;

		private static final int ID_SOLD = 3;

		private static final int ID_UNLIST = 4;

		private static final int ID_DELETE = 5;

		private static final int ID_EDIT = 6;
		
		private static final int ID_MESSAGE = 7;

		private FragmentActivity context;
		private OrderHistory view;
		private List<Item> items = Collections.synchronizedList(new ArrayList<Item>());
		private Handler itemLoadHandler;

		public ProductAdapter(OrderHistory view, FragmentActivity context, List<Item> items) {
			super();
			this.view = view;
			this.context = context;

			if (items != null) {
				this.items.addAll(items);
			}

			init();
		}

		@SuppressLint("HandlerLeak")
		private void init() {
			//
			itemLoadHandler = new Handler() {
				@Override
				public void handleMessage(Message msg) {
					//
					if (msg.getData() != null) {
						Item item = (Item) msg.getData().get("item");  
						ShopApplication.editProduct(context, item);
					}
				}
			};
		}
		
		private int getResId() {
			return R.layout.product;
		}
		
		public void redraw() {
			//
			super.notifyDataSetChanged();
		}

		@SuppressLint("HandlerLeak")
		public View getView(final int position, View convertView, ViewGroup parent) {

			View v = convertView;
			// 
			if (v == null) {
				LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
						Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(getResId(), parent, false);
			} else {
				//
				//v.forceLayout();
			}

			final Item item = (Item) getItem(position);

			if (item != null) {
				//
				initImageDisplay(item, v);
			}

			return v;
		}
		
		private void initImageDisplay(final Item item, final View v) {
			// 
			ImageViewHolder viewHolder;
			
			if (v.getTag() != null && v.getTag() instanceof ImageViewHolder) {
				viewHolder = (ImageViewHolder)v.getTag();
			} else {
				//
				viewHolder = new ImageViewHolder();
				
				viewHolder.root = (ViewGroup) v.findViewById(R.id.root);
				viewHolder.itemDescLayout = (ViewGroup) v.findViewById(R.id.itemDescLayout);
				viewHolder.imgView = (RecyclingImageView) v.findViewById(R.id.itemImage);
				viewHolder.nameView = (TextView) v.findViewById(R.id.itemTitle);
				viewHolder.descView = (TextView) v.findViewById(R.id.itemDesc);
				viewHolder.priceView = (TextView) v.findViewById(R.id.itemPrice);
				viewHolder.condView = (TextView) v.findViewById(R.id.itemCondition);
				viewHolder.likes = (View) v.findViewById(R.id.likeLayout);
				viewHolder.likeIcon = (ImageView) v.findViewById(R.id.likeIcon);
				viewHolder.itemOfferLayout = (ViewGroup) v.findViewById(R.id.itemOfferLayout);
				viewHolder.itemOffer = (TextView) v.findViewById(R.id.itemOffer);
				
				v.setTag(viewHolder);
			}
			
			setSize(viewHolder.root);
			
			viewHolder.descView.setVisibility(View.GONE);
			viewHolder.condView.setVisibility(View.GONE);
			viewHolder.likes.setVisibility(View.GONE);
			viewHolder.likeIcon.setVisibility(View.GONE);
			
			if (currentTab != null && Utils.equals(currentTab.getTag(), BUYING_TAB)) {
				//
				if (item.getOffer() == null) {
					viewHolder.itemOfferLayout.setVisibility(View.GONE);
					viewHolder.itemDescLayout.setVisibility(View.VISIBLE);
				} else {
					viewHolder.itemOfferLayout.setVisibility(View.VISIBLE);
					viewHolder.itemDescLayout.setVisibility(View.GONE);
					
					String status = "Purchased";
					
					com.gogoplay.data.Message offer = item.getOffer();
					
					if (Action.buy == offer.getAction()) {
						//
						double cost = 0;
						
						try {
							cost = Double.parseDouble(offer.getOffer());
						} catch (Exception ex) {
							cost = 0;
						}
						
						status = getString(R.string.offeredCaps) + " " + Utils.formatPrice(cost); 
					} else if (Action.trade == offer.getAction()) {
						//"Trade Requested";
						status = getString(R.string.tradeRequested); 
					}
					
					viewHolder.itemOffer.setText(status);
				}
			} else {
				//
				viewHolder.itemOfferLayout.setVisibility(View.GONE);
				viewHolder.itemDescLayout.setVisibility(View.VISIBLE);
			}
			
			if (!Utils.isEmpty(item.getImages())) {
				// Fetch and set bitmap for item
				String image = item.getImages().get(0).getUri();
				
				view.loadThumbnail(image, viewHolder.imgView);
			} else {
				// no images
				viewHolder.imgView.setScaleType(ScaleType.CENTER);
				viewHolder.imgView.setImageResource(R.drawable.loading);
			}
			
			CharSequence title = Utils.isEmpty(item.getTitle()) ? item.getDescription() : item.getTitle();
			viewHolder.nameView.setText(Utils.emptyIfNull(title));
			
			viewHolder.priceView.setText(Utils.formatPrice(context, item.getPrice()));

			v.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					//
					//
					if (currentTab != null && !Utils.equals(currentTab.getTag(), WATCHING_TAB)) {
						showQuickActionPopup(v, item, getDataService().getUser(context));
					} else {
						//
						view.displayProductDetails(item);
					}
				}
			});
		}
		
		public void showQuickActionPopup(final View v, final Item item, final User user) {
			//
			QuickAction quickAction = new QuickAction(context);
			boolean showMsgs = (currentTab != null && Utils.equals(currentTab.getTag(), BUYING_TAB)) || 
							   (getDataService().getMessageCountForItem(context, item.getId()) > 0);
			
			if (showMsgs) {
				//
				ActionItem message = new ActionItem(ID_MESSAGE, context
						.getResources().getString(R.string.view_messages), context
						.getResources().getDrawable(R.drawable.ic_menu_notifications));
				
				quickAction.addActionItem(message);
			}
			
			ActionItem details = new ActionItem(ID_DETAILS, context
					.getResources().getString(R.string.view_details), context
					.getResources().getDrawable(R.drawable.ic_action_picture));

			quickAction.addActionItem(details);
			
			ActionItem share = new ActionItem(ID_SHARE, context
					.getResources().getString(R.string.share), context
					.getResources().getDrawable(R.drawable.ic_action_share));
			
			quickAction.addActionItem(share);
			
			if (currentTab != null && Utils.equals(currentTab.getTag(), SELLING_TAB)) {
				//
				if (item.getStatus() != Status.sold) {
					ActionItem markAsSold = new ActionItem(ID_SOLD, context
							.getResources().getString(R.string.mark_sold), context
							.getResources().getDrawable(R.drawable.ic_action_labels));
				
					quickAction.addActionItem(markAsSold);
				}
				
				ActionItem unlist = new ActionItem(ID_UNLIST, context
						.getResources().getString(item.getStatus() == Status.active ? R.string.unlist : R.string.relist), context
						.getResources().getDrawable(item.getStatus() == Status.active ? R.drawable.ic_menu_report_image : R.drawable.ic_menu_add));
				
				quickAction.addActionItem(unlist);
				
				if (!showMsgs) {
					ActionItem delete = new ActionItem(ID_DELETE, context
							.getResources().getString(R.string.delete), context
							.getResources().getDrawable(R.drawable.ic_clear_dark));
					
					quickAction.addActionItem(delete);
				}
				
				ActionItem edit = new ActionItem(ID_EDIT, context
						.getResources().getString(R.string.edit), context
						.getResources().getDrawable(R.drawable.ic_menu_edit));
				
				quickAction.addActionItem(edit);
			}
			
			
			OnActionItemClickListener actionItemListener = new OnActionItemClickListener() {
				@Override
				public void onItemClick(QuickAction quickAction, int pos, int actionId) {
					//
					if (actionId == ID_DETAILS) {
						//
						view.displayProductDetails(item);
					} else if (actionId == ID_SHARE) {
						//
						shareProduct(item, user);
					} else if (actionId == ID_SOLD) {
						//
						if (isDataServiceConnected()) {
							item.setStatus(Status.sold);
							showMessage("Saving...");
							getDataService().editItem(OrderHistory.this, item, refreshHandler);
						}
					} else if (actionId == ID_UNLIST) {
						if (isDataServiceConnected()) {
							item.setStatus(item.getStatus() == Status.active ? Status.inactive : Status.active);
							showMessage("Saving...");
							getDataService().editItem(OrderHistory.this, item, refreshHandler);
						}
					} else if (actionId == ID_DELETE) {
						if (isDataServiceConnected()) {
							showMessage("Deleting...");
							getDataService().deleteItem(OrderHistory.this, item, refreshHandler);
						}
					} else if (actionId == ID_EDIT) {
						editProduct(item);
					} else if (actionId == ID_MESSAGE) {
						//
						if (Utils.equals(currentTab.getTag(), SELLING_TAB) && item.getSeller() == null) {
							//
							item.setSeller(getUser());
						}
						
						ShopApplication.goToMessages(OrderHistory.this, item);
					}
				}
			};
			
			// setup the action item click listener
			quickAction.setOnActionItemClickListener(actionItemListener);
			quickAction.show(v);
		}
		
		public void shareProduct(Item item, User user) {
			//
			Utils.shareProduct(context, view, item, user);
		}
		
		public void editProduct(Item item) {
			//
			if (isDataServiceConnected()) {
				//
				showMessage("Saving...");
				getDataService().getItem(context, item.getId(), null, itemLoadHandler);
			}
		}
		
		private void setSize(ViewGroup root) {
			// 
			LayoutParams params = root.getLayoutParams();
			
			// calculate width
			final DisplayMetrics screenSize = new DisplayMetrics();
			context.getWindowManager().getDefaultDisplay().getMetrics(screenSize);
			
			double screenWidth = screenSize.widthPixels;//context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT ? screenSize.widthPixels : screenSize.heightPixels;
			
			double swFactor = getNumColumns();
			
			params.width = (int) (screenWidth/swFactor);
			params.height = params.width;
			
			root.setLayoutParams(params);
		}
		
		public double getNumColumns() {
			// calculate width
			final DisplayMetrics screenSize = new DisplayMetrics();
			context.getWindowManager().getDefaultDisplay().getMetrics(screenSize);

			double screenWidth = screenSize.widthPixels;
			double swFactor = 1;

			double prod_pref_size = 0;
			
			prod_pref_size = context.getResources().getDimensionPixelSize(R.dimen.product_img_size);

			swFactor = Math.ceil(screenWidth / prod_pref_size);
			
			if (swFactor < 1) {
				swFactor = 1;
			}
			
			return (int) swFactor;
		}

		@Override
		public int getCount() {
			return items.size();
		}

		@Override
		public Object getItem(int position) {
			Object item = null;
			
			try {
				item = items.get(position);
			} catch (Exception ex) {
				Log.e(TAG, "Unable to get item", ex);
			}
			
			return item;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		/**
		 * Append these items to the end of the list
		 * 
		 * @param items
		 *            - items to append
		 */
		public void append(List<Item> items) {
			if (items != null) {
				this.items.addAll(items);
				super.notifyDataSetChanged();
			}
		}
		
		public void refresh(List<Item> items) {
			//
			this.items.clear();
			append(items);
		}

		public Context getContext() {
			return context;
		}
	}

	@Override
	public void onLastItemVisible() {
		// 
		loadNextPage();
	}

	@Override
	public void onRefresh(PullToRefreshBase<StaggeredGridView> refreshView) {
		// 
		refresh();
	}
}
