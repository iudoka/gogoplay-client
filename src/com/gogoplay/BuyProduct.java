package com.gogoplay;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.internal.widget.ListPopupWindow;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.gogoplay.data.Item;
import com.gogoplay.data.Item.Status;
import com.gogoplay.data.MessageThread;
import com.gogoplay.data.Notification.Action;
import com.gogoplay.ui.ShopActionBarActivity;
import com.gogoplay.ui.ShopApplication;
import com.gogoplay.ui.widget.ProgressDisplay;
import com.gogoplay.ui.widget.ShopEditText;
import com.gogoplay.util.Utils;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.Required;

// TODO: If item is free show a different view hide price entry 
public class BuyProduct extends ShopActionBarActivity implements ValidationListener {
	//
	public static final String ITEM_KEY = "params";
	private static final NumberFormat priceFormat = NumberFormat.getNumberInstance();
	
	static {
		//
		priceFormat.setMinimumFractionDigits(2);
		priceFormat.setMaximumFractionDigits(2);
	}
	
	private Item item;
	private Item tradeItem;
	
	private Validator validator;
	
	private ScrollView scroller;
	private ViewGroup tradeOffer;
	private ItemListAdapter tradeAdapter;
	private Handler tradeLoadHandler;
	private Handler tradeHandler;
	private ViewGroup buyArea;
	@Required(order = 1)
	private ShopEditText buyOffer;
	private ProgressDisplay progress;
	private TextView currency;
	private TextView offerItemName;
	private TextView offerItemPrice;
	private ImageView offerItemImage;
	private TextView itemName;
	private TextView itemPrice;
	private ImageView itemImage;
	private Button buyBtn;
	private Button cancelBtn;
	
	private boolean waitingToLoad;
	
	private boolean trade;
	private ListPopupWindow itemSelector;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// 
		super.onCreate(savedInstanceState);
		setContentView(R.layout.buy_product);
		
		createUI(savedInstanceState);
		setupListeners();
	}
	
	private void createUI(Bundle savedInstanceState) {
		//
		validator = new Validator(this);
		validator.setValidationListener(this);
		
		scroller = (ScrollView) findViewById(R.id.scroller);
		progress = (ProgressDisplay) findViewById(R.id.progress);
		tradeOffer = (ViewGroup) findViewById(R.id.tradeOffer);
		//tradeItem = new TextView(this);
    	//tradeItem.setText(getString(R.string.tradeOffer));
		tradeAdapter = new ItemListAdapter(this, null);
		offerItemName = (TextView) findViewById(R.id.offerItemName);
		offerItemPrice = (TextView) findViewById(R.id.offerItemPrice);
		offerItemImage = (ImageView) findViewById(R.id.offerItemImage);
		
		itemSelector = new ListPopupWindow(this);
	    itemSelector.setAdapter(tradeAdapter);
	    itemSelector.setModal(true); 
	    itemSelector.setBackgroundDrawable(getResources().getDrawable(R.drawable.abc_menu_dropdown_panel_holo_light));
	 
	    itemSelector.setAnchorView(findViewById(R.id.title)); 
	 
	    itemSelector.setWidth(ListPopupWindow.FILL_PARENT); 
	 
	    itemSelector.setOnItemClickListener(new OnItemClickListener() { 
	 
	        @Override 
	        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	            // show selection
	        	tradeItem = (Item) tradeAdapter.getItem(position);
	        	
	        	if (tradeItem != null) {
		        	findViewById(R.id.addInstr).setVisibility(View.GONE);
		        	
		        	offerItemName.setText(tradeItem.getTitle());
		        	offerItemName.setVisibility(View.VISIBLE);
		        	
		        	offerItemPrice.setText(Utils.formatPrice(BuyProduct.this, tradeItem.getPrice()));
		        	offerItemPrice.setVisibility(View.VISIBLE);
		        	
		        	if (!Utils.isEmpty(tradeItem.getImages())) {
		        		loadThumbnail(tradeItem.getImages().get(0).getUri(), offerItemImage);
		        	}
	        	}
	        	
	        	itemSelector.dismiss();
	        } 
	    });
		
		//tradeOffer.setAdapter(tradeAdapter);
		buyArea = (ViewGroup) findViewById(R.id.buyArea);
		buyOffer = (ShopEditText) findViewById(R.id.buyOffer);
		currency = (TextView) findViewById(R.id.currency);
		currency.setVisibility(View.VISIBLE);
		itemName = (TextView) findViewById(R.id.itemName);
		itemPrice = (TextView) findViewById(R.id.itemPrice);
		itemImage = (ImageView) findViewById(R.id.itemImage);
		buyBtn = (Button) findViewById(R.id.buyBtn);
		cancelBtn = (Button) findViewById(R.id.cancelBtn);
	}

	@SuppressLint("HandlerLeak")
	private void setupListeners() {
		//
		tradeOffer.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 
				if (tradeAdapter.getCount() <= 0) {
					//
					//Toast.makeText(BuyProduct.this, getString(R.string.noTradeListings), Toast.LENGTH_LONG).show();
					ShopApplication.showMessage(BuyProduct.this, getString(R.string.noTradeListings));
				} else {
					//
					itemSelector.show();
				}
			}
		});
		
		tradeLoadHandler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				// 
				if (msg.getData() != null) {
					@SuppressWarnings("unchecked")
					ArrayList<Item> items = (ArrayList<Item>) msg.getData().get("items");
					ArrayList<Item> validItems = new ArrayList<Item>();
					
					for (Item i : items) {
						//
						if (i.getStatus() == Status.active) {
							//
							validItems.add(i);
						}
					}
					
					tradeAdapter.setItems(validItems);
					hideProgress();
				}
			}
		};
		
		tradeHandler = new Handler() {
			@SuppressWarnings("unchecked")
			@Override
			public void handleMessage(Message msg) {
				// 
				hideProgress();
				
				if (msg.getData() != null) {
					//
					if (msg.getData().containsKey("messages")) {
						//
						boolean allowOffer = !offerExists((List<MessageThread>) msg.getData().get("messages"));
						
						if (allowOffer) {
							if (!trade) {
								//
								getDataService().buyItem(BuyProduct.this, item, Utils.parseDouble(buyOffer.getText().toString()), tradeHandler);
							} else {
								getDataService().tradeItem(BuyProduct.this, item, tradeItem, tradeHandler);
							}
						} else {
							//
							Toast.makeText(BuyProduct.this, "You have already made the same offer!", Toast.LENGTH_LONG).show();
						}
					} else {
						//
						String alert = "Offer Sent!";
						
						if (!msg.getData().getBoolean("success")) {
							alert = "Unable to send offer!";
						}
						
						Toast.makeText(BuyProduct.this, alert, Toast.LENGTH_LONG).show();
						onBackPressed();
					}
				}
			}
		};
		
		buyBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 
				validator.validate();
			}
		});
		
		// Add custom validation rules
		Rule<View> rule = new Rule<View>(getString(R.string.select_item)) {

			@Override
			public boolean isValid(final View view) {
				//
				boolean valid = true;
				
				if (trade) 
					valid = tradeItem != null;
				
				scroller.post(new Runnable() {
					public void run() {
						view.requestFocusFromTouch();
					}
				});
				
				return valid;
			}
		};
		validator.put(tradeOffer, rule);
		
		cancelBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 
				onBackPressed();
			}
		});
		
		buyOffer.setFilters(new InputFilter[] {new CurrencyFormatInputFilter()});
		
		buyOffer.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// 
				int currencyColor = getResources().getColor(R.color.gray);
				//
				if (!Utils.isEmpty(s)) {
					//
					currencyColor = getResources().getColor(R.color.dark_gray);
				}
				
				currency.setTextColor(currencyColor);
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
		buyOffer.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				//
				v.setText(priceFormat.format(Double.parseDouble(v.getText().toString())));
				
				return false;
			}
		});
	}
	
	private boolean offerExists(List<MessageThread> list) {
		// 
		boolean exists = false;
		
		if (list == null) {
			return false;
		}
		
		for (MessageThread thread : list) {
			for (com.gogoplay.data.Message msg : thread.getMessages()) {
				//
				if (trade) {
					//
					 if (msg.getAction() == Action.trade) {
						 //
						 if (tradeItem.getId().equalsIgnoreCase(msg.getOffer())) {
							 exists = true;
							 break;
						 }
					 }
				} else {
					//
					if (msg.getAction() == Action.buy) {
						double oldOffer = Utils.parseDouble(msg.getOffer());
						double newOffer = Utils.parseDouble(buyOffer.getText().toString());
						
						if (oldOffer == newOffer) {
							exists = true;
							break;
						}
					}
				}
			}
		}
		
		return exists;
	}

	private class ItemListAdapter extends ArrayAdapter<Item> {
		//
		private static final int resLayout = R.layout.product_marker;
		private LayoutInflater inflater;
		private ArrayList<Item> items;
		
		public ItemListAdapter(Context context, ArrayList<Item> items) {
			super(context, resLayout);
			
			inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			
			if (items == null) {
				items = new ArrayList<Item>();
			}
			
			this.items = items;
		}
		
		public void setItems(ArrayList<Item> items) {
			//
			this.items.clear();
			this.items.addAll(items);
			
			notifyDataSetChanged();
		}
		
		@Override
		public int getCount() {
			// 
			return items.size();
		}
		
		@Override
		public Item getItem(int position) {
			// 
			try {
				return items.get(position);
			} catch (Exception ex) {
				return null;
			}
		}
		
		@Override 
	    public View getDropDownView(int position, View convertView,ViewGroup parent) {
	        return getCustomView(position, convertView, parent);
	    } 
	  
	    @Override 
	    public View getView(int position, View convertView, ViewGroup parent) {
	        return getCustomView(position, convertView, parent);
	    	/*Item item = items.get(position);
        	tradeItem.setText(item.getTitle() + " - " + Utils.formatPrice(BuyProduct.this, item.getPrice()));
	    	return tradeItem;*/
	    } 
	  
	    public View getCustomView(int position, View convertView, ViewGroup parent) {
	    	//
	        View v = inflater.inflate(resLayout, parent, false);
	        
	        ImageView itemImage = (ImageView) v.findViewById(R.id.itemImage);
			TextView itemTitle = (TextView) v.findViewById(R.id.itemTitle);
			TextView itemDesc = (TextView) v.findViewById(R.id.itemDesc);
			TextView itemPrice = (TextView) v.findViewById(R.id.itemPrice);
			TextView itemCondition = (TextView) v.findViewById(R.id.itemCondition);
	        
	            // Set values for spinner each row
	        	Item item = items.get(position);
	        	itemImage.setVisibility(View.VISIBLE);
	            
	        	if (!Utils.isEmpty(item.getImages())) {
	        		loadThumbnail(item.getImages().get(0).getUri(), itemImage);
	        	} else {
	        		//
	        		itemImage.setScaleType(ScaleType.FIT_CENTER);
	        		itemImage.setImageResource(R.drawable.loading);
	        	}
	        	
	        	itemTitle.setVisibility(View.VISIBLE);
	        	itemTitle.setText(Utils.emptyIfNull(item.getTitle()));
	        	
	        	itemDesc.setVisibility(View.VISIBLE);
	        	
	        	if (Utils.isEmpty(item.getDescription())) {
					//itemDesc.setVisibility(View.GONE);
					if (itemTitle.getVisibility() != View.GONE){
						itemTitle.setVisibility(View.GONE);
						itemDesc.setText(Utils.trimToFit(item.getTitle(), 25, "..."));
					}
				} else {
					itemDesc.setText(Utils.trimToFit(item.getDescription(), 25, "..."));
				}
	        	
	        	itemPrice.setText(Utils.formatPrice(BuyProduct.this, item.getPrice()));
				
	        	itemCondition.setText(Utils.trimToFit(item.getCondition(), 25, ""));
				itemCondition.setVisibility(Utils.isEmpty(item.getCondition()) ? View.GONE : View.VISIBLE);
   
	  
	        return v;
	    }
	}

	public class CurrencyFormatInputFilter implements InputFilter {

		Pattern mPattern = Pattern.compile("(0|[1-9]+[0-9]*)?(\\.[0-9]{0,2})?");

		@Override
		public CharSequence filter(CharSequence source, int start, int end,
				Spanned dest, int dstart, int dend) {

			String result = dest.subSequence(0, dstart) + source.toString()
					+ dest.subSequence(dend, dest.length());

			Matcher matcher = mPattern.matcher(result);

			if (!matcher.matches())
				return dest.subSequence(dstart, dend);

			return null;
		}
	}
	
	@Override
	protected void onResumeFragments() {
		// 
		super.onResumeFragments();
		
		if (isDataServiceConnected()) {
			loadItem();
		} else {
			waitingToLoad = true;
		}
	}
	
	@Override
	public String getViewFooter() {
		// 
		return null;
	}

	@Override
	protected void dataServiceConnected(boolean connected) {
		// 
		if (connected && waitingToLoad) {
			//
			waitingToLoad = false;
			loadItem();
		}
	}
	
	@SuppressWarnings("rawtypes")
	private void loadItem() {
		//
		Intent i = getIntent();
		trade = false;
		
		if (i != null && i.getExtras() != null) {
			
			Object param = i.getExtras().get(ITEM_KEY);
			
			if (param instanceof Item) {
				item = (Item) param;
			} else {
				item = (Item)((ArrayList)param).get(0);
				trade = true;
			}
			
			populate();
		}
	}
	
	// 
	private void populate() {
		//
		if (item == null) {
			return;
		}
		
		String title = getString(R.string.buy);
		
		buyArea.setVisibility(View.VISIBLE);
		tradeOffer.setVisibility(View.GONE);
		
		if (trade) {
			title = getString(R.string.trade);
			//tradeOffer.setSelection(0);
			buyArea.setVisibility(View.GONE);
			tradeOffer.setVisibility(View.VISIBLE);
			
			// load trade options
			if (isDataServiceConnected()) {
				//
				showProgress();
				getDataService().getUserItems(this, null, getUser().getId(), tradeLoadHandler, true);
			}
		}
		
		setViewTitle(title + " - " + Utils.emptyIfNull(item.getTitle()), null);
		
		buyOffer.setText(priceFormat.format(item.getPrice()));
		itemName.setText(item.getTitle());
		itemPrice.setText(Utils.formatPrice(this, item.getPrice()));
		
		if (!Utils.isEmpty(item.getImages())) {
			loadThumbnail(item.getImages().get(0).getUri(), itemImage);
		} else {
			itemImage.setImageResource(R.drawable.loading);
		}
	}
	
	private void showProgress() {
		progress.setVisibility(View.VISIBLE);
	}
	
	private void hideProgress() {
		progress.setVisibility(View.GONE);
	}
	
	private void makeOffer() {
		//
		showProgress();
		
		if (isDataServiceConnected()) {
			
			getDataService().getMessagesForItem(this, item.getId(), tradeHandler);
		}
	}

	@Override
	public String getViewTitle() {
		// 
		return "Buy";
	}

	@Override
	public void onValidationSucceeded() {
		// 
		makeOffer();
	}

	@Override
	public void onValidationFailed(View failedView, Rule<?> failedRule) {
		// 
		String message = failedRule.getFailureMessage();

		if (failedView instanceof EditText) {
			failedView.requestFocus();
			((EditText) failedView).setError(message);
		} else {
			ShopApplication.showMessage(this, message);
		}
	}

}
