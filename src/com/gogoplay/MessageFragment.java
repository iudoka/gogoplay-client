package com.gogoplay;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gogoplay.data.Item;
import com.gogoplay.data.MessageThread;
import com.gogoplay.data.User;
import com.gogoplay.data.Notification.Action;
import com.gogoplay.ui.ShopApplication;
import com.gogoplay.ui.ShopFragment;
import com.gogoplay.ui.widget.ActionDisplayListener;
import com.gogoplay.ui.widget.Avatar;
import com.gogoplay.ui.widget.ProgressDisplay;
import com.gogoplay.util.Utils;

public class MessageFragment extends ShopFragment {
	//
	private List<MessageThread> messageThreads;
	private HashMap<String, Item> cachedItems = new HashMap<String, Item>();
	
	private ProgressDisplay progress;
	private ListView notificationList;
	private ViewGroup itemHeader;
	private ImageView itemImage;
	private TextView itemTitle;
	private TextView itemDescr;
	private ViewGroup messageEntryPanel;
	private EditText message;
	private View sendBtn;
	private MessageAdapter adapter;
	private String leftUserId;
	
	private Handler handler;

	private Item item;
	
	@Override
	public void dataServiceConnected(boolean connected) {
		// 
		
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
		//
		View v = inflater.inflate(R.layout.message_fragment, parent, false);
		createUI(v);
		setupListeners();
		return v;
	}
	
	private void createUI(View v) {
		//
		itemHeader = (ViewGroup) v.findViewById(R.id.itemHeader);
		itemImage = (ImageView) v.findViewById(R.id.itemImage);
		itemTitle = (TextView) v.findViewById(R.id.itemTitle);
		itemDescr = (TextView) v.findViewById(R.id.itemDescr);
		messageEntryPanel = (ViewGroup) v.findViewById(R.id.messageEntryPanel);
		message = (EditText) v.findViewById(R.id.message);
		sendBtn = (View) v.findViewById(R.id.sendMessage);
		adapter = new MessageAdapter();
		notificationList = (ListView) v.findViewById(R.id.messageList);
		notificationList.setAdapter(adapter);
		//notificationList.addHeaderView(itemHeader);
		progress = (ProgressDisplay) v.findViewById(R.id.progress);
		//notificationList.setOnRefreshListener(this);
	}
	
	private static class MessageViewHolder {
		ImageView avatarLeft;
		ImageView avatarRight;
		TextView message;
		TextView messageDate;
		ViewGroup bubble;
		ViewGroup tradeOffer;
		TextView tradeName;
		TextView tradePrice;
		ImageView tradeImage;
	}
	
	private static class ThreadViewHolder {
		ImageView avatar;
		TextView title;
		TextView messageCount;
	}
	
	private boolean isMessageThread() {
		//
		return messageThreads.size() > 1;
	}
	
	@SuppressLint("HandlerLeak")
	private class MessageAdapter extends BaseAdapter {
		//
		private static final int threadResId = R.layout.message_thread;
		private static final int msgResId = R.layout.message_bubble;
		
		public MessageAdapter() {
			//
			registerDataSetObserver(new DataSetObserver() { 
			    @Override 
			    public void onChanged() { 
			        super.onChanged(); 
			        notificationList.setSelection(adapter.getCount() - 1);    
			    } 
			}); 
		}
		
		@Override
		public int getCount() {
			// 
			if (messageThreads != null) {
				if (isMessageThread()) {
					return messageThreads.size();
				} else {
					return messageThreads.get(0).getMessages().size();
				}
			}
			
			return 0;
		}

		@Override
		public Object getItem(int position) {
			// 
			if (isMessageThread()) {
				return messageThreads.get(position);
			} else {
				return messageThreads.get(0).getMessages().get(position);
			}
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			//
			final Object msg = getItem(position);
			
			if (msg instanceof com.gogoplay.data.Message) {
				//
				return initMessage(convertView, parent, (com.gogoplay.data.Message) msg);
			} else if (msg instanceof MessageThread) {
				//
				return initThread(convertView, parent, (MessageThread) msg);
			}
			
			return convertView;
		}
		
		private View initMessage(View v, ViewGroup parent, final com.gogoplay.data.Message theMsg) {
			//
			MessageViewHolder h = null;
			
			if (v == null) {
				LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(
						Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(msgResId, parent, false);
			} else if (v.getTag() != null && v.getTag() instanceof MessageViewHolder) {
				h = (MessageViewHolder) v.getTag();
			} else {
				v.forceLayout();
			}
			
			if (h == null) {
				//
				h = new MessageViewHolder();
				h.avatarLeft = (ImageView) v.findViewById(R.id.avatarLeft);
				h.avatarRight = (ImageView) v.findViewById(R.id.avatarRight);
				h.message = (TextView) v.findViewById(R.id.message);
				h.messageDate = (TextView) v.findViewById(R.id.messageDate);
				h.bubble = (ViewGroup) v.findViewById(R.id.bubble);
				h.tradeOffer = (ViewGroup) v.findViewById(R.id.tradeOffer);
				h.tradeName = (TextView) v.findViewById(R.id.tradeName);
				h.tradePrice = (TextView) v.findViewById(R.id.tradePrice);
				h.tradeImage = (ImageView) v.findViewById(R.id.tradeImage);
				v.setTag(h);
			}
			
			//
			h.avatarLeft.setVisibility(View.GONE);
			h.avatarRight.setVisibility(View.GONE);
				
			ImageView view = h.avatarLeft;
			int bubbleRes = R.drawable.message_bubble_left;
				
			if (!theMsg.getFrom().getId().equalsIgnoreCase(leftUserId)) {
				view = h.avatarRight;
				bubbleRes = R.drawable.message_bubble_right;
			}
				
			view.setVisibility(View.VISIBLE);
			
			final ImageView imgView = view;
			view.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// 
					final ActionDisplayListener displayListener = new ActionDisplayListener() {
						
						@Override
						public boolean canShow() {
							// 
							User currUser = getUser();
								
							if (currUser != null) {
								return !Utils.equals(currUser.getId(), theMsg.getFrom().getId());
							}
							
							return true;
						}
						
						@Override
						public void followUser() {
							//
							getDataService().followUser(getActivity(), theMsg.getFrom(), new Handler());
						}
						
						@Override
						public void viewProfile() {
							//
							ShopApplication.goToProfile(getActivity(), theMsg.getFrom());
						}
						
						@Override
						public void reportUser() {
							//
							getDataService().reportUser(getActivity(), theMsg.getFrom(), new Handler());
						}
					};
					
					Avatar.showUserPopup(getActivity(), imgView, theMsg.getFrom(), getUser(), displayListener);
				}
			});
			
			((Messages)getActivity()).loadThumbnail((theMsg).getFrom().getPhoto(), view);
			h.bubble.setBackgroundResource(bubbleRes);
				
			h.message.setText(theMsg.getMessage() != null ? theMsg.getMessage() : theMsg.getNotification());
			h.messageDate.setText(Utils.printDate(theMsg.getDateReceived()));
			
			h.tradeOffer.setVisibility(View.GONE);
			if (theMsg.getAction() == Action.trade) {
				//
				final ViewGroup tradeOffer = h.tradeOffer;
				final ImageView tradeImage = h.tradeImage;
				final TextView tradeName = h.tradeName;
				final TextView tradePrice = h.tradePrice;
				
				final Item tradeItem = cachedItems.get(theMsg.getOffer());
				
				if (tradeItem != null) {
					//
					tradeOffer.setVisibility(View.VISIBLE);
					tradeName.setText(tradeItem.getTitle());
					tradePrice.setText(Utils.formatPrice(tradeItem.getPrice()));
					((Messages)getActivity()).loadThumbnail(tradeItem.getImages().get(0).getUri(), tradeImage);
					
					tradeOffer.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// 
							ShopApplication.goToProductDetails(getActivity(), tradeItem.getId());
						}
					});
				} else {
					getDataService().getItem(getActivity(), theMsg.getOffer(), null, new Handler() {
						//
						@Override
						public void handleMessage(Message msg) {
							// 
							if (msg.getData() != null && !isHidden() && getActivity() != null) {
								//
								final Item item = (Item) msg.getData().get("item");
								cachedItems.put(item.getId(), item);
								
								tradeOffer.setVisibility(View.VISIBLE);
								tradeName.setText(item.getTitle());
								tradePrice.setText(Utils.formatPrice(item.getPrice()));
								((Messages)getActivity()).loadThumbnail(item.getImages().get(0).getUri(), tradeImage);
								
								tradeOffer.setOnClickListener(new OnClickListener() {
									
									@Override
									public void onClick(View v) {
										// 
										ShopApplication.goToProductDetails(getActivity(), item.getId());
									}
								});
							}
						}
					});
				}
			}
			
			return v;
		}
		
		private View initThread(View v, ViewGroup parent, final MessageThread thread) {
			//
			ThreadViewHolder h = null;
			
			if (v == null) {
				LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(
						Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(threadResId, parent, false);
			} else if (v.getTag() != null && v.getTag() instanceof ThreadViewHolder) {
				h = (ThreadViewHolder) v.getTag();
			} else {
				v.forceLayout();
			}
			
			if (h == null) {
				//
				h = new ThreadViewHolder();
				h.avatar = (ImageView) v.findViewById(R.id.avatar);
				h.title = (TextView) v.findViewById(R.id.title);
				h.messageCount = (TextView) v.findViewById(R.id.messageCount);
				v.setTag(h);
			}
			
			final User fromUser = thread.getFrom(getUser());
			
			if (fromUser != null) {
				//
				((Messages)getActivity()).loadThumbnail(fromUser.getPhoto(), h.avatar);
				h.title.setVisibility(View.VISIBLE);
				h.title.setText(fromUser.getNickname() + " - " + Utils.printDate(thread.getDate()));
			} else {
				h.title.setVisibility(View.GONE);
			}
			
			final View imgView = h.avatar;
			h.avatar.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// 
					final ActionDisplayListener displayListener = new ActionDisplayListener() {
						
						@Override
						public boolean canShow() {
							// 
							User currUser = getUser();
								
							if (currUser != null) {
								return !Utils.equals(currUser.getId(), fromUser.getId());
							}
							
							return true;
						}
						
						@Override
						public void followUser() {
							//
							getDataService().followUser(getActivity(), fromUser, new Handler());
						}
						
						@Override
						public void viewProfile() {
							//
							ShopApplication.goToProfile(getActivity(), fromUser);
						}
						
						@Override
						public void reportUser() {
							//
							getDataService().reportUser(getActivity(), fromUser, new Handler());
						}
					};
					
					Avatar.showUserPopup(getActivity(), imgView, fromUser, getUser(), displayListener);
				}
			});
			
			int count = thread.getMessages().size();
			h.messageCount.setText(count + " message" + (count != 1 ? "s" : ""));
			
			v.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View view) {
					// 
					ArrayList<MessageThread> threads = new ArrayList<MessageThread>();
					threads.add(thread);
					
					((Messages)getActivity()).showMessagesScreen(threads);
				}
			});
			
			return v;
		}

		public void add(com.gogoplay.data.Message mess) {
			// 
			//messageThreads.get(0).addMessage(mess);
			notifyDataSetChanged();
		}

		public void setData(@SuppressWarnings("rawtypes") List messages) {
			// 
			notifyDataSetChanged();
		}
	}
	
	@SuppressLint("HandlerLeak")
	private void setupListeners() {
		//
		handler = new Handler() {
			//
			@Override
			public void handleMessage(Message msg) {
				// 
				if (msg.getData() != null) {
					//
					com.gogoplay.data.Message mess = (com.gogoplay.data.Message) msg.getData().get("message");
					
					if (mess != null) {
						messageThreads.get(0).addMessage(mess);
						adapter.add(mess);
					} else {
						Toast.makeText(getActivity(), "Unable to send message", Toast.LENGTH_LONG).show();
					}
					
					progress.setVisibility(View.GONE);
				}
			}
		};
		
		itemHeader.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 
				if (item != null) {
					ShopApplication.goToProductDetails(getActivity(), item.getId());
				}
			}
		});
		
		sendBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 
				String msg = Utils.nullEmptyString(message.getText().toString().trim());
				
				if (msg != null) {
					progress.setVisibility(View.VISIBLE);
					getDataService().sendItemMessage(getActivity(), item, msg, handler);
					message.setText("");
				}
			}
		});
	}

	@Override
	public void refresh() {
		// 
		/*adapter = new NotificationAdapter(this, getActivity(), null);
		notificationList = (PullToRefreshListView) getView().findViewById(R.id.grid);
		notificationList.setAdapter(adapter);
		notificationList.setOnRefreshListener(this);*/
	}

	public void setData(Item item, List<MessageThread> threads) {
		// 
		if (!Utils.isEmpty(item.getImages())) {
			loadThumbnail(item.getImages().get(0).getUri(), itemImage);
		}
		
		this.item = item;
		itemTitle.setText(Utils.emptyIfNull(item.getTitle()) + " - " + Utils.formatPrice(getActivity(), item.getPrice()));
		itemDescr.setText(Utils.emptyIfNull(item.getDescription()));
		leftUserId = item.getSeller().getId();
		
		messageThreads = threads;
		
		if (messageThreads.size() == 1) {
			adapter.setData(messageThreads.get(0).getMessages());
			messageEntryPanel.setVisibility(View.VISIBLE);
		} else {
			adapter.setData(messageThreads);
			// hide message entry area
			messageEntryPanel.setVisibility(View.GONE);
		}
	}
}
