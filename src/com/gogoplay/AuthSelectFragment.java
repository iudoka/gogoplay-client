package com.gogoplay;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.gogoplay.R;
import com.gogoplay.data.User;
import com.gogoplay.ui.ShopApplication;
import com.gogoplay.ui.ShopFragment;
import com.gogoplay.ui.widget.FacebookLoginButton;
import com.gogoplay.ui.widget.FacebookLoginButton.Mode;
import com.gogoplay.ui.widget.ProgressDisplay;
import com.gogoplay.util.Utils;

public class AuthSelectFragment extends ShopFragment {
	//
	private Handler handler;
	private Mode mode;
	//private View root;
	private View fbBtn;
	private TextView fbBtnTxt;
	private View emailBtn;
	private TextView emailBtnTxt;
	private Button modeBtn;
	private ProgressDisplay progress;
	
	@Override
	public void dataServiceConnected(boolean connected) {
		//
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	 public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
		 //
		 View v = inflater.inflate(R.layout.auth_select_fragment, parent, false);
		 createUI(v);
		 setupListeners();
		 return v;
	 }
	
	private void createUI(View v) {
		//
		handler = new SignupHandler();
		//root = v.findViewById(R.id.root);
		
		fbBtn = (View) v.findViewById(R.id.fbBtn);
		fbBtnTxt = (TextView) v.findViewById(R.id.fbBtnText);
		
		emailBtn = (View) v.findViewById(R.id.emailBtn);
		emailBtnTxt = (TextView) v.findViewById(R.id.emailBtnText);
		emailBtn.setVisibility(View.GONE);
		
		View orLabel = (View) v.findViewById(R.id.orLabel);
		orLabel.setVisibility(View.GONE);
		
		modeBtn = (Button) v.findViewById(R.id.modeBtn);
		
		progress = (ProgressDisplay) v.findViewById(R.id.progress);

		// set appropriate mode
		// if first time launching app, mode = signup
		Mode newMode = Mode.Signup;
		
		setMode(newMode);
	}
	
	protected void setMode(Mode mode) {
		//
		this.mode = mode;
		FacebookLoginButton.setMode(mode);
		
		if (fbBtnTxt != null) {
			fbBtnTxt.setText((mode == Mode.Login ? "Log in" : "Sign up") + " with Facebook");
			emailBtnTxt.setText((mode == Mode.Login ? "Log in" : "Sign up") + " with email");
			modeBtn.setText(mode == Mode.Login ? "Sign up" : "Log in");
		}
	}
	
	private void setupListeners() {
		//
		modeBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Mode newMode = Mode.Signup;
				
				if (mode == newMode) {
					newMode = Mode.Login;
				}
				
				setMode(newMode);
			}
		});
		
		emailBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// Go to entry fragment
				((LoginScreen)getActivity()).showEntryScreen();
			}
		});
		

		fbBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// Go to entry fragment
				progress.setVisibility(View.VISIBLE);
				((LoginScreen)getActivity()).startFBSession();
			}
		});
	}
	
	@Override
	public void onResume() {
		//
		super.onResume();
		//Intent intent = new Intent(this, LoginActivity.class);

		//startActivityForResult(intent, 1);
	}
	
	@Override
	public void onPause() {
		//
		super.onPause();
	}
	
	@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
	
	@SuppressLint("HandlerLeak")
	private class SignupHandler extends Handler {
		//
		@Override
		public void handleMessage(Message msg) {
			boolean success = msg.getData().getBoolean("success");
			progress.setVisibility(View.GONE);
			
			if (success) {
				//
				if (getActivity() != null)
					((LoginScreen)getActivity()).onBackPressed();
			} else {
				StringBuilder errorMsg = new StringBuilder(getString(R.string.loginError));
				
				if (FacebookLoginButton.mode == Mode.Signup) {
					errorMsg = new StringBuilder(getString(R.string.signupError));
				}
				
				if (!Utils.isEmpty(msg.getData().getString("msg"))) {
					errorMsg.append("\n-" + msg.getData().getString("msg"));
				}
				
				ShopApplication.showMessage(getActivity(), errorMsg.toString());
			}
		}
	}

	//@Override
	public String getViewTitle() {
		return getString(R.string.app_name);
	}
	
	public void setUser(User user) {
		//
		if (isDataServiceConnected()) {
			getDataService().signUp(getActivity(), user, handler);
		}
	}
	
	public void refresh() {
		//
		Log.i(getTag(), "Refreshing!");
	}
	
	@Override
	public String getTitle() {
		// 
		return "";
	}
}
