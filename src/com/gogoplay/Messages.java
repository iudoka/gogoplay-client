package com.gogoplay;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.ViewGroup;

import com.gogoplay.data.Item;
import com.gogoplay.data.MessageThread;
import com.gogoplay.data.User;
import com.gogoplay.ui.ShopActionBarActivity;
import com.gogoplay.ui.widget.ProgressDisplay;
import com.gogoplay.util.Utils;

public class Messages extends ShopActionBarActivity {
	//
	private static final String THREADS = "threads";
	private static final String MSGS = "messages";
	
	private Item item;
	private String userId; // specific user's messages to display
	private MessageFragment messageThreads;
	private MessageFragment messages;
	private Handler loadHandler;
	private ProgressDisplay progress;
	private ViewGroup emptyArea;
	private List<MessageThread> threads;
	private boolean savedInstanceState;
	
	@SuppressLint("HandlerLeak")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.messages);
		
		emptyArea = (ViewGroup) findViewById(R.id.emptyArea);
		emptyArea.setVisibility(View.GONE);
		progress = (ProgressDisplay) findViewById(R.id.progress);
		
		loadHandler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				// 
				if (msg.getData() != null) {
					//
					if (msg.getData().containsKey("messages")) {
						//
						emptyArea.setVisibility(View.VISIBLE);
						
						@SuppressWarnings("unchecked")
						List<MessageThread> threads = (List<MessageThread>)msg.getData().get("messages");
						Messages.this.threads = threads;
						
						if (threads != null) {
							if (threads.size() == 1) {
								//
								showMessagesScreen(threads);
							} else {
								
								if (userId == null) {
									//
									showMessageThreadScreen(threads);
								} else {
									//
									MessageThread userThread = null;
									
									for (MessageThread thread : threads) {
										//
										User from = thread.getFrom(getUser());
										
										if (from != null && Utils.equals(userId, from.getId())) {
											userThread = thread;
											break;
										}
									}
									
									if (userThread != null) {
										//
										ArrayList<MessageThread> userThreads = new ArrayList<MessageThread>();
										userThreads.add(userThread);
										showMessagesScreen(userThreads);
									}
								}
							}
						}
					} else if (msg.getData().containsKey("item")) {
						//
						Item item = (Item) msg.getData().get("item");
						setItem(item);
						getDataService().getMessagesForItem(Messages.this, item.getId(), loadHandler);
					}
				}
				
				hideProgress();
			}
		};
		
		restoreFragments(savedInstanceState);
	}
	
	private void restoreFragments(Bundle savedInstanceStateBundle) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        if (savedInstanceStateBundle != null) {
        	messageThreads = (MessageFragment) manager.getFragment(savedInstanceStateBundle, THREADS);
        	messages = (MessageFragment) manager.getFragment(savedInstanceStateBundle, MSGS);
        }
        
        if (messageThreads == null) {
        	messageThreads = new MessageFragment();
        	transaction.add(R.id.root, messageThreads, THREADS);
        }
        
        if (messages == null) {
        	messages = new MessageFragment();
        	transaction.add(R.id.root, messages, MSGS);
        }
        
        transaction.commit();
        savedInstanceState = false;
	}
	
	@Override
	protected void onResume() {
		// 
		super.onResume();
		savedInstanceState = false;
	}
	
	private void showProgress() {
		//
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.hide(messageThreads)
                .hide(messages)
                .commit();
		progress.setVisibility(View.VISIBLE);
	}
	
	private void hideProgress() {
		progress.setVisibility(View.GONE);
	}
	
	@Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        
        FragmentManager manager = getSupportFragmentManager();
        manager.putFragment(outState, THREADS, messageThreads);
        manager.putFragment(outState, MSGS, messages);
        
        savedInstanceState = true;
	}
	
	public void showMessageThreadScreen(List<MessageThread> threads) {
		//
		emptyArea.setVisibility(View.GONE);
		messageThreads.setData(item, threads);
		
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.hide(messages)
                .show(messageThreads)
                .commit();
	}
	
	public void showMessagesScreen(List<MessageThread> threads) {
		//
		emptyArea.setVisibility(View.GONE);
		messages.setData(item, threads);
		
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.hide(messageThreads)
                .show(messages)
                .commit();
	}
	
	@Override
	public void onBackPressed() {
		// 
		if (threads.size() > 1) {
			//
			if (!messages.isHidden() && !savedInstanceState) {
				showMessageThreadScreen(threads);
				return;
			}
		}
		
		super.onBackPressed();
	}
	
	@Override
	public String getViewFooter() {
		// 
		return null;
	}

	@Override
	protected void dataServiceConnected(boolean connected) {
		// load data when connected 
		if (connected) {
			//
			load();
		}
	}
	
	@SuppressWarnings("rawtypes")
	public void load() {
		//
		Intent intent = getIntent();
		userId = null;
		Item item = null;
		String itemId = null;
		Object param = intent.getExtras().get("params");
    	
    	if (param instanceof Item) {
    		item = (Item) param;
    		itemId = item.getId();
    	} else if (param instanceof Map) {
    		itemId = (String) ((Map)param).get("itemId");
    		userId = (String) ((Map)param).get("userId");
    	}
		
    	showProgress();
    	if (item != null) {
    		//
    		setItem(item);
    		
    		getDataService().getMessagesForItem(this, itemId, loadHandler);
    	} else {
    		//
    		getDataService().getItem(this, itemId, null, loadHandler);
    	}
    	
    	
	}
	
	private void setItem(Item item) {
		this.item = item;
		
		if (item != null) {
			//setViewTitle("Messages - " + item.getTitle(), null);
		}
	}

	@Override
	public String getViewTitle() {
		// 
		return "Messages";
	}

}
