package com.gogoplay.ui.widget;

import android.content.Context;
import android.util.AttributeSet;

import com.facebook.android.R;
import com.facebook.widget.LoginButton;

public class FacebookLoginButton extends LoginButton {

	public enum Mode {
		Login,
		Signup
	}
	
	public static Mode mode = Mode.Signup;
	
	public FacebookLoginButton(Context context) {
		super(context);
	}
	
	public FacebookLoginButton(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public FacebookLoginButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	public static void setMode(Mode type) {
		FacebookLoginButton.mode = type;
	}
	
	public void setButtonText() {
		setText(mode == Mode.Login ? getResources().getString(R.string.com_facebook_loginview_log_in_button) :
									 getResources().getString(com.gogoplay.R.string.com_facebook_loginview_sign_in_button));
	}
}
