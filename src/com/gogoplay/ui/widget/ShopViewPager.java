package com.gogoplay.ui.widget;

import java.util.ArrayList;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class ShopViewPager extends ViewPager {
	//
	private OnPageChangeListener listener = new OnPageChangeListener() {
		
		@Override
		public void onPageSelected(int arg0) {
			// 
			for (OnPageChangeListener l : listeners) {
				//
				l.onPageSelected(arg0);
			}
		}
		
		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			// 
			for (OnPageChangeListener l : listeners) {
				//
				l.onPageScrolled(arg0, arg1, arg2);
			}
		}
		
		@Override
		public void onPageScrollStateChanged(int arg0) {
			// 
			for (OnPageChangeListener l : listeners) {
				//
				l.onPageScrollStateChanged(arg0);
			}
		}
	};
	
	private ArrayList<OnPageChangeListener> listeners = new ArrayList<OnPageChangeListener>();

	public ShopViewPager(Context context) {
		this(context, null);
	}

	public ShopViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		init();
	}
	
	private void init() {
		//
	}
	
	@Override
	public void setAdapter(PagerAdapter adapter) {
		// 
		super.setAdapter(adapter);
		
		if (adapter != null) {
			super.setOnPageChangeListener(listener);
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		try {
			return super.onTouchEvent(ev);
		} catch (IllegalArgumentException ex) {
			ex.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		try {
			return super.onInterceptTouchEvent(ev);
		} catch (IllegalArgumentException ex) {
			ex.printStackTrace();
		}
		return false;
	}
	
	@Override 
    public void setOnPageChangeListener(OnPageChangeListener listener) {
        //super.setOnPageChangeListener(listener);
        //this.listener = listener;
		listeners.add(listener);
    } 

    @Override 
    public void setCurrentItem(int item) {
        // when you pass set current item to 0, 
        // the listener won't be called so we call it on our own 
        boolean invokeMeLater = false;

        if(super.getCurrentItem() == 0 && item == 0)
            invokeMeLater = true;

        super.setCurrentItem(item);

        if(invokeMeLater)
            listener.onPageSelected(0);
    }
}
