package com.gogoplay.ui.widget;

public interface ActionDisplayListener {
	//
	public boolean canShow();
	
	public void followUser();
	public void viewProfile();
	public void reportUser();
}
