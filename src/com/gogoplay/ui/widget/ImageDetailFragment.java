/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gogoplay.ui.widget;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.gogoplay.R;
import com.gogoplay.data.DataService;
import com.gogoplay.ui.ShopFragment;
import com.gogoplay.ui.ShopFragmentActivity;
import com.imagezoom.ImageAttacher.OnPhotoTapListener;

/**
 * This fragment will populate the children of the ViewPager from {@link ImageDetailActivity}.
 */
public class ImageDetailFragment extends ShopFragment {
    private static final String IMAGE_DATA_EXTRA = "extra_image_data";
    private String mImageUrl;
    private PinchImageView mImageView;
    private ImagePreviewList parent;
    //private DataService service;

    /**
     * Factory method to generate a new instance of the fragment given an image number.
     *
     * @param imageUrl The image url to load
     * @return A new instance of ImageDetailFragment with imageNum extras
     */
    public static ImageDetailFragment newInstance(String imageUrl, ImagePreviewList parent, DataService service) {
        final ImageDetailFragment f = new ImageDetailFragment();

        f.parent = parent;
        //f.service = service;
        final Bundle args = new Bundle();
        args.putString(IMAGE_DATA_EXTRA, imageUrl);
        f.setArguments(args);

        return f;
    }

    /**
     * Empty constructor as per the Fragment documentation
     */
    public ImageDetailFragment() {}

    /**
     * Populate image using a url from extras, use the convenience factory method
     * {@link ImageDetailFragment#newInstance(String)} to create this fragment.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mImageUrl = getArguments() != null ? getArguments().getString(IMAGE_DATA_EXTRA) : null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate and locate the main ImageView
        final View v = inflater.inflate(R.layout.preview_img, container, false);
        mImageView = (PinchImageView) v.findViewById(R.id.itemImage);
        
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Use the parent activity to load the image asynchronously into the ImageView (so a single
        // cache can be used over all pages in the ViewPager
        ((ShopFragmentActivity)parent.getParentActivity()).loadThumbnail(mImageUrl, mImageView);

        // Pass clicks on the ImageView to the parent activity to handle
        /*if (OnClickListener.class.isInstance(getActivity()) && Utils.hasHoneycomb()) {
            mImageView.setOnClickListener((OnClickListener) getActivity());
        }*/
        
        mImageView.getAttacher().setOnPhotoTapListener(new OnPhotoTapListener() {
			
			@Override
			public void onPhotoTap(View arg0, float arg1, float arg2) {
				//
				parent.hideInstructions();
			}
		});
        
        mImageView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 
				parent.hideInstructions();
			}
		});
        /*
        mImageView.getAttacher().setOnMatrixChangeListener(new OnMatrixChangedListener() {
			
			@Override
			public void onMatrixChanged(RectF arg0) {
				parent.hideInstructions();
			}
		});*/
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mImageView != null) {
            // Cancel any pending image work
            //ImageWorker.cancelWork(mImageView);
            mImageView.setImageDrawable(null);
        }
    }

	@Override
	public void dataServiceConnected(boolean connected) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getTitle() {
		// 
		return null;
	}

	@Override
	public void refresh() {
		// 
	}
}
