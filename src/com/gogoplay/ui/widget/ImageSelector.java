package com.gogoplay.ui.widget;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView.ScaleType;
import android.widget.Toast;

import com.aviary.android.feather.FeatherActivity;
import com.aviary.android.feather.library.Constants;
import com.gogoplay.R;
import com.gogoplay.data.DataService;
import com.gogoplay.data.Image;
import com.gogoplay.util.ImageUtil;

public class ImageSelector extends Fragment {

	public enum State {
		EMPTY, IMAGE_SELECTED, IMAGE_EDITED;
	}

	private static final int QUALITY = 50;
	private static final int IMG_WIDTH = 640;
	private static final int IMG_HEIGHT = IMG_WIDTH;
	private static final int THUMBNAIL_SIZE = 64;
	private static final String TEMP_DIR = "/.temp/";

	private static final String TAG = "ImageSelector";
	private static final String TEMPORARY_IMG_FILE = "picture";
	private static final String TEMPORARY_IMG_FILE_EXT = ".jpg";
	private static final int ACTION_TAKE_PHOTO = 0;
	private static final int ACTION_CHOOSE_PHOTO = 1;
	private static final int ACTION_EDIT_PHOTO = 2;
	private static final int EMPTY_IMG_RES_ID = R.drawable.ic_add;
	private static boolean useCamera = true;

	private State state = State.EMPTY;
	private Handler uiHandler;
	private Handler imageLoadHandler;
	private Bitmap emptyImg;
	private ShopImageView imagePane;
	private ImageButton closeBtn;
	private ImageButton editBtn;

	private Uri imageUri;
	private File tempFile;
	private Bitmap selectedImage;

	public Bitmap getSelectedImage() {
		return selectedImage;
	}

	public Uri getImageUri() {
		return imageUri;
	}

	public void requestFocus() {
		//
		imagePane.requestFocus();
	}

	public static void setUseCamera(boolean useCamera) {
		ImageSelector.useCamera = useCamera;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i(TAG, "Just created Image Selector Fragment/widget " + this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.image_selector, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		createUI();
		setupListeners();
	}

	public static boolean isIntentAvailable(Context context, Intent intent) {
		//
		final PackageManager packageManager = context.getPackageManager();
		List<ResolveInfo> list = packageManager.queryIntentActivities(intent,
				PackageManager.MATCH_DEFAULT_ONLY);

		return list.size() > 0;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		//
		if (resultCode == Activity.RESULT_OK) {
			//
			if (requestCode == ACTION_TAKE_PHOTO) {
				//
				grabImage(requestCode, data);
			} else if (requestCode == ACTION_CHOOSE_PHOTO) {
				//
				grabImage(requestCode, data);
			} else if (requestCode == ACTION_EDIT_PHOTO) {
				//
				Bundle extra = data.getExtras();
				boolean changed = false;

				if (null != extra) {
					// image has been changed by the user?
					changed = extra.getBoolean(Constants.EXTRA_OUT_BITMAP_CHANGED);
				}

				if (changed) {
					//
					imageUri = data.getData();
					grabImage(requestCode, data);
				}
			}
		}
	}

	private void grabImage(int type, Intent data) {
		//
		ContentResolver cr = getActivity().getContentResolver();

		try {
			// Display a thumbnail
			Bitmap bm = null;

			if (data != null) {
				imageUri = data.getData();
			}

			if (type == ACTION_TAKE_PHOTO) {
				File f = new File(imageUri.getPath());

				// Fix any rotation issues
				ExifInterface exif = new ExifInterface(f.getPath());
				int orientation = exif.getAttributeInt(
						ExifInterface.TAG_ORIENTATION,
						ExifInterface.ORIENTATION_NORMAL);

				int angle = 0;
				if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
					angle = 90;
				} else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
					angle = 180;
				} else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
					angle = 270;
				}

				FileInputStream fis = new FileInputStream(f);

				Bitmap bmp = ImageUtil.getScaledBitmap(fis, IMG_WIDTH, IMG_HEIGHT);

				//angle = 90;
				// Fix rotation
				if (angle != 0) {
					//
					Matrix mat = new Matrix();
					mat.postRotate(angle);
					bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(),
							bmp.getHeight(), mat, true);
				}

				// store rotated image
				FileOutputStream out = new FileOutputStream(f);
				bmp.compress(Bitmap.CompressFormat.JPEG, QUALITY, out);

				// fetch scaled version of image data for thumbnail bitmap
				bm = Bitmap.createScaledBitmap(bmp, THUMBNAIL_SIZE, THUMBNAIL_SIZE, false);

			} else if (type == ACTION_CHOOSE_PHOTO) {
				//
				bm = readBitmap(cr, imageUri);
				
			} else {
				//
				bm = MediaStore.Images.Thumbnails.getThumbnail(cr,
						Integer.parseInt(imageUri.getLastPathSegment()),
						MediaStore.Images.Thumbnails.MICRO_KIND,
						(BitmapFactory.Options) null);
			}

			selectImage(bm);
		} catch (Exception ex) {
			Log.e(TAG, "Unable to set image", ex);
		}
	}

	// Loads a sample sized version of image to avoid OOM exceptions
	public Bitmap readBitmap(ContentResolver cr, Uri selectedImage) {
		//
		Bitmap bm = null;
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 5;
		AssetFileDescriptor fileDescriptor = null;
		try {
			fileDescriptor = cr.openAssetFileDescriptor(selectedImage, "r");
		} catch (FileNotFoundException e) {
			Log.e(TAG, "Unable to find file: " + selectedImage, e);
		} finally {
			try {
				bm = BitmapFactory.decodeFileDescriptor(
						fileDescriptor.getFileDescriptor(), null, options);
				fileDescriptor.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return bm;
	}

	@SuppressLint("HandlerLeak")
	private void createUI() {
		//
		uiHandler = new Handler();
		
		imageLoadHandler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				// 
				if (msg.getData() != null) {
					Bitmap bitmap = (Bitmap) msg.getData().getParcelable("image");
					
					if (bitmap != null) {
						//
						selectImage(bitmap);
					}
				}
			}
		};
		
		ImageUtil.loadFromResource(getActivity(), EMPTY_IMG_RES_ID,
				new Handler() {

					@Override
					public void handleMessage(Message msg) {
						//
						emptyImg = (Bitmap) msg.getData().get(
								ImageUtil.RESOURCE_ID);
					}
				});

		imagePane = (ShopImageView) getView().findViewById(R.id.imagePane);
		closeBtn = (ImageButton) getView().findViewById(R.id.closeImageBtn);
		editBtn = (ImageButton) getView().findViewById(R.id.editImageBtn);

		if (selectedImage != null) {
			selectImage(selectedImage);
		}
	}

	private void setupListeners() {
		//
		imagePane.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//
				grabImage();
			}
		});

		closeBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//
				clearSelection();
			}
		});

		editBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//
				editSelection();
			}
		});
	}
	
	public void grabImage() {
		//
		if (state == State.EMPTY) {
			// send intent to select image
			if (useCamera) {
				//
				getImageFromCamera();
			} else {
				//
				getImageFromGallery();
			}
		} else if (state == State.IMAGE_SELECTED) {
			// call aviary library to edit selected image
		}
	}

	private void editSelection() {
		//
		state = State.IMAGE_EDITED;
		Intent newIntent = new Intent(getActivity(), FeatherActivity.class);
		newIntent.setData(imageUri);
		newIntent.putExtra(Constants.EXTRA_IN_API_KEY_SECRET, ImageUtil.getAviaryKey(getActivity()));
		startActivityForResult(newIntent, ACTION_EDIT_PHOTO);
	}

	private void getImageFromCamera() {
		//
		Intent intent = new Intent(
				android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		File photo = null;

		try {
			// place where to store taken picture
			photo = createTemporaryFile();
			photo.delete();

			imageUri = Uri.fromFile(photo);
			intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);

			startActivityForResult(intent, ACTION_TAKE_PHOTO);
		} catch (IOException ex) {
			Toast.makeText(getActivity(), R.string.camera_file_creation_error,
					Toast.LENGTH_LONG).show();
			Log.e(TAG, "Unable to create temporary file for camera", ex);
		}
	}

	private File createTemporaryFile() throws IOException {
		//
		if (tempFile == null) {
			File tempDir = Environment.getExternalStorageDirectory();
			tempDir = new File(tempDir.getAbsolutePath() + TEMP_DIR);

			if (!tempDir.exists()) {
				tempDir.mkdir();
			}

			tempFile = File.createTempFile(TEMPORARY_IMG_FILE,
					TEMPORARY_IMG_FILE_EXT, tempDir);
		}

		return tempFile;
	}

	public void cleanTempDirectory() {
		//
		File tempDir = Environment.getExternalStorageDirectory();
		tempDir = new File(tempDir.getAbsolutePath() + TEMP_DIR);

		if (tempDir.exists() && tempDir.delete()) {
			Log.i(TAG, "Successfully cleaned temp directory!");
		}
	}

	private void getImageFromGallery() {
		//
		Intent intent = new Intent();
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_GET_CONTENT);

		if (isIntentAvailable(getActivity(), intent)) {
			startActivityForResult(Intent.createChooser(intent,
					getString(R.string.select_picture)), ACTION_CHOOSE_PHOTO);
		} else {
			// TODO: let user know unable to select an image
		}
	}
	
	public void setImage(DataService service, Image image) {
		//
		if (image != null) {
			//
			
			if (service != null) {
				//
				service.loadImage(getActivity(), image, imageLoadHandler);
			}
			/*imagePane.setImageListener(new ImageListener() {
				
				@Override
				public void onImageSet(Drawable drawable) {
					//
					state = State.IMAGE_SELECTED;
					closeBtn.setVisibility(View.VISIBLE);
					editBtn.setVisibility(View.GONE);
				}
				
				@Override
				public void onImageCleared() {
					// 
					state = State.EMPTY;
					closeBtn.setVisibility(View.GONE);
					editBtn.setVisibility(View.GONE);
				}
			});
			
			fragment.loadThumbnail(image.getUri(), imagePane);*/
		} else {
			//imagePane.setImageDrawable(null);
			clearSelection();
		}
	}

	private void selectImage(final Bitmap image) {
		//
		uiHandler.post(new Runnable() {
			public void run() {
				// clear selected image
				if (state == State.EMPTY || state == State.IMAGE_EDITED) {
					state = State.IMAGE_SELECTED;
					imagePane.getDrawable().setCallback(null);
					selectedImage = image;
					imagePane.setScaleType(ScaleType.FIT_XY);
					imagePane.setImageBitmap(selectedImage);
					closeBtn.setVisibility(View.VISIBLE);
					
					if (imageUri != null) {
						editBtn.setVisibility(View.VISIBLE);
					} else {
						editBtn.setVisibility(View.GONE);
					}
				}
			}
		});
	}
	
	public State getState() {
		//
		return state;
	}

	public void clearSelection() {
		//
		uiHandler.post(new Runnable() {
			public void run() {
				// clear selected image
				if (state == State.IMAGE_SELECTED
						|| state == State.IMAGE_EDITED) {
					state = State.EMPTY;
					imagePane.getDrawable().setCallback(null);
					selectedImage = null;
					imageUri = null;
					imagePane.setScaleType(ScaleType.CENTER_INSIDE);
					imagePane.setImageBitmap(emptyImg);
					closeBtn.setVisibility(View.GONE);
					editBtn.setVisibility(View.GONE);
				}
			}
		});
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}
}
