package com.gogoplay.ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ScrollView;

import com.handmark.pulltorefresh.library.OverscrollHelper;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;
import com.handmark.pulltorefresh.library.R;

public class ShopScrollView extends PullToRefreshScrollView {
	//
	private static final int THRESHOLD = 75; //dp
	private static final String TAG = "ShopScrollView";
	private ShopScrollViewListener listener;
	private int bottomThreshold;
	
	public ShopScrollView(Context context) {
		super(context);
		init(context);
	}

	public ShopScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public ShopScrollView(Context context,
			com.handmark.pulltorefresh.library.PullToRefreshBase.Mode mode) {
		super(context, mode);
		init(context);
	}

	public ShopScrollView(Context context,
			com.handmark.pulltorefresh.library.PullToRefreshBase.Mode mode,
			com.handmark.pulltorefresh.library.PullToRefreshBase.AnimationStyle style) {
		super(context, mode, style);
		init(context);
	}
	
	private void init(Context context) {
		//
		// get the density of the screen and do some maths with it on the max
		// overscroll distance
		// variable so that you get similar behaviors no matter what the screen
		// size
		final DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		final float density = metrics.density;

		bottomThreshold = (int) (density * THRESHOLD);
	}
	
	@Override
	protected ScrollView createRefreshableView(Context context, AttributeSet attrs) {
		ScrollView scrollView;
		if (VERSION.SDK_INT >= VERSION_CODES.GINGERBREAD) {
			scrollView = new InternalScrollViewSDK9(context, attrs);
		} else {
			scrollView = new ScrollView(context, attrs) {
				
				@Override
				protected void onScrollChanged(int l, int t, int oldl, int oldt) {
					//
					super.onScrollChanged(l, t, oldl, oldt);
					
					if (listener != null) {
						// We take the last son in the scrollview
					    View view = (View) getChildAt(getChildCount() - 1);
					    int diff = (view.getBottom() - (getHeight() + getScrollY()));
					    Log.i(TAG, "Scroll diff: " + diff + " threshold: " + bottomThreshold);
			
					    // if diff is zero, then the bottom has been reached
					    if (diff <= bottomThreshold) {
					        listener.onScrolledToEnd();
					    }
					}
				}
			};
		}

		scrollView.setId(R.id.scrollview);
		return scrollView;
	}
	
	public void setScrollViewListener(ShopScrollViewListener listener) {
		this.listener = listener;
	}
	
	public interface ShopScrollViewListener {
		
		public void onScrollStarted();
		public void onScrollStopped();
		public void onScrolledToEnd();
	}
	
	@TargetApi(9)
	final class InternalScrollViewSDK9 extends ScrollView {

		public InternalScrollViewSDK9(Context context, AttributeSet attrs) {
			super(context, attrs);
		}

		@Override
		protected boolean overScrollBy(int deltaX, int deltaY, int scrollX, int scrollY, int scrollRangeX,
				int scrollRangeY, int maxOverScrollX, int maxOverScrollY, boolean isTouchEvent) {

			final boolean returnValue = super.overScrollBy(deltaX, deltaY, scrollX, scrollY, scrollRangeX,
					scrollRangeY, maxOverScrollX, maxOverScrollY, isTouchEvent);

			// Does all of the hard work...
			OverscrollHelper.overScrollBy(ShopScrollView.this, deltaX, scrollX, deltaY, scrollY,
					getScrollRange(), isTouchEvent);

			return returnValue;
		}

		/**
		 * Taken from the AOSP ScrollView source
		 */
		private int getScrollRange() {
			int scrollRange = 0;
			if (getChildCount() > 0) {
				View child = getChildAt(0);
				scrollRange = Math.max(0, child.getHeight() - (getHeight() - getPaddingBottom() - getPaddingTop()));
			}
			return scrollRange;
		}
		
		@Override
		protected void onScrollChanged(int l, int t, int oldl, int oldt) {
			//
			super.onScrollChanged(l, t, oldl, oldt);
			
			if (listener != null) {
				// We take the last son in the scrollview
			    View view = (View) getChildAt(getChildCount() - 1);
			    int diff = (view.getBottom() - (getHeight() + getScrollY()));
			    Log.i(TAG, "Scroll diff: " + diff + " threshold: " + bottomThreshold);
			    
			    // if diff is zero, then the bottom has been reached
			    if (diff <= bottomThreshold) {
			        listener.onScrolledToEnd();
			    }
			}
		}
	}
}
