package com.gogoplay.ui.widget;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.view.GestureDetectorCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.gogoplay.R;
import com.gogoplay.data.Comment;
import com.gogoplay.data.DataService;
import com.gogoplay.data.Item;
import com.gogoplay.data.User;
import com.gogoplay.ui.ShopActionBarActivity;
import com.gogoplay.ui.ShopActivityBase;
import com.gogoplay.ui.ShopApplication;
import com.gogoplay.util.ImageUtil;
import com.gogoplay.util.Utils;

public class CommentSection extends Fragment {
	//
	private static final String TAG = "CommentList";

	private ViewGroup root;
	private CommentAdapter adapter;
	private EditText commentBox;
	private Button sendBtn;
	private ListView commentList;

	private List<Comment> comments = Collections.synchronizedList(new ArrayList<Comment>());

	private CommentListener listener;

	private Item item;

	private GestureDetectorCompat mDetector;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i(TAG, "Just created CommentList Fragment/widget " + this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.comment_section, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		// if (savedInstanceState != null)
		// return;

		createUI();
		setupListeners();
	}

	private void createUI() {
		//
		root = (ViewGroup) getView().findViewById(R.id.root);
		root.setVisibility(View.GONE);
		commentBox = (EditText) getView().findViewById(R.id.commentBox);
		sendBtn = (Button) getView().findViewById(R.id.sendComment);
		sendBtn.setEnabled(false);
		
		adapter = new CommentAdapter((ShopActivityBase)getActivity(), getActivity());
		commentList = (ListView) getView().findViewById(R.id.commentList);
		commentList.setAdapter(adapter);
		
		// Instantiate the gesture detector with the
        // application context and an implementation of
        // GestureDetector.OnGestureListener
        mDetector = new GestureDetectorCompat(getActivity(), new DeleteGestureDetector(commentList));
	}
	
	public View getListView() {
		//
		return commentList;
	}

	private void setupListeners() {
		//
		
		commentBox.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// 
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// 
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// 
				sendBtn.setEnabled(!Utils.isEmpty(s.toString()));
			}
		});
		
		sendBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!Utils.isEmpty("" + commentBox.getText()) && 
					((ShopActionBarActivity)getActivity()).getDataService().checkAuthentication(getActivity())) {
					//
					
					Comment comment = new Comment();
					comment.setText(String.valueOf(commentBox.getText()));
					comment.setUser(((ShopActionBarActivity)getActivity()).getDataService().getUser(getActivity()));
					comment.setDate(new Date());
					
					if (listener != null) {
						listener.commentAdded(comment);
					}
					
					comments.add(0, comment);
					adapter.notifyDataSetChanged();
					
					// clear comment box
					commentBox.setText("");
				}
			}
		});
		
		commentList.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// 
				mDetector.onTouchEvent(event);
				return false;
			}
		});
	}
	
	public class DeleteGestureDetector extends SimpleOnGestureListener {
	    private ListView list;

	    public DeleteGestureDetector(ListView list) {
	        this.list = list;
	    }

	    @Override
	    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
	    	//
	        if (showDeleteButton(e1, e2))
	        	return true;
	        
	        return super.onFling(e1, e2, velocityX, velocityY);
	    }

	    private boolean showDeleteButton(MotionEvent e1, MotionEvent e2) {
	        int pos = list.pointToPosition((int)e1.getX(), (int)e1.getY());
	        boolean show = (e2.getX() - e1.getX()) < 0;
	        return showDeleteButton(pos, show);
	    }

	    private boolean showDeleteButton(int pos, boolean show) {
	        View child = list.getChildAt(pos);
        	Comment comment = comments.get(pos);
        	
	        if (child != null && comment != null){
	        	//
	        	ViewGroup delete = (ViewGroup) child.findViewById(R.id.removeComment);
	        	
	        	DataService service = null;
				
				if (((ShopActionBarActivity)getActivity()).isDataServiceConnected()) {
					service = ((ShopActionBarActivity)getActivity()).getDataService();
				}
				
	        	if (service != null) {
					User user = service.getUser(getActivity());
					User commentUser = comment.getUser();
					
					if (user != null && commentUser != null &&
						Utils.equals(user.getId(), commentUser.getId())) {
			            
			            if (delete != null)
			                if (show) {
			                    delete.setVisibility(View.VISIBLE);
			                } else {
			                    delete.setVisibility(View.GONE);
			                }
					} else if (delete != null) {
						delete.setVisibility(View.GONE);
					}
	        	} else if (delete != null) {
	        		delete.setVisibility(View.GONE);
	        	}
	        	
	            return true;
	        }
	        return false;
	    }
	}

	
	private static class ViewHolder {
		RecyclingImageView avatar;
		TextView username;
		TextView likeDate;
		TextView commentView;
		ViewGroup removeComment;
	}

	@SuppressLint("HandlerLeak")
	private class CommentAdapter extends BaseAdapter {
		//
		private Context context;
		@SuppressWarnings("unused")
		private ShopActivityBase view;
		private Bitmap unknown;
		
		@SuppressLint("HandlerLeak")
		public CommentAdapter(ShopActivityBase view, Context context) {
			this.view = view;
			this.context = context;
			
			ImageUtil.loadFromResource(context, R.drawable.anonymous, new Handler() {
				
				@Override
				public void handleMessage(Message msg) {
					//
					unknown = (Bitmap)msg.getData().get(ImageUtil.RESOURCE_ID);
				}
			});
		}

		@Override
		public int getCount() {
			return comments.size();
		}

		@Override
		public Object getItem(int position) {
			return comments.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}
		
		public boolean removeComment(Comment c) {
			//
			boolean removed = comments.remove(c);
			
			if (removed) {
				if (listener != null) {
					listener.commentRemoved(c);
				}
				setComments(comments);
			}
			
			return removed;
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			//
			View v = convertView;
			ViewHolder h = null;

			if (v == null) {
				LayoutInflater vi = (LayoutInflater) context.getSystemService(
						Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(R.layout.comment, parent, false);
			} else if (v.getTag() != null && v.getTag() instanceof ViewHolder) {
				h = (ViewHolder) v.getTag();
			}
			
			if (h == null) {
				//
				h = new ViewHolder();
				h.avatar = (RecyclingImageView) v.findViewById(R.id.avatar);
				h.username = (TextView) v.findViewById(R.id.username);
				h.likeDate = (TextView) v.findViewById(R.id.likeDate);
				h.commentView = (TextView) v.findViewById(R.id.comment);
				h.removeComment = (ViewGroup) v.findViewById(R.id.removeComment);
				v.setTag(h);
			}

			final Comment comment = (Comment) getItem(position);

			if (comment != null) {
				
				if (comment.getUser() != null) {
					//
					String image = comment.getUser().getPhoto();
					DataService service = null;
					
					if (((ShopActionBarActivity)getActivity()).isDataServiceConnected()) {
						service = ((ShopActionBarActivity)getActivity()).getDataService();
					}
	
					if (!Utils.isEmpty(image) && service != null) {
						Log.i(TAG, "Fetching image with uri: " + image + " item pos: " + position);
						
						service.loadThumbnail(context, h.avatar, image, new Handler());
					} else {
						h.avatar.setImageBitmap(unknown);
					}
					
					final DataService finalService = service;
					
					final ActionDisplayListener displayListener = new ActionDisplayListener() {
						
						@Override
						public boolean canShow() {
							// 
							if (finalService != null) {
								//
								User currUser = finalService.getUser(getActivity());
								
								if (currUser != null) {
									return !Utils.equals(currUser.getId(), comment.getUser().getId());
								}
							}
							
							return true;
						}
						
						@Override
						public void followUser() {
							//
							if (finalService != null) {
								finalService.followUser(context, comment.getUser(), new Handler());
							}
						}
						
						@Override
						public void viewProfile() {
							//
							if (finalService != null) {
								ShopApplication.goToProfile(getActivity(), comment.getUser());
							}
						}
						
						@Override
						public void reportUser() {
							//
							if (finalService != null) {
								finalService.reportUser(context, comment.getUser(), new Handler());
							}
						}
					};
					
					final View clickOn = h.avatar;
					
					h.avatar.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							// 
							if (finalService != null) {
								Avatar.showUserPopup(context, clickOn, comment.getUser(), finalService.getUser(context), displayListener);
							}
						}
					});

					h.username.setText(comment.getUser().getNickname());
					
					h.username.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							// 
							if (finalService != null) {
								Avatar.showUserPopup(context, clickOn, comment.getUser(), finalService.getUser(context), displayListener);
							}
						}
					});
					
					/*if (service != null) {
						User user = service.getUser(context);
						User commentUser = comment.getUser();
						
						if (user != null && commentUser != null &&
							Utils.equals(user.getId(), commentUser.getId())) {*/
							//
							h.removeComment.setOnClickListener(new OnClickListener() {
								
								@Override
								public void onClick(View v) {
									// 
									//Toast.makeText(context, "Not yet implemented!", Toast.LENGTH_SHORT).show();
									if (item.getComments().remove(comment)) {
										//
										removeComment(comment);
									}
								}
							});
							
							//h.removeComment.setVisibility(View.VISIBLE);
						/*} else {
							//
							//h.removeComment.setVisibility(View.GONE);
						}
					}*/
				} else {
					//
					h.removeComment.setVisibility(View.GONE);
				}
				
				CharSequence parse = Utils.parseSpecialChars(comment.getText(), context);
				h.commentView.setText(parse);

				h.likeDate.setText(Utils.printDate(comment.getDate()));
			}

			return v;
		}

		@Override
		public boolean areAllItemsEnabled() {
			// 
			return true;
		}

		@Override
		public boolean isEnabled(int position) {
			// TODO Auto-generated method stub
			return true;
		}

		@Override
		public boolean isEmpty() {
			return comments.isEmpty();
		}
	}
	
	public void addComment(Comment comment) {
		if (getComments().add(comment)) {
			//
			setComments(getComments());
		}
	}
	
	public void setItem(Item item) {
		//
		this.item = item;
	}

	public void setComments(List<Comment> comments) {
		//
		this.comments = new ArrayList<Comment>();
		
		if (comments != null) {
			this.comments.addAll(comments);
			
			adapter.notifyDataSetChanged();
		}
		
		if (!Utils.isEmpty(this.comments)) {
			root.setVisibility(View.VISIBLE);
		} else {
			root.setVisibility(View.INVISIBLE);
		}
	}
	
	public List<Comment> getComments() {
		return comments;
	}

	public void setCommentListener(CommentListener listener) {
		this.listener = listener;
	}
	
	public interface CommentListener {
		//
		public void commentAdded(Comment comment);
		public void commentRemoved(Comment comment);
	}
}
