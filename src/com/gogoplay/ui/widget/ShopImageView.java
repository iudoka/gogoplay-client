package com.gogoplay.ui.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

public class ShopImageView extends ImageView {
	//
	private ImageListener listener;
	
	public interface ImageListener {
		//
		public void onImageSet(Drawable drawable);
		public void onImageCleared();
	}
	
    public ShopImageView(Context context) {
        this(context,null);
    }

    public ShopImageView(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }
    
    public ShopImageView(Context context, AttributeSet attrs, int defStyle) {
    	super(context,attrs,defStyle);
    }
    
    @Override
    public void setImageDrawable(Drawable drawable) {
        // Call super to set new Drawable
        super.setImageDrawable(drawable);
        
        if (listener != null) {
        	if (drawable == null) {
        		listener.onImageCleared();
        	} else {
        		listener.onImageSet(drawable);
        	}
        }
    }
    
    public void setImageListener(ImageListener listener) {
    	//
    	this.listener = listener;
    }
}
