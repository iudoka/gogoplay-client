package com.gogoplay.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gogoplay.R;
import com.gogoplay.util.Utils;

public class ToggleButton extends LinearLayout {
	private Context context;
	private TextView button1;
	private TextView button2;
	private int selected1Background;
	private int selected2Background;
	private int selectedForeground;
	private int deselectedBackground;
	private int deselectedForeground;
	private int selection;
	private OnClickListener clickListener;

	public ToggleButton(Context context) {
		this(context, null);
	}

	public ToggleButton(Context context, AttributeSet attrs) {
		this(context, attrs, R.attr.toggleButtonStyle);
	}

	//
	public ToggleButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		String inflater = Context.LAYOUT_INFLATER_SERVICE;
		LayoutInflater lif = (LayoutInflater) getContext().getSystemService(
				inflater);
		lif.inflate(R.layout.toggle_btn, this, true);

		this.context = context;
		createUI();
		setupListeners();

		init(context, attrs, defStyle);
	}

	private void init(Context context, AttributeSet attrs, int defStyle) {
		//
		// load the styled attributes and set their properties
		TypedArray attributes = context.obtainStyledAttributes(attrs,
				R.styleable.ToggleButton, defStyle, 0);

		String button1Text = attributes
				.getString(R.styleable.ToggleButton_button1Text);
		button1.setText(Utils.emptyIfNull(button1Text));

		String button2Text = attributes
				.getString(R.styleable.ToggleButton_button2Text);
		button2.setText(Utils.emptyIfNull(button2Text));

		int selection = attributes
				.getInt(R.styleable.ToggleButton_selection, 1);

		if (selection == 1) {
			selectButton1();
		} else {
			selectButton2();
		}
		
		// We no longer need our attributes TypedArray, give it back to cache
		attributes.recycle();
	}

	private void createUI() {
		//
		selected1Background = R.drawable.toggle_btn_sel_1;
		selected2Background = R.drawable.toggle_btn_sel_2;
		selectedForeground = R.color.white;
		deselectedBackground = R.color.transparent;
		deselectedForeground = R.color.blue;

		button1 = (TextView) findViewById(R.id.button1);
		button2 = (TextView) findViewById(R.id.button2);
	}

	private void setupListeners() {
		//
		button1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//
				selectButton1();
				
				if (clickListener != null)
					clickListener.onClick(ToggleButton.this);
			}
		});

		button2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//
				selectButton2();
				
				if (clickListener != null)
					clickListener.onClick(ToggleButton.this);
			}
		});
	}

	public int getSelection() {
		//
		return selection;
	}
	
	public boolean isButton1Selected() {
		//
		return getSelection() == 1;
	}

	public void selectButton1() {
		//
		selection = 1;
		select(button1, selected1Background, button2);
	}
	
	public boolean isButton2Selected() {
		//
		return getSelection() == 2;
	}

	public void selectButton2() {
		selection = 2;
		select(button2, selected2Background, button1);
	}

	private void select(TextView select, int selectionBackground, TextView deselect) {
		//
		select.setBackgroundResource(selectionBackground);
		select.setTextColor(getResources().getColor(selectedForeground));
		deselect.setBackgroundColor(getResources().getColor(deselectedBackground));
		deselect.setTextColor(getResources().getColor(deselectedForeground));
	}
	
	@Override
	public void setOnClickListener(OnClickListener l) {
		// 
		clickListener = l;
	}
}
