package com.gogoplay.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ScrollView;

import com.gogoplay.ui.widget.ShopScrollView.ShopScrollViewListener;

public class ShopSimpleScrollView extends ScrollView {
	//
	private static final int THRESHOLD = 150; //dp
	//private static final String TAG = "ShopSimpleScrollView";
	private ShopScrollViewListener listener;
	private int bottomThreshold;
	private Runnable scrollerTask;
	private int newCheck = 100;
	private int initialPosition = 0;
	
	public ShopSimpleScrollView(Context context) {
		super(context);
		init(context);
	}

	public ShopSimpleScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public ShopSimpleScrollView(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	private void init(Context context) {
		//
		// get the density of the screen and do some maths with it on the max
		// overscroll distance
		// variable so that you get similar behaviors no matter what the screen
		// size
		final DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		final float density = metrics.density;

		bottomThreshold = (int) (density * THRESHOLD);
		
		scrollerTask = new Runnable() {

	        public void run() {

	            int newPosition = getScrollY();
	            if(initialPosition - newPosition == 0){//has stopped

	                if(listener != null){

	                    listener.onScrollStopped();
	                }
	            }else{
	                initialPosition = getScrollY();
	                ShopSimpleScrollView.this.postDelayed(scrollerTask, newCheck);
	            }
	        }
	    };
	    
	    setOnTouchListener(new OnTouchListener() {

	        public boolean onTouch(View v, MotionEvent event) {

	            if (event.getAction() == MotionEvent.ACTION_UP) {

	                startScrollerTask();
	            }

	            return false;
	        }
	    });
	}
	
	public void startScrollerTask() {
		initialPosition = getScrollY();
		this.postDelayed(scrollerTask, newCheck);
	}

	public void setScrollViewListener(ShopScrollViewListener listener) {
		this.listener = listener;
	}
	
	@Override
	protected void onScrollChanged(int l, int t, int oldl, int oldt) {
		//
		super.onScrollChanged(l, t, oldl, oldt);
		
		if (listener != null) {
			// We take the last son in the scrollview
		    View view = (View) getChildAt(getChildCount() - 1);
		    int diff = (view.getBottom() - (getHeight() + getScrollY()));
		    
		    // if diff is zero, then the bottom has been reached
		    if (diff <= bottomThreshold) {
			    //Log.i(TAG, "Scroll diff: " + diff + " threshold: " + bottomThreshold);
		        listener.onScrolledToEnd();
		    }
		}
	}
	
}
