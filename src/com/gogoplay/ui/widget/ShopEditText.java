package com.gogoplay.ui.widget;

import android.content.Context;
import android.text.TextUtils.TruncateAt;
import android.util.AttributeSet;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class ShopEditText extends EditText {

	public ShopEditText(Context context) {
		super(context);
		init();
	}
	
	public ShopEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	
	public ShopEditText(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	private void init() {
		//
		setEllipsize(TruncateAt.END);
		
		setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    setImeVisibility(true);
                } else {
                    setImeVisibility(false);
                }
            }
        });
	}
	
	private Runnable mShowImeRunnable = new Runnable() {
	    public void run() {
	        InputMethodManager imm = (InputMethodManager) getContext()
	                .getSystemService(Context.INPUT_METHOD_SERVICE);

	        if (imm != null) {
	            imm.showSoftInput(ShopEditText.this,0);
	        }
	    }
	};

	private void setImeVisibility(final boolean visible) {
	    if (visible) {
	        post(mShowImeRunnable);
	    } else {
	        removeCallbacks(mShowImeRunnable);
	        InputMethodManager imm = (InputMethodManager) getContext()
	                .getSystemService(Context.INPUT_METHOD_SERVICE);

	        if (imm != null) {
	            imm.hideSoftInputFromWindow(getWindowToken(), 0);
	        }
	    }
	}
	
	public void showKeyboard() {
		//
		requestFocus();
	}
	
	public void hideKeyboard() {
		//
		clearFocus();
	}
}
