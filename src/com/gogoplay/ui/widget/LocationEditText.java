package com.gogoplay.ui.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.gogoplay.util.LocationUtil;

@SuppressLint("HandlerLeak")
public class LocationEditText extends ShopEditText {
	//
	private LocationUtil locationUtil;
	private String locationStr;
	private Location location;
	
	public LocationEditText(Context context) {
		super(context);
		
		init();
	}
	
	public LocationEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		init();
	}
	
	public LocationEditText(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		
		init();
	}

	private void init() {
		//
		setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_POSTAL_ADDRESS);
		setImeOptions(EditorInfo.IME_ACTION_SEARCH);
		setImeActionLabel("Search", getImeActionId());
		
//		setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (hasFocus) {
//                    setImeVisibility(true);
//                } else {
//                    setImeVisibility(false);
//                }
//            }
//        });
		
		setOnEditorActionListener(new EditText.OnEditorActionListener() {
		    @Override
		    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		        if (actionId == EditorInfo.IME_ACTION_SEARCH ||
		            actionId == EditorInfo.IME_ACTION_DONE ||
		            (event.getAction() == KeyEvent.ACTION_DOWN &&
		             event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
		        	//
		        	String searchTxt = getText().toString();
		        	
		        	if (locationUtil != null && (location == null || !searchTxt.equalsIgnoreCase(locationStr))) {
			    		Location loc = locationUtil.getLocation(searchTxt);
			            setLocation(loc);
		        	}
		            return true;
		        }
		        return false;
		    }
		});
	}
	
	public void setCurrentLocation() {
		//
		if (locationUtil != null) {
			initLocation();
		}
	}
	
	public Location getLocation() {
		return location;
	}
	
	public void setLocation(Location loc) {
		//
        if (loc != null) {
        	location = loc;
        	locationStr = location.getExtras().getString(LocationUtil.EXTRA_CITY_AND_STATE_CODE);
        	setText(locationStr);
        } else {
        	String message = "Unable to find location";
        	Toast toast = Toast.makeText(getContext(), message, Toast.LENGTH_LONG);
			toast.show();
        }
	}
	
	public void setLocationUtil(LocationUtil locationUtil) {
		//
		if (locationUtil != null) {
			this.locationUtil = locationUtil;
			initLocation();
		}
	}
	
	private void initLocation() {
		//
		Location loc = null;
		
		if (locationUtil != null) {
			loc = locationUtil.getCurrentLocation();
		}
		
		setLocation(loc);
	}
	
//	private Runnable mShowImeRunnable = new Runnable() {
//	    public void run() {
//	        InputMethodManager imm = (InputMethodManager) getContext()
//	                .getSystemService(Context.INPUT_METHOD_SERVICE);
//
//	        if (imm != null) {
//	            imm.showSoftInput(LocationEditText.this,0);
//	        }
//	    }
//	};
//
//	private void setImeVisibility(final boolean visible) {
//	    if (visible) {
//	        post(mShowImeRunnable);
//	    } else {
//	    	removeCallbacks(mShowImeRunnable);
//	        InputMethodManager imm = (InputMethodManager) getContext()
//	                .getSystemService(Context.INPUT_METHOD_SERVICE);
//
//	        if (imm != null) {
//	            imm.hideSoftInputFromWindow(getWindowToken(), 0);
//	        }
//	    }
//	}
//	
//	public void showKeyboard() {
//		//
//		requestFocus();
//	}
//	
//	public void hideKeyboard() {
//		//
//		clearFocus();
//	}
}
