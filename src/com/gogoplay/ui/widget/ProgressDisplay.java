package com.gogoplay.ui.widget;

import com.gogoplay.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ProgressDisplay extends LinearLayout {
	//
	private TextView textView;
	
	public ProgressDisplay(Context context) {
        super(context);
    }

    public ProgressDisplay(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }
	
	public ProgressDisplay(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        
        // Inflate layer
        String inflater = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater lif = (LayoutInflater)getContext().getSystemService(inflater);
        lif.inflate(R.layout.progress_display, this, true);
        textView = (TextView) findViewById(R.id.text);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ProgressDisplay);
        CharSequence s = a.getString(R.styleable.ProgressDisplay_text);
        setText(s);
        a.recycle();
	}
	
	public void showText(boolean show) {
		if (show) {
			setBackgroundResource(R.drawable.progress_round_rectangle);
			textView.setVisibility(View.VISIBLE);
		}
	}
	
	public void setText(CharSequence text) {
		textView.setText(text);
	}
}
