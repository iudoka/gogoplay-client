package com.gogoplay.ui.widget;

import java.io.Serializable;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.gogoplay.R;
import com.gogoplay.ui.ShopFragmentActivity;
import com.gogoplay.util.Utils;

public class TextEntryFragment extends ShopFragmentActivity {
	//
	public static final String EXTRA_ITEM = "item";
	public static final String EXTRA_TITLE = "title";
	public static final String EXTRA_TEXT = "text";
	public static final String EXTRA_SEND_TEXT = "sendText";
	public static final String EXTRA_HINT = "hint";
	public static final String EXTRA_MAX_CHARS = "maxChars";
	
	private int maxCharacters = 1000;
	
	private ViewGroup root;
	private TextView title;
	private TextView cancelBtn;
	private TextView postBtn;
	private EditText textEditor;
	private TextView countText;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// remove title

		setContentView(R.layout.text_entry);

		createUI();
		setupListeners();
	}
	
	@Override
	protected void onResume() {
		// 
		super.onResume();
		
		Intent intent = getIntent();
		
		if (intent.getExtras().containsKey(EXTRA_TITLE)) {
			title.setText(intent.getExtras().getString(EXTRA_TITLE));
		}
		
		if (intent.getExtras().containsKey(EXTRA_MAX_CHARS)) {
			maxCharacters = intent.getExtras().getInt(EXTRA_MAX_CHARS);
			countText.setText(maxCharacters + "");
		}
		
		if (intent.getExtras().containsKey(EXTRA_TEXT)) {
			textEditor.setText(intent.getExtras().getString(EXTRA_TEXT));
		}
		
		if (intent.getExtras().containsKey(EXTRA_SEND_TEXT)) {
			postBtn.setText(intent.getExtras().getString(EXTRA_SEND_TEXT));
		}
		
		if (intent.getExtras().containsKey(EXTRA_HINT)) {
			textEditor.setHint(intent.getExtras().getString(EXTRA_HINT));
		}
		
		showKeyboard();
	}

	private void createUI() {
		// 
		root = (ViewGroup) findViewById(R.id.root);
		cancelBtn = (TextView) findViewById(R.id.cancelBtn);
		title = (TextView) findViewById(R.id.title);
		postBtn = (TextView) findViewById(R.id.postBtn);
		postBtn.setVisibility(View.INVISIBLE);
		textEditor = (EditText) findViewById(R.id.textEditor);
		//textEditor.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES | InputType.TYPE_TEXT_FLAG_AUTO_CORRECT);
		textEditor.setFilters(new InputFilter[] {new InputFilter.LengthFilter(maxCharacters)});
				
		countText = (TextView) findViewById(R.id.countText);
		countText.setText(maxCharacters + "");
	}
	
	private void setupListeners() {
		// 
		root.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// 
				textEditor.requestFocus();
				return false;
			}
		});
		
		cancelBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 
				hideKeyboard();
				finish();
			}
		});
		
		postBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 
				hideKeyboard();
		        
		        Intent data = new Intent();
		        
		        if (getIntent().getSerializableExtra(EXTRA_ITEM) != null) {
		        	// echo item info back to the sender
		        	data.putExtra(EXTRA_ITEM, (Serializable)getIntent().getExtras().get(EXTRA_ITEM));
		        }
		        
				data.putExtra(EXTRA_TEXT, textEditor.getText().toString());
				
				setResult(RESULT_OK, data);
				finish();
			}
		});
		
		textEditor.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    setImeVisibility(true);
                } else {
                    setImeVisibility(false);
                }
            }
        });
		
		textEditor.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// 
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// 
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// 
				updateCount();
				
				postBtn.setVisibility(!Utils.isEmpty(s.toString()) ? View.VISIBLE : View.INVISIBLE);
			}
		});
	}

	private void updateCount() {
		int count = maxCharacters - textEditor.getText().length();
		countText.setText(count + "");
	}
	
	private Runnable mShowImeRunnable = new Runnable() {
	    public void run() {
	        InputMethodManager imm = (InputMethodManager) getApplicationContext()
	                .getSystemService(Context.INPUT_METHOD_SERVICE);

	        if (imm != null) {
	            imm.showSoftInput(textEditor,0);
	        }
	    }
	};

	private void setImeVisibility(final boolean visible) {
	    if (visible) {
	        root.post(mShowImeRunnable);
	    } else {
	        root.removeCallbacks(mShowImeRunnable);
	        InputMethodManager imm = (InputMethodManager) getApplicationContext()
	                .getSystemService(Context.INPUT_METHOD_SERVICE);

	        if (imm != null) {
	            imm.hideSoftInputFromWindow(textEditor.getWindowToken(), 0);
	        }
	    }
	}
	
	private void showKeyboard() {
		//
		textEditor.requestFocus();
	}
	
	private void hideKeyboard() {
		//
		textEditor.clearFocus();
	}

	@Override
	public String getTag() {
		// 
		return getClass().getSimpleName();
	}

	@Override
	protected void serviceConnected() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getViewTitle() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getViewFooter() {
		// TODO Auto-generated method stub
		return null;
	}
}
