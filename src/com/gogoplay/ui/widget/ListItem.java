package com.gogoplay.ui.widget;

import java.util.List;

import com.gogoplay.ui.ShopFragment;

public class ListItem {
	//
	
	public enum Type {
		Header,
		Profile,
		Navigation,
		Spacer, 
		Action
	}
	
	private String title;
	private int icon1 = -1;
	private int icon2 = -1;
	private Type type;
	private ShopFragment target;
	private List<ListItem> subList;
	
	public ListItem(String title, int icon1, int icon2, Type type, ShopFragment target) {
		super();
		this.title = title;
		this.icon1 = icon1;
		this.icon2 = icon2;
		this.type = type;
		this.target = target;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the icon1
	 */
	public int getIcon1() {
		return icon1;
	}
	/**
	 * @param icon1 the icon1 to set
	 */
	public void setIcon1(int icon1) {
		this.icon1 = icon1;
	}
	/**
	 * @return the icon2
	 */
	public int getIcon2() {
		return icon2;
	}
	/**
	 * @param icon2 the icon2 to set
	 */
	public void setIcon2(int icon2) {
		this.icon2 = icon2;
	}
	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(Type type) {
		this.type = type;
	}
	/**
	 * @return the target
	 */
	public ShopFragment getTarget() {
		return target;
	}
	/**
	 * @param target the target to set
	 */
	public void setTarget(ShopFragment target) {
		this.target = target;
	}
	/**
	 * @return the subList
	 */
	public List<ListItem> getSubList() {
		return subList;
	}
	/**
	 * @param subList the subList to set
	 */
	public void setSubList(List<ListItem> subList) {
		this.subList = subList;
	}
	
}
