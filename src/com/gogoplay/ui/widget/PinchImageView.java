package com.gogoplay.ui.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import com.imagezoom.ImageAttacher;

public class PinchImageView extends RecyclingImageView {
	//
	ImageAttacher mAttacher;
	
    public PinchImageView(Context context) {
        this(context, null, 0);
    }

    public PinchImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PinchImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
    	//
    	mAttacher = new ImageAttacher(this);
        ImageAttacher.MAX_ZOOM = 2.0f; // Double the current Size
        ImageAttacher.MIN_ZOOM = 1.0f; // Half the current Size
        //MatrixChangeListener mMaListener = new MatrixChangeListener();
        //mAttacher.setOnMatrixChangeListener(mMaListener);
        //PhotoTapListener mPhotoTap = new PhotoTapListener();
        //mAttacher.setOnPhotoTapListener(mPhotoTap);
    }
    
    public void setImageBitmap(Bitmap bitmap) {
    	setScaleType(ScaleType.MATRIX);
    	mAttacher.zoomTo(0, 0, 0);
    	super.setImageBitmap(bitmap);
    }
    
    @Override
    public void setImageDrawable(Drawable drawable) {
    	// 
    	setScaleType(ScaleType.MATRIX);
    	mAttacher.zoomTo(ImageAttacher.MIN_ZOOM, 0, 0);
    	super.setImageDrawable(drawable);
    }
    
    public ImageAttacher getAttacher() {
    	//
    	return mAttacher;
    }
}
