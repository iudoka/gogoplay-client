package com.gogoplay.ui.widget;

import java.text.SimpleDateFormat;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.gogoplay.R;
import com.gogoplay.data.DataService;
import com.gogoplay.data.User;
import com.gogoplay.util.Utils;

public class ProfileHeader extends FrameLayout {

	private Context context;
	
	private static final int s_FOLLOW_BACKGROUND = R.color.light_blue;
	private static final int s_UNFOLLOW_BACKGROUND = R.color.gray;
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM, yyyy", Locale.US);

	private User user;
	
	private TextView listCount;
	private TextView followersCount;
	private TextView followingCount;
	private TextView followBtn;
	private RatingBar rating;
	private CircularImageView avatarImage;
	private TextView username;
	private TextView listingsLabel;
	private ProgressDisplay progress;

	private DataService dataService;

	private TextView joinDate;
	
	public ProfileHeader(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		
		String inflater = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater lif = (LayoutInflater)getContext().getSystemService(inflater);
        lif.inflate(R.layout.profile_header, this, true);
        
        this.context = context;
        createUI();
        setupListeners();
	}
	
	public void setDataService(DataService service) {
		this.dataService = service;
	}
	
	private DataService getDataService() {
		return dataService;
	}
	
	private void createUI() {
		//
		progress = (ProgressDisplay) findViewById(R.id.progress);
		listCount = (TextView) findViewById(R.id.listingCount);
		followersCount = (TextView) findViewById(R.id.followersCount);
		followingCount = (TextView) findViewById(R.id.followingCount);
		followBtn = (TextView) findViewById(R.id.followBtn);
		rating = (RatingBar) findViewById(R.id.rating);
		rating.setClickable(false);
		joinDate = (TextView) findViewById(R.id.joinDate);
		avatarImage = (CircularImageView) findViewById(R.id.avatarImage);
		username = (TextView) findViewById(R.id.username);
		listingsLabel = (TextView) findViewById(R.id.listingsLabel);
	}
	
	@SuppressLint("HandlerLeak")
	private void setupListeners() {
		// 
		followBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//
				progress.setVisibility(View.VISIBLE);
				getDataService().followUser(context, user, new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// 
						progress.setVisibility(View.GONE);
						boolean followed = msg.getData().getBoolean("success");
						
						if (followed) {
							//
							updateFollowButton();
						}
					}
				});
			}
		});
	}

	private boolean isCurrentUser() {
		//
		User appUser = getDataService().getUser(context);
		
		if (appUser != null) {
			//
			return Utils.equals(appUser.getId(), user.getId());
		}
		
		return false;
	}
	
	public void setUser(User user, int listingCount) {
		//
		this.user = user;
		populate(listingCount);
	}

	private void populate(int listingCount) {
		//
		if (user != null) {
			//
			listCount.setText(listingCount + "");
//			followingCount.setText(followingAmt + "");
//			followersCount.setText(followerAmt + "");
			username.setText(user.getNickname());
			rating.setRating(user.getRating());
			
			if (user.getDateCreated() != null) {
				joinDate.setText(context.getString(R.string.joinedLabel) + " " + dateFormat.format(user.getDateCreated()));
			}
			
			followBtn.setVisibility(View.GONE);
			
			if (isDataServiceConnected()) {
				//
				if (!Utils.isEmpty(user.getPhoto())) {
					dataService.loadThumbnail(context, avatarImage, user.getPhoto(), new Handler());
				}
				
				boolean isAppUser = isCurrentUser();

				followBtn.setVisibility(isAppUser ? View.GONE : View.VISIBLE);

				if (!isAppUser) {
					//
					updateFollowButton();
				}
			}
			
			listingsLabel.setText(listingCount <= 0 ? context.getString(R.string.no_current_listings) : (listingCount + " " + context.getString(R.string.current_listings)));
		}
	}
	
	private boolean isDataServiceConnected() {
		//
		return dataService != null;
	}

	private void updateFollowButton() {
		//
		User currUser = DataService.getCurrentUser(context);
		
		if (currUser != null) {
			boolean isFollowing = currUser.isFollowing(user);
			
			followBtn.setText(isFollowing ? context.getString(R.string.unfollow)
					: context.getString(R.string.follow));
			followBtn.setBackgroundColor(isFollowing ? getResources().getColor(s_UNFOLLOW_BACKGROUND)
					: getResources().getColor(s_FOLLOW_BACKGROUND));
			followBtn.setVisibility(View.VISIBLE);
		} else {
			followBtn.setVisibility(View.GONE);
		}
	}
}
