package com.gogoplay.ui.widget;

import net.londatiga.android.ActionItem;
import net.londatiga.android.QuickAction;
import net.londatiga.android.QuickAction.OnActionItemClickListener;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gogoplay.R;
import com.gogoplay.data.DataService;
import com.gogoplay.data.User;
import com.gogoplay.ui.ShopApplication;
import com.gogoplay.util.Utils;

public class Avatar extends LinearLayout implements ActionDisplayListener {
	//
	private static final int ID_FOLLOW = 1;
	private static final int ID_PROFILE = 2;
	private static final int ID_REPORT = 3;
	
	private Context context;
	private ImageView image;
	private TextView name;
	private TextView extraInfo;
	private User user;
	private DataService service;
	
	public Avatar (Context context, AttributeSet attrs) {
        super(context, attrs);
        
        String inflater = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater lif = (LayoutInflater)getContext().getSystemService(inflater);
        lif.inflate(R.layout.avatar, this, true);
        
        this.context = context;
        createUI();
        setupListeners();
    }
	
	private void createUI() {
		//
		name = (TextView) findViewById(R.id.avatarName);
		image = (ImageView) findViewById(R.id.avatarImage);
		extraInfo = (TextView) findViewById(R.id.extraInfo);
		
		//addQuickActionPopup(context, this, this);
	}

	private void setupListeners() {
		//
		setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 
				if (service != null) {
					showUserPopup(context, Avatar.this, user, service.getUser(context), Avatar.this);
				}
			}
		});
	}
	
	@Override
	public boolean canShow() {
		//
		if (user != null && service != null) {
			//
			User currUser = service.getUser(context);
			
			if (currUser != null) {
				return !Utils.equals(user.getId(), currUser.getId());
			}
		}
		
		return true;
	}
	
	@Override
	public void followUser() {
		//
		if (service != null) {
			service.followUser(context, user, new Handler());
		}
	}
	
	@Override
	public void viewProfile() {
		//
		if (service != null) {
			ShopApplication.goToProfile(context, user);
		}
	}
	
	@Override
	public void reportUser() {
		//
		if (service != null) {
			service.reportUser(context, user, new Handler());
		}
	}
	
	public static void showUserPopup(final Context context, View v, final User theUser, final User currentUser, final ActionDisplayListener listener) {
		//
		// Quick action popup
		ActionItem profileItem = new ActionItem(ID_PROFILE, context
				.getResources().getString(R.string.profile), context
				.getResources().getDrawable(R.drawable.ic_menu_friendslist));

		ActionItem followUserItem = null;
		ActionItem reportUserItem = null;
		
		if (theUser != null && currentUser != null && 
			!Utils.equals(theUser.getId(), currentUser.getId())) {
			//
			boolean following = currentUser.isFollowing(theUser);
			followUserItem = new ActionItem(ID_FOLLOW, following ? context.getResources().getString(R.string.unfollow) : 
						context.getResources().getString(R.string.follow), 
						following ? context.getResources().getDrawable(R.drawable.ic_clear_dark) :
						context.getResources().getDrawable(R.drawable.ic_menu_add));
			
			reportUserItem = new ActionItem(ID_REPORT, context
					.getResources().getString(R.string.report), context
					.getResources().getDrawable(R.drawable.ic_menu_report_image));
		}

		// use setSticky(true) to disable QuickAction dialog being dismissed
		// after an item is clicked
		// uploadItem.setSticky(true);

		final QuickAction mQuickAction = new QuickAction(context);

		if (followUserItem != null)
			mQuickAction.addActionItem(followUserItem);
		
		if (profileItem != null)
			mQuickAction.addActionItem(profileItem);
		
		if (reportUserItem != null)
			mQuickAction.addActionItem(reportUserItem);

		OnActionItemClickListener actionItemListener = new OnActionItemClickListener() {
			@Override
			public void onItemClick(QuickAction quickAction, int pos, int actionId) {
				//ActionItem actionItem = quickAction.getActionItem(pos);

				if (actionId == ID_FOLLOW) {
					listener.followUser();
				} else if (actionId == ID_PROFILE) {
					listener.viewProfile();
				} else if (actionId == ID_REPORT) {
					listener.reportUser();
				}
			}
		};
		
		// setup the action item click listener
		mQuickAction.setOnActionItemClickListener(actionItemListener);
		mQuickAction.show(v);
	}
	
	public void setExtraInfo(String info) {
		//
		extraInfo.setVisibility(View.VISIBLE);
		extraInfo.setText(info);
	}
    
    public void setUser(User user,  DataService service) {
    	//
    	this.user = user;
    	this.service = service;
    	
    	if (user == null) return;
    	
    	String theName = user.getNickname();
    	
    	if (Utils.isEmpty(theName)) {
    		//
    		theName = user.getFirstName() + user.getLastName();
    	}
    	
    	name.setText(theName);
    	
    	if (user.getAvatar() != null) {
    		image.setImageBitmap(user.getAvatar());
    	} else if (!Utils.isEmpty(user.getPhoto()) && service != null) {
    		service.loadThumbnail(getContext(), image, user.getPhoto(), new Handler());
    	}
    }
}
