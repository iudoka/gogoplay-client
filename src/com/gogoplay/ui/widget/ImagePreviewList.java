package com.gogoplay.ui.widget;

import java.util.List;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gogoplay.ProductDetail;
import com.gogoplay.R;
import com.gogoplay.data.DataService;
import com.gogoplay.data.Image;

public class ImagePreviewList extends RelativeLayout {
	//
	private TextView instructions;
	private ImageButton close;
	//private HorizontalListView listView;
	//private PinchImageView imageView;
	private List<Image> images;
	private ImagePagerAdapter adapter;
	private ViewPager imageView;
	//private DataService service;
	//private PreviewAdapter adapter;
	private CirclePageIndicator indicator;
	//private Context context;
	private ImagePreviewParent parent;
	
	public ImagePreviewList(Context context) {
        super(context);
    }

    public ImagePreviewList(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }
	
	public ImagePreviewList(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        
        //this.context = context;
        // Inflate layer
        String inflater = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater lif = (LayoutInflater)getContext().getSystemService(inflater);
        lif.inflate(R.layout.picture_preview, this, true);

        //listView = (HorizontalListView) findViewById(R.id.previewList);
        //adapter = new PreviewAdapter(context);
        //listView.setAdapter(adapter);
        imageView = (ViewPager)findViewById(R.id.productPager);
        instructions = (TextView)findViewById(R.id.userInstr);
        indicator = (CirclePageIndicator)findViewById(R.id.indicator);
        //imageView.setPageMargin(2);
        //imageView.setOffscreenPageLimit(2);
        
        
        //imageView = (PinchImageView)findViewById(R.id.itemImage);
        /*imageView.getAttacher().setOnMatrixChangeListener(new OnMatrixChangedListener() {
			
			@Override
			public void onMatrixChanged(RectF arg0) {
				//instructions.setVisibility(View.GONE);
			}
		});
        
        imageView.getAttacher().setOnPhotoTapListener(new OnPhotoTapListener() {
			
			@Override
			public void onPhotoTap(View arg0, float arg1, float arg2) {
				instructions.setVisibility(View.GONE);
			}
		});*/
        
        imageView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 
				hideInstructions();
			}
		});
		
        close = (ImageButton) findViewById(R.id.closeBtn);
        
        close.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//
				closePressed(v);
			}
		});
	}
	
	public ImagePreviewParent getParentActivity() {
		return parent;
	}
	
	public void hideInstructions() {
		//instructions.setVisibility(View.INVISIBLE);
	}
	
	public void show(List<Image> images, int initialPosition, DataService service) {
		//
		//this.service = service;
		this.images = images;
		
		///adapter.setItems(images);
		if (service != null) {
			instructions.setVisibility(View.VISIBLE);
			
	        adapter = new ImagePagerAdapter(parent.getSupportFragmentManager(), images.size(), service);
	        imageView.setAdapter(adapter);
	        indicator.setViewPager(imageView);
	        
	        imageView.setCurrentItem(initialPosition, true);
			
			//service.loadThumbnail(context, imageView, image, new Handler());
		}
		setVisibility(View.VISIBLE);
	}
	
	public void closePressed(View view) {
		//
		setVisibility(View.GONE);
		parent.popupClosed();
	}
	
	private class ImagePagerAdapter extends FragmentStatePagerAdapter {
        private final int size;
        private DataService service;

        public ImagePagerAdapter(FragmentManager fm, int size, DataService service) {
            super(fm);
            this.size = size;
            this.service = service;
        }

        @Override
        public int getCount() {
            return size;
        }

        @Override
        public Fragment getItem(int position) {
            return ImageDetailFragment.newInstance(images.get(position).getUri(), ImagePreviewList.this, service);
        }
    }

	public void setActivity(ProductDetail parent) {
		// 
		//this.parent = parent;
	}
	
	public void setShopFragmentActivity(ImagePreviewParent parent) {
		this.parent = parent;
	}
	
	public interface ImagePreviewParent {
		
		public void loadThumbnail(final String uri, final ImageView imageView);
		public FragmentManager getSupportFragmentManager();
		public void popupClosed();
	}
}
