package com.gogoplay.ui.widget;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import com.gogoplay.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Canvas;
import android.graphics.Movie;
import android.net.Uri;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.View;

@SuppressLint("ViewConstructor")
public class GIFView extends View {

	private long movieStart;
	private Movie movie;
	
	public GIFView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initializeView(attrs);
	}

	public GIFView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initializeView(attrs);
	}
	
	private void initializeView(AttributeSet attrs) {
		//
		if (attrs != null) {
			TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.GIFView, 0, 0);
			String gifSource = a.getString(R.styleable.GIFView_src);
			String sourceName = Uri.parse(gifSource).getLastPathSegment().replace("gif",  "");
			int gifId = getResources().getIdentifier(sourceName, "drawable", getContext().getPackageName());
			setImageBitmap(BitmapFactory.decodeResource(getContext().getResources(), gifId));
			a.recycle();
		}
	}
	
	public void setImageBitmap(Bitmap img) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream(); 
		img.compress(CompressFormat.PNG, 0 /*ignored for PNG*/, bos); 
		byte[] bitmapdata = bos.toByteArray();
		ByteArrayInputStream bs = new ByteArrayInputStream(bitmapdata);
		movie = Movie.decodeStream(bs);
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		canvas.drawColor(Color.TRANSPARENT);
		super.onDraw(canvas);
		long now = SystemClock.uptimeMillis();
		if (movieStart == 0) {
			movieStart = now;
		}
		
		if (movie != null) {
			//
			int relTime = (int) ((now - movieStart) % movie.duration());
			movie.setTime(relTime);
			movie.draw(canvas, getWidth() - movie.width(), getHeight() - movie.height());
			this.invalidate();
		}
	}
}
