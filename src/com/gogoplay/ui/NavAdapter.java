/**
 * 
 */
package com.gogoplay.ui;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gogoplay.R;
import com.gogoplay.data.User;
import com.gogoplay.ui.widget.ListItem;
import com.gogoplay.ui.widget.ListItem.Type;
import com.gogoplay.util.Utils;

/**
 * @author iudoka
 * 
 */
public class NavAdapter extends BaseAdapter {
	//
	private static final String TAG = "PopularProductsAdapter";
	private static final int TITLE_RES_ID = R.layout.nav_title;
	private static final int ITEM_RES_ID = R.layout.nav_item;
	private static final int PROFILE_RES_ID = R.layout.nav_profile;

	private ShopActionBarActivity parentActivity;
	private Context context;
	private List<ListItem> navItems;
	//private int selectedItem;

	public NavAdapter(ShopActionBarActivity parentActivity, Context context, List<ListItem> navItems) {
		super();
		this.parentActivity = parentActivity;
		this.context = context;
		this.navItems = navItems;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {

		View v = convertView;
		final ListItem item = (ListItem) getItem(position);

		LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);

		if (item.getType() == ListItem.Type.Header) {
			v = vi.inflate(TITLE_RES_ID, parent, false);
		} else if (item.getType() == ListItem.Type.Profile) {
			v = vi.inflate(PROFILE_RES_ID, parent, false);
		} else {
			v = vi.inflate(ITEM_RES_ID, parent, false);
		}
		
		//highlightItem(position, v);
		
		TextView nameView = (TextView) v.findViewById(R.id.navTitle);
		nameView.setVisibility(View.VISIBLE);
		nameView.setText(item.getTitle());
		
		final ImageView icon1 = (ImageView) v.findViewById(R.id.navItemIcon);
		final ImageView icon2 = (ImageView) v.findViewById(R.id.navItemIcon2);
		
		if (item.getType() != ListItem.Type.Header) {
			//
			icon1.setVisibility(View.VISIBLE);
			icon2.setVisibility(View.VISIBLE);
			
			if (item.getType() == ListItem.Type.Navigation || item.getType() == ListItem.Type.Action) {
				//
				if (item.getIcon1() > 0) {
					icon1.setImageResource(item.getIcon1());
				} else {
					icon1.setVisibility(View.INVISIBLE);
				}
				
				if (item.getIcon2() > 0) {
					icon2.setImageResource(item.getIcon2());
				} else {
					icon2.setVisibility(View.GONE);
				}
			} else if (item.getType() == ListItem.Type.Profile) {
				//
				User user = null;
				
				if (parentActivity.getDataService() != null) {
					user = parentActivity.getDataService().getUser(context);
					
					if (user != null) {
						//
						if (user.getAvatar() != null) {
							icon1.setImageBitmap(user.getAvatar());
						} else if (!Utils.isEmpty(user.getPhoto())) {//user.getAvatar() != null) {
							parentActivity.loadThumbnail(user.getPhoto(), icon1);
						} else {
							icon1.setImageResource(item.getIcon1());
						}
						
						icon2.setImageResource(item.getIcon2());
						nameView.setText(user.getFirstName() + " " + user.getLastName());
					} else {
						
						icon1.setVisibility(View.GONE);
						icon2.setVisibility(View.GONE);
						
						nameView.setText(context.getString(R.string.user_login_option));
						nameView.setTextColor(parentActivity.getResources().getColor(R.color.blue));
					}
				} else {
					// hide entire area
					icon1.setVisibility(View.GONE);
					icon2.setVisibility(View.GONE);
					nameView.setVisibility(View.GONE);
				}
			}
		} else {
			//
			icon1.setVisibility(View.GONE);
			icon2.setVisibility(View.GONE);
		}

		return v;
	}
	
	
	/*public void setSelectedItem(int selectedItem) {
        this.selectedItem = selectedItem;
    }
	
	private void highlightItem(int position, View result) {
        if(position == selectedItem) {
            // you can define your own color of selected item here
            result.setBackgroundColor(context.getResources().getColor(R.color.light_blue));
        } else {
            // you can define your own default selector here
            result.setBackgroundDrawable(context.getResources().getDrawable(android.R.drawable.list_selector_background));
        }
    }*/
	
	@Override
	public boolean isEnabled(int position) {
		ListItem item = (ListItem) getItem(position);
		
		return (item.getType() != Type.Header);
	}

	@Override
	public int getCount() {
		return navItems.size();
	}

	@Override
	public Object getItem(int position) {
		Object item = null;

		try {
			item = navItems.get(position);
		} catch (Exception ex) {
			Log.e(TAG, "Unable to get item", ex);
		}

		return item;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public Context getContext() {
		return context;
	}
}
