package com.gogoplay.ui;

public interface ShopActivityBase {
	public static final int s_ENTRY_ANIM = android.R.anim.fade_in;
	public static final int s_EXIT_ANIM = android.R.anim.fade_out;
	
	public String getViewTitle();
	public String getViewFooter();
	public boolean isVisible();
	public String getTag();
}
