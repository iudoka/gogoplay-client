package com.gogoplay.ui;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.gogoplay.FullScreenPreview;
import com.gogoplay.R;
import com.gogoplay.data.Image;
import com.gogoplay.data.Item;
import com.gogoplay.ui.widget.ScaleLayout;
import com.gogoplay.util.Utils;
import com.nineoldandroids.view.ViewHelper;

@SuppressLint("ValidFragment")
public class ProductImageFragment extends Fragment {
	/**
	 * 
	 */
	//
	//private static final NumberFormat format = NumberFormat.getCurrencyInstance();
	private static final int resId = R.layout.product_details_img;//product_gallery_item;
	
	public static final String ITEM = "ITEM";
	public static final String BITMAP = "BITMAP";
	public static final String IMAGE_FETCHER = "IMAGE_FETCHER";
	
	private ShopActionBarActivity view;

	public ProductImageFragment() {
		super();
	}
	
	public ProductImageFragment(Context context, ShopActionBarActivity view) {
		super();
		
		this.view = view;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		//
		if (container == null) {
			return null;
		}
		
		final Item item = (Item) getArguments().getSerializable(ITEM);
		final int pos = getArguments().getInt("pos");
		final String itemUri = item.getImages().get(pos).getUri();

		View v = inflater.inflate(resId, container, false);

		if (!Utils.isEmpty(itemUri)) {
			ImageView imgView = (ImageView) v.findViewById(R.id.itemImage);
			
			imgView.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					//
					Intent intent = new Intent(view, FullScreenPreview.class);
					intent.putExtra(FullScreenPreview.s_EXTRA_IMAGE_GALLERY, new ArrayList<Image>(item.getImages()));
					intent.putExtra(FullScreenPreview.s_EXTRA_START_INDEX, pos);
						
					startActivity(intent);
				}
			});
			
			view.loadThumbnail(itemUri, imgView);
		}
		
		ScaleLayout root = (ScaleLayout) v.findViewById(R.id.root);
		float scale = this.getArguments().getFloat("scale");
		root.setScaleBoth(scale);
		boolean isBlurred = this.getArguments().getBoolean("isBlurred");
		
		if(isBlurred) {
			//
			ViewHelper.setAlpha(root, .6f);
			ViewHelper.setRotationY(root, 60.0f);
		}

		return v;
	}
	
	public static Fragment newInstance(Context context, int pos, float scale,boolean IsBlured)
	{
		
		Bundle b = new Bundle();
		b.putInt("pos", pos);
		b.putFloat("scale", scale);
		b.putBoolean("isBlurred", IsBlured);
		return Fragment.instantiate(context, ProductImageFragment.class.getName(), b);
	}

	public static Fragment newInstance(Context context, ShopActionBarActivity view, Item item, Bitmap loading, int pos, float scale, boolean isBlurred) {
		
		ProductImageFragment fragment = new ProductImageFragment(context, view);
		Bundle b = new Bundle(3);
		b.putSerializable(ProductImageFragment.ITEM, item);
		b.putParcelable(ProductImageFragment.BITMAP, loading);
		b.putInt("pos", pos);
		b.putFloat("scale", scale);
		b.putBoolean("isBlurred", isBlurred);
		fragment.setArguments(b);

		//return fragment;
		Fragment f = Fragment.instantiate(context, ProductImageFragment.class.getName(), b);
		
		((ProductImageFragment)f).view = view;
		
		return f;
	}
}