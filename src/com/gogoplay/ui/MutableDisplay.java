package com.gogoplay.ui;

public interface MutableDisplay {
	//
	public enum DisplayType {
		SINGLE_ITEM,
		TWO_PER_LINE,
		THREE_PER_LINE
	}
	
	public void setDisplayType(DisplayType type);
	
	public DisplayType getCurrentDisplayType();
	
	public void toggleDisplay();
	
	public CharSequence getNextDisplayTypeName();
	
	public int getNextDisplayTypeIcon();
}
