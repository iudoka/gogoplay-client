package com.gogoplay.ui;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import com.flurry.android.FlurryAgent;
import com.gogoplay.data.DataService;
import com.gogoplay.data.DataService.DataBinder;
import com.gogoplay.util.FlurryUtils;

public abstract class ShopActivity extends Activity implements ShopActivityBase {
	//
	private boolean visible = false;
	private DataService dataService;
	private boolean serviceConnected = false;
	
	/** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection connection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder serviceBinder) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            DataBinder binder = (DataBinder) serviceBinder;
            dataService = binder.getService();
            serviceConnected = true;
            
            serviceConnected();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            serviceConnected = false;
        }
    };
    
    protected abstract void serviceConnected();
    
    protected boolean isServiceConnected() {
    	return serviceConnected;
    }
    
    public DataService getDataService() {
    	return dataService;
    }
	
	@Override
    protected void onStart() {
		FlurryAgent.onStartSession(this, FlurryUtils.getFlurryKey(this));
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, DataService.class);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        if (serviceConnected) {
            unbindService(connection);
            serviceConnected = false;
        }
        
        // hide soft input keyboard
        onClose();
        FlurryAgent.onEndSession(this);
    }

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ShopApplication.setScreenSize(getScreenDimensions());
		visible = true;
		// requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);

		setTitle(getViewTitle());
	}
	
	public DisplayMetrics getScreenDimensions() {
		// Fetch screen height and width, to use as our max size when loading images as this
        // activity runs full screen
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        //final int height = displayMetrics.heightPixels;
        //final int width = displayMetrics.widthPixels;
        
        return displayMetrics;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		visible = false;
		
		//unbindDrawables(this);
	}

	@Override
	public void onAttachedToWindow() {
		super.onAttachedToWindow();
		//Window window = getWindow();
		// Eliminates color banding
		// window.setFormat(PixelFormat.RGBA_8888);
		// window.addFlags(WindowManager.LayoutParams.FLAG_DITHER);
	}
	
	@Override
	public void setContentView(int layoutResID) {
		//
		overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
		super.setContentView(layoutResID);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		//this.menu = menu;
		// populate menu items
		// SpotApplication.createMenuOptions(this, menu);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// SpotApplication.menuOptionSelected(this, item);

		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
	    super.onConfigurationChanged(newConfig);
	    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}
	
	public void loadThumbnail(final String uri, final ImageView imageView) {
		//
		/*
		 * (new Handler()).post(new Runnable() { public void run() {
		 * thumbnailFetcher.setPauseWork(false); thumbnailFetcher.loadImage(uri,
		 * imageView); } });
		 */

		if (isServiceConnected()) {
			getDataService().loadThumbnail(this, imageView, uri, new Handler());
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		visible = false;
	}

	@Override
	protected void onResume() {
		super.onResume();
		visible = true;
	}

	public boolean isVisible() {
		return visible;
	}
	
	public void onClose() {
		// Add any logic needed to do at closing
		InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

	    //check if no view has focus:
	    View view = getCurrentFocus();
	    if (view == null)
	        return;

	    inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	}

	public abstract String getViewTitle();
}
