package com.gogoplay.ui;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.gogoplay.R;
import com.gogoplay.data.DataService;
import com.gogoplay.data.DataService.DataBinder;
import com.gogoplay.data.User;
import com.gogoplay.util.FlurryUtils;
import com.gogoplay.util.ImageUtil;
import com.gogoplay.util.Utils;

public abstract class ShopActionBarActivity extends ActionBarActivity implements
		ShopActivityBase {
	//
	// private ImageFetcher thumbnailFetcher;
	private Menu menu;
	private View titleView;
	private TextView header;
	private TextView footer;
	private boolean visible = false;
	private DataService dataService;
	private boolean dataServiceConnected = false;

	/** Defines callbacks for service binding, passed to bindService() */
	private ServiceConnection connection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName className,
				IBinder serviceBinder) {
			// We've bound to LocalService, cast the IBinder and get
			// LocalService instance
			DataBinder binder = (DataBinder) serviceBinder;
			dataService = binder.getService();
			dataServiceConnected = true;
			dataService.resumeTasks();
			dataServiceConnected(true);
		}

		@Override
		public void onServiceDisconnected(ComponentName arg0) {
			dataServiceConnected = false;
			dataService = null;
			dataServiceConnected(false);
		}
	};

	protected abstract void dataServiceConnected(boolean connected);

	public boolean isDataServiceConnected() {
		return dataServiceConnected || dataService != null;
	}

	public DataService getDataService() {
		//
		if (isDataServiceConnected()) {
			return dataService;
		}

		return null;
	}

	public Menu getMenu() {
		return menu;
	}

	@Override
	protected void onStart() {
		FlurryAgent.onStartSession(this, FlurryUtils.getFlurryKey(this));
		super.onStart();
		// Bind to LocalService
		Intent intent = new Intent(this, DataService.class);
		bindService(intent, connection, Context.BIND_AUTO_CREATE);
	}

	@Override
	protected void onStop() {
		super.onStop();
		visible = false;
		
		// Unbind from the service
		if (dataServiceConnected) {
			unbindService(connection);
			dataServiceConnected = false;
			dataServiceConnected(false);
		}
		
		// hide soft input keyboard
		onClose();
		FlurryAgent.onEndSession(this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		//
		super.onCreate(savedInstanceState);
		ShopApplication.setScreenSize(getScreenDimensions());

		try {
			ActionBar actionBar = getSupportActionBar();
	
			if (actionBar != null) {
				//actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
				
				actionBar.setDisplayShowTitleEnabled(true);
				actionBar.setDisplayHomeAsUpEnabled(true);
				
				// set custom actionbar title
				this.getActionBar().setDisplayShowCustomEnabled(true);
				this.getActionBar().setDisplayShowTitleEnabled(false);
		
				LayoutInflater inflator = LayoutInflater.from(this);
				titleView = inflator.inflate(R.layout.app_header, null);
		
				//if you need to customize anything else about the text, do it here.
				//I'm using a custom TextView with a custom font in my layout xml so all I need to do is set title
				header = (TextView)titleView.findViewById(R.id.header);
				footer = (TextView)titleView.findViewById(R.id.footer);
		
				//assign the view to the actionbar
				this.getActionBar().setCustomView(titleView);
				
				setViewTitle(getViewTitle(), getViewFooter());
			
				//set the actionbar to use the custom view (can also be done with a style)
				//getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		
				//set the custom view to use
				//getActionBar().setCustomView(R.layout.custom_ab);
			}
		} catch (Exception ex) {}
	}
	
	@Override
	public void setContentView(int layoutResID) {
		//
		overridePendingTransition(s_ENTRY_ANIM, s_EXIT_ANIM);
		super.setContentView(layoutResID);
	}

	@Override
	protected void onPause() {
		super.onPause();
		visible = false;

		// thumbnailFetcher.setPauseWork(true);
		if (dataServiceConnected) {
			dataService.stopCurrentTasks();
		}
	}

	/*
	 * @Override public boolean onCreateOptionsMenu(Menu menu) {
	 * getMenuInflater().inflate(R.menu.main, menu); return
	 * super.onCreateOptionsMenu(menu); }
	 */

	public boolean onCreateOptionsMenu(Menu menu) {
		this.menu = menu;
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			break;
		case R.id.action_settings:
			ShopApplication.goToSettings(this);
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed() {
		// 
		onClose();
		super.onBackPressed();
	}

	@Override
	protected void onResume() {
		super.onResume();
		visible = true;
		// thumbnailFetcher.setPauseWork(false);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		visible = false;
		// thumbnailFetcher.closeCache();
	}
	
	public void onClose() {
		// Add any logic needed to do at closing
		InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

	    //check if no view has focus:
	    View view = getCurrentFocus();
	    if (view == null)
	        return;

	    inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	}

	public DisplayMetrics getScreenDimensions() {
		// Fetch screen height and width, to use as our max size when loading
		// images as this
		// activity runs full screen
		final DisplayMetrics displayMetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		// final int height = displayMetrics.heightPixels;
		// final int width = displayMetrics.widthPixels;

		return displayMetrics;
	}

	@Override
	public abstract String getViewTitle();
	
	
	public void setViewTitle(CharSequence headerText, CharSequence footerText) {
		//
		header.setVisibility((Utils.isEmpty(headerText)) ? View.GONE : View.VISIBLE);
		header.setText(Utils.emptyIfNull(headerText));
		
		setTitleFooter(footerText);
	}
	
	public void setTitleFooter(CharSequence footerText) {
		//
		footer.setVisibility((Utils.isEmpty(footerText)) ? View.GONE : View.VISIBLE);
		footer.setText(Utils.emptyIfNull(footerText));
	}

	@Override
	public boolean isVisible() {
		return visible;
	}

	@Override
	public String getTag() {
		return this.getClass().getSimpleName();
	}

	protected void loadBitmapResource(int resId, Handler handler) {
		//
		ImageUtil.loadFromResource(this, resId, handler);
	}

	public void loadThumbnail(final String uri, final ImageView imageView) {
		//
		/*
		 * (new Handler()).post(new Runnable() { public void run() {
		 * thumbnailFetcher.setPauseWork(false); thumbnailFetcher.loadImage(uri,
		 * imageView); } });
		 */

		if (isDataServiceConnected()) {
			getDataService().loadThumbnail(this, imageView, uri, new Handler());
		}
	}

	protected void showMessage(String msg) {
		//
		ShopApplication.showMessage(this, msg);
	}
	
	protected User getUser() {
		//
		if (isDataServiceConnected()) {
			return getDataService().getUser(this);
		}
		
		return null;
	}
}
