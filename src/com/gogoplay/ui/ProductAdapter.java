/**
 * 
 */
package com.gogoplay.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

import com.gogoplay.Products;
import com.gogoplay.R;
import com.gogoplay.data.Comment;
import com.gogoplay.data.DataService;
import com.gogoplay.data.Item;
import com.gogoplay.data.User;
import com.gogoplay.ui.MutableDisplay.DisplayType;
import com.gogoplay.ui.widget.Avatar;
import com.gogoplay.ui.widget.RecyclingImageView;
import com.gogoplay.util.LocationUtil;
import com.gogoplay.util.Utils;

/**
 * @author iudoka
 * 
 */
@SuppressLint("HandlerLeak")
public class ProductAdapter extends BaseAdapter {
	//
	private static final String TAG = "PopularProductsAdapter";
	//private static final int resId = R.layout.product;

	private FragmentActivity context;
	private Products view;
	private List<Item> items = Collections.synchronizedList(new ArrayList<Item>());
	private LocationUtil locationUtil;

	public ProductAdapter(Products view, FragmentActivity context, List<Item> items) {
		super();
		this.view = view;
		this.context = context;

		if (items != null) {
			this.items.addAll(items);
		}

		init();
	}

	private void init() {
		//
		locationUtil = new LocationUtil(view.getActivity());
	}

	public void destroy() {
		//
	}
	
	private int getResId() {
		if (view.isFeedDisplay()) {
			return R.layout.product_feed;
		} else {
			return R.layout.product;
		}
	}
	
	public List<Item> getItems() {
		return items;
	}
	
	private boolean isValid(View v) {
		//
		if (v != null) {
			//
			Object tag = v.getTag();
			
			if (tag != null) {
				//
				if (view.isFeedDisplay() && (tag instanceof ImageViewHolder)) {
					return false; // cannot recycle view - re-inflate
				}
				
				if (!view.isFeedDisplay() && (tag instanceof FeedViewHolder)) {
					return false; // cannot recycle view - re-inflate
				}
				
				return true;
			} else {
				return true;
			}
		} else {
			//
			return false;
		}
	}

	@SuppressLint("HandlerLeak")
	public View getView(final int position, View convertView, ViewGroup parent) {

		View v = convertView;
		// TODO - fix this to speed things up - maybe have 2 diff adapters and switch when user requests
		if (!isValid(v)) {
			Log.i(TAG, "Invalid view re-inflating");
			LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(getResId(), parent, false);
		} else {
			//
			//v.forceLayout();
		}

		final Item item = (Item) getItem(position);

		if (item != null) {
			if (view.isFeedDisplay()) {
				initFeedDisplay(item, v);
			} else {
				initImageDisplay(item, v);
			}
		}

		return v;
	}
	
	static class ImageViewHolder {
		//
		ViewGroup root;
		RecyclingImageView imgView;
		//ViewPager imgPager;
		//CirclePageIndicator indicator;
		TextView nameView;
		TextView descView;
		TextView priceView;
		TextView condView;
		View likes;
		ImageView likeIcon;
	}
	
	private void initImageDisplay(final Item item, final View v) {
		// 
		ImageViewHolder viewHolder;
		
		if (v.getTag() != null && v.getTag() instanceof ImageViewHolder) {
			viewHolder = (ImageViewHolder)v.getTag();
		} else {
			//
			viewHolder = new ImageViewHolder();
			
			viewHolder.root = (ViewGroup) v.findViewById(R.id.root);
			viewHolder.imgView = (RecyclingImageView) v.findViewById(R.id.itemImage);
			//viewHolder.imgPager = (ViewPager) v.findViewById(R.id.itemGallery);
			//viewHolder.indicator = (CirclePageIndicator) v.findViewById(R.id.indicator);
			viewHolder.nameView = (TextView) v.findViewById(R.id.itemTitle);
			viewHolder.descView = (TextView) v.findViewById(R.id.itemDesc);
			viewHolder.priceView = (TextView) v.findViewById(R.id.itemPrice);
			viewHolder.condView = (TextView) v.findViewById(R.id.itemCondition);
			viewHolder.likes = (View) v.findViewById(R.id.likeLayout);
			viewHolder.likeIcon = (ImageView)v.findViewById(R.id.likeIcon);
			
			v.setTag(viewHolder);
		}
		
		/*ProductImageAdapter topAdapter = new ProductImageAdapter(this.view.getFragmentManager(), this.view, context);
		imgPager.setAdapter(topAdapter);
		
		indicator.setViewPager(imgPager);
		
		topAdapter.setItems(item.getImages());*/
		
		if (!Utils.isEmpty(item.getImages())) {
			// Fetch and set bitmap for item
			String image = item.getImages().get(0).getUri();
			
			view.loadThumbnail(image, viewHolder.imgView);
		} else {
			// no images
			viewHolder.imgView.setScaleType(ScaleType.CENTER);
			viewHolder.imgView.setImageResource(R.drawable.loading);
		}

		/*viewHolder.nameView.setText(Utils.trimToFit(item.getTitle(), 25, "..."));
		viewHolder.nameView.setVisibility(Utils.isEmpty(item.getTitle()) ? View.INVISIBLE : View.VISIBLE);
		
		if (Utils.isEmpty(item.getDescription())) {
			//descView.setVisibility(View.GONE);
			if (viewHolder.nameView.getVisibility() != View.INVISIBLE){
				viewHolder.nameView.setVisibility(View.INVISIBLE);
				viewHolder.descView.setText(Utils.trimToFit(item.getTitle(), 25, "..."));
			}
		} else {
			viewHolder.descView.setText(Utils.trimToFit(item.getDescription(), 25, "..."));
		}*/
		
		CharSequence title = Utils.isEmpty(item.getTitle()) ? item.getDescription() : item.getTitle();
		viewHolder.nameView.setText(Utils.emptyIfNull(title));
		viewHolder.descView.setVisibility(View.GONE);
		
		viewHolder.priceView.setText(Utils.formatPrice(context, item.getPrice()));
		
		if (view.getCurrentDisplayType() != DisplayType.SINGLE_ITEM) {
			//
			//priceView.setTextSize(view.getResources().getDimension(R.dimen.small_font));
			viewHolder.priceView.setVisibility(View.INVISIBLE);
		} else {
			//
			//priceView.setTextSize(view.getResources().getDimension(R.dimen.header1));
			viewHolder.priceView.setVisibility(View.VISIBLE);
		}
		
		//viewHolder.condView.setText(Utils.trimToFit(item.getCondition(), 25, ""));
		//viewHolder.condView.setVisibility(Utils.isEmpty(item.getCondition()) ? View.INVISIBLE : View.VISIBLE);
		viewHolder.condView.setVisibility(View.GONE);

		v.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//
				view.displayProductDetails(item);
			}
		});
		
		if (item.isLiked()) {
			// show different icon	
			viewHolder.likeIcon.setImageResource(R.drawable.ic_heart_blue);
		} else {
			viewHolder.likeIcon.setImageResource(R.drawable.ic_heart_gray);
		}
		
		final View theView = v;
		
		viewHolder.likes.setVisibility(View.GONE);
		if (view != null && view.getUser() != null && 
			item != null && item.getSeller() != null &&
			!Utils.equals(view.getUser().getId(), item.getSeller().getId())) {
			// Prevent sellers from liking their own items
			viewHolder.likes.setVisibility(View.VISIBLE);
			
			viewHolder.likes.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					//
					view.showMessage("");
					// like the item,
					// user must be logged in to like
					getDataService().likeItem(context, item, new Handler() {
						@Override
						public void handleMessage(Message msg) {
							// 
							ImageView likeIcon = (ImageView)theView.findViewById(R.id.likeIcon);
							
							if (item.isLiked()) {
								likeIcon.setImageResource(R.drawable.ic_heart_blue);
							} else {
								likeIcon.setImageResource(R.drawable.ic_heart_gray);
							}
	
							view.hideMessage();
						}
					});
				}
			});
		}
	}
	
	static class FeedViewHolder {
		//
		String itemId;
		ViewGroup root;
		RecyclingImageView imgView;
		TextView nameView;
		TextView descView;
		TextView priceView;
		TextView condView;
		TextView quantityView;
		TextView locationView;
		TextView commentCount;
		ViewGroup comment1;
		TextView commentText1;
		ViewGroup comment2;
		TextView commentText2;
		TextView extraComments;
		ViewGroup likeBtn;
		ViewGroup commentBtn;
		ViewGroup shareBtn;
		Avatar sellerAvatar;
		Avatar commentAvatar1;
		Avatar commentAvatar2;
		TextView distance;
		//View likes;
	}

	private static final int gone = View.GONE;

	private void initFeedDisplay(final Item item, final View v) {
		// 
		FeedViewHolder viewHolder;
		
		if (v.getTag() != null && v.getTag() instanceof FeedViewHolder) {
			viewHolder = (FeedViewHolder)v.getTag();
			//update = viewHolder.itemId != item.getId();
		} else {
			//
			viewHolder = new FeedViewHolder();
			
			viewHolder.imgView = (RecyclingImageView) v.findViewById(R.id.itemImage);
			viewHolder.nameView = (TextView) v.findViewById(R.id.itemTitle);
			viewHolder.descView = (TextView) v.findViewById(R.id.itemDesc);
			viewHolder.priceView = (TextView) v.findViewById(R.id.itemPrice);
			viewHolder.condView = (TextView) v.findViewById(R.id.itemCondition);
			viewHolder.quantityView = (TextView) v.findViewById(R.id.itemQuantity);
			viewHolder.locationView = (TextView) v.findViewById(R.id.itemLocation);
			viewHolder.commentCount = (TextView) v.findViewById(R.id.commentCount);
			viewHolder.comment1 = (ViewGroup) v.findViewById(R.id.comment1);
			viewHolder.commentText1 = (TextView) v.findViewById(R.id.itemComment1);
			viewHolder.comment2 = (ViewGroup) v.findViewById(R.id.comment2);
			viewHolder.commentText2 = (TextView) v.findViewById(R.id.itemComment2);
			viewHolder.extraComments = (TextView) v.findViewById(R.id.extraComments);
			viewHolder.likeBtn = (ViewGroup) v.findViewById(R.id.likeBtn);
			viewHolder.commentBtn = (ViewGroup) v.findViewById(R.id.commentBtn);
			viewHolder.shareBtn = (ViewGroup) v.findViewById(R.id.shareBtn);
			viewHolder.sellerAvatar = (Avatar) v.findViewById(R.id.sellerAvatar);
			viewHolder.commentAvatar1 = (Avatar) v.findViewById(R.id.commentAvatar1);
			viewHolder.commentAvatar2 = (Avatar) v.findViewById(R.id.commentAvatar2);
			viewHolder.distance = (TextView) v.findViewById(R.id.distance);
			
			v.setTag(viewHolder);
		}
		
		if (!Utils.isEmpty(item.getImages())) {
			// Fetch and set bitmap for item
			String image = item.getImages().get(0).getUri();

			view.loadThumbnail(image, viewHolder.imgView);
		} else {
			// no images
			viewHolder.imgView.setScaleType(ScaleType.CENTER);
			viewHolder.imgView.setImageResource(R.drawable.loading);
		}
		
		if (item.getSeller() != null) {
			viewHolder.sellerAvatar.setUser(item.getSeller(), view.getDataService());
		}

		viewHolder.nameView.setText(Utils.trimToFit(item.getTitle(), 25, "..."));
		//viewHolder.nameView.setVisibility(Utils.isEmpty(item.getTitle()) ? View.INVISIBLE : View.VISIBLE);
		
		viewHolder.descView.setText(Utils.trimToFit(item.getDescription(), 140, "..."));
		
		viewHolder.priceView.setText(Utils.formatPrice(context, item.getPrice()));
		viewHolder.quantityView.setText(item.getQuantity() + " " + context.getResources().getString(R.string.remaining));
		
		if (Utils.isEmpty(item.getAddress())) {
			loadLocation(item, viewHolder.locationView);
		} else {
			viewHolder.locationView.setText(item.getAddress());
		}
		
		viewHolder.condView.setText(Utils.trimToFit(item.getCondition(), 25, ""));
		//viewHolder.condView.setVisibility(Utils.isEmpty(item.getCondition()) ? View.INVISIBLE : View.VISIBLE);
		
		int commentSize = item.getCommentCount();
		
		viewHolder.commentCount.setText(commentSize + "");
		viewHolder.extraComments.setText(context.getString(R.string.view_all_comments).replace("%%", commentSize + ""));
		viewHolder.extraComments.setVisibility(gone);
		
		if (!Utils.isEmpty(item.getComments())) {
			ArrayList<Comment> comments = new ArrayList<Comment>(item.getComments());
			
			viewHolder.commentAvatar1.setUser(comments.get(0).getUser(), view.getDataService());
			
			viewHolder.comment1.setVisibility(View.VISIBLE);
			viewHolder.commentText1.setText(Utils.parseSpecialChars(comments.get(0).getText(), context));
			
			if (commentSize > 1) {
				
				viewHolder.commentAvatar2.setUser(comments.get(1).getUser(), view.getDataService());
				
				viewHolder.comment2.setVisibility(View.VISIBLE);
				viewHolder.commentText2.setText(Utils.parseSpecialChars(comments.get(1).getText(), context));
				
				if (commentSize > 2) {
					viewHolder.extraComments.setVisibility(View.VISIBLE);
				}
			} else {
				viewHolder.comment2.setVisibility(gone);
				viewHolder.extraComments.setVisibility(gone);
			}
		} else {
			viewHolder.comment1.setVisibility(gone);
			viewHolder.comment2.setVisibility(gone);
			viewHolder.extraComments.setVisibility(gone);
		}
		
		if (!Utils.isEmpty(item.getDistance())) {
			//
			try {
				StringBuilder distance = new StringBuilder();
				int dist = (int)Math.round(LocationUtil.mongoUnitsToMiles(Double.parseDouble(item.getDistance())));
				distance.append(dist + " " + (dist != 1 ? context.getString(R.string.miles_away) : context.getString(R.string.mile_away)));
				
				viewHolder.distance.setVisibility(View.VISIBLE);
				viewHolder.distance.setText(distance);
			} catch (Exception ex) {
				//
				viewHolder.distance.setVisibility(View.GONE);
			}
		} else {
			//
			viewHolder.distance.setVisibility(View.GONE);
		}

		v.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//
				//ShopApplication.goToProductDetails(context, item.getId(), item);
				view.displayProductDetails(item);
			}
		});
		
		updateLikeCount(v, item);
		
		final View finalV = v;
		
		viewHolder.likeBtn.setVisibility(View.GONE);
		viewHolder.commentBtn.setVisibility(View.GONE);
		
		if (!Utils.isSameUser(view.getUser(), item.getSeller())) {
			// Prevent sellers from liking their own items
			viewHolder.likeBtn.setVisibility(View.VISIBLE);
			viewHolder.commentBtn.setVisibility(View.VISIBLE);
			
			viewHolder.likeBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					//
					view.showMessage("");
					// like the item,
					// user must be logged in to like
					getDataService().likeItem(context, item, new Handler() {
						@Override
						public void handleMessage(Message msg) {
							//
							updateLikeCount(finalV, item);
	
							view.hideMessage();
						}
					});
				}
			});
			
			viewHolder.commentBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// 
					view.addComment(item);
				}
			});
		}
		
		viewHolder.shareBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//
				shareProduct(item, getDataService().getUser(context));
			}
		});
	}
	
	public void shareProduct(Item item, User user) {
		//
		Utils.shareProduct(context, view.getActivity(), item, user);
	}
	
	private void updateLikeCount(View v, Item item) {
		//
		TextView likeCount = (TextView) v.findViewById(R.id.likeCount);
		TextView itemLikes = (TextView) v.findViewById(R.id.itemLikes);
		ViewGroup itemLikesArea = (ViewGroup) v.findViewById(R.id.itemLikesLayout);
		ImageView likeIcon = (ImageView)v.findViewById(R.id.likeBtnIcon);
		TextView likeBtnText = (TextView) v.findViewById(R.id.likeBtnText);

		likeCount.setText(item.getLikeCount() + "");
		
		StringBuilder likeStr = new StringBuilder();
		
		itemLikesArea.setVisibility(item.getLikeCount() > 0 ? View.VISIBLE : gone);
		
		if (item.isLiked()) {
			// show different icon	
			likeIcon.setImageResource(R.drawable.ic_heart_blue);
			likeBtnText.setText(context.getString(R.string.liked));
			
			likeStr.append("You");
			
			if (item.getLikeCount() > 1) {
				int count = item.getLikeCount() - 1;
				likeStr.append(" and " + (count) + " other" + (count > 1 ? "s" : " person") + " like this");
			} else {
				likeStr.append(" like this");
			}
		} else {
			//
			
			likeIcon.setImageResource(R.drawable.ic_heart_red);
			likeBtnText.setText(context.getString(R.string.like));
			likeStr.append(item.getLikeCount() + " other" + (item.getLikeCount() > 1 ? "s" : " person") + " like" + (item.getLikeCount() > 1 ? "" : "s") + " this");
		}
		
		itemLikes.setText(likeStr.toString());
	}
	
	@SuppressLint("HandlerLeak")
	private void loadLocation(final Item item, final TextView locationView) {
		//
		
		if (item.getLocation() != null) {
			new LoadLocationTask(item.getLocation()[0], item.getLocation()[1], locationUtil, new Handler() {
				//
				@Override
				public void handleMessage(Message msg) {
					boolean found = msg.getData().getBoolean("success");
					String loc = msg.getData().getString("location");
					
					if (!found) {
						loc = context.getString(R.string.unknown);
					}
					
					locationView.setText(loc);
				}
			}).execute();
		} else {
			locationView.setText(context.getString(R.string.unknown));
		}
	}
	
	private class LoadLocationTask extends AsyncTask<Void, Void, Location> {
		//
		private LocationUtil locationUtil;
		private Handler callback;
		private double lat;
		private double lon;
		
		public LoadLocationTask(double lat, double lon, LocationUtil locationUtil, Handler callback) {
			this.lat = lat;
			this.lon = lon;
			this.callback = callback;
			this.locationUtil = locationUtil;
		}
		
		@Override
		protected Location doInBackground(Void... params) {
			// 
			return locationUtil.getLocation(lat, lon);
		}
		
		@Override
		protected void onPostExecute(Location result) {
			// 
			Message m = new Message();
			Bundle b = new Bundle();
			b.putBoolean("success", result != null);
			
			if (result != null) {
				b.putString("location", result.getExtras().getString(LocationUtil.EXTRA_CITY_AND_STATE_CODE));
			}

			m.setData(b);

			callback.sendMessage(m);
		}
	}

	@SuppressWarnings("unused")
	private void setSize(ViewGroup root, boolean smallerItems) {
		// 
		LayoutParams params = root.getLayoutParams();
		
		// calculate width
		final DisplayMetrics screenSize = new DisplayMetrics();
		context.getWindowManager().getDefaultDisplay().getMetrics(screenSize);
		
		double screenWidth = screenSize.widthPixels;//context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT ? screenSize.widthPixels : screenSize.heightPixels;
		
		double swFactor = getNumColumns();
		
		params.width = (int) (screenWidth/swFactor);
		params.height = params.width;
		
		root.setLayoutParams(params);
	}
	
	public double getNumColumns() {
		// calculate width
		final DisplayMetrics screenSize = new DisplayMetrics();
		context.getWindowManager().getDefaultDisplay().getMetrics(screenSize);

		double screenWidth = screenSize.widthPixels;
		double swFactor = 1;

		double prod_pref_size = 0;
		
		if (view.isFeedDisplay()) {
			prod_pref_size = context.getResources().getDimensionPixelSize(R.dimen.product_feed_size);
		} else {
			prod_pref_size = context.getResources().getDimensionPixelSize(R.dimen.product_img_size);
		}

		swFactor = Math.round(screenWidth / prod_pref_size);
		
		if (swFactor < 1) {
			swFactor = 1;
		}
		
		return (int) swFactor;
	}

	private DataService getDataService() {
		return view.getDataService();
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Object getItem(int position) {
		Object item = null;
		
		try {
			item = items.get(position);
		} catch (Exception ex) {
			Log.e(TAG, "Unable to get item", ex);
		}
		
		return item;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public void clear() {
		items.clear();
		super.notifyDataSetChanged();
	}

	/**
	 * Append these items to the end of the list
	 * 
	 * @param items
	 *            - items to append
	 */
	public void append(List<Item> items) {
		if (items != null) {
			this.items.addAll(items);
			super.notifyDataSetChanged();
		}
	}
	
	public void refresh(List<Item> items) {
		//
		this.items.clear();
		append(items);
	}
	
	public void redraw() {
		//
		super.notifyDataSetChanged();
	}
	
	public void update(Item item) {
		
		boolean found = false;
		
		if (item != null) {
			for (int i = 0; i < items.size(); i++) {
				if (item.getId() != null && Utils.equals(item.getId(), items.get(i).getId())) {
					found = items.get(i).merge(item);
					break;
				}
			}
		}
		
		if (found) {
			//
			super.notifyDataSetChanged();
		}
	}

	public Context getContext() {
		return context;
	}
}
