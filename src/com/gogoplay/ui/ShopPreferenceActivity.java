package com.gogoplay.ui;

import com.flurry.android.FlurryAgent;
import com.gogoplay.R;
import com.gogoplay.util.FlurryUtils;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.view.MenuItem;

public class ShopPreferenceActivity extends PreferenceActivity {
	//
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setTitle(getString(R.string.settings));
    }
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			break;
		default:
			return super.onOptionsItemSelected(item);
		}
		return true;
	}
	
	@Override
	protected void onStart() {
		// 
		FlurryAgent.onStartSession(this, FlurryUtils.getFlurryKey(this));
		super.onStart();
	}
	
	@Override
	protected void onStop() {
		// 
		super.onStop();
		FlurryAgent.onEndSession(this);
	}
}
