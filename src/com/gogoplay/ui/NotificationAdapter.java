/**
 * 
 */
package com.gogoplay.ui;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.CursorAdapter;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gogoplay.R;
import com.gogoplay.data.DataService;
import com.gogoplay.data.Notification;
import com.gogoplay.data.Notification.Action;
import com.gogoplay.data.User;
import com.gogoplay.ui.widget.ActionDisplayListener;
import com.gogoplay.ui.widget.Avatar;
import com.gogoplay.util.ImageUtil;
import com.gogoplay.util.Utils;

/**
 * @author iudoka
 * 
 */
public class NotificationAdapter extends CursorAdapter {
	//
	private static final String TAG = "NotificationAdapter";
	private static final int resId = R.layout.notification;

	private Context context;
	private ShopFragment view;
	private List<Notification> items = new ArrayList<Notification>();
	private Bitmap unknown;
	
	private LayoutInflater inflater;

	private LayoutInflater getInflater(Context context) {
		//
		if (inflater == null) {
			//
			inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}
		
		return inflater;
	}

	public NotificationAdapter(ShopFragment view, Context context, List<Notification> items) {
		//
		super(context, null, true);
		this.view = view;
		this.context = context;

		if (items != null) {
			this.items.addAll(items);
		}

		init();
	}

	@SuppressLint("HandlerLeak")
	private void init() {
		//
		ImageUtil.loadFromResource(context, R.drawable.anonymous, new Handler() {
			
			@Override
			public void handleMessage(Message msg) {
				//
				unknown = (Bitmap)msg.getData().get(ImageUtil.RESOURCE_ID);
			}
		});
	}

	public void destroy() {
		//
	}
	
	private static class ViewHolder {
		//
		public ImageView avatar;
		public TextView details;
		public TextView when;
		
	}
	
	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		//
		return getInflater(context).inflate(resId, parent, false);
	}
	
	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		//
		Notification notif = Notification.parse(cursor);
		
		bind(notif, view);
	}
	
	private void bind(final Notification notification, View v) {
		//

		if (notification != null) {
			//
			ViewHolder holder;
			
			if (v.getTag() != null && v.getTag() instanceof ViewHolder) {
				holder = (ViewHolder) v.getTag();
			} else {
				holder = new ViewHolder();
				
				holder.avatar = (ImageView) v.findViewById(R.id.avatar);
				holder.details = (TextView) v.findViewById(R.id.details);
				holder.when = (TextView) v.findViewById(R.id.when);
			}
			
			if (notification.getFrom() != null) {
				//
				if (notification.getFrom().getPhoto() != null) {
					// Fetch and set bitmap for comment
					String image = notification.getFrom().getPhoto();

					Log.i(TAG, "Fetching image " + " with uri: " + image);
					view.loadThumbnail(image, holder.avatar);
				} else {
					holder.avatar.setImageBitmap(unknown);
				}
				
				final ShopActionBarActivity parent = ((ShopActionBarActivity)view.getActivity());
				final DataService finalService = parent.getDataService();
				
				final ActionDisplayListener displayListener = new ActionDisplayListener() {
					
					@Override
					public boolean canShow() {
						// 
						if (finalService != null) {
							//
							User currUser = parent.getUser();
							
							if (currUser != null) {
								return !Utils.equals(currUser.getId(), notification.getFrom().getId());
							}
						}
						
						return true;
					}
					
					@Override
					public void followUser() {
						//
						if (finalService != null) {
							finalService.followUser(context, notification.getFrom(), new Handler());
						}
					}
					
					@Override
					public void viewProfile() {
						//
						if (finalService != null) {
							ShopApplication.goToProfile(context, notification.getFrom());
						}
					}
					
					@Override
					public void reportUser() {
						//
						if (finalService != null) {
							finalService.reportUser(context, notification.getFrom(), new Handler());
						}
					}
				};
				
				final View clickOn = holder.avatar;
				
				holder.avatar.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// 
						if (finalService != null) {
							Avatar.showUserPopup(context, clickOn, notification.getFrom(), parent.getUser(), displayListener);
						}
					}
				});
				
			}  else {
				holder.avatar.setImageBitmap(unknown);
			}
			
			holder.details.setText(Utils.emptyIfNull(notification.getNotification()));
			
			if (notification.getDateReceived() != null) {
				//
				holder.when.setText(context.getString(R.string.about) + " " + DateUtils.getRelativeTimeSpanString(notification.getDateReceived().getTime()));
			} else {
				holder.when.setText("");
			}
			
			v.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// 
					if (Action.buy != notification.getAction() &&
						Action.trade != notification.getAction() &&
						Action.message != notification.getAction()) {
						if (!Utils.isEmpty(notification.getItemId())) {
							//
							ShopApplication.goToProductDetails(view.getActivity(), notification.getItemId());
						}
					} else {
						// 
						ShopApplication.goToMessages(context, notification.getItemId(), notification.getFrom().getId());
					}
				}
			});
		}
	}
}
