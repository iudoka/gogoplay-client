package com.gogoplay.ui;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import com.gogoplay.R;
import com.gogoplay.data.DataService;
import com.gogoplay.data.User;

public abstract class ShopFragment extends Fragment {
	//
	//private ImageFetcher thumbnailFetcher;

	//
	
	private Menu menu;

	// Callback from activity to tell fragment service has been connected
	public abstract void dataServiceConnected(boolean connected);
	
	protected boolean isDataServiceConnected() {
		boolean bound = false;
		
		if (getActivity() instanceof ShopActionBarActivity) {
			//
			bound = ((ShopActionBarActivity)getActivity()).isDataServiceConnected();
		}
		
		return bound;
	}
	
	public DataService getDataService() {
		//
		DataService service = null;
		
		if (getActivity() instanceof ShopActionBarActivity) {
			//
			service = ((ShopActionBarActivity)getActivity()).getDataService();
		}
		
		return service;
	}
	
	public User getUser() {
		//
		if (isDataServiceConnected()) {
			return getDataService().getUser(getActivity());
		}
		
		return null;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// 
		super.onActivityCreated(savedInstanceState);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// 
		super.onCreate(savedInstanceState);
		
		/*ImageCacheParams cacheParams = new ImageCacheParams(getActivity(), DataService.THUMBNAIL_CACHE_DIR);
        cacheParams.setMemCacheSizePercent(0.05f); // Set memory cache to 5% of app memory
        
		 // The ImageFetcher takes care of loading images into our ImageView children asynchronously
        thumbnailFetcher = new ImageFetcher(getActivity(), 186);
        thumbnailFetcher.setLoadingImage(R.drawable.loading);
        thumbnailFetcher.addImageCache(cacheParams);*/
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return super.onCreateView(inflater, container, savedInstanceState);
	}
	
	@Override
	public void onDestroy() {
		// 
		super.onDestroy();
		//thumbnailFetcher.closeCache();
	}
	
	@Override
	public void onDetach() {
		// 
		super.onDetach();
	}
	
	@Override
	public void onStart() {
		// 
		super.onStart();
	}
	
	@Override
	public void onStop() {
		// 
		super.onStop();
	}
	
	@Override
	public void onResume() {
		// 
		super.onResume();
		//thumbnailFetcher.setPauseWork(false);
	}
	
	@Override
	public void onPause() {
		// 
		super.onPause();
		//thumbnailFetcher.setPauseWork(true);
	}
	
	public void loadThumbnail(final String uri, final ImageView imageView) {
		//
		/*(new Handler()).post(new Runnable() {
			public void run() {
				thumbnailFetcher.loadImage(uri, imageView);
			}
		});*/
		
		if (isDataServiceConnected()) {
			getDataService().loadThumbnail(getActivity(), imageView, uri, new Handler());
		}
	}
	
	public String getTitle() {
		return getString(R.string.app_name);
	}
	
	public String getFooter() {
		return null;
	}
	
	public abstract void refresh();
	
	public boolean isInit() {
		return false;
	}
	
	public void update() {
		//
	}
	
	public boolean canShow() {
		return true;
	}
	
	public void onNavShowing(boolean showing) {
		//
	}
	
	public String getFragmentTag() {
		return getClass().getName();
	}
	
	public void setMenu(Menu menu) {
		//
		this.menu = menu;
	}
	
	public Menu getMenu() {
		return menu;
	}

	public void onClose() {
		// Add any logic needed to do at closing
		InputMethodManager inputManager = (InputMethodManager) getActivity()
	            .getSystemService(Context.INPUT_METHOD_SERVICE);

	    //check if no view has focus:
	    View view = getActivity().getCurrentFocus();
	    if (view == null)
	        return;

	    inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	}
}
