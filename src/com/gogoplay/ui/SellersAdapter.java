/**
 * 
 */
package com.gogoplay.ui;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gogoplay.R;
import com.gogoplay.data.Seller;

/**
 * @author iudoka
 * 
 */
public class SellersAdapter extends BaseAdapter {
	//
	private static final String TAG = "SellersAdapter";
	private static final int resId = R.layout.seller;
	//private static final NumberFormat priceFormat = NumberFormat.getCurrencyInstance();

	private Context context;
	//private ShopFragment view;
	private List<Seller> sellers = new ArrayList<Seller>();

	public SellersAdapter(ShopFragment view, Context context, List<Seller> sellers) {
		super();
		//this.view = view;
		this.context = context;

		if (sellers != null) {
			this.sellers.addAll(sellers);
		}

		init();
	}

	private void init() {
		//
	}

	public void destroy() {
		//
	}

	public View getView(final int position, View convertView, ViewGroup parent) {

		View v = convertView;

		if (v == null) {
			LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(resId, parent, false);
		} else {
			//
		}

		final Seller item = (Seller) getItem(position);

		if (item != null) {
			TextView nameView = (TextView) v.findViewById(R.id.name);
			
			nameView.setText(item.getId());

			v.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					//
					ShopApplication.goToProductDetails(context, item.getId());
				}
			});
		}

		return v;
	}

	@Override
	public int getCount() {
		return sellers.size();
	}

	@Override
	public Object getItem(int position) {
		Object item = null;
		
		try {
			item = sellers.get(position);
		} catch (Exception ex) {
			Log.e(TAG, "Unable to get item", ex);
		}
		
		return item;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public void clear() {
		sellers.clear();
		super.notifyDataSetChanged();
	}

	/**
	 * Append these items to the end of the list
	 * 
	 * @param items
	 *            - items to append
	 */
	public void append(List<Seller> sellers) {
		if (sellers != null) {
			this.sellers.addAll(sellers);
			super.notifyDataSetChanged();
		}
	}
	
	public void refresh(List<Seller> sellers) {
		//
		this.sellers.clear();
		append(sellers);
	}

	public Context getContext() {
		return context;
	}
}
