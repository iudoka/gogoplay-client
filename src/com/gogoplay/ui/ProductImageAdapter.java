/**
 * 
 */
package com.gogoplay.ui;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.gogoplay.R;
import com.gogoplay.data.Image;
import com.gogoplay.data.Item;
import com.gogoplay.ui.widget.ScaleLayout;
import com.nineoldandroids.view.ViewHelper;

/**
 * @author iudoka
 * 
 */
public class ProductImageAdapter extends FragmentPagerAdapter implements
		ViewPager.OnPageChangeListener {
	//
	static final String TAG = "ProductImageAdapter";

	public final static float BIG_SCALE = 1.0f;
	public final static float SMALL_SCALE = 0.8f;
	public final static float DIFF_SCALE = BIG_SCALE - SMALL_SCALE;

	private Context context;
	private ShopActionBarActivity view;
	private Item item;
	private List<Image> items = new ArrayList<Image>();

	private boolean swipedLeft = false;
	private int lastPage = 0;
	private ScaleLayout cur = null;
	private ScaleLayout next = null;
	private ScaleLayout prev = null;
	private ScaleLayout prevprev = null;
	private ScaleLayout nextnext = null;

	private FragmentManager fm;
	private ViewPager pager;
	private float scale;
	private boolean isBlurred;
	private static float minAlpha = 0.6f;
	private static float maxAlpha = 1f;
	private static float minDegree = 60.0f;
	private int counter = 0;

	public ProductImageAdapter(FragmentManager fm, ShopActionBarActivity view, ViewPager pager,
			Context context) {
		super(fm);

		this.fm = fm;
		this.view = view;
		this.pager = pager;
		this.context = context;

		init();
	}

	public static float getMinDegree() {
		return minDegree;
	}

	public static float getMinAlpha() {
		return minAlpha;
	}

	public static float getMaxAlpha() {
		return maxAlpha;
	}

	private void init() {
		//
	}

	public void destroy() {
		//
		// imageFetcher.quit();
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Fragment getItem(int position) {

		// make the first pager bigger than others
		if (position == pager.getCurrentItem())
			scale = BIG_SCALE;
		else {
			scale = SMALL_SCALE;
			isBlurred = true;
		}

		Log.d("position", String.valueOf(position));
		Fragment curFragment = ProductImageFragment.newInstance(context, view,
				item, null, position, scale, isBlurred);
		cur = getRootView(position);
		next = getRootView(position + 1);
		prev = getRootView(position - 1);

		return curFragment;
	}

	// @Override
	// public Fragment getItem(int position) {
	// //
	// return ProductImageFragment.newInstance(items.get(position), context,
	// view, null);
	// }

	@Override
	public int getItemPosition(Object object) {
		return POSITION_NONE;
	}

	/*
	 * @Override public long getItemId(int position) { return position; }
	 */

	public void clear() {
		Log.i(TAG, "Just cleared Adapter " + this);
		items.clear();
		super.notifyDataSetChanged();
	}
	
	public void setItem(Item item) {
		//
		this.item = item;
		setImages(item.getImages());
	}

	/**
	 * Set the items in the list
	 * 
	 * @param items
	 *            - items to set
	 */
	public void setImages(List<Image> items) {
		//
		if (items != null) {
			this.items = items;
			super.notifyDataSetChanged();
		}
	}

	public void refresh(List<Image> items) {
		//
		this.items.clear();
		setImages(items);
	}

	public Context getContext() {
		return context;
	}

	@Override
	public Parcelable saveState() {
		return null;
	}

	@Override
	public void onPageScrolled(int position, float positionOffset,
			int positionOffsetPixels) {
		if (positionOffset >= 0f && positionOffset <= 1f) {
			positionOffset = positionOffset * positionOffset;
			cur = getRootView(position);
			next = getRootView(position + 1);
			prev = getRootView(position - 1);
			nextnext = getRootView(position + 2);
			
			if (cur != null) {
				ViewHelper.setAlpha(cur, maxAlpha - 0.5f * positionOffset);
				cur.setScaleBoth(BIG_SCALE - DIFF_SCALE * positionOffset);

				ViewHelper.setRotationY(cur, 0);
			}
			
			if (next != null)
				ViewHelper.setAlpha(next, minAlpha + 0.5f * positionOffset);
			
			if (prev != null)
				ViewHelper.setAlpha(prev, minAlpha + 0.5f * positionOffset);

			if (nextnext != null) {
				ViewHelper.setAlpha(nextnext, minAlpha);
				ViewHelper.setRotationY(nextnext, -minDegree);
			}

			if (next != null) {
				next.setScaleBoth(SMALL_SCALE + DIFF_SCALE * positionOffset);
				ViewHelper.setRotationY(next, -minDegree);
			}
			
			if (prev != null) {
				ViewHelper.setRotationY(prev, minDegree);
			}

			/*
			 * To animate it properly we must understand swipe direction this
			 * code adjusts the rotation according to direction.
			 */
			if (swipedLeft) {
				if (next != null)
					ViewHelper.setRotationY(next, -minDegree + minDegree
							* positionOffset);
				if (cur != null)
					ViewHelper
							.setRotationY(cur, 0 + minDegree * positionOffset);
			} else {
				if (next != null)
					ViewHelper.setRotationY(next, -minDegree + minDegree
							* positionOffset);
				if (cur != null) {
					ViewHelper
							.setRotationY(cur, 0 + minDegree * positionOffset);
				}
			}
		}
		if (positionOffset >= 1f) {
			ViewHelper.setAlpha(cur, maxAlpha);
		}
	}

	@Override
	public void onPageSelected(int position) {

		/*
		 * to get finger swipe direction
		 */
		if (lastPage <= position) {
			swipedLeft = true;
		} else if (lastPage > position) {
			swipedLeft = false;
		}
		lastPage = position;
	}

	@Override
	public void onPageScrollStateChanged(int state) {
	}

	private ScaleLayout getRootView(int position) {
		ScaleLayout ly;
		try {
			ly = (ScaleLayout) fm
					.findFragmentByTag(this.getFragmentTag(position)).getView()
					.findViewById(R.id.root);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return null;
		}
		if (ly != null)
			return ly;
		return null;
	}

	private String getFragmentTag(int position) {
		return "android:switcher:" + pager.getId() + ":" + position;
	}
}
