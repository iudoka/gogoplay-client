/**
 * 
 */
package com.gogoplay.ui;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewConfiguration;

import com.gogoplay.BuyProduct;
import com.gogoplay.EditProduct;
import com.gogoplay.Home;
import com.gogoplay.LoginScreen;
import com.gogoplay.Messages;
import com.gogoplay.OrderHistory;
import com.gogoplay.ProductDetail;
import com.gogoplay.ProfileScreen;
import com.gogoplay.R;
import com.gogoplay.data.Item;
import com.gogoplay.data.User;
import com.gogoplay.pref.ShopPreferences;
//import android.widget.Toast;
//import android.view.ViewGroup;
//import android.widget.AdapterView;

/**
 * @author iudoka
 */
public class ShopApplication extends Application {
	//
	private static final String TAG = "Shop Application";
	private static final Class<? extends Activity> s_login = LoginScreen.class;
	private static final Class<? extends Activity> s_profile = ProfileScreen.class;
	private static final Class<? extends Activity> s_home = Home.class;
	private static final Class<? extends Activity> s_productDetails = ProductDetail.class;
	private static final Class<? extends Activity> s_editProduct = EditProduct.class;
	private static final Class<? extends Activity> s_buyProduct = BuyProduct.class;
	private static final Class<? extends Activity> s_settings = ShopPreferences.class;
	private static final Class<? extends Activity> s_orderHistory = OrderHistory.class;
	private static final Class<? extends Activity> s_messages = Messages.class;

	private static DisplayMetrics screenDimensions;
	private static boolean goBackHome = false;
	
	public static final String CONFIRM_MSG = "confirmed";

	/**
	 * 
	 */
	public ShopApplication() {
		super();
	}
	
	@Override
	public void onCreate() {
		// 
		super.onCreate();
		
		try {
	        ViewConfiguration config = ViewConfiguration.get(this);
	        Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
	        if(menuKeyField != null) {
	            menuKeyField.setAccessible(true);
	            menuKeyField.setBoolean(config, false);
	        }
	    } catch (Exception ex) {
	        // Ignore
	    }
	}
	
	public static void setGoBackHome(boolean goHome) {
		goBackHome = goHome;
	}
	
	public static boolean getGoBackHome() {
		return goBackHome;
	}
	
	public static void goToSignUp(Context context) {
		goTo(context, s_login);
	}
	
	public static void goToProfile(Context context, User user) {
		goTo(context, s_profile, user);
	}
	
	public static void goToOrderHistory(Context context, User user) {
		goTo(context, s_orderHistory, user);
	}
	
	public static void editProduct(Context context, Item item) {
		//
		if (item == null) {
			return;
		}
		
		goTo(context, s_editProduct, item);
	}
	
	public static void buyProduct(Context context, Item item) {
		//
		if (item == null) {
			return;
		}
		
		goTo(context, s_buyProduct, item);
	}
	
	public static void tradeProduct(Context context, Item item) {
		//
		if (item == null) {
			return;
		}
		
		ArrayList<Object> params = new ArrayList<Object>();
		params.add(item);
		
		goTo(context, s_buyProduct, params);
	}

	public static void goHome(Context context) {
		goTo(context, s_home);
	}
	
	public static void goToProductDetails(Context context, String productId) {
		goTo(context, s_productDetails, productId);
	}
	
	public static void goToMessages(Context context, Item item) {
		//
		goTo(context, s_messages, item);
	}
	
	public static void goToMessages(Context context, String itemId, String userId) {
		//
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("itemId", itemId);
		params.put("userId", userId);
		
		goTo(context, s_messages, params);
	}
	
	public static void goToSettings(Context context) {
		goTo(context, s_settings);
	}
	
	public static void goTo(Context context, Class<? extends Activity> class1) {
		//
		goTo(context, class1, null);
	}

	public static void goTo(Context context, Class<? extends Activity> class1, Serializable params) {
		//
		try {
			String name = "com.gogoplay.intent.action." + class1.getSimpleName();

			Log.i(TAG, "Starting Intent " + name);

			Intent myIntent = new Intent(context, class1);
			//myIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			
			if (params != null) {
				myIntent.putExtra("params", params);
			}
			
			if (context instanceof ShopActionBarActivity) {
				((ShopActionBarActivity)context).startActivityForResult(myIntent, 0);
			} else {
				//
				context.startActivity(myIntent);
			}
			
		} catch (ActivityNotFoundException ex) {
			ex.printStackTrace();
		}
	}
	
	public static void unbindDrawables(View view) {
		//
		/*if (view == null) return;
		
		if (view.getBackground() != null) {
			view.getBackground().setCallback(null);
		}

		if (view instanceof ViewGroup && !(view instanceof AdapterView)) {
			for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
				//
				unbindDrawables(((ViewGroup) view).getChildAt(i));
			}
			try {
				((ViewGroup) view).removeAllViews();
			} catch (UnsupportedOperationException e) {
				Log.e(TAG, "Unable to remove views", e);
			}
		}

	    System.gc();*/
	}
	
	public static void showMessage(Context context, String message) {
		showMessage(context, context.getString(R.string.app_name), message);
	}
	
	public static void showMessage(Context context, String title, String message) {
		//
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		 
		builder.setTitle(title);
		builder.setMessage(message);
		builder.setPositiveButton(context.getString(R.string.ok), null);
		builder.show();
		
		/*Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
		toast.show();*/
	}
	
	public static void showConfirmation(Context context, String title, String message, String posBtn, String negBtn, final Handler handler) {
		//
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(title).
		        setMessage(message).
		        setPositiveButton(posBtn, new OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// 
						Message msg = new Message();
						msg.getData().putBoolean(CONFIRM_MSG, true);
						
						handler.sendMessage(msg);
					}
				}).
		        setNegativeButton(negBtn, new OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// 
						Message msg = new Message();
						msg.getData().putBoolean(CONFIRM_MSG, false);
						
						handler.sendMessage(msg);
					}
				}).show();
	}

	public static void setScreenSize(DisplayMetrics screenDimensions) {
		// 
		ShopApplication.screenDimensions = screenDimensions;
	}
	
	public static DisplayMetrics getScreenSize() {
		return screenDimensions;
	}
	
	public static String getDeviceId(Context context) {
		//
		TelephonyManager tManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		String uid = tManager.getDeviceId();
		
		return uid;
	}
}
