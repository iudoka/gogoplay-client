package com.gogoplay;

import java.util.ArrayList;
import java.util.Collections;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.gogoplay.data.Notification;
import com.gogoplay.data.Notification.Action;
import com.gogoplay.data.NotificationContentProvider;
import com.gogoplay.notification.PushNotificationHelper;
import com.gogoplay.ui.NotificationAdapter;
import com.gogoplay.ui.ShopActionBarActivity;
import com.gogoplay.ui.ShopApplication;
import com.gogoplay.ui.ShopFragment;
import com.gogoplay.ui.widget.ProgressDisplay;
import com.gogoplay.ui.widget.ShopEditText;
import com.gogoplay.util.Utils;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

public class Notifications extends ShopFragment implements LoaderCallbacks<Cursor>, OnRefreshListener<ListView> {
	//
	private View root;
	private ProgressDisplay progress;
	private ViewGroup emptyArea;
	private PullToRefreshListView notificationList;
	private NotificationAdapter adapter;
	private NotificationsHandler handler;
	private boolean waitingForService;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i(getTag(), "Just created Notifications Fragment " + this);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		//
		return inflater.inflate(R.layout.notifications, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (savedInstanceState != null)
			return;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		createUI();
		setupListeners();
	}
	
	@Override
    public void onResume() {
        super.onResume();
		
		if (isDataServiceConnected()) {
			//
			attachDataService();
		} else {
			waitingForService = true;
		}
    }
	
	@Override
	public void update() {
		// 
		attachDataService();
	}
	
	@Override
	public void onViewStateRestored(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewStateRestored(savedInstanceState);
	}
	
	private void attachDataService() {
		//
        //adapter.setDataService(getDataService());
        
        //if (adapter.getCount() == 0) {
        	//populate();
        //}
		//loadNotifications();
		getLoaderManager().restartLoader(0, null, this);
	}
	
	public void clearNotifications() {
		
		ShopApplication.showConfirmation(getActivity(), getString(R.string.clear_notifications), getString(R.string.clear_confirm), getString(R.string.yesCaps), getString(R.string.noCaps), handler);
	}
	
	@SuppressLint("HandlerLeak")
	public void sendNotification() {
		//
		ShopEditText entry = (ShopEditText) getView().findViewById(R.id.enter_notification);
		final String entryTxt = entry.getText().toString();
		
		final PushNotificationHelper helper = new PushNotificationHelper(getActivity(), (ShopActionBarActivity)getActivity());
		helper.register(new Handler() {
			@Override
			public void handleMessage(Message msg) {
				// 
				String regId = msg.getData().getString("registrationId");
				
				if (!Utils.isEmpty(regId)) {
					//
					if (isDataServiceConnected()) {
						getDataService().sendPushNotification(getActivity(), regId, entryTxt, new Handler() {
							//
							@Override
							public void handleMessage(Message msg) {
								String result = msg.getData().getString("result");
								Toast toast = Toast.makeText(getActivity(), result, Toast.LENGTH_LONG);
								toast.show();
							}
						});
						
						helper.sendRegistrationIdToBackend(regId, new Handler() {});
					}
				}
			}
		}, true);
	}

    @Override
    public void onStop() {
        super.onStop();
    }
    
    @Override
    public void onPause() {
    	super.onPause();
    }

	@Override
	public void onDestroy() {
		//
		super.onDestroy();
		
		ShopApplication.unbindDrawables(root);
	}

	private void createUI() {
		//
		root = getView().findViewById(R.id.root);
		emptyArea = (ViewGroup) getView().findViewById(R.id.emptyArea);
		getView().findViewById(R.id.send_notification).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 
				sendNotification();
			}
		});
		
		progress = (ProgressDisplay) getView().findViewById(R.id.progress);

		adapter = new NotificationAdapter(this, getActivity(), null);
		notificationList = (PullToRefreshListView) getView().findViewById(R.id.grid);
		notificationList.setEmptyView(emptyArea);
		notificationList.setAdapter(adapter);
		notificationList.setOnRefreshListener(this);
		loadNotifications();
		
		// create handlers on UI main thread
		handler = new NotificationsHandler();
	}

	private void setupListeners() {
		//
	}

	public void refresh() {
		//
		populate();
	}

	public void populate() {
		//
		
		if (isDataServiceConnected()) {
			Log.i(getTag(), "Just populated Notifications Fragment " + this);
			showMessage("Loading...");
			
			update();
			getDataService().getNotifications(getActivity(), handler);
		}
	}
	
	private void showMessage(String message) {
		//
		progress.setVisibility(View.VISIBLE);
		progress.setText(message);
	}
	
	private void hideMessage() {
		progress.setVisibility(View.GONE);
	}
	
	/**
	 * Safest place to update UI items
	 */
	@SuppressLint("HandlerLeak")
	private class NotificationsHandler extends Handler {
		//
		@SuppressWarnings("unchecked")
		@Override
		public void handleMessage(Message msg) {
			//
			if (msg.getData().getBoolean(ShopApplication.CONFIRM_MSG)) {
				// clear all
				clearAll();
				adapter.swapCursor(null);
			} else {
			
				notificationList.onRefreshComplete();
				ArrayList<Notification> notifications = (ArrayList<Notification>) msg.getData().get("notifications");
				
				if (!Utils.isEmpty(notifications)) {
					// sort descending by date
					Collections.sort(notifications);
					
					for (int i = notifications.size() - 1; i >= 0; i--) {
						//
						Notification notif = notifications.get(i);
						notif.setUniqueId(getUser().getId());
						notif.setTo(getUser());
						notif.store(getActivity());
						
						if (notif.getAction() == Action.buy ||
							notif.getAction() == Action.trade ||
							notif.getAction() == Action.message) {
							// save the offer to buy your item or message regarding your item
							(new com.gogoplay.data.Message(notif)).store(getActivity());
						}
					}
				}
			}
			
			
			
			hideMessage();
		}
	}
	
	private void clearAll() {
		//
		//dataHelper.emptyTable();
		ContentResolver cr = getActivity().getContentResolver();
		Uri uri = NotificationContentProvider.CONTENT_URI.buildUpon().appendPath("uuid").appendPath(getUser().getId()).build();
    	int rowsDeleted = cr.delete(uri, null, null);
    	
    	if (rowsDeleted > 0) {
    		Log.i(getTag(), "Cleared all " + rowsDeleted + " notifications");
    	}
	}

	@Override
	public void dataServiceConnected(boolean connected) {
		//
		if (connected && waitingForService) {
			waitingForService = false;
			attachDataService();
		}
	}
	
	@Override
	public String getTitle() {
		return "Notifications";//getString(R.string.notification_list_title);
	}
	
	@Override
	public boolean canShow() {
		// 
		if (isDataServiceConnected()) {
			return getDataService().checkAuthentication(getActivity());
		} else {
			return false;
		}
	}

	@Override
	public void onRefresh(PullToRefreshBase<ListView> refreshView) {
		// 
		refresh();
	}
	
	private void loadNotifications() {
		//
	    getLoaderManager().initLoader(0, null, this);
	}

	@Override
	public android.support.v4.content.Loader<Cursor> onCreateLoader(int id,
			Bundle args) {
		// 
		String[] projection = NotificationContentProvider.getProjection();
		String userId = "-1";
		
		if (getUser() != null) {
			userId = getUser().getId();
		}
		
		Uri uri = NotificationContentProvider.CONTENT_URI.buildUpon().appendPath("uuid").appendPath(userId).build();
			
		CursorLoader cursorLoader = new CursorLoader(getActivity(),
	        uri, projection, null, null, null);
		
	    return cursorLoader;
	}

	@Override
	public void onLoadFinished(android.support.v4.content.Loader<Cursor> loader,
			Cursor data) {
		//
		adapter.swapCursor(data);
		//refresh();
	}

	@Override
	public void onLoaderReset(android.support.v4.content.Loader<Cursor> loader) {
		// 
		adapter.swapCursor(null);
	}
}
