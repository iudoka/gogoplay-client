package com.gogoplay;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.gogoplay.data.Item;
import com.gogoplay.ui.ShopActionBarActivity;
import com.gogoplay.util.Utils;

public class EditProduct extends ShopActionBarActivity {
	
	public static final String ITEM_KEY = "params";
	
	private AddProduct addProductView;
	private Handler uiHandler;

	private boolean loadItem;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// 
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_product);
		
		createUI(savedInstanceState);
	}
	
	@SuppressLint("HandlerLeak")
	private void createUI(Bundle savedInstanceState) {
		// 
		uiHandler = new Handler() {
			//
			@Override
			public void handleMessage(Message msg) {
				// 
				Item item = (Item) msg.getData().get("item");
				addProductView.editItem(item);
			}
		};
		
		restoreFragments(savedInstanceState);
	}

	private void restoreFragments(Bundle savedInstanceState) {
		// 
		FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        
        if (savedInstanceState != null) {
        	// reload fragment state
        	addProductView = (AddProduct) manager.getFragment(savedInstanceState, AddProduct.class.getName());
        }
        
        if (addProductView == null) {
        	addProductView = new AddProduct();
        	transaction.add(R.id.root, addProductView, Account.class.getName());
        }
        
        transaction.commit();
	}
	
	@Override
	protected void onResume() {
		// 
		super.onResume();
		
		if (isDataServiceConnected()) {
			loadItem();
		} else {
			//
			loadItem = true;
		}
	}
	
	private void loadItem() {
		//
		Intent i = getIntent();
		
		if (i != null && i.getExtras() != null) {
			Item item = (Item)i.getExtras().get(ITEM_KEY);
			setViewTitle(getString(R.string.edit) + " - " + Utils.emptyIfNull(item.getTitle()), null);
			addProductView.editItem(item);
		}

		loadItem = false;
	}

	@Override
	public String getViewFooter() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void dataServiceConnected(boolean connected) {
		// 
		if (connected && loadItem) {
			//
			loadItem();
		}
	}

	@Override
	public String getViewTitle() {
		// 
		return "Edit";
	}

}
