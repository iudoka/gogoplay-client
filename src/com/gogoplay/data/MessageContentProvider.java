package com.gogoplay.data;

import java.util.Arrays;
import java.util.HashSet;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

public class MessageContentProvider extends ContentProvider {
	//
	private static final String TAG = MessageContentProvider.class.getSimpleName();
	
	private MessageHelper database;

	// used for the UriMacher
	private static final int MESSAGES = 11;
	private static final int MESSAGE_ID = 22;
	private static final int MESSAGE_ITEM_ID = 33;
	private static final int MESSAGE_UUID = 44;

	public static final String AUTHORITY = "com.gogoplay.messagecontentprovider";

	public static final String BASE_PATH = "messages";
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
			+ "/" + BASE_PATH);

	public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
			+ "/messages";
	public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
			+ "/message";

	private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);

	static {
		sURIMatcher.addURI(AUTHORITY, BASE_PATH, MESSAGES);
		sURIMatcher.addURI(AUTHORITY, BASE_PATH + "/#", MESSAGE_ID);
		sURIMatcher.addURI(AUTHORITY, BASE_PATH + "/item/*", MESSAGE_ITEM_ID);
		sURIMatcher.addURI(AUTHORITY, BASE_PATH + "/uuid/*", MESSAGE_UUID);
	}

	@Override
	public boolean onCreate() {
		database = new MessageHelper(getContext());
		return true;
	}
	
	/*public Cursor queryByItemId(Context context, String itemId, String userId) {
		// Uisng SQLiteQueryBuilder instead of query() method
		MessageHelper helper = new MessageHelper(context);
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
		
		// Set the table
		queryBuilder.setTables(MessageHelper.MESSAGE_TABLE);
		
		queryBuilder.appendWhere(MessageHelper.MESSAGE_ITEM_ID_COLUMN + "='" + itemId + "' AND " + 
								 MessageHelper.MESSAGE_UUID_COLUMN + "='" + userId + "'");
		
		Cursor cursor = null;
		
		try {
			SQLiteDatabase db = helper.getWritableDatabase();
			cursor = queryBuilder.query(db, getProjection(), null, null, null, null, MessageHelper.MESSAGE_ID_COLUMN + " ASC");
			// make sure that potential listeners are getting notified
			//cursor.setNotificationUri(getContext().getContentResolver(), uri);
		} catch(Exception ex) {
			Log.e(TAG, "Unable to query database", ex);
		} finally {
			if (helper != null) {
				//helper.close();
				//helper = null;
			}
		}

		return cursor;
	}
	
	public Cursor queryByUniqueId(Context context, String uniqueId) {
		// 
		database = new MessageHelper(context);
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
		
		// Set the table
		queryBuilder.setTables(MessageHelper.MESSAGE_TABLE);
		
		queryBuilder.appendWhere(MessageHelper.MESSAGE_UUID_COLUMN + "='" + uniqueId + "'");
		
		Cursor cursor = null;
		
		try {
			SQLiteDatabase db = database.getWritableDatabase();
			cursor = queryBuilder.query(db, getProjection(), null, null, null, null, MessageHelper.MESSAGE_ID_COLUMN + " ASC");
			// make sure that potential listeners are getting notified
			//cursor.setNotificationUri(getContext().getContentResolver(), uri);
		} catch(Exception ex) {
			Log.e(TAG, "Unable to query database", ex);
		} finally {
			if (database != null) {
				//database.close();
			}
		}

		return cursor;
	}*/

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {

		// Uisng SQLiteQueryBuilder instead of query() method
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

		// check if the caller has requested a column which does not exists
		checkColumns(projection);

		// Set the table
		queryBuilder.setTables(MessageHelper.MESSAGE_TABLE);

		int uriType = sURIMatcher.match(uri);
		switch (uriType) {
		case MESSAGES:
			break;
		case MESSAGE_ID:
			// adding the ID to the original query
			queryBuilder.appendWhere(MessageHelper.MESSAGE_ID_COLUMN + "="
					+ uri.getLastPathSegment());
			break;
		case MESSAGE_ITEM_ID:
			// find all offers for the given item
			String path = uri.getLastPathSegment();
			String[] clauses = path.split("-");
			
			String itemId = clauses[0];
			String userId = clauses[1];
			
			queryBuilder.appendWhere(MessageHelper.MESSAGE_ITEM_ID_COLUMN + "='" + itemId + "' AND " + 
					 MessageHelper.MESSAGE_UUID_COLUMN + "='" + userId + "'");
			
			sortOrder = MessageHelper.MESSAGE_ID_COLUMN + " ASC";
			
			break;
		case MESSAGE_UUID:
			// find all offers for the given item
			queryBuilder.appendWhere(MessageHelper.MESSAGE_UUID_COLUMN + "='" + uri.getLastPathSegment() + "'");
			
			sortOrder = MessageHelper.MESSAGE_ID_COLUMN + " ASC";
			
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}

		Cursor cursor = null;
		
		try {
			SQLiteDatabase db = database.getWritableDatabase();
			cursor = queryBuilder.query(db, projection, selection,
					selectionArgs, null, null, sortOrder);
			// make sure that potential listeners are getting notified
			cursor.setNotificationUri(getContext().getContentResolver(), uri);
		} catch(Exception ex) {
			Log.e(TAG, "Unable to query database", ex);
		}

		return cursor;
	}

	@Override
	public String getType(Uri uri) {
		//
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		int uriType = sURIMatcher.match(uri);
		SQLiteDatabase sqlDB = database.getWritableDatabase();

		long id = 0;
		switch (uriType) {
		case MESSAGES:
			id = sqlDB.insert(MessageHelper.MESSAGE_TABLE, null, values);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return Uri.parse(BASE_PATH + "/" + id);
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		int uriType = sURIMatcher.match(uri);
		SQLiteDatabase sqlDB = database.getWritableDatabase();
		int rowsDeleted = 0;
		switch (uriType) {
		case MESSAGES:
			rowsDeleted = sqlDB.delete(MessageHelper.MESSAGE_TABLE,
					selection, selectionArgs);
			break;
		case MESSAGE_ID:
			String id = uri.getLastPathSegment();
			if (TextUtils.isEmpty(selection)) {
				rowsDeleted = sqlDB.delete(
						MessageHelper.MESSAGE_TABLE,
						MessageHelper.MESSAGE_ID_COLUMN + "=" + id, null);
			} else {
				rowsDeleted = sqlDB.delete(
						MessageHelper.MESSAGE_TABLE,
						MessageHelper.MESSAGE_ID_COLUMN + "=" + id + " and "
								+ selection, selectionArgs);
			}
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsDeleted;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		int uriType = sURIMatcher.match(uri);
		SQLiteDatabase sqlDB = database.getWritableDatabase();
		int rowsUpdated = 0;
		switch (uriType) {
		case MESSAGES:
			rowsUpdated = sqlDB.update(MessageHelper.MESSAGE_TABLE,
					values, selection, selectionArgs);
			break;
		case MESSAGE_ID:
			String id = uri.getLastPathSegment();
			if (TextUtils.isEmpty(selection)) {
				rowsUpdated = sqlDB.update(
						MessageHelper.MESSAGE_TABLE, values,
						MessageHelper.MESSAGE_ID_COLUMN + "=" + id, null);
			} else {
				rowsUpdated = sqlDB.update(
						MessageHelper.MESSAGE_DATABASE_NAME, values,
						MessageHelper.MESSAGE_ID_COLUMN + "=" + id + " and "
								+ selection, selectionArgs);
			}
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsUpdated;
	}
	
	public static String[] getProjection() {
		//
		return new String[] { MessageHelper.MESSAGE_ID_COLUMN,
				MessageHelper.MESSAGE_UUID_COLUMN,
				MessageHelper.MESSAGE_ITEM_ID_COLUMN,
				MessageHelper.MESSAGE_OFFER_COLUMN,
				MessageHelper.MESSAGE_FROM_ID_COLUMN,
				MessageHelper.MESSAGE_FROM_COLUMN,
				MessageHelper.MESSAGE_TO_COLUMN,
				MessageHelper.MESSAGE_ACTION_COLUMN,
				MessageHelper.MESSAGE_NOTIF_COLUMN,
				MessageHelper.MESSAGE_MSG_COLUMN,
				MessageHelper.MESSAGE_DATE_COLUMN};
	}
	
	private void checkColumns(String[] projection) {
		//
		if (projection != null) {
			HashSet<String> requestedColumns = new HashSet<String>(
					Arrays.asList(projection));
			HashSet<String> availableColumns = new HashSet<String>(
					Arrays.asList(getProjection()));
			// check if all columns which are requested are available
			if (!availableColumns.containsAll(requestedColumns)) {
				throw new IllegalArgumentException("Unknown columns in projection");
			}
		}
	}
}
