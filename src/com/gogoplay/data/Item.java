/**
 * 
 */
package com.gogoplay.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import com.gogoplay.util.Utils;

/**
 * @author iudoka
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Item implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2382557709476529909L;
	
	public static enum Status {
		active,
		inactive,
		sold
	}
	
	public static enum Gender {
		M,
		F,
		A
	}
	
	//
	private String id;
	private String title;
	private String email;
	private String condition;
	private String category;
	private Status status;
	private String size;
	private String detail;
	private String distance;
	private String color;
	private double price;
	private int quantity;
	private int likeCount;
	private int commentCount;
	private String address;
	private double[] location;
	@JsonIgnore
	private Date dateCreated;
	private User seller;
	private List<Image> images;
	private String brand;
	private String age;
	private Gender gender;
	private List<String> tags;
	private List<String> shareOn;
	private Set<Comment> comments;
	
	@JsonIgnore
	private Message offer;
	
	@JsonIgnore
	private boolean liked;

	@JsonIgnore
	private boolean watched;
	
	@JsonIgnore
	private int messageCount;
	
	public Item() {
		//
		super();
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the status
	 */
	public Status getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Status status) {
		this.status = status;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date creationDate) {
		this.dateCreated = creationDate;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	public double[] getLocation() {
		return location;
	}

	public void setLocation(double[] location) {
		this.location = location;
	}

	@JsonIgnore
	public List<Image> getImages() {
		if (images == null) {
			images = new ArrayList<Image>();
		}
		
		return images;
	}

	@JsonProperty
	public void setImages(List<Image> images) {
		this.images = images;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getDescription() {
		return detail;
	}
	
	public void setDescription(String description) {
		this.detail = description;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@JsonIgnore
	public int getLikeCount() {
		return likeCount;
	}

	public void setLikeCount(int likeCount) {
		this.likeCount = likeCount;
	}

	/**
	 * @return the commentCount
	 */
	@JsonIgnore
	public int getCommentCount() {
		return commentCount;
	}

	/**
	 * @param commentCount the commentCount to set
	 */
	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the brand
	 */
	public String getBrand() {
		return brand;
	}

	/**
	 * @param brand the brand to set
	 */
	public void setBrand(String brand) {
		this.brand = brand;
	}

	/**
	 * @return the age
	 */
	public String getAge() {
		return age;
	}

	/**
	 * @param age the age to set
	 */
	public void setAge(String age) {
		this.age = age;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public List<String> getTags() {
		if (tags == null) {
			tags = new ArrayList<String>();
		}
		
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}
	
	public void addTag(String tag) {
		if (!Utils.isEmpty(tag)) {
			//
			getTags().add(tag);
		}
	}

	public List<String> getShareOn() {
		//
		if (shareOn == null) {
			shareOn = new ArrayList<String>();
		}
		
		return shareOn;
	}

	public void setShareOn(List<String> shareOn) {
		this.shareOn = shareOn;
	}
	
	public void addShareOn(String shareOn) {
		if (!Utils.isEmpty(shareOn)) {
			getShareOn().add(shareOn);
		}
	}

	@JsonIgnore
	public Set<Comment> getComments() {
		if (comments == null) {
			comments = new TreeSet<Comment>();
		}
		
		return comments;
	}

	@JsonProperty
	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}
	
	public void addComment(Comment comment) {
		//
		getComments().add(comment);
	}
	
	public void addImage(String imageUri) {
		addImage(new Image("", "", imageUri));
	}

	public void addImage(Image image) {
		//
		if (image != null) {
			getImages().add(image);
		}
	}

	public boolean isLiked() {
		return liked;
	}

	public void setLiked(boolean liked) {
		this.liked = liked;
	}

	/**
	 * @return the owner
	 */
	public User getSeller() {
		return seller;
	}

	/**
	 * @param owner the owner to set
	 */
	public void setSeller(User owner) {
		this.seller = owner;
	}
	
	public boolean isWatched() {
		return watched;
	}

	public void setWatched(boolean watched) {
		// 
		this.watched = watched;
	}

	/**
	 * @return the buyOffer
	 */
	public Message getOffer() {
		return offer;
	}

	/**
	 * @param buyOffer the buyOffer to set
	 */
	public void setOffer(Message offer) {
		this.offer = offer;
	}

	/**
	 * @return the messageCount
	 */
	public int getMessageCount() {
		return messageCount;
	}

	/**
	 * @param messageCount the messageCount to set
	 */
	public void setMessageCount(int messageCount) {
		this.messageCount = messageCount;
	}

	public boolean merge(Item other) {
		// 
		if (other == null) return false;
		
		setId(other.getId());
		setTitle(other.getTitle());
		setEmail(other.getEmail());
		setCondition(other.getCondition());
		setCategory(other.getCategory());
		setStatus(other.getStatus());
		setSize(other.getSize());
		setDetail(other.getDetail());
		setColor(other.getColor());
		setPrice(other.getPrice());
		setQuantity(other.getQuantity());
		setLikeCount(other.getLikeCount());
		setCommentCount(other.getCommentCount());
		setAddress(other.getAddress());
		setLocation(other.getLocation());
		setDateCreated(other.getDateCreated());
		setSeller(other.getSeller());
		setImages(other.getImages());
		setTags(other.getTags());
		setBrand(other.getBrand());
		setAge(other.getAge());
		setShareOn(other.getShareOn());
		setComments(other.getComments());
		setLiked(other.isLiked());
		setWatched(other.isWatched());
		setOffer(other.getOffer());
		
		return true;
	}
}
