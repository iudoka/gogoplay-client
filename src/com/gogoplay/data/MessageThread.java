package com.gogoplay.data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.gogoplay.util.Utils;

public class MessageThread implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -278632807627844478L;
	
	private String id; // unique thread id
	private String itemId; // associated item id
	private String recipientId; // user id of recipient
	private List<Message> messages;
	
	public MessageThread(String id, String itemId, String recipientId, List<Message> messages) {
		//
		this.id = id;
		this.itemId = itemId;
		this.recipientId = recipientId;
		this.messages = messages;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * @return the itemId
	 */
	public String getItemId() {
		return itemId;
	}
	
	/**
	 * @param itemId the itemId to set
	 */
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getRecipientId() {
		return recipientId;
	}

	public void setRecipientId(String recipient) {
		this.recipientId = recipient;
	}

	public List<Message> getMessages() {
		return messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

	public void addMessage(Message msg) {
		// 
		messages.add(msg);
	}
	
	public User getFrom(User currentUser) {
		//
		User d = null;
		
		if (!Utils.isEmpty(messages)) {
			//
			for (Message msg : messages) {
				//
				if (Utils.equals(currentUser.getId(), msg.getFrom().getId())) {
					d = msg.getTo();
				} else {
					d = msg.getFrom();
				}
			}
		}
		
		return d;
	}
	
	public Date getDate() {
		//
		Date d = null;
		
		if (!Utils.isEmpty(messages)) {
			//
			for (Message msg : messages) {
				//
				if (d == null) {
					d = msg.getDateReceived();
				} else if (msg.getDateReceived().after(d)){
					//
					d = msg.getDateReceived();
				}
			}
		}
		
		return d;
	}
	
	public String toString() {
		//
		return "Item: " + itemId + " Recipient: " + recipientId + " Message Size: " + messages.size(); 
	}
}
