package com.gogoplay.data;

import java.util.ArrayList;
import java.util.List;

public class ReportReason {
	//
	private String description;
	private List<String> details;

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the details
	 */
	public List<String> getDetails() {
		
		if (details == null) {
			details = new ArrayList<String>();
		}
		
		return details;
	}

	/**
	 * @param details
	 *            the details to set
	 */
	public void setDetails(List<String> details) {
		this.details = details;
	}
	
	public void addDetail(String reason) {
		getDetails().add(reason);
	}
}
