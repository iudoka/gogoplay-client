package com.gogoplay.data;

import java.util.Arrays;
import java.util.HashSet;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

public class NotificationContentProvider extends ContentProvider {
	//
	private static final String TAG = "NotificationContentProvider";
	
	private NotificationHelper database;

	// used for the UriMacher
	private static final int NOTIFS = 11;
	private static final int NOTIF_ID = 22;
	private static final int NOTIF_BY_USER = 33;

	private static final String AUTHORITY = "com.gogoplay.notifcontentprovider";

	private static final String BASE_PATH = "notifs";
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
			+ "/" + BASE_PATH);

	public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
			+ "/notifs";
	public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
			+ "/notif";

	private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);

	static {
		sURIMatcher.addURI(AUTHORITY, BASE_PATH, NOTIFS);
		sURIMatcher.addURI(AUTHORITY, BASE_PATH + "/#", NOTIF_ID);
		sURIMatcher.addURI(AUTHORITY, BASE_PATH + "/uuid/*", NOTIF_BY_USER);
	}

	@Override
	public boolean onCreate() {
		database = new NotificationHelper(getContext());
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		// hard coding sort order - latest notification on top
		sortOrder = NotificationHelper.NOTIF_ID_COLUMN + " DESC";

		// Uisng SQLiteQueryBuilder instead of query() method
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

		// check if the caller has requested a column which does not exists
		checkColumns(projection);

		// Set the table
		queryBuilder.setTables(NotificationHelper.NOTIF_TABLE);

		int uriType = sURIMatcher.match(uri);
		switch (uriType) {
		case NOTIFS:
			break;
		case NOTIF_ID:
			// adding the ID to the original query
			queryBuilder.appendWhere(NotificationHelper.NOTIF_ID_COLUMN + "="
					+ uri.getLastPathSegment());
			break;
		case NOTIF_BY_USER:
			// query by unique id
			queryBuilder.appendWhere(NotificationHelper.NOTIF_UUID_COLUMN + "='" + uri.getLastPathSegment() + "'");
			
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}

		Cursor cursor = null;
		
		try {
			SQLiteDatabase db = database.getWritableDatabase();
			cursor = queryBuilder.query(db, projection, selection,
					selectionArgs, null, null, sortOrder);
			// make sure that potential listeners are getting notified
			cursor.setNotificationUri(getContext().getContentResolver(), uri);
		} catch(Exception ex) {
			Log.e(TAG, "Unable to query database", ex);
		}

		return cursor;
	}

	@Override
	public String getType(Uri uri) {
		//
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		int uriType = sURIMatcher.match(uri);
		SQLiteDatabase sqlDB = database.getWritableDatabase();

		long id = 0;
		switch (uriType) {
		case NOTIFS:
			id = sqlDB.insert(NotificationHelper.NOTIF_TABLE, null, values);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return Uri.parse(BASE_PATH + "/" + id);
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		int uriType = sURIMatcher.match(uri);
		SQLiteDatabase sqlDB = database.getWritableDatabase();
		int rowsDeleted = 0;
		switch (uriType) {
		case NOTIFS:
			rowsDeleted = sqlDB.delete(NotificationHelper.NOTIF_TABLE,
					selection, selectionArgs);
			break;
		case NOTIF_ID:
			String id = uri.getLastPathSegment();
			if (TextUtils.isEmpty(selection)) {
				rowsDeleted = sqlDB.delete(
						NotificationHelper.NOTIF_TABLE,
						NotificationHelper.NOTIF_ID_COLUMN + "=" + id, null);
			} else {
				rowsDeleted = sqlDB.delete(
						NotificationHelper.NOTIF_TABLE,
						NotificationHelper.NOTIF_ID_COLUMN + "=" + id + " and "
								+ selection, selectionArgs);
			}
			break;
		case NOTIF_BY_USER:
			// query by unique id
			rowsDeleted = sqlDB.delete(NotificationHelper.NOTIF_TABLE, 
						 			   NotificationHelper.NOTIF_UUID_COLUMN + "='" + uri.getLastPathSegment() + "'", 
						 			   selectionArgs);
			
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsDeleted;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		/*int uriType = sURIMatcher.match(uri);
		SQLiteDatabase sqlDB = database.getWritableDatabase();
		int rowsUpdated = 0;
		switch (uriType) {
		case NOTIFS:
			rowsUpdated = sqlDB.update(NotificationHelper.NOTIF_TABLE,
					values, selection, selectionArgs);
			break;
		case NOTIF_ID:
			String id = uri.getLastPathSegment();
			if (TextUtils.isEmpty(selection)) {
				rowsUpdated = sqlDB.update(
						NotificationHelper.NOTIF_TABLE, values,
						NotificationHelper.NOTIF_ID_COLUMN + "=" + id, null);
			} else {
				rowsUpdated = sqlDB.update(
						NotificationHelper.NOTIF_DATABASE_NAME, values,
						NotificationHelper.NOTIF_ID_COLUMN + "=" + id + " and "
								+ selection, selectionArgs);
			}
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsUpdated;*/
		return 0;
	}
	
	public static String[] getProjection() {
		return new String[] { NotificationHelper.NOTIF_ID_COLUMN,
			NotificationHelper.NOTIF_UUID_COLUMN,
			NotificationHelper.NOTIF_ITEM_ID_COLUMN,
			NotificationHelper.NOTIF_OFFER_ITEM_ID_COLUMN,
			NotificationHelper.NOTIF_FROM_ID_COLUMN,
			NotificationHelper.NOTIF_FROM_COLUMN,
			NotificationHelper.NOTIF_TO_COLUMN,
			NotificationHelper.NOTIF_ACTION_COLUMN,
			NotificationHelper.NOTIF_NOTIF_COLUMN,
			NotificationHelper.NOTIF_MSG_COLUMN,
			NotificationHelper.NOTIF_DATE_COLUMN };
	}
	
	private void checkColumns(String[] projection) {
		if (projection != null) {
			HashSet<String> requestedColumns = new HashSet<String>(
					Arrays.asList(projection));
			HashSet<String> availableColumns = new HashSet<String>(
					Arrays.asList(getProjection()));
			// check if all columns which are requested are available
			if (!availableColumns.containsAll(requestedColumns)) {
				throw new IllegalArgumentException("Unknown columns in projection");
			}
		}
	}
}
