package com.gogoplay.data;

import java.io.Serializable;

import android.graphics.Bitmap;

public class Image implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3663273836991731444L;
	
	private String _id;
	private String origFileName;
	private String uri;
	private Bitmap bitmap;
	
	public Image(String id, String origFileName, String uri) {
		this(id, origFileName, uri, null);
	}
	
	public Image(String id, String origFileName, String uri, Bitmap bitmap) {
		this._id = id;
		this.origFileName = origFileName;
		this.uri = uri;
		this.bitmap = bitmap;
	}

	public String getId() {
		return _id;
	}

	public void setId(String id) {
		this._id = id;
	}

	public String getOrigFileName() {
		return origFileName;
	}

	public void setOrigFileName(String origFileName) {
		this.origFileName = origFileName;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public Bitmap getBitmap() {
		return bitmap;
	}

	public void setBitmap(Bitmap bitmap) {
		this.bitmap = bitmap;
	}
}
