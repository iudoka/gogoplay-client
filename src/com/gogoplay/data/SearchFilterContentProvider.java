package com.gogoplay.data;

import java.util.Arrays;
import java.util.HashSet;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

public class SearchFilterContentProvider extends ContentProvider {
	//
	private static final String TAG = "SearchFilterContentProvider";
	
	private FilterHelper database;

	// used for the UriMacher
	private static final int FILTERS = 11;
	private static final int FILTER_ID = 22;

	private static final String AUTHORITY = "com.gogoplay.filtercontentprovider";

	private static final String BASE_PATH = "filters";
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
			+ "/" + BASE_PATH);

	public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
			+ "/filters";
	public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
			+ "/filter";

	private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);

	static {
		sURIMatcher.addURI(AUTHORITY, BASE_PATH, FILTERS);
		sURIMatcher.addURI(AUTHORITY, BASE_PATH + "/#", FILTER_ID);
	}

	@Override
	public boolean onCreate() {
		database = new FilterHelper(getContext());
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {

		// hard coding sort order
		sortOrder = FilterHelper.FILTER_NAME_COLUMN + " ASC";
		
		// Uisng SQLiteQueryBuilder instead of query() method
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

		// check if the caller has requested a column which does not exists
		checkColumns(projection);

		// Set the table
		queryBuilder.setTables(FilterHelper.FILTER_TABLE);

		int uriType = sURIMatcher.match(uri);
		switch (uriType) {
		case FILTERS:
			break;
		case FILTER_ID:
			// adding the ID to the original query
			queryBuilder.appendWhere(FilterHelper.FILTER_ID_COLUMN + "="
					+ uri.getLastPathSegment());
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}

		Cursor cursor = null;
		
		try {
			SQLiteDatabase db = database.getWritableDatabase();
			cursor = queryBuilder.query(db, projection, selection,
					selectionArgs, null, null, sortOrder);
			// make sure that potential listeners are getting notified
			cursor.setNotificationUri(getContext().getContentResolver(), uri);
		} catch(Exception ex) {
			Log.e(TAG, "Unable to query database", ex);
		}

		return cursor;
	}

	@Override
	public String getType(Uri uri) {
		//
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		int uriType = sURIMatcher.match(uri);
		SQLiteDatabase sqlDB = database.getWritableDatabase();

		long id = 0;
		switch (uriType) {
		case FILTERS:
			id = sqlDB.insert(FilterHelper.FILTER_TABLE, null, values);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return Uri.parse(BASE_PATH + "/" + id);
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		int uriType = sURIMatcher.match(uri);
		SQLiteDatabase sqlDB = database.getWritableDatabase();
		int rowsDeleted = 0;
		switch (uriType) {
		case FILTERS:
			rowsDeleted = sqlDB.delete(FilterHelper.FILTER_TABLE,
					selection, selectionArgs);
			break;
		case FILTER_ID:
			String id = uri.getLastPathSegment();
			if (TextUtils.isEmpty(selection)) {
				rowsDeleted = sqlDB.delete(
						FilterHelper.FILTER_TABLE,
						FilterHelper.FILTER_ID_COLUMN + "=" + id, null);
			} else {
				rowsDeleted = sqlDB.delete(
						FilterHelper.FILTER_TABLE,
						FilterHelper.FILTER_ID_COLUMN + "=" + id + " and "
								+ selection, selectionArgs);
			}
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsDeleted;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		int uriType = sURIMatcher.match(uri);
		SQLiteDatabase sqlDB = database.getWritableDatabase();
		int rowsUpdated = 0;
		switch (uriType) {
		case FILTERS:
			rowsUpdated = sqlDB.update(FilterHelper.FILTER_TABLE,
					values, selection, selectionArgs);
			break;
		case FILTER_ID:
			String id = uri.getLastPathSegment();
			if (TextUtils.isEmpty(selection)) {
				rowsUpdated = sqlDB.update(
						FilterHelper.FILTER_TABLE, values,
						FilterHelper.FILTER_ID_COLUMN + "=" + id, null);
			} else {
				rowsUpdated = sqlDB.update(
						FilterHelper.FILTER_DATABASE_NAME, values,
						FilterHelper.FILTER_ID_COLUMN + "=" + id + " and "
								+ selection, selectionArgs);
			}
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsUpdated;
	}

	private void checkColumns(String[] projection) {
		String[] available = { FilterHelper.FILTER_ID_COLUMN,
				FilterHelper.FILTER_NAME_COLUMN,
				FilterHelper.FILTER_AGE_COLUMN,
				FilterHelper.FILTER_GENDER_COLUMN,
				FilterHelper.FILTER_BRAND_COLUMN,
				FilterHelper.FILTER_LOCATION_COLUMN,
				FilterHelper.FILTER_PRICE_MIN_COLUMN,
				FilterHelper.FILTER_PRICE_MAX_COLUMN,
				FilterHelper.FILTER_CATEGORY_COLUMN,
				FilterHelper.FILTER_CONDITION_COLUMN,
				FilterHelper.FILTER_SELECTED_COLUMN};
		if (projection != null) {
			HashSet<String> requestedColumns = new HashSet<String>(
					Arrays.asList(projection));
			HashSet<String> availableColumns = new HashSet<String>(
					Arrays.asList(available));
			// check if all columns which are requested are available
			if (!availableColumns.containsAll(requestedColumns)) {
				throw new IllegalArgumentException("Unknown columns in projection");
			}
		}
	}
}
