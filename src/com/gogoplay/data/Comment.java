package com.gogoplay.data;

import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable, Comparable<Comment> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8518128806008239717L;
	
	//
	private String id;
	private Date date;
	private User user;
	private String text;
	private int likes;
	private String replyToId; // id of original comment if this is a response
	
	public Comment() {
		super();
	}
	
	public Comment(String id, Date date, User user, String text, int likes, String replyToId) {
		//
		super();
		
		this.id = id;
		this.date = date;
		this.user = user;
		this.text = text;
		this.likes = likes;
		this.replyToId = replyToId;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getLikes() {
		return likes;
	}
	public void setLikes(int likes) {
		this.likes = likes;
	}
	public String getReplyToId() {
		return replyToId;
	}
	public void setReplyToId(String replyToId) {
		this.replyToId = replyToId;
	}

	@Override
	public int compareTo(Comment another) {
		// 
		if (another != null && (another instanceof Comment)) {
			//
			return ((Comment) another).getDate().compareTo(getDate());
		}
		
		return -1;
	}
}
