package com.gogoplay.data;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.Session;
import com.flurry.android.Constants;
import com.flurry.android.FlurryAgent;
import com.gogoplay.R;
import com.gogoplay.data.Notification.Action;
import com.gogoplay.data.User.PushToken;
import com.gogoplay.data.User.UserType;
import com.gogoplay.data.server.ImageCache.ImageCacheParams;
import com.gogoplay.data.server.ImageFetcher;
import com.gogoplay.data.server.ServerUtils;
import com.gogoplay.data.server.ShopClient;
import com.gogoplay.data.server.ShopClient.ServiceResponse;
import com.gogoplay.pref.NotificationPreference;
import com.gogoplay.pref.Sorter;
import com.gogoplay.ui.ShopApplication;
import com.gogoplay.util.FileUtil;
import com.gogoplay.util.FlurryUtils;
import com.gogoplay.util.ImageUtil;
import com.gogoplay.util.Utils;

/**
 * Used to fetch data from the server
 * 
 * @author iudoka
 */
public class DataService extends Service {
	//
	public static int imageWidth;

	private static final String TAG = "DataService";
	private static final int s_FEATURE_SIZE = 6;
	private static final int s_INITIAL_LOAD_SIZE = 12; // ServerUtils.s_NUM_ITEMS_PER_PAGE;
	private static final boolean s_LOAD_IMAGES_IMMEDIATE = false;
	public static final String s_THUMBNAIL_CACHE_DIR = "thumbs";
	public static final String s_IMAGE_CACHE_DIR = "images";
	private static final String s_USER_FILE_NAME = "appUser.json";
	private static final String s_SORTER_FILE_NAME = "appSort.json";
	//private static final String s_SHARED_PREF_NAME = "BABY_APP_PREFS";

	private static final ObjectMapper objectMapper = new ObjectMapper();

	static {
		objectMapper.setSerializationInclusion(Inclusion.NON_NULL);
	}

	// current user
	private User user;
	private Sorter sorter;
	private ShopClient shopClient;
	private int imageThumbSize;
	private int imageFullSize;
	private ImageFetcher thumbnailFetcher;
	private ImageFetcher imageFetcher;
	private DataBinder binder = new DataBinder();
	private HashSet<UserStateListener> userListeners = new HashSet<UserStateListener>();

	// Network connectivity check
	private boolean waitingOnConnection = false;

	// private ConnectionMonitor connectionMonitor = new ConnectionMonitor();

	public class DataBinder extends Binder {
		public DataService getService() {
			// Return this instance of DataService so clients can call public
			// methods
			return DataService.this;
		}
	}

	@Override
	public void onCreate() {
		super.onCreate();

		ServerUtils.init(this);

		// Create data access gateways
		shopClient = new ShopClient();

		DisplayMetrics screenSize = ShopApplication.getScreenSize();

		imageThumbSize = ImageUtil.IMAGE_SIZE;//(int) (screenSize.density * screenSize.widthPixels);// (int)(screenSize.density
																				// *
																				// screenSize.widthPixels);//getResources().getDimensionPixelSize(R.dimen.image_thumbnail_size);
		imageFullSize = screenSize.widthPixels; // getResources().getDimensionPixelSize(R.dimen.image_full_size);

		ImageCacheParams cacheParams = new ImageCacheParams(this, s_THUMBNAIL_CACHE_DIR);
		cacheParams.setMemCacheSizePercent(0.05f); // Set memory cache to 15% of app memory

		// The ImageFetcher takes care of loading images into our ImageView
		// children asynchronously
		thumbnailFetcher = new ImageFetcher(this, imageThumbSize);
		thumbnailFetcher.setLoadingImage(R.drawable.loading);
		thumbnailFetcher.addImageCache(cacheParams);
		// thumbnailFetcher.setImageFadeIn(true);

		cacheParams = new ImageCacheParams(this, s_IMAGE_CACHE_DIR);
		cacheParams.setMemCacheSizePercent(0.01f); // Set memory cache to 0.01% of app memory

		//
		imageFetcher = new ImageFetcher(this, imageFullSize);
		imageFetcher.setLoadingImage(R.drawable.loading);
		imageFetcher.addImageCache(cacheParams);
		// imageFetcher.setImageFadeIn(true);

		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);

		boolean clearCache = prefs.getBoolean("pref_clear_cache", false);

		if (clearCache) {
			clearCache();
		}

		Log.i(TAG, "Finished creating data service");
	}

	public void clearCache() {
		//
		// new Thread() {
		// public void run() {

		// thumbnailFetcher.setPauseWork(false);
		// thumbnailFetcher.setExitTasksEarly(true);
		thumbnailFetcher.clearCache();

		// imageFetcher.setPauseWork(false);
		// imageFetcher.setExitTasksEarly(true);
		imageFetcher.clearCache();
		Log.i(TAG, "Done clearing!");
		// System.exit(0);
		// }
		// }.start();
	}

	public void pauseThumbnailFetcher(boolean pause) {
		//
		thumbnailFetcher.setPauseWork(pause);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// The service is starting, due to a call to startService()
		return Service.START_STICKY;
	}

	@Override
	public boolean onUnbind(Intent intent) {
		stopCurrentTasks();
		Log.i(TAG, "Service unbound " + intent);
		return true;
	}

	@Override
	public void onRebind(Intent intent) {
		// A client is binding to the service with bindService(),
		// after onUnbind() has already been called
		resumeTasks();
		Log.i(TAG, "Service rebound!");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		thumbnailFetcher.closeCache();
		imageFetcher.closeCache();
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
		System.gc();
	}

	@Override
	public IBinder onBind(Intent intent) {
		return binder;
	}

	public void stopCurrentTasks() {
		//
		Log.i(TAG, "Stopping current tasks");
		thumbnailFetcher.setExitTasksEarly(true);
		thumbnailFetcher.flushCache();
	}

	public void resumeTasks() {
		Log.i(TAG, "Resuming processing tasks");
		thumbnailFetcher.setExitTasksEarly(false);

		new Thread() {
			public void run() {
				//
				System.gc();
			}
		}.start();
	}

	/**
	 * @return the sorter
	 */
	public Sorter getSorter(Context context) {
		//
		if (sorter == null) {
			// try to load from store
			byte[] bytes = FileUtil.loadBytes(context, s_SORTER_FILE_NAME);

			if (bytes != null) {
				try {
					sorter = Utils.unmarshall(bytes, Sorter.CREATOR);
				} catch (Exception ex) {
					sorter = null;
				}
			}
		}

		return sorter;
	}
	
	public static boolean isSorterSet(Context context) {
		//
		boolean set = false;
		
		byte[] bytes = FileUtil.loadBytes(context, s_SORTER_FILE_NAME);

		if (bytes != null) {
			try {
				//objectMapper.readValue(sorterJson, Sorter.class);
				Utils.unmarshall(bytes, Sorter.CREATOR);
				set = true;
			} catch (Exception ex) {
				set = false;
			}
		}
		
		return set;
	}
	
	public static Sorter saveSorter(Context context, Sorter sorter) {
		//
		byte[] sorterStr = null;
		try {
			if (sorter != null) {
				sorterStr = Utils.marshall(sorter);
			}

			FileUtil.store(context, s_SORTER_FILE_NAME, sorterStr);
		} catch (Exception ex) {
			sorter = null;
		}
		
		return sorter;
	}

	/**
	 * @param sorter
	 *            the sorter to set
	 */
	public void setSorter(Context context, Sorter sorter) {
		//
		this.sorter = saveSorter(context, sorter);
	}

	public void loadThumbnail(final Context context, final ImageView imageView,
			final String uri, Handler respHandler) {
		//
		respHandler.post(new Runnable() {
			public void run() {
				if (checkConnectivity(context)) {
					thumbnailFetcher.loadImage(uri, imageView, null);
				}
			}
		});
	}

	public void loadFullImage(final Context context, final ImageView imageView,
			final String uri, Handler respHandler) {
		//
		respHandler.post(new Runnable() {
			public void run() {
				if (checkConnectivity(context)) {
					imageFetcher.loadImage(uri, imageView);
				}
			}
		});
	}
	
	public void loadImage(final Context context, final Image imageInfo, final Handler uiCallback) {
		//
		if (checkConnectivity(context)) {
			new LoadImageTask(context, imageInfo.getUri(), uiCallback).execute();
		} else {
			Message m = new Message();
			Bundle b = new Bundle();
			b.putParcelable("image", null);
			b.putSerializable("message",
					"Unable to load image - try connecting to the internet");

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}

	private void loadItemImage(Item item) {
		//
		ArrayList<Item> items = new ArrayList<Item>();
		items.add(item);
		loadItemImages(items);
	}

	private void loadItemImages(ArrayList<Item> items) {
		//
		if (s_LOAD_IMAGES_IMMEDIATE) {
			//
			for (Item item : items) {
				String image = null;

				if (!Utils.isEmpty(item.getImages())) {
					image = item.getImages().get(0).getUri();

					loadThumbnail(null, null, image, new Handler());
				}
			}
		}
	}

	public void likeItem(final Context context, final Item item,
			Handler uiCallback) {
		//
		if (checkConnectivity(context)) {
			if (checkAuthentication(context)) {
				new LikeItemTask(context, item, user, uiCallback).execute();
			}
		} else {
			//
			Message m = new Message();
			Bundle b = new Bundle();
			b.putSerializable("success", false);

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}
	
	public void watchItem(final Context context, final Item item, Handler uiCallback) {
		//
		if (checkConnectivity(context)) {
			if (checkAuthentication(context)) {
				new WatchItemTask(context, item, user, uiCallback).execute();
			}
		} else {
			//
			Message m = new Message();
			Bundle b = new Bundle();
			b.putSerializable("success", false);

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}
	
	public void reportItem(final Context context, final Item item, Handler uiCallback) {
		//
		if (checkConnectivity(context)) {
			if (checkAuthentication(context)) {
				new ReportItemTask(context, item, user, uiCallback).execute();
			}
		} else {
			//
			Message m = new Message();
			Bundle b = new Bundle();
			b.putSerializable("success", false);

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}
	
	public void reportUser(final Context context, final User userToReport, Handler uiCallback) {
		//
		if (checkConnectivity(context)) {
			if (checkAuthentication(context)) {
				new ReportUserTask(context, userToReport, user, uiCallback).execute();
			}
		} else {
			//
			Message m = new Message();
			Bundle b = new Bundle();
			b.putSerializable("success", false);

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}
	
	public void followUser(final Context context, final User userToFollow, Handler uiCallback) {
		//
		if (checkConnectivity(context)) {
			if (checkAuthentication(context)) {
				new FollowUserTask(context, userToFollow, user, uiCallback).execute();
			}
		} else {
			//
			Message m = new Message();
			Bundle b = new Bundle();
			b.putSerializable("success", false);

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}

	public void addComment(final Context context, final Comment comment,
			final Item item, Handler uiCallback) {
		//
		if (checkConnectivity(context)) {
			if (checkAuthentication(context)) {
				new AddCommentTask(context, comment, item, user,
						uiCallback).execute();
			}
		} else {
			//
			Message m = new Message();
			Bundle b = new Bundle();
			b.putSerializable("success", false);

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}
	
	public void removeComment(final Context context, final Comment comment,
			final Item item, Handler uiCallback) {
		//
		if (checkConnectivity(context)) {
			if (checkAuthentication(context)) {
				new RemoveCommentTask(context, comment.getId(), item.getId(), user,
						uiCallback).execute();
			}
		} else {
			//
			Message m = new Message();
			Bundle b = new Bundle();
			b.putSerializable("success", false);

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}

	/*
	 * Products Methods
	 */
	
	public void updateNotificationPreferences(final Context context, final NotificationPreference prefs, Handler uiCallback) {
		//
		if (checkConnectivity(context)) {
			if (checkAuthentication(context)) {
				Log.i(TAG, "Updating notification preferences");
				new UpdatePreferenceTask(context, prefs, user, uiCallback).execute();
			}
		} else {
			Message m = new Message();
			Bundle b = new Bundle();
			b.putBoolean("updated", false);
			b.putSerializable("message",
					"Unable to update preferences - try connecting to the internet");

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}
	
	public void buyItem(final Context context, final Item item, double amount, Handler uiCallback) {
		//
		if (checkConnectivity(context)) {
			if (checkAuthentication(context)) {
				new BuyItemTask(context, item, amount, user, uiCallback).execute();
			}
		} else {
			//
			Message m = new Message();
			Bundle b = new Bundle();
			b.putSerializable("success", false);

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}
	
	public void tradeItem(final Context context, final Item item, Item other, Handler uiCallback) {
		//
		if (checkConnectivity(context)) {
			if (checkAuthentication(context)) {
				new TradeItemTask(context, item, other, user, uiCallback).execute();
			}
		} else {
			//
			Message m = new Message();
			Bundle b = new Bundle();
			b.putSerializable("success", false);

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}
	
	public void sendItemMessage(final Context context, final Item item, String message, Handler uiCallback) {
		//
		if (checkConnectivity(context)) {
			if (checkAuthentication(context)) {
				new SendItemMessageTask(context, item, message, user, uiCallback).execute();
			}
		} else {
			//
			Message m = new Message();
			Bundle b = new Bundle();
			b.putSerializable("success", false);

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}

	public void uploadItem(final Context context, Item uploadItem,
			Handler uiCallback) {
		//
		if (checkConnectivity(context)) {
			if (checkAuthentication(context)) {
				Log.i(TAG, "Uploading new item " + uploadItem);
				new UploadItemTask(context, uploadItem, user, uiCallback)
						.execute();
			}
		} else {
			Message m = new Message();
			Bundle b = new Bundle();
			b.putBoolean("upload", false);
			b.putSerializable("message",
					"Unable to upload - try connecting to the internet");

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}
	
	public void editItem(final Context context, Item uploadItem,
			Handler uiCallback) {
		//
		if (checkConnectivity(context)) {
			if (checkAuthentication(context)) {
				Log.i(TAG, "Editing item " + uploadItem);
				new EditItemTask(context, uploadItem, user, uiCallback).execute();
			}
		} else {
			Message m = new Message();
			Bundle b = new Bundle();
			b.putBoolean("upload", false);
			b.putSerializable("message",
					"Unable to upload - try connecting to the internet");

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}
	
	public void getMessagesForItem(final Context context, String itemId, Handler uiCallback) {
		//
		if (checkAuthentication(context)) {
			Log.i(TAG, "Fetching messages/offers for item " + itemId);
			new FetchItemMessagesTask(context, itemId, user, uiCallback).execute();
		}
	}
	
	public int getMessageCountForItem(final Context context, String itemId) {
		//
		ContentResolver cr = context.getContentResolver();
		Uri uri = MessageContentProvider.CONTENT_URI.buildUpon().appendPath("item").appendPath(itemId + "-" + user.getId()).build();
    	Cursor cursor = cr.query(uri, MessageContentProvider.getProjection(), null, null, null);
		
		if (cursor != null) {
			//
			return cursor.getCount();
		}
		
		return 0;
	}
	
	public void deleteItem(final Context context, Item item, Handler uiCallback) {
		//
		if (checkConnectivity(context)) {
			if (checkAuthentication(context)) {
				Log.i(TAG, "Deleting item " + item);
				new DeleteItemTask(context, item, user, uiCallback).execute();
			}
		} else {
			Message m = new Message();
			Bundle b = new Bundle();
			b.putBoolean("upload", false);
			b.putSerializable("message", "Unable to delete - try connecting to the internet");

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}

	public void searchItems(final Context context, String searchString,
			String lastItemId, boolean refresh, Handler uiCallback) {
		//
		if (checkConnectivity(context)) {
			Log.i(TAG, "Searching for items - for refresh " + refresh);
			new SearchItemsTask(context, searchString, lastItemId, refresh,
					getUser(context), uiCallback).execute();
		} else {
			//
			Message m = new Message();
			Bundle b = new Bundle();
			b.putBoolean("refresh", refresh);
			b.putSerializable("items", new ArrayList<Item>());

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}

	public void getWatchedItems(final Context context,
			final String lastItemId, final Handler uiCallback, boolean refresh) {
		//
		if (checkConnectivity(context)) {
			Log.i(TAG, "Loading liked items - for refresh " + refresh);
			new FetchWatchedItemsTask(context, uiCallback, lastItemId,
					getUser(context), refresh).execute();
		} else {
			//
			Message m = new Message();
			Bundle b = new Bundle();
			b.putBoolean("refresh", refresh);
			b.putSerializable("items", new ArrayList<Item>());

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}

	public void getItems(final Context context, final String lastItemId,
			final Handler uiCallback) {
		getItems(context, lastItemId, uiCallback, false);
	}

	public void getItems(final Context context, final String lastItemId,
			final Handler uiCallback, final boolean refresh) {
		//
		if (checkConnectivity(context)) {
			Log.i(TAG, "Loading home items for refresh " + refresh);
			new FetchItemsTask(context, lastItemId, getUser(context),
					uiCallback, refresh).execute();
		} else {
			//
			Message m = new Message();
			Bundle b = new Bundle();
			b.putSerializable("items", new ArrayList<Item>());

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}
	
	public void getUserItems(final Context context, final String lastItemId, final String userId,
			final Handler uiCallback, final boolean refresh) {
		//
		if (checkConnectivity(context)) {
			Log.i(TAG, "Loading user items for refresh " + refresh);
			new FetchUserItemsTask(context, lastItemId, userId, getUser(context),
					uiCallback, refresh).execute();
		} else {
			//
			Message m = new Message();
			Bundle b = new Bundle();
			b.putSerializable("items", new ArrayList<Item>());

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}
	
	public void getMyOffers(final Context context, final Handler uiCallback) {
		//
		if (checkConnectivity(context) &&
			checkAuthentication(context)) {
			Log.i(TAG, "Loading items user has made offers on...");
			new FetchMyOffersTask(context, getUser(context), uiCallback).execute();
		} else {
			//
			Message m = new Message();
			Bundle b = new Bundle();
			b.putBoolean("refresh", true);
			b.putSerializable("items", new ArrayList<Item>());

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}

	public void getItem(final Context context, final String itemId,
			final Item item, Handler uiCallback) {
		//
		if (checkConnectivity(context)) {
			Log.i(TAG, "Loading item " + itemId + " object: " + item);
			new FetchItemTask(context, itemId, item, getUser(context),
					uiCallback).execute();
		} else {
			//
			Message m = new Message();
			Bundle b = new Bundle();
			b.putSerializable("item", null);

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}

	public void getSellers(final Context context, final String lastSellerId,
			final Handler uiCallback, final boolean refresh) {
		//
		if (checkConnectivity(context)) {
			Log.i(TAG, "Loading sellers");
			new FetchSellersTask(context, lastSellerId, uiCallback, refresh)
					.execute();
		} else {
			//
			Message m = new Message();
			Bundle b = new Bundle();
			b.putBoolean("refresh", refresh);
			b.putSerializable("sellers", new ArrayList<Seller>());

			m.setData(b);

			// loadItemImages(items);

			uiCallback.sendMessage(m);
		}
	}

	public void getNotifications(final Context context, final Handler uiCallback) {
		//
		if (checkConnectivity(context)) {
			Log.i(TAG, "Loading notifications");
			new FetchNotificationsTask(context, uiCallback).execute();
		} else {
			//
			Message m = new Message();
			Bundle b = new Bundle();
			b.putSerializable("notifications", new ArrayList<Notification>());

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}
	
	public void sendPushToken(final Context context, final PushToken token, Handler uiCallback) {
		//
		if (checkConnectivity(context)) {
			new SendPushTokenTask(context, token, uiCallback).execute();
		}
	}
	
	private class SendPushTokenTask extends AsyncTask<Void, Void, Boolean> {
		//
		private Handler uiCallback;
		private Context context;
		private PushToken token;

		public SendPushTokenTask(Context context, PushToken token, Handler uiCallback) {
			//
			this.context = context;
			this.token = token;
			this.uiCallback = uiCallback;
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			User userToken = getUser(context);
			
			if (userToken != null) {
				return shopClient.sendPushToken(context, userToken.getToken(), token);
			} else {
				return false;
			}
		}

		@Override
		protected void onPreExecute() {
			System.gc();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
		}

		@Override
		protected void onPostExecute(Boolean result) {
			Message m = new Message();
			Bundle b = new Bundle();
			b.putBoolean("result", result);
			b.putString("registrationId", token.getDeviceToken());

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}

	public void sendPushNotification(final Context context,
			final String registrationId, final String message,
			Handler uiCallback) {
		//
		new SendPushNotificationTask(context, registrationId, message,
				uiCallback).execute();
	}

	private class SendPushNotificationTask extends
			AsyncTask<Void, Void, String> {
		private Handler uiCallback;
		private Context context;
		private String registrationId;
		private String message;

		public SendPushNotificationTask(Context context, String registrationId,
				String message, Handler uiCallback) {
			//
			this.context = context;
			this.registrationId = registrationId;
			this.message = message;
			this.uiCallback = uiCallback;
		}

		@Override
		protected String doInBackground(Void... params) {
			return shopClient.sendPushNotification(context, registrationId, message);
		}

		@Override
		protected void onPreExecute() {
			System.gc();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
		}

		@Override
		protected void onPostExecute(String result) {
			Message m = new Message();
			Bundle b = new Bundle();
			b.putSerializable("result", result);

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}

	private class AddCommentTask extends AsyncTask<Void, Void, Boolean> {
		private Handler uiCallback;
		private Context context;
		private Comment comment;
		private Item item;
		private User user;

		public AddCommentTask(Context context, Comment comment, Item item,
				User user, Handler uiCallback) {
			this.context = context;
			this.uiCallback = uiCallback;
			this.comment = comment;
			this.item = item;
			this.user = user;
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			return shopClient.addComment(context, comment, item, user);
		}

		@Override
		protected void onPreExecute() {
			System.gc();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
		}

		@Override
		protected void onPostExecute(Boolean added) {
			HashMap<String, String> flurryUser = new HashMap<String, String>();
			
			if (user != null) {
				flurryUser.put("Username", user.getNickname());
				flurryUser.put("Email", user.getEmail());
				flurryUser.put("Name", user.getName());
			}
			flurryUser.put("Comment", comment.getText());
			flurryUser.put("Item", item.getId());
			flurryUser.put("Success", added.toString());
			FlurryAgent.logEvent(FlurryUtils.s_ADD_COMMENT, flurryUser);
			
			Message m = new Message();
			Bundle b = new Bundle();
			b.putSerializable("success", added);

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}
	
	private class RemoveCommentTask extends AsyncTask<Void, Void, Boolean> {
		private Handler uiCallback;
		private Context context;
		private String commentId;
		private String itemId;
		private User user;

		public RemoveCommentTask(Context context, String commentId, String itemId,
				User user, Handler uiCallback) {
			this.context = context;
			this.uiCallback = uiCallback;
			this.commentId = commentId;
			this.itemId = itemId;
			this.user = user;
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			return shopClient.removeComment(context, itemId, commentId,
					user.getToken());
		}

		@Override
		protected void onPreExecute() {
			System.gc();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
		}

		@Override
		protected void onPostExecute(Boolean added) {
			HashMap<String, String> flurryUser = new HashMap<String, String>();
			if (user != null) {
				//
				flurryUser.put("Username", user.getNickname());
				flurryUser.put("Email", user.getEmail());
				flurryUser.put("Name", user.getName());
			}
			flurryUser.put("ItemId", itemId);
			flurryUser.put("Success", added.toString());
			FlurryAgent.logEvent(FlurryUtils.s_REMOVE_COMMENT, flurryUser);
			
			Message m = new Message();
			Bundle b = new Bundle();
			b.putSerializable("success", added);

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}

	private class LikeItemTask extends AsyncTask<Void, Void, Boolean> {
		@SuppressWarnings("unused")
		private Context context;
		private Item item;
		private Handler uiCallback;
		private User user;

		public LikeItemTask(Context context, Item item, User user,
				Handler uiCallback) {
			this.context = context;
			this.uiCallback = uiCallback;
			this.item = item;
			this.user = user;
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			//
			Boolean success;

			if (!item.isLiked()) {
				success = shopClient.likeItem(DataService.this, item, user);

				if (success) {
					//
					item.setLiked(true);
					item.setLikeCount(item.getLikeCount() + 1);
				}
			} else {
				success = shopClient.unlikeItem(DataService.this, item.getId(), user.getToken());

				if (success) {
					//
					item.setLiked(false);
					item.setLikeCount(item.getLikeCount() - 1);
				}
			}

			return success;
		}

		@Override
		protected void onPreExecute() {
			System.gc();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
		}

		@Override
		protected void onPostExecute(Boolean liked) {
			HashMap<String, String> flurryUser = new HashMap<String, String>();
			if (user != null) {
				flurryUser.put("Username", user.getNickname());
				flurryUser.put("Email", user.getEmail());
				flurryUser.put("Name", user.getName());
			}
			flurryUser.put("ItemId", item.getId());
			flurryUser.put("Success", liked.toString());
			FlurryAgent.logEvent(item.isLiked() ? FlurryUtils.s_LIKE : FlurryUtils.s_UNLIKE, flurryUser);
			
			Message m = new Message();
			Bundle b = new Bundle();
			b.putSerializable("success", liked);

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}
	
	private class WatchItemTask extends AsyncTask<Void, Void, Boolean> {
		//
		@SuppressWarnings("unused")
		private Context context;
		private Item item;
		private Handler uiCallback;
		private User user;

		public WatchItemTask(Context context, Item item, User user, Handler uiCallback) {
			this.context = context;
			this.uiCallback = uiCallback;
			this.item = item;
			this.user = user;
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			//
			Boolean success;

			if (!item.isWatched()) {
				success = shopClient.watchItem(DataService.this, item, user);

				if (success) {
					//
					item.setWatched(true);
				}
			} else {
				success = shopClient.unwatchItem(DataService.this, item.getId(),
						user.getToken());

				if (success) {
					//
					item.setWatched(false);
				}
			}

			return success;
		}

		@Override
		protected void onPreExecute() {
			System.gc();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
		}

		@Override
		protected void onPostExecute(Boolean watched) {
			//
			HashMap<String, String> flurryUser = new HashMap<String, String>();
			if (user != null) {
				flurryUser.put("Username", user.getNickname());
				flurryUser.put("Email", user.getEmail());
				flurryUser.put("Name", user.getName());
			}
			flurryUser.put("ItemId", item.getId());
			flurryUser.put("Success", watched.toString());
			FlurryAgent.logEvent(item.isWatched() ? FlurryUtils.s_WATCH : FlurryUtils.s_UNWATCH, flurryUser);
			
			Message m = new Message();
			Bundle b = new Bundle();
			b.putSerializable("success", watched);

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}
	
	private class ReportItemTask extends AsyncTask<Void, Void, Boolean> {
		//
		@SuppressWarnings("unused")
		private Context context;
		private Item item;
		private Handler uiCallback;
		private User user;

		public ReportItemTask(Context context, Item item, User user, Handler uiCallback) {
			this.context = context;
			this.uiCallback = uiCallback;
			this.item = item;
			this.user = user;
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			//
			Boolean success = shopClient.reportItem(DataService.this, item.getId(), user.getToken());

			return success;
		}

		@Override
		protected void onPreExecute() {
			System.gc();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
		}

		@Override
		protected void onPostExecute(Boolean reported) {
			//
			HashMap<String, String> flurryUser = new HashMap<String, String>();
			if (user != null) {
				flurryUser.put("Username", user.getNickname());
				flurryUser.put("Email", user.getEmail());
				flurryUser.put("Name", user.getName());
			}
			flurryUser.put("ItemReported", item.getId());
			flurryUser.put("Success", reported.toString());
			FlurryAgent.logEvent(FlurryUtils.s_REPORT_ITEM, flurryUser);
			
			Message m = new Message();
			Bundle b = new Bundle();
			b.putSerializable("success", reported);

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}
	
	private class ReportUserTask extends AsyncTask<Void, Void, Boolean> {
		//
		@SuppressWarnings("unused")
		private Context context;
		private User userToReport;
		private Handler uiCallback;
		private User user;

		public ReportUserTask(Context context, User userToReport, User user, Handler uiCallback) {
			this.context = context;
			this.uiCallback = uiCallback;
			this.userToReport = userToReport;
			this.user = user;
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			//
			Boolean success = shopClient.reportUser(DataService.this, userToReport.getId(), user.getToken());

			return success;
		}

		@Override
		protected void onPreExecute() {
			System.gc();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
		}

		@Override
		protected void onPostExecute(Boolean reported) {
			//
			HashMap<String, String> flurryUser = new HashMap<String, String>();
			if (user != null) {
				//
				flurryUser.put("Username", user.getNickname());
				flurryUser.put("Email", user.getEmail());
				flurryUser.put("Name", user.getName());
			}
			flurryUser.put("UserReported", userToReport.getEmail());
			flurryUser.put("Success", reported.toString());
			FlurryAgent.logEvent(FlurryUtils.s_REPORT_USER, flurryUser);
			
			Message m = new Message();
			Bundle b = new Bundle();
			b.putSerializable("success", reported);

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}

	private class FetchItemTask extends AsyncTask<Void, Void, Item> {
		@SuppressWarnings("unused")
		private Context context;
		private Handler uiCallback;
		private String itemId;
		private Item item;
		private User user;

		public FetchItemTask(Context context, String itemId, Item item,
				User user, Handler uiCallback) {
			this.context = context;
			this.uiCallback = uiCallback;
			this.itemId = itemId;
			this.item = item;
			this.user = user;
		}

		@Override
		protected Item doInBackground(Void... params) {
			//
			String userToken = null;

			if (user != null) {
				userToken = user.getToken();
			}

			return shopClient
					.getItem(DataService.this, itemId, userToken, item);
		}

		@Override
		protected void onPreExecute() {
			System.gc();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
		}

		@Override
		protected void onPostExecute(Item item) {
			Message m = new Message();
			Bundle b = new Bundle();
			b.putSerializable("item", item);

			Log.i(TAG, "Loading item " + item.getId() + " object: " + item);

			m.setData(b);

			loadItemImage(item);

			uiCallback.sendMessage(m);
		}
	}

	private class FetchWatchedItemsTask extends
			AsyncTask<Void, Void, ArrayList<Item>> {
		//
		@SuppressWarnings("unused")
		private Context context;
		private Handler uiCallback;
		private String lastItemId;
		private boolean refresh;
		private User user;

		public FetchWatchedItemsTask(Context context, Handler uiCallback,
				String lastItemId, User user, boolean refresh) {
			this.context = context;
			this.uiCallback = uiCallback;
			this.lastItemId = lastItemId;
			this.refresh = refresh;
			this.user = user;
		}

		@Override
		protected ArrayList<Item> doInBackground(Void... params) {
			//
			String userToken = null;

			if (user != null) {
				userToken = user.getToken();
			}

			return shopClient.getWatchedItems(DataService.this, lastItemId,
					sorter, s_FEATURE_SIZE, userToken);
		}

		@Override
		protected void onPreExecute() {
			System.gc();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
		}

		@Override
		protected void onPostExecute(ArrayList<Item> items) {
			Message m = new Message();
			Bundle b = new Bundle();
			b.putBoolean("refresh", refresh);
			b.putSerializable("items", items);
			b.putString("lastItemId", lastItemId);

			m.setData(b);

			// loadItemImages(items);

			uiCallback.sendMessage(m);
		}
	}

	private class FetchItemsTask extends AsyncTask<Void, Void, ArrayList<Item>> {
		//
		@SuppressWarnings("unused")
		private Context context;
		private Handler uiCallback;
		private String lastItemId;
		private boolean refresh;
		private User user;

		public FetchItemsTask(Context context, String lastItemId, User user,
				Handler uiCallback, boolean refresh) {
			this.context = context;
			this.lastItemId = lastItemId;
			this.uiCallback = uiCallback;
			this.refresh = refresh;
			this.user = user;
		}

		@Override
		protected ArrayList<Item> doInBackground(Void... params) {
			//
			String userToken = null;

			if (user != null) {
				userToken = user.getToken();
			}

			return shopClient.getItems(DataService.this, lastItemId, sorter,
					s_INITIAL_LOAD_SIZE, userToken);
		}

		@Override
		protected void onPreExecute() {
			System.gc();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
		}

		@Override
		protected void onPostExecute(ArrayList<Item> items) {
			Message m = new Message();
			Bundle b = new Bundle();
			b.putString("lastItemId", lastItemId);
			b.putBoolean("refresh", refresh);
			b.putSerializable("items", items);

			m.setData(b);

			// loadItemImages(items);

			uiCallback.sendMessage(m);
		}
	}
	
	private class FetchMyOffersTask extends AsyncTask<Void, Void, ArrayList<Item>> {
		//
		private Context context;
		private Handler uiCallback;
		private User user;
		
		public FetchMyOffersTask(Context context, User user, Handler uiCallback) {
			//
			this.context = context;
			this.user = user;
			this.uiCallback = uiCallback;
		}
		
		@Override
		protected ArrayList<Item> doInBackground(Void... params) {
			//
			ArrayList<Item> items = null;
			
			ContentResolver cr = getContentResolver();
			Uri uri = MessageContentProvider.CONTENT_URI.buildUpon().appendPath("uuid").appendPath(user.getId()).build();
			
	    	Cursor cursor = cr.query(uri, MessageContentProvider.getProjection(), null, null, null);
			
			
			if (cursor != null && cursor.moveToFirst()) {
				//
				HashMap<String, com.gogoplay.data.Message> itemOffers = new HashMap<String, com.gogoplay.data.Message>();
				
				do {
					com.gogoplay.data.Message newMsg = new com.gogoplay.data.Message(cursor);
					
					if (newMsg.getAction() != Action.buy &&
						newMsg.getAction() != Action.trade) {
						//
						continue;
					}
					
					com.gogoplay.data.Message oldMsg = itemOffers.get(newMsg.getItemId());
					
					if (oldMsg != null) {
						// offer exists
						
						if (newMsg.getDateReceived().after(oldMsg.getDateReceived())) {
							itemOffers.put(newMsg.getItemId(), newMsg);
						}
					} else {
						// no previous offers exists
						itemOffers.put(newMsg.getItemId(),  newMsg);
					}
					
				} while (cursor.moveToNext());
				
				cursor.close();
				
				if (!itemOffers.isEmpty()) {
					//
					items = new ArrayList<Item>();
					
					for (String itemId : itemOffers.keySet()) {
						// load each item
						Item item = shopClient.getItem(context, itemId, user.getToken());
						
						if (item != null && item.getStatus() == com.gogoplay.data.Item.Status.active) {
							//
							item.setOffer(itemOffers.get(itemId));
							items.add(item);
						}
					}
				}
			}
			
			return items;
		}

		@Override
		protected void onPreExecute() {
			System.gc();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
		}

		@Override
		protected void onPostExecute(ArrayList<Item> items) {
			Message m = new Message();
			Bundle b = new Bundle();
			b.putBoolean("refresh", true);
			b.putSerializable("items", items);

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}
	
	private class FetchUserItemsTask extends AsyncTask<Void, Void, ArrayList<Item>> {
		//
		@SuppressWarnings("unused")
		private Context context;
		private Handler uiCallback;
		private String lastItemId;
		private boolean refresh;
		private String userId;
		private User user;

		public FetchUserItemsTask(Context context, String lastItemId, String userId, User user,
				Handler uiCallback, boolean refresh) {
			this.context = context;
			this.lastItemId = lastItemId;
			this.uiCallback = uiCallback;
			this.refresh = refresh;
			this.userId = userId;
			this.user = user;
		}

		@Override
		protected ArrayList<Item> doInBackground(Void... params) {
			//
			if (user != null) {
				return shopClient.getUserItems(DataService.this, userId, lastItemId, s_INITIAL_LOAD_SIZE, user.getToken());
			} else {
				return null;
			}
		}

		@Override
		protected void onPreExecute() {
			System.gc();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
		}

		@Override
		protected void onPostExecute(ArrayList<Item> items) {
			Message m = new Message();
			Bundle b = new Bundle();
			b.putString("lastItemId", lastItemId);
			b.putBoolean("refresh", refresh);
			b.putSerializable("items", items);

			m.setData(b);

			// loadItemImages(items);

			uiCallback.sendMessage(m);
		}
	}

	private class SearchItemsTask extends
			AsyncTask<Void, Void, ArrayList<Item>> {
		//
		@SuppressWarnings("unused")
		private Context context;
		private Handler uiCallback;
		private String lastItemId;
		private String searchString;
		private boolean refresh;
		private User user;

		public SearchItemsTask(Context context, String searchString,
				String lastItemId, boolean refresh, User user,
				Handler uiCallback) {
			this.context = context;
			this.searchString = searchString;
			this.lastItemId = lastItemId;
			this.uiCallback = uiCallback;
			this.refresh = refresh;
			this.user = user;
		}

		@Override
		protected ArrayList<Item> doInBackground(Void... params) {
			//
			String userToken = null;

			if (user != null) {
				userToken = user.getToken();
			}

			return shopClient.searchItems(DataService.this, searchString,
					lastItemId, sorter, s_INITIAL_LOAD_SIZE, userToken);
		}

		@Override
		protected void onPreExecute() {
			System.gc();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
		}

		@Override
		protected void onPostExecute(ArrayList<Item> items) {
			//
			HashMap<String, String> flurryUser = new HashMap<String, String>();
			
			if (user != null) {
				//
				flurryUser.put("Username", user.getNickname());
				flurryUser.put("Email", user.getEmail());
				flurryUser.put("Name", user.getName());
			}
			
			flurryUser.put("SearchValue", searchString);
			flurryUser.put("ResultSize", (items == null ? 0 : items.size()) + "");
			FlurryAgent.logEvent(FlurryUtils.s_SEARCH, flurryUser);
			
			Message m = new Message();
			Bundle b = new Bundle();
			b.putBoolean("refresh", refresh);
			b.putSerializable("items", items);
			b.putInt("totalCount", items.size());
			b.putString("lastItemId", lastItemId);

			m.setData(b);

			// loadItemImages(items);

			uiCallback.sendMessage(m);
		}
	}
	
	private class LoadImageTask extends AsyncTask<Void, Void, Bitmap> {
		@SuppressWarnings("unused")
		private Context context;
		private Handler uiCallback;
		private String url;
		
		public LoadImageTask(Context context, String url, Handler uiCallback) {
			this.context = context;
			this.url = url;
			this.uiCallback = uiCallback;
		}
		
		@Override
		protected Bitmap doInBackground(Void... params) {
			// 
			Bitmap img = null;
			
			try {
				URL theUrl = new URL(url);
				img = BitmapFactory.decodeStream(theUrl.openConnection().getInputStream());
			} catch (Exception ex) {}
			
			return img;
		}
		
		@Override
		protected void onPostExecute(Bitmap image) {
			Message m = new Message();
			Bundle b = new Bundle();
			b.putParcelable("image", image);

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}
	
	private class FetchItemMessagesTask extends AsyncTask<Void, Void, ArrayList<MessageThread>> {
		//
		private Context context;
		private String itemId;
		private User user;
		private Handler uiCallback;
		
		public FetchItemMessagesTask(Context context, String itemId, User user, Handler uiCallback) {
			//
			this.context = context;
			this.itemId = itemId;
			this.user = user;
			this.uiCallback = uiCallback;
		}
		
		@Override
		protected ArrayList<MessageThread> doInBackground(Void... params) {
			// 
			ArrayList<MessageThread> messageThreads = null;
			ContentResolver cr = context.getContentResolver();
			
			Uri uri = MessageContentProvider.CONTENT_URI.buildUpon().appendPath("item").appendPath(itemId + "-" + user.getId()).build();
	    	Cursor cursor = cr.query(uri, MessageContentProvider.getProjection(), null, null, null);
			
			if (cursor != null && cursor.moveToFirst()) {
				//
				HashMap<String, List<com.gogoplay.data.Message>> messages = new HashMap<String, List<com.gogoplay.data.Message>>();
				
				do {
					com.gogoplay.data.Message msg = new com.gogoplay.data.Message(cursor);
					String userId = msg.getFrom().getId();
					
					if (userId.equalsIgnoreCase(user.getId()) && msg.getTo() != null) {
						userId = msg.getTo().getId();
					}
					
					List<com.gogoplay.data.Message> msgs = messages.get(userId);
					
					if (msgs == null) {
						msgs = new ArrayList<com.gogoplay.data.Message>();
						messages.put(userId, msgs);
					}
					
					msgs.add(msg);
				} while (cursor.moveToNext());
				
				cursor.close();
				
				if (!messages.isEmpty()) {
					//
					messageThreads = new ArrayList<MessageThread>();
					
					for (String userId : messages.keySet()) {
						//
						messageThreads.add(new MessageThread(UUID.randomUUID().toString(), itemId, userId, messages.get(userId)));
					}
				}
			}
			
			return messageThreads;
		}
		
		@Override
		protected void onPostExecute(ArrayList<MessageThread> messages) {
			Message m = new Message();
			Bundle b = new Bundle();
			b.putSerializable("messages", messages);

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}
	
	private class EditItemTask extends AsyncTask<Void, Void, ServiceResponse> {
		//
		@SuppressWarnings("unused")
		private Context context;
		private Handler uiCallback;
		private Item item;
		private User user;

		public EditItemTask(Context context, Item item, User user,
				Handler uiCallback) {
			this.context = context;
			this.uiCallback = uiCallback;
			this.item = item;
			this.user = user;
		}

		@Override
		protected ServiceResponse doInBackground(Void... params) {
			//
			return shopClient.editItem(DataService.this, item, user.getToken());
		}

		@Override
		protected void onPreExecute() {
			System.gc();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
		}

		@Override
		protected void onPostExecute(ServiceResponse upload) {
			Message m = new Message();
			Bundle b = new Bundle();
			b.putBoolean("upload", upload.success);
			b.putSerializable("message", upload.message);

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}
	
	private class DeleteItemTask extends AsyncTask<Void, Void, Boolean> {
		//
		@SuppressWarnings("unused")
		private Context context;
		private Handler uiCallback;
		private Item item;
		private User user;

		public DeleteItemTask(Context context, Item item, User user,
				Handler uiCallback) {
			this.context = context;
			this.uiCallback = uiCallback;
			this.item = item;
			this.user = user;
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			//
			return shopClient.deleteItem(DataService.this, item.getId(), user.getToken());
		}

		@Override
		protected void onPreExecute() {
			System.gc();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
		}

		@Override
		protected void onPostExecute(Boolean deleted) {
			Message m = new Message();
			Bundle b = new Bundle();
			b.putBoolean("deleted", deleted);
			b.putSerializable("message", deleted ? "Success" : "Unable to delete!");

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}

	private class UploadItemTask extends AsyncTask<Void, Void, ServiceResponse> {
		//
		@SuppressWarnings("unused")
		private Context context;
		private Handler uiCallback;
		private Item item;
		private User user;

		public UploadItemTask(Context context, Item item, User user,
				Handler uiCallback) {
			this.context = context;
			this.uiCallback = uiCallback;
			this.item = item;
			this.user = user;
		}

		@Override
		protected ServiceResponse doInBackground(Void... params) {
			//
			return shopClient.uploadItem(DataService.this, item, user.getToken());
		}

		@Override
		protected void onPreExecute() {
			System.gc();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
		}

		@Override
		protected void onPostExecute(ServiceResponse upload) {
			Message m = new Message();
			Bundle b = new Bundle();
			b.putBoolean("upload", upload.success);
			b.putSerializable("message", upload.message);
			b.putString("id", upload.id);

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}
	
	private class BuyItemTask extends AsyncTask<Void, Void, Boolean> {
		//
		@SuppressWarnings("unused")
		private Context context;
		private Handler uiCallback;
		private Item item;
		private double amount;
		private User user;

		public BuyItemTask(Context context, Item item, double amount, User user,
				Handler uiCallback) {
			this.context = context;
			this.uiCallback = uiCallback;
			this.item = item;
			this.amount = amount;
			this.user = user;
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			//
			return shopClient.buyItem(DataService.this, item, amount, user);
		}

		@Override
		protected void onPreExecute() {
			System.gc();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
		}

		@Override
		protected void onPostExecute(Boolean success) {
			Message m = new Message();
			Bundle b = new Bundle();
			b.putBoolean("success", success);

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}
	
	private class TradeItemTask extends AsyncTask<Void, Void, Boolean> {
		//
		@SuppressWarnings("unused")
		private Context context;
		private Handler uiCallback;
		private Item item;
		private Item other;
		private User user;

		public TradeItemTask(Context context, Item item, Item other, User user,
				Handler uiCallback) {
			this.context = context;
			this.uiCallback = uiCallback;
			this.item = item;
			this.other = other;
			this.user = user;
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			//
			return shopClient.tradeItem(DataService.this, item, other, user);
		}

		@Override
		protected void onPreExecute() {
			System.gc();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
		}

		@Override
		protected void onPostExecute(Boolean success) {
			Message m = new Message();
			Bundle b = new Bundle();
			b.putBoolean("success", success);

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}
	
	private class SendItemMessageTask extends AsyncTask<Void, Void, com.gogoplay.data.Message> {
		//
		private Context context;
		private Handler uiCallback;
		private Item item;
		private String message;
		private User user;

		public SendItemMessageTask(Context context, Item item, String message, User user,
				Handler uiCallback) {
			this.context = context;
			this.uiCallback = uiCallback;
			this.item = item;
			this.message = message;
			this.user = user;
		}

		@Override
		protected com.gogoplay.data.Message doInBackground(Void... params) {
			//
			return shopClient.sendMessage(context, item, message, item.getSeller().getId(), user);
		}

		@Override
		protected void onPreExecute() {
			System.gc();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
		}

		@Override
		protected void onPostExecute(com.gogoplay.data.Message msg) {
			Message m = new Message();
			Bundle b = new Bundle();
			b.putBoolean("success", msg != null);
			b.putSerializable("message", msg);

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}
	
	// Users
	private class FollowUserTask extends AsyncTask<Void, Void, Boolean> {
		@SuppressWarnings("unused")
		private Context context;
		private User userToFollow;
		private Handler uiCallback;
		private User user;

		public FollowUserTask(Context context, User userToFollow, User user, Handler uiCallback) {
			this.context = context;
			this.uiCallback = uiCallback;
			this.userToFollow = userToFollow;
			this.user = user;
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			//
			Boolean success;

			if (!user.isFollowing(userToFollow)) {
				success = shopClient.followUser(DataService.this, userToFollow.getId(), user);

				if (success) {
					//
					user.follow(userToFollow);
					storeUser(user);
				}
			} else {
				success = shopClient.unfollowUser(DataService.this, userToFollow.getId(), user.getToken());

				if (success) {
					//
					user.unfollow(userToFollow);
					storeUser(user);
				}
			}

			return success;
		}

		@Override
		protected void onPreExecute() {
			System.gc();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
		}

		@Override
		protected void onPostExecute(Boolean liked) {
			Message m = new Message();
			Bundle b = new Bundle();
			b.putSerializable("success", liked);

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}

	// Notifications
	private class UpdatePreferenceTask extends AsyncTask<Void, Void, Boolean> {
		//
		@SuppressWarnings("unused")
		private Context context;
		private Handler uiCallback;
		private NotificationPreference pref;
		private User user;

		public UpdatePreferenceTask(Context context, NotificationPreference pref, User user,
				Handler uiCallback) {
			this.context = context;
			this.uiCallback = uiCallback;
			this.pref = pref;
			this.user = user;
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			//
			return shopClient.updateNotificationPreference(DataService.this, pref, user.getToken());
		}

		@Override
		protected void onPreExecute() {
			System.gc();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
		}

		@Override
		protected void onPostExecute(Boolean updated) {
			Message m = new Message();
			Bundle b = new Bundle();
			b.putBoolean("updated", updated);

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}
	
	private class FetchNotificationsTask extends
			AsyncTask<Void, Void, ArrayList<Notification>> {
		//
		@SuppressWarnings("unused")
		private Context context;
		private Handler uiCallback;

		public FetchNotificationsTask(Context context, Handler uiCallback) {
			this.context = context;
			this.uiCallback = uiCallback;
		}

		@Override
		protected ArrayList<Notification> doInBackground(Void... params) {
			//
			/*ArrayList<Notification> notifs = new ArrayList<Notification>();
			
			for (int i = 0; i < 10; i++) {
				//
				Notification notif = new Notification();
				notif.setAction(Notification.Action.like);
				notif.setNotification("iudoka likes Po");
				notif.setId(i);
				notif.setItemId("test");
				notif.setDateReceived(new java.util.Date());
				notifs.add(notif);
			}
			
			return notifs;*/
			
			if (user != null) {
				return shopClient.getNotifications(DataService.this, user.getToken(), true);
			}
			
			return null;
		}

		@Override
		protected void onPreExecute() {
			System.gc();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
		}

		@Override
		protected void onPostExecute(ArrayList<Notification> notifications) {
			Message m = new Message();
			Bundle b = new Bundle();
			b.putSerializable("notifications", notifications);

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}

	private class FetchSellersTask extends
			AsyncTask<Void, Void, ArrayList<Seller>> {
		//
		@SuppressWarnings("unused")
		private Context context;
		private Handler uiCallback;
		private String lastSellerId;
		private boolean refresh;

		public FetchSellersTask(Context context, String lastSellerId,
				Handler uiCallback, boolean refresh) {
			this.context = context;
			this.lastSellerId = lastSellerId;
			this.uiCallback = uiCallback;
			this.refresh = refresh;
		}

		@Override
		protected ArrayList<Seller> doInBackground(Void... params) {
			//
			return shopClient.getSellers(DataService.this, lastSellerId,
					s_INITIAL_LOAD_SIZE);
		}

		@Override
		protected void onPreExecute() {
			System.gc();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
		}

		@Override
		protected void onPostExecute(ArrayList<Seller> sellers) {
			Message m = new Message();
			Bundle b = new Bundle();
			b.putBoolean("refresh", refresh);
			b.putSerializable("sellers", sellers);

			m.setData(b);

			// loadItemImages(items);

			uiCallback.sendMessage(m);
		}
	}

	// -------------------------------
	// Network Connection Listener
	// --------------------------------
	public boolean checkConnectivity(Context context) {
		//
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		boolean isConnected = activeNetwork != null
				&& activeNetwork.isConnectedOrConnecting();

		if (!isConnected) {
			//
			if (!waitingOnConnection) {
				//
				waitingOnConnection = true;
				/*ShopApplication
						.showMessage(
								context,
								context.getString(R.string.no_network_connection_toast));*/

				Toast.makeText(context, context.getString(R.string.no_network_connection_toast), Toast.LENGTH_LONG).show();
				waitForInternetConnectivity(context);
			}
		} else {
			waitingOnConnection = false;
		}

		return isConnected;
	}

	public boolean checkAuthentication(Context context) {
		//
		boolean auth = isAuthenticated(context);

		if (!auth) {
			// redirect to login/signup page
			ShopApplication.goToSignUp(context);
		}

		return auth;
	}

	public boolean isAuthenticated(Context context) {
		boolean auth = false;
		user = getUser(context);

		if (user != null) {
			// User is authorized if they have a valid token
			auth = !Utils.isEmpty(user.getToken());
		}

		return auth;
	}

	private void waitForInternetConnectivity(final Context context) {
		//
		new Thread() {
			public void run() {
				try {
					while (waitingOnConnection) {
						Thread.sleep(1000);
						checkConnectivity(context);
					}
				} catch (InterruptedException ex) {
					//

				}
			}
		}.start();
	}

	/*
	 * private class ConnectionMonitor extends BroadcastReceiver {
	 * 
	 * @Override public void onReceive(Context context, Intent intent) { String
	 * action = intent.getAction(); if
	 * (!action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) return; boolean
	 * noConnectivity = intent.getBooleanExtra(
	 * ConnectivityManager.EXTRA_NO_CONNECTIVITY, false); NetworkInfo
	 * aNetworkInfo = (NetworkInfo) intent
	 * .getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO); if
	 * (!noConnectivity) { if ((aNetworkInfo.getType() ==
	 * ConnectivityManager.TYPE_MOBILE) || (aNetworkInfo.getType() ==
	 * ConnectivityManager.TYPE_WIFI)) { // start your service stuff here } }
	 * else { if ((aNetworkInfo.getType() == ConnectivityManager.TYPE_MOBILE) ||
	 * (aNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI)) { // stop your
	 * service stuff here } } } }
	 */

	public User getUser(Context context) {
		//
		if (user == null) {
			// try to load from store
			String userJson = FileUtil.load(context, s_USER_FILE_NAME);

			if (!Utils.isEmpty(userJson)) {
				//
				try {
					user = objectMapper.readValue(userJson, User.class);
				} catch (Exception ex) {
					user = null;
				}
			}
		}

		if (user != null && (UserType.facebook == user.getType())) {
			// TODO: also periodically validate Facebook token as it can become
			// invalidated
			//
		}

		return user;
	}
	
	public static User getCurrentUser(Context context) {
		//
		User user = null;
		String userJson = FileUtil.load(context, s_USER_FILE_NAME);

		if (!Utils.isEmpty(userJson)) {
			//
			try {
				user = objectMapper.readValue(userJson, User.class);
			} catch (Exception ex) {
				user = null;
			}
		}
		
		return user;
	}

	private void setUser(User user) {
		//
		this.user = user;

		if (user == null) {
			logOut(this, false);
		} else {
			storeUser(user);
		}
	}

	private void storeUser(User user) {
		// store user info
		try {
			user.setPassword(null); // do not store password
			FileUtil.store(this, s_USER_FILE_NAME,
					objectMapper.writeValueAsString(user));
		} catch (Exception ex) {
			logOut(this);
		}
	}

	// Facebook login/sign up should call this
	public void signUp(final Context context, User user, Handler uiCallback) {
		// if returned user doesn't have a token then sign up failed
		if (checkConnectivity(context)) {
			new SignUpTask(context, user, uiCallback).execute();
		} else {
			//
			setUser(null);

			Message m = new Message();
			Bundle b = new Bundle();
			b.putBoolean("success", false);

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}

	public void logIn(final Context context, String username, String password,
			Handler uiCallback) {
		//
		if (checkConnectivity(context)) {
			new LogInTask(context, username, password, uiCallback).execute();
		} else {
			//
			setUser(null);
			// updateLoggedIn(user);

			Message m = new Message();
			Bundle b = new Bundle();
			b.putBoolean("success", false);

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}

	public void logOut(Context context) {
		logOut(context, true);
	}

	public void logOut(Context context, boolean notify) {
		// log out - clear stored user object
		if (user != null) {
			//
			new LogoutTask(context, user).execute();
		}
		
		if (FileUtil.store(context, s_USER_FILE_NAME, "")) {
			user = null;
			updateLoggedOut();
		}
	}
	
	private class LogoutTask extends AsyncTask<Void, Void, Void> {
		//
		private Context context;
		private User user;
		
		public LogoutTask(Context context, User user) {
			this.context = context;
			this.user = user;
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// 
			if (user.getType() == UserType.facebook) {
				// end facebook session
				Session session = new Session.Builder(context).build();
		        if (session != null && !session.isClosed()) {
		            session.closeAndClearTokenInformation();
		        }
			}

			HashMap<String, String> flurryUser = new HashMap<String, String>();
			if (user != null) {
				// flurry analytics
				flurryUser.put("Username", user.getNickname());
				flurryUser.put("Email", user.getEmail());
				flurryUser.put("Name", user.getName());
			}
			FlurryAgent.logEvent(FlurryUtils.s_LOGOUT, flurryUser);
			
//			if (FileUtil.store(context, s_USER_FILE_NAME, "")) {
//				//updateLoggedOut();
//			}
			
			return null;
		}
	}

	private class SignUpTask extends AsyncTask<Void, Void, User> {
		//
		@SuppressWarnings("unused")
		private Context context;
		private Handler uiCallback;
		private User userInfo;

		public SignUpTask(Context context, User userInfo, Handler uiCallback) {
			this.context = context;
			this.userInfo = userInfo;
			this.uiCallback = uiCallback;
		}

		@Override
		protected User doInBackground(Void... params) {
			//
			HashMap<String, String> flurryUser = new HashMap<String, String>();
			flurryUser.put("Username", userInfo.getNickname());
			flurryUser.put("Email", userInfo.getEmail());
			flurryUser.put("Name", userInfo.getName());
			FlurryAgent.logEvent(FlurryUtils.s_LOGIN, flurryUser);
			
			if (!Utils.isEmpty(userInfo.getSex())) {
				FlurryAgent.setGender("male".equalsIgnoreCase(userInfo.getSex()) ? Constants.MALE : Constants.FEMALE);
			}
			
			ServiceResponse response = shopClient.signUpUser(DataService.this,
					userInfo);

			if (response.success && response.responseObject != null) {
				return (User) response.responseObject;
			}

			return null;
		}

		@Override
		protected void onPreExecute() {
			System.gc();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
		}

		@Override
		protected void onPostExecute(User user) {
			//
			setUser(user);

			Message m = new Message();
			Bundle b = new Bundle();
			b.putBoolean("success", user != null);
			b.putString("msg", "Unable to reach server!");

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}

	private class LogInTask extends AsyncTask<Void, Void, User> {
		//
		@SuppressWarnings("unused")
		private Context context;
		private Handler uiCallback;
		private String username;
		private String password;

		public LogInTask(Context context, String username, String password,
				Handler uiCallback) {
			this.context = context;
			this.username = username;
			this.password = password;
			this.uiCallback = uiCallback;
		}

		@Override
		protected User doInBackground(Void... params) {
			//
			HashMap<String, String> flurryUser = new HashMap<String, String>();
			flurryUser.put("Username", username);
			FlurryAgent.logEvent(FlurryUtils.s_LOGIN, flurryUser);
			
			ServiceResponse response = shopClient.loginUser(DataService.this,
					username, password);

			if (response.success && response.responseObject != null
					&& response.responseObject instanceof User) {
				return (User) response.responseObject;
			}

			return null;
		}

		@Override
		protected void onPreExecute() {
			System.gc();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
		}

		@Override
		protected void onPostExecute(User user) {
			//
			setUser(user);
			updateLoggedIn(user);

			Message m = new Message();
			Bundle b = new Bundle();
			b.putBoolean("success", user != null);

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}

	private void updateLoggedIn(User user) {
		for (UserStateListener l : userListeners) {
			l.userLoggedIn(user);
		}
	}

	private void updateLoggedOut() {
		for (UserStateListener l : userListeners) {
			l.userLoggedOut();
		}
	}

	/**
	 * @return the thumbnailFetcher
	 */
	public ImageFetcher getThumbnailFetcher() {
		return thumbnailFetcher;
	}

	public void addUserStateListener(UserStateListener listener) {
		//
		userListeners.add(listener);
	}

	public void removeUserStateListener(UserStateListener listener) {
		userListeners.remove(listener);
	}

	public interface UserStateListener {
		//
		public void userLoggedIn(User user);

		public void userLoggedOut();
	}
}
