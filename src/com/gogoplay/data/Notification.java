package com.gogoplay.data;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class Notification implements Serializable, Comparable<Notification> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6977382917141356846L;
	
	protected static final ObjectMapper objectMapper = new ObjectMapper();
	
	static {
		objectMapper.setSerializationInclusion(Inclusion.NON_NULL);
	}
	
	@SuppressLint("SimpleDateFormat")
	protected static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");//.SSSZ");
	
	static {
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
	}

	public static enum Action {
		like,
		comment,
		buy,
		trade,
		follow,
		watch,
		message
	}
	
	private long id;
	private String uniqueId;
	private String itemId;
	private String offer;
	private User from;
	private User to;
	private Action action;
	private String notification;
	private String message;
	private Date dateReceived;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}
	
	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getOffer() {
		return offer;
	}

	public void setOffer(String offer) {
		this.offer = offer;
	}

	public String getNotification() {
		return notification;
	}

	public void setNotification(String notification) {
		this.notification = notification;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public User getFrom() {
		return from;
	}

	public void setFrom(User seller) {
		this.from = seller;
	}

	/**
	 * @return the to
	 */
	public User getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(User to) {
		this.to = to;
	}

	/**
	 * @return the action
	 */
	public Action getAction() {
		return action;
	}

	/**
	 * @param action the action to set
	 */
	public void setAction(Action action) {
		this.action = action;
	}

	/**
	 * @return the dateReceived
	 */
	public Date getDateReceived() {
		return dateReceived;
	}

	/**
	 * @param dateReceived the dateReceived to set
	 */
	public void setDateReceived(Date dateReceived) {
		if (dateReceived == null) {
			dateReceived = new Date();
		}
		this.dateReceived = dateReceived;
	}
	
	@Override
	public int compareTo(Notification another) {
		// 
		if (another != null && (another instanceof Notification)) {
			//
			return ((Notification) another).getDateReceived().compareTo(getDateReceived());
		}
		
		return -1;
	}
	
	public String toString() {
		return notification;
	}
	
	public void store(Context context) {
		//
		ContentValues values = new ContentValues();
		
		values.put(NotificationHelper.NOTIF_UUID_COLUMN, getUniqueId()); 
		values.put(NotificationHelper.NOTIF_ITEM_ID_COLUMN, getItemId());
		values.put(NotificationHelper.NOTIF_OFFER_ITEM_ID_COLUMN, getOffer());
		
		String userFrom = null;
		
		if (getFrom() != null) {
			//
			try {
				userFrom = objectMapper.writeValueAsString(getFrom());
			} catch (Exception ex) {}
		}
		
		values.put(NotificationHelper.NOTIF_FROM_ID_COLUMN, getFrom().getId());
		values.put(NotificationHelper.NOTIF_FROM_COLUMN, userFrom);
		
		String userTo = null;
		
		if (getTo() != null) {
			//
			try {
				userTo = objectMapper.writeValueAsString(getTo());
			} catch (Exception ex) {}
		}
		
		values.put(NotificationHelper.NOTIF_TO_COLUMN, userTo);
		values.put(NotificationHelper.NOTIF_ACTION_COLUMN, getAction() + "");
		values.put(NotificationHelper.NOTIF_NOTIF_COLUMN, getNotification());
		values.put(NotificationHelper.NOTIF_MSG_COLUMN, getMessage());
		
		String date = null;
		
		if (getDateReceived() != null) {
			date = dateFormat.format(getDateReceived());
		}
		
		values.put(NotificationHelper.NOTIF_DATE_COLUMN, date);
		
		// insert notification
		context.getContentResolver().insert(NotificationContentProvider.CONTENT_URI, values);
	}
	
	public static Notification parse(Cursor cursor) {
		//
		Notification notif = null;
		
		if (cursor != null) {
			//
			notif = new Notification();
			
			notif.setId(cursor.getLong(cursor.getColumnIndex(NotificationHelper.NOTIF_ID_COLUMN)));
			notif.setUniqueId(cursor.getString(cursor.getColumnIndex(NotificationHelper.NOTIF_UUID_COLUMN)));
			notif.setItemId(cursor.getString(cursor.getColumnIndex(NotificationHelper.NOTIF_ITEM_ID_COLUMN)));
			notif.setOffer(cursor.getString(cursor.getColumnIndex(NotificationHelper.NOTIF_OFFER_ITEM_ID_COLUMN)));
			
			String fromJson = cursor.getString(cursor.getColumnIndex(NotificationHelper.NOTIF_FROM_COLUMN));
			String toJson = cursor.getString(cursor.getColumnIndex(NotificationHelper.NOTIF_TO_COLUMN));
			
			User from = null;
			User to = null;
			
			try {
				//
				from = objectMapper.readValue(fromJson, User.class);
			} catch (Exception ex) {}
			
			try {
				//
				to = objectMapper.readValue(toJson, User.class);
			} catch (Exception ex) {}
			
			notif.setFrom(from);
			notif.setTo(to);
			notif.setAction(Action.valueOf(cursor.getString(cursor.getColumnIndex(NotificationHelper.NOTIF_ACTION_COLUMN))));
			notif.setNotification(cursor.getString(cursor.getColumnIndex(NotificationHelper.NOTIF_NOTIF_COLUMN)));
			notif.setMessage(cursor.getString(cursor.getColumnIndex(NotificationHelper.NOTIF_MSG_COLUMN)));
			
			String dateString = cursor.getString(cursor.getColumnIndex(NotificationHelper.NOTIF_DATE_COLUMN));
			Date date = null;
			
			try {
				//
				date = dateFormat.parse(dateString);
			} catch (Exception ex) {}
			
			notif.setDateReceived(date);
		}
		
		return notif;
	}
}
