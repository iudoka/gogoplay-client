package com.gogoplay.data;

import java.io.Serializable;

public class ItemLocation implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4570328647160036589L;
	
	private String longitude;
	private String latitude;
	
	public ItemLocation(String longitude, String latitude) {
		this.longitude = longitude;
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

}
