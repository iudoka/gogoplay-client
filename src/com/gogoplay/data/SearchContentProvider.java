package com.gogoplay.data;

import java.util.Arrays;
import java.util.HashSet;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

public class SearchContentProvider extends ContentProvider {
	//
	private static final String TAG = "SearchContentProvider";
	
	private SearchEntryHelper database;

	// used for the UriMacher
	private static final int SEARCHES = 10;
	private static final int SEARCH_ID = 20;

	private static final String AUTHORITY = "com.gogoplay.searchcontentprovider";

	private static final String BASE_PATH = "searches";
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
			+ "/" + BASE_PATH);

	public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
			+ "/searches";
	public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
			+ "/search";

	private static final UriMatcher sURIMatcher = new UriMatcher(
			UriMatcher.NO_MATCH);

	static {
		sURIMatcher.addURI(AUTHORITY, BASE_PATH, SEARCHES);
		sURIMatcher.addURI(AUTHORITY, BASE_PATH + "/#", SEARCH_ID);
	}

	@Override
	public boolean onCreate() {
		database = new SearchEntryHelper(getContext());
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {

		// hard coding sort order
		sortOrder = SearchEntryHelper.SEARCH_ID_COLUMN + " DESC";
		
		// Uisng SQLiteQueryBuilder instead of query() method
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

		// check if the caller has requested a column which does not exists
		checkColumns(projection);

		// Set the table
		queryBuilder.setTables(SearchEntryHelper.SEARCH_ENTRY_TABLE);

		int uriType = sURIMatcher.match(uri);
		switch (uriType) {
		case SEARCHES:
			break;
		case SEARCH_ID:
			// adding the ID to the original query
			queryBuilder.appendWhere(SearchEntryHelper.SEARCH_ID_COLUMN + "="
					+ uri.getLastPathSegment());
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}

		Cursor cursor = null;
		
		try {
			SQLiteDatabase db = database.getWritableDatabase();
			cursor = queryBuilder.query(db, projection, selection,
					selectionArgs, null, null, sortOrder);
			// make sure that potential listeners are getting notified
			cursor.setNotificationUri(getContext().getContentResolver(), uri);
		} catch(Exception ex) {
			Log.e(TAG, "Unable to query database", ex);
		}

		return cursor;
	}

	@Override
	public String getType(Uri uri) {
		//
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		int uriType = sURIMatcher.match(uri);
		SQLiteDatabase sqlDB = database.getWritableDatabase();

		long id = 0;
		switch (uriType) {
		case SEARCHES:
			id = sqlDB.insert(SearchEntryHelper.SEARCH_ENTRY_TABLE, null,
					values);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return Uri.parse(BASE_PATH + "/" + id);
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		int uriType = sURIMatcher.match(uri);
		SQLiteDatabase sqlDB = database.getWritableDatabase();
		int rowsDeleted = 0;
		switch (uriType) {
		case SEARCHES:
			rowsDeleted = sqlDB.delete(SearchEntryHelper.SEARCH_ENTRY_TABLE,
					selection, selectionArgs);
			break;
		case SEARCH_ID:
			String id = uri.getLastPathSegment();
			if (TextUtils.isEmpty(selection)) {
				rowsDeleted = sqlDB.delete(
						SearchEntryHelper.SEARCH_ENTRY_TABLE,
						SearchEntryHelper.SEARCH_ID_COLUMN + "=" + id, null);
			} else {
				rowsDeleted = sqlDB.delete(
						SearchEntryHelper.SEARCH_ENTRY_TABLE,
						SearchEntryHelper.SEARCH_ID_COLUMN + "=" + id + " and "
								+ selection, selectionArgs);
			}
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsDeleted;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		int uriType = sURIMatcher.match(uri);
		SQLiteDatabase sqlDB = database.getWritableDatabase();
		int rowsUpdated = 0;
		switch (uriType) {
		case SEARCHES:
			rowsUpdated = sqlDB.update(SearchEntryHelper.SEARCH_ENTRY_TABLE,
					values, selection, selectionArgs);
			break;
		case SEARCH_ID:
			String id = uri.getLastPathSegment();
			if (TextUtils.isEmpty(selection)) {
				rowsUpdated = sqlDB.update(
						SearchEntryHelper.SEARCH_ENTRY_TABLE, values,
						SearchEntryHelper.SEARCH_ID_COLUMN + "=" + id, null);
			} else {
				rowsUpdated = sqlDB.update(
						SearchEntryHelper.SEARCH_DATABASE_NAME, values,
						SearchEntryHelper.SEARCH_ID_COLUMN + "=" + id + " and "
								+ selection, selectionArgs);
			}
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsUpdated;
	}

	private void checkColumns(String[] projection) {
		String[] available = { SearchEntryHelper.SEARCH_ID_COLUMN,
				SearchEntryHelper.SEARCH_TEXT_COLUMN };
		if (projection != null) {
			HashSet<String> requestedColumns = new HashSet<String>(
					Arrays.asList(projection));
			HashSet<String> availableColumns = new HashSet<String>(
					Arrays.asList(available));
			// check if all columns which are requested are available
			if (!availableColumns.containsAll(requestedColumns)) {
				throw new IllegalArgumentException(
						"Unknown columns in projection");
			}
		}
	}
}
