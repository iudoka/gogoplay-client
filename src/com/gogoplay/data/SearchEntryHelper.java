package com.gogoplay.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SearchEntryHelper extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 4;

	public static final String SEARCH_DATABASE_NAME = "searches.db";
	public static final String SEARCH_ID_COLUMN = "_id";
	public static final String SEARCH_TEXT_COLUMN = "search_text";
	public static final String SEARCH_ENTRY_TABLE = "searches";

	private static final String TABLE_CREATE = "CREATE TABLE "
			+ SEARCH_ENTRY_TABLE + " (" + SEARCH_ID_COLUMN
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + SEARCH_TEXT_COLUMN + " TEXT);";

	public SearchEntryHelper(Context context) {
		super(context, SEARCH_DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		//
		db.execSQL(TABLE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(SearchEntryHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + SEARCH_ENTRY_TABLE);
		onCreate(db);
	}

	public void emptyTable() {
		//
		getWritableDatabase().execSQL("delete from "+ SEARCH_ENTRY_TABLE);
	}
	
	
}
