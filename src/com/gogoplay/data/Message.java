package com.gogoplay.data;

import java.util.Date;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class Message extends Notification {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8219115270582506197L;
	
	public Message() {
		//
	}
	
	public Message(Cursor cursor) {
		//
		parse(cursor, this);
	}
	
	public Message(Notification notification) {
		//
		setId(notification.getId());
		setUniqueId(notification.getUniqueId());
		setItemId(notification.getItemId());
		setOffer(notification.getOffer());
		setFrom(notification.getFrom());
		setTo(notification.getTo());
		setAction(notification.getAction());
		setNotification(notification.getNotification());
		setMessage(notification.getMessage());
		setDateReceived(notification.getDateReceived());
	}
	
	@Override
	public void store(Context context) {
		//
		ContentValues values = new ContentValues();
		
		values.put(MessageHelper.MESSAGE_UUID_COLUMN, getUniqueId()); 
		values.put(MessageHelper.MESSAGE_ITEM_ID_COLUMN, getItemId());
		values.put(MessageHelper.MESSAGE_OFFER_COLUMN, getOffer());
		
		String userFrom = null;
		
		if (getFrom() != null) {
			//
			try {
				userFrom = objectMapper.writeValueAsString(getFrom());
			} catch (Exception ex) {}
		}
		
		values.put(MessageHelper.MESSAGE_FROM_ID_COLUMN, getFrom().getId());
		values.put(MessageHelper.MESSAGE_FROM_COLUMN, userFrom);
		
		String userTo = null;
		
		if (getTo() != null) {
			//
			try {
				userTo = objectMapper.writeValueAsString(getTo());
			} catch (Exception ex) {}
		}
		
		values.put(MessageHelper.MESSAGE_TO_COLUMN, userTo);
		values.put(MessageHelper.MESSAGE_ACTION_COLUMN, getAction() + "");
		values.put(MessageHelper.MESSAGE_NOTIF_COLUMN, getNotification());
		values.put(MessageHelper.MESSAGE_MSG_COLUMN, getMessage());
		
		String date = null;
		
		if (getDateReceived() != null) {
			date = dateFormat.format(getDateReceived());
		}
		
		values.put(MessageHelper.MESSAGE_DATE_COLUMN, date);
		
		context.getContentResolver().insert(MessageContentProvider.CONTENT_URI, values);
	}

	public static void parse(final Cursor cursor, final Message message) {
		//
		if (cursor != null) {
			//
			message.setId(cursor.getLong(cursor.getColumnIndex(MessageHelper.MESSAGE_ID_COLUMN)));
			message.setUniqueId(cursor.getString(cursor.getColumnIndex(MessageHelper.MESSAGE_UUID_COLUMN)));
			message.setItemId(cursor.getString(cursor.getColumnIndex(MessageHelper.MESSAGE_ITEM_ID_COLUMN)));
			message.setOffer(cursor.getString(cursor.getColumnIndex(MessageHelper.MESSAGE_OFFER_COLUMN)));
			
			String fromJson = cursor.getString(cursor.getColumnIndex(MessageHelper.MESSAGE_FROM_COLUMN));
			String toJson = cursor.getString(cursor.getColumnIndex(MessageHelper.MESSAGE_TO_COLUMN));
			
			User from = null;
			User to = null;
			
			try {
				//
				from = objectMapper.readValue(fromJson, User.class);
			} catch (Exception ex) {}
			
			try {
				//
				to = objectMapper.readValue(toJson, User.class);
			} catch (Exception ex) {}
			
			message.setFrom(from);
			message.setTo(to);
			message.setAction(Action.valueOf(cursor.getString(cursor.getColumnIndex(MessageHelper.MESSAGE_ACTION_COLUMN))));
			message.setNotification(cursor.getString(cursor.getColumnIndex(MessageHelper.MESSAGE_NOTIF_COLUMN)));
			message.setMessage(cursor.getString(cursor.getColumnIndex(MessageHelper.MESSAGE_MSG_COLUMN)));
			
			String dateString = cursor.getString(cursor.getColumnIndex(MessageHelper.MESSAGE_DATE_COLUMN));
			Date date = null;
			
			try {
				//
				date = dateFormat.parse(dateString);
			} catch (Exception ex) {}
			
			message.setDateReceived(date);
		}
	}
	
	public String toString() {
		//
		return getMessage();
	}
}
