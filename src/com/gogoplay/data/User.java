package com.gogoplay.data;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import android.graphics.Bitmap;

import com.facebook.model.GraphUser;
import com.gogoplay.util.Utils;

/**
 * "user": { "type": "facebook", // or "gogosing" "id": "32423994932", "token":
 * "aklfkdsfjkakdjl23rfj", "fname": "Joe", "lname": "Smith", "email":
 * "joe.smith@acme.com", "sex": "male", "photo":
 * "http://graph.facebook.com/3298924098832/picture?type=large" "password":
 * "aljfadf" // if "gogosing" type }
 * 
 * @author iudoka
 * 
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class User implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1168833912518339575L;
	
	public static enum UserType {
		facebook,
		custom
	}
	
	public static class PushToken implements Serializable {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = -2129517257839090459L;
		
		public enum DeviceType {
			ios,
			android;
		}
		
		private DeviceType deviceType;
		private String deviceId;
		private String deviceToken;
		
		/**
		 * @return the deviceType
		 */
		public DeviceType getDeviceType() {
			return deviceType;
		}
		/**
		 * @param deviceType the deviceType to set
		 */
		public void setDeviceType(DeviceType deviceType) {
			this.deviceType = deviceType;
		}
		/**
		 * @return the deviceId
		 */
		public String getDeviceId() {
			return deviceId;
		}
		/**
		 * @param deviceId the deviceId to set
		 */
		public void setDeviceId(String deviceId) {
			this.deviceId = deviceId;
		}
		/**
		 * @return the deviceToken
		 */
		public String getDeviceToken() {
			return deviceToken;
		}
		/**
		 * @param deviceToken the deviceToken to set
		 */
		public void setDeviceToken(String deviceToken) {
			this.deviceToken = deviceToken;
		}
	}

	//
	private UserType type;
	private String id;
	private String token;
	@JsonProperty("fname")
	private String firstName;
	@JsonProperty("lname")
	private String lastName;
	private String email;
	private String sex;
	private String photo;
	@JsonIgnore
	private Bitmap avatar;
	private String nickname;
	private float rating;
	private PushToken pushToken;
	
	private String password;
	
	private Set<User> following;
	
	private Date dateCreated;

	public User() {
		//
	}
	
	public User(GraphUser fbUserInfo, String token) {
		// 
		this();
		type = UserType.facebook;
		setToken(token);
		setFirstName(fbUserInfo.getFirstName());
		setLastName(fbUserInfo.getLastName());
		setEmail(Utils.toString(fbUserInfo.getProperty("email")));
		setSex(Utils.toString(fbUserInfo.getProperty("gender")));
		setId(fbUserInfo.getId());
		
		if (!Utils.isEmpty(getEmail())) {
			//
			setNickname(getEmail().substring(0, getEmail().indexOf("@")));
		} else {
			setNickname(getFirstName() + getLastName());
		}
		
		if (!Utils.isEmpty(getId())) {
			setPhoto("http://graph.facebook.com/"+ getId() +"/picture?type=small");
		}
	}

	/**
	 * The id
	 * @return the user id
	 */
	public String getId() {
		return id;
	}

	/**
	 * sets user id
	 * @param id - user id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * user's avatar
	 * @return avatar
	 */
	public Bitmap getAvatar() {
		return avatar;
	}

	/**
	 * set users avatar
	 * @param avatar users image
	 */
	public void setAvatar(Bitmap avatar) {
		this.avatar = avatar;
	}

	/**
	 * @return the type
	 */
	public UserType getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(UserType type) {
		this.type = type;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the nickname
	 */
	public String getNickname() {
		//
		return nickname;
	}

	/**
	 * @param nickname the nickname to set
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the sex
	 */
	public String getSex() {
		return sex;
	}

	/**
	 * @param sex the sex to set
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}

	/**
	 * @return the photo
	 */
	public String getPhoto() {
		return photo;
	}

	/**
	 * @param photo the photo to set
	 */
	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getName() {
		//
		return getFirstName() + " " + getLastName();
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the pushToken
	 */
	public PushToken getPushToken() {
		return pushToken;
	}

	/**
	 * @param pushToken the pushToken to set
	 */
	public void setPushToken(PushToken pushToken) {
		this.pushToken = pushToken;
	}

	/**
	 * @return the rating
	 */
	public float getRating() {
		return rating;
	}

	/**
	 * @param rating the rating to set
	 */
	public void setRating(float rating) {
		this.rating = rating;
	}
	
	public boolean follow(User user) {
		//
		return getFollowing().add(user);
	}
	
	public boolean unfollow(User user) {
		//
		boolean removed = false;
		Iterator<User> iter = getFollowing().iterator();
		
		while (iter.hasNext()) {
			//
			if (iter.next().getId().equalsIgnoreCase(user.getId())) {
				iter.remove();
				removed = true;
				break;
			}
		}
		
		return removed;
	}

	/**
	 * @return the following
	 */
	public Set<User> getFollowing() {
		//
		if (following == null) {
			//
			following = new HashSet<User>();
		}
		
		return following;
	}

	/**
	 * @param following the following to set
	 */
	public void setFollowing(Set<User> following) {
		this.following = following;
	}

	/**
	 * @return the joinDate
	 */
	public Date getDateCreated() {
		return dateCreated;
	}

	/**
	 * @param joinDate the joinDate to set
	 */
	public void setDateCreated(Date joinDate) {
		this.dateCreated = joinDate;
	}

	/**
	 * Check if current user is following the given user
	 * @param user
	 * @return
	 */
	public boolean isFollowing(User user) {
		// 
		boolean follow = false;
		
		if (!Utils.isEmpty(following)) {
			for (User u : following) {
				//
				if (u.getId().equalsIgnoreCase(user.getId())) {
					follow = true;
					break;
				}
			}
		}
		
		return follow;
	}
}
