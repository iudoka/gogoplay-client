package com.gogoplay.data;

import com.gogoplay.pref.Sorter;

/**
 * Bean to track search filters
 * 
 * @author iudoka
 */
public class SearchFilter {
	//
	private long id;
	private String name;
	private String age;
	private String gender;
	private String brand;
	private Sorter location;
	private double priceMin;
	private double priceMax;
	private String category;
	private String condition;
	private boolean selected;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the age
	 */
	public String getAge() {
		return age;
	}

	/**
	 * @param age
	 *            the age to set
	 */
	public void setAge(String age) {
		this.age = age;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender
	 *            the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the brand
	 */
	public String getBrand() {
		return brand;
	}

	/**
	 * @param brand the brand to set
	 */
	public void setBrand(String brand) {
		this.brand = brand;
	}

	/**
	 * @return the location
	 */
	public Sorter getLocation() {
		return location;
	}

	/**
	 * @param location
	 *            the location to set
	 */
	public void setLocation(Sorter location) {
		this.location = location;
	}

	/**
	 * @return the price
	 */
	public double getPriceMin() {
		return priceMin;
	}
	
	/**
	 * @return the price
	 */
	public double getPriceMax() {
		return priceMax;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPriceMin(double price) {
		this.priceMin = price;
	}
	
	/**
	 * @param price
	 *            the price to set
	 */
	public void setPriceMax(double price) {
		this.priceMax = price;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the condition
	 */
	public String getCondition() {
		return condition;
	}

	/**
	 * @param condition
	 *            the condition to set
	 */
	public void setCondition(String condition) {
		this.condition = condition;
	}

	/**
	 * @return the selected
	 */
	public boolean isSelected() {
		return selected;
	}

	/**
	 * @param selected the selected to set
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
}
