package com.gogoplay.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Persists Notifications
 * @author iudoka
 *
 */
public class NotificationHelper extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 2;
	
	public static final String NOTIF_DATABASE_NAME = "notifs.db";
	public static final String NOTIF_ID_COLUMN = "_id";
	public static final String NOTIF_UUID_COLUMN = "uuid";
	public static final String NOTIF_ITEM_ID_COLUMN = "item_id";
	public static final String NOTIF_OFFER_ITEM_ID_COLUMN = "offer_item_id";
	public static final String NOTIF_FROM_ID_COLUMN = "from_id";
	public static final String NOTIF_FROM_COLUMN = "from_user";
	public static final String NOTIF_TO_COLUMN = "to_user";
	public static final String NOTIF_ACTION_COLUMN = "action";
	public static final String NOTIF_NOTIF_COLUMN = "notif";
	public static final String NOTIF_MSG_COLUMN = "msg";
	public static final String NOTIF_DATE_COLUMN = "create_date";
	
	public static final String NOTIF_TABLE = "notifs";

	private static final String TABLE_CREATE = "CREATE TABLE "
			+ NOTIF_TABLE + " (" 
			+ NOTIF_ID_COLUMN + " INTEGER PRIMARY KEY AUTOINCREMENT,"
			+ NOTIF_UUID_COLUMN + " TEXT,"
			+ NOTIF_ITEM_ID_COLUMN + " TEXT,"
			+ NOTIF_OFFER_ITEM_ID_COLUMN + " TEXT,"
			+ NOTIF_FROM_ID_COLUMN + " TEXT,"
			+ NOTIF_FROM_COLUMN + " TEXT,"
			+ NOTIF_TO_COLUMN + " TEXT,"
			+ NOTIF_ACTION_COLUMN + " TEXT,"
			+ NOTIF_NOTIF_COLUMN + " TEXT,"
			+ NOTIF_MSG_COLUMN + " TEXT,"
			+ NOTIF_DATE_COLUMN + " TEXT);";

	public NotificationHelper(Context context) {
		super(context, NOTIF_DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		//
		db.execSQL(TABLE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(NotificationHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + NOTIF_TABLE);
		onCreate(db);
	}

	public void emptyTable() {
		//
		getWritableDatabase().execSQL("delete from "+ NOTIF_TABLE);
	}
	
	
}
