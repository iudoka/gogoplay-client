package com.gogoplay.data.server;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import org.apache.http.entity.ContentType;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.gogoplay.data.Comment;
import com.gogoplay.data.Image;
import com.gogoplay.data.Item;
import com.gogoplay.data.Item.Gender;
import com.gogoplay.data.Item.Status;
import com.gogoplay.data.Message;
import com.gogoplay.data.Notification.Action;
import com.gogoplay.data.Notification;
import com.gogoplay.data.Seller;
import com.gogoplay.data.User;
import com.gogoplay.data.User.PushToken;
import com.gogoplay.data.User.UserType;
import com.gogoplay.pref.NotificationPreference;
import com.gogoplay.pref.Sorter;
import com.gogoplay.util.ImageUtil;
import com.gogoplay.util.Utils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.stream.JsonReader;

public class ShopClient extends RestClient {
	//
	private static final String TAG = "ShopClient";
	
	private static final ObjectMapper objectMapper = new ObjectMapper();
	private static final int s_imageSize = 320; // 320x320
	
	static {
		objectMapper.setSerializationInclusion(Inclusion.NON_NULL);
	}

	@SuppressLint("SimpleDateFormat")
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");//.SSSZ");
	
	static {
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
	}

	public ShopClient() {
		super();
	}
	
	public static class ServiceResponse {
		public boolean success;
		public String id;
		public String message;
		public Object responseObject;
		
		public ServiceResponse(boolean success, String id, String message) {
			this(success, id, message, null);
		}
		
		public ServiceResponse(boolean success, String id, String message, Object responseObject) {
			this.success = success;
			this.id = id;
			this.message = message;
			this.responseObject = responseObject;
		}
	}
	
	/**
	 * Sign up custom user (email/password) or sign up/log in Facebook user
	 * 
	 * @param context
	 * @param user
	 * @return
	 */
	public ServiceResponse signUpUser(Context context, User user) {
		//
		ServiceResponse response = new ServiceResponse(true, "", "");
		
		try {
			String userJson = objectMapper.writeValueAsString(user);
			
			String url = ServerUtils.buildSignUpUrl(null);
			
			Log.i(TAG, "Signing up user " + url);
			HashMap<String, String> headers = new HashMap<String, String>();
			headers.put("user", userJson);
			
			String result = post(url, null, userJson);//, getServerAuthToken(context), ServerUtils.generateCryptographicNonce());
			
			JsonObject resultElem = new JsonParser().parse(result).getAsJsonObject();
			
			if (resultElem.has("error")) {
				// error occurred
				Log.e(TAG, "error occured: " + result);
				
				// User already exists try to pull their information
				User theUser = pullUserInfo(user);
				
				if (theUser != null) {
					// check if user token has changed
					
					if (!Utils.equals(user.getToken(), theUser.getToken())) {
						//
						updateUserToken(theUser.getToken(), user.getToken());
						theUser.setToken(user.getToken());
					}
					
					user = theUser;
				}
			} else {
				//
				if (resultElem.has("userId")) {
					// set OUR user id
					user.setId(resultElem.get("userId").getAsString());
				} else {
					user = pullUserInfo(user);
				}
			}
			
			if (UserType.custom == user.getType()) {
				// TODO: Check for error
				// TODO: for custom sign up's get server generated token and update user object
			} else {
				response.responseObject = user;
			}
		} catch(Exception ex) {
			Log.e(TAG, "Unable to sign up user: " + ex);
			response.success = false;
			response.message = "Unable to reach server!";
		}
		
		return response;
	}
	
	public boolean updateUserToken(String currentToken, String newToken) {
		//
		String url = ServerUtils.buildTokenUrl();
		
		Log.i(TAG, "Updating user token via: " + url);
		String tokenJson = "{\"token\":" + "\"" + newToken + "\"}";
		
		String result = put(url, createAuthHeader(currentToken), tokenJson);
		
		return result != null;
	}
	
	public User pullUserInfo(User user) {
		//
		try {
			String url = ServerUtils.buildMeUrl();
			
			String jsonString = Utils.trim(getString(url, createAuthHeader(user.getToken()), null, null));
			JsonElement jsonItem = new JsonParser().parse(jsonString);
			
			user = parseUser(jsonItem.getAsJsonObject(), user);
			
			// pull user's following list
			url = ServerUtils.buildFollowingUrl();
			jsonString = Utils.trim(getString(url, createAuthHeader(user.getToken()), null, null));
			jsonItem = new JsonParser().parse(jsonString);
			
			parseFollowing(jsonItem.getAsJsonArray(), user);
			
		} catch(Exception ex) {
			Log.e(TAG, "Unable to sign up user: " + ex);
		}
		
		return user;
	}
	
	/**
	 * Custom log-in using email and password
	 * 
	 * @param context
	 * @param username
	 * @param password
	 * @return
	 */
	public ServiceResponse loginUser(Context context, String username, String password) {
		//
		ServiceResponse response = new ServiceResponse(true, "", "");
		
		try {
			User user = new User();
			user.setEmail(username);
			user.setPassword(password);
			
			String userJson = objectMapper.writeValueAsString(user);
			
			String url = ServerUtils.buildLoginUrl(null);
			
			Log.i(TAG, "Login in user " + url);
			
			HashMap<String, String> headers = new HashMap<String, String>();
			headers.put("user", userJson);
			
			String result = post(url, null, userJson);//, getServerAuthToken(context), ServerUtils.generateCryptographicNonce());
			// TODO: Check for error
			boolean error = (result == null);
			
			if (!error) {
				// TODO store token if email account
				if (user.getType() == UserType.custom) {
					
				}
				
				// load user image
				/*if (user.getAvatar() == null && !Utils.isEmpty(user.getPhoto())) {
					user.setAvatar(getImage(context, user.getPhoto(), 48, 48));
				}*/
			}
		} catch(Exception ex) {
			Log.e(TAG, "Unable to log in user: " + ex);
			ex.printStackTrace();
			response.success = false;
		}
		
		return response;
	}
	
	public ServiceResponse editItem(Context context, Item item, String userToken) {
		//
		/*ServiceResponse response = new ServiceResponse(true, "", "");
		response.success = false;
		response.message = "Not Yet Implemented!";
		
		return response;*/
		
		return uploadItem(context, item, userToken, true);
	}
	
	public ServiceResponse uploadItem(Context context, Item item, String userToken) {
		//
		return uploadItem(context, item, userToken, false);
	}
	
	/**
	 * @param item
	 * @return
	 */
	public ServiceResponse uploadItem(Context context, Item item, String userToken, boolean update) {
		//
		ServiceResponse response = new ServiceResponse(true, "", "");
		
		try {
			//
			String json = objectMapper.writeValueAsString(item);
			
			// build multipart message, post to server to upload it
			List<FormBodyPart> multipartMsg = new ArrayList<FormBodyPart>();
			
			//int index = 1;
			for (Image image : item.getImages()) {
				//
				String imageUri = image.getUri();
				
				if (!Utils.isEmpty(imageUri) || image.getBitmap() != null) {
					Bitmap b = image.getBitmap();
					
					if (b == null) {
						if (imageUri.startsWith("http")) {
							// pull bitmap from url
							InputStream is = ImageUtil.getBitmapInputStream(imageUri);
							final BitmapFactory.Options options = new BitmapFactory.Options();
							options.inJustDecodeBounds = true;
							BitmapFactory.decodeStream(is, null, options);
							
							options.inJustDecodeBounds = false;
							options.inSampleSize = ImageUtil.calculateInSampleSize(options, ImageUtil.IMAGE_SIZE, ImageUtil.IMAGE_SIZE);
							
							b = BitmapFactory.decodeStream(ImageUtil.getBitmapInputStream(imageUri), null, options);
						} else {
							// pull bitmap from file
							final BitmapFactory.Options options = new BitmapFactory.Options();
							options.inJustDecodeBounds = true;
							BitmapFactory.decodeFile(imageUri, options);
							
							options.inJustDecodeBounds = false;
							options.inSampleSize = ImageUtil.calculateInSampleSize(options, ImageUtil.IMAGE_SIZE, ImageUtil.IMAGE_SIZE);
							
							b = BitmapFactory.decodeFile(imageUri, options);
						}
					}
					
					if (b != null) {
						// bitmap available
						ByteArrayOutputStream bao = new ByteArrayOutputStream();
						b.compress(CompressFormat.JPEG, 100, bao);
						
						String imgName = Utils.emptyIfNull(imageUri);
						int slashIndex = imgName.lastIndexOf("/");
						
						if (slashIndex >= 0) {
							imgName = imgName.substring(slashIndex + 1);
						}
						
						FormBodyPart picture = new FormBodyPart("fileName", imgName, bao.toByteArray(), ContentType.APPLICATION_OCTET_STREAM);
						multipartMsg.add(picture);
					}
					
					//index++;
				} //else {
					//break;
				//}
			}
			
			FormBodyPart newProduct = new FormBodyPart("newProduct", "", json, ContentType.APPLICATION_JSON);
			multipartMsg.add(newProduct);
			
			String serverUrl = null;
			
			if (!update) {
				serverUrl = ServerUtils.buildAddItemUrl(userToken);
			} else {
				serverUrl = ServerUtils.buildEditItemUrl(item.getId(), userToken);
			}
			
			String resJson = uploadMultipart(serverUrl, createAuthHeader(userToken), multipartMsg, getServerAuthToken(context), ServerUtils.generateCryptographicNonce(), update);
			
			if (resJson != null) {
				response.success = true;
				
				JsonElement jsonItem = new JsonParser().parse(resJson);
				
				if (!update) {
					String id = jsonItem.getAsJsonObject().get("itemId").getAsString();
				
					response.id = id;
					item.setId(id);
				}
			} else {
				response.success = false;
			}
		} catch(Exception ex) {
			response.success = false;
			response.message = ex.toString();
			Log.e(TAG, "Unable to upload item", ex);
		}
		
		return response;
	}

	public Item getItem(Context context, String id, String userToken) {
		return getItem(context, id, userToken, null);
	}
	
	public Item getItem(Context context, String id, String userToken, Item item) {
		//
		String url = ServerUtils.buildGetItemUrl(id, userToken);
		
		Log.i(TAG, "Retrieving item with id " + id + " from url " + url + " item " + item);
		
		try {
			//String jsonString = "{\"otype\":\"listing\",\"data\":{\"status\":\"active\",\"location\":[38.808274,-76.831996],\"address\":\"Upper Marlboro, 20772\",\"LIKE-count\":0,\"COMMENT-count\":0,\"creation\":\"2014-06-20T02:49:03.635Z\"},\"_id\":\"53a3a11f49d53ddb795f2240\",\"assocs\":[{\"id1\":\"53a3a11f49d53ddb795f2240\",\"atype\":\"LIST\",\"id2\":\"53a3a11f49d53ddb795f223f\",\"_id\":\"53a3a11f49d53ddb795f2242\",\"object\":{\"otype\":\"item\",\"data\":{\"title\":\"test\",\"images\":[{\"origFileName\":\"/mnt/sdcard/DCIM/Camera/1386398470809.jpg\",\"uri\":\"https://s3.amazonaws.com/gogosing-images/2/9b/29b4b1e03d22841b20a4888385bdced6\"}],\"price\":109.88,\"condition\":null,\"size\":null,\"color\":null,\"quantity\":1,\"tags\":[\"toy\"],\"detail\":\"test toy\",\"shareOn\":null,\"creation\":\"2014-06-20T02:49:03.633Z\"},\"_id\":\"53a3a11f49d53ddb795f223f\"}}]}";
			String jsonString = Utils.trim(getString(url, createAuthHeader(userToken), null, null));//getServerAuthToken(context), ServerUtils.generateCryptographicNonce()));
			JsonElement jsonItem = new JsonParser().parse(jsonString);
			
			if (item == null) {
				item = new Item();
			}
			
			parseItem(jsonItem.getAsJsonObject(), item);
		} catch (Exception ex) {
			Log.e(TAG, "Unable to process item ", ex);
		}
		
		return item;
	}
	
	/**
	 * POST api/abuse/:id      => :id = itemid
	 */
	public boolean reportItem(Context context, String itemId, String userToken) {
		//
		String url = ServerUtils.buildReportItemUrl(itemId);
		
		Log.i(TAG, "Reporting item with id " + itemId + " from url " + url);
		
		try {
			String jsonString = Utils.trim(post(url, createAuthHeader(userToken), null));
			JsonElement jsonItem = new JsonParser().parse(jsonString);
			Log.i(TAG, "Got back " + jsonItem);
		} catch (Exception ex) {
			Log.e(TAG, "Unable to process item ", ex);
			return false;
		}
		
		return true;
	}
	
	public boolean reportUser(Context context, String userId, String userToken) {
		//
		String url = ServerUtils.buildReportUserUrl(userId);
		
		Log.i(TAG, "Reporting item with id " + userId + " from url " + url);
		
		try {
			String jsonString = Utils.trim(post(url, createAuthHeader(userToken), null));
			JsonElement jsonItem = new JsonParser().parse(jsonString);
			Log.i(TAG, "Got back " + jsonItem);
		} catch (Exception ex) {
			Log.e(TAG, "Unable to process item ", ex);
			return false;
		}
		
		return true;
	}
	
	public boolean likeItem(Context context, Item item, User user) {
		//
		String userToken = user.getToken();
		String itemId = item.getId();
		String url = ServerUtils.buildLikeItemUrl(itemId, userToken);
		
		Log.i(TAG, "Liking item with id " + itemId + " from url " + url);
		
		try {
			String jsonString = Utils.trim(post(url, createAuthHeader(userToken), null));
			JsonElement jsonItem = new JsonParser().parse(jsonString);
			Log.i(TAG, "Got back " + jsonItem);
		} catch (Exception ex) {
			Log.e(TAG, "Unable to process item ", ex);
			return false;
		}
		
		String alert = user.getNickname() + " likes " + item.getTitle();
		
		JsonObject data = new JsonObject();
		data.addProperty("action", "like");
		data.addProperty("display", alert);
		
		return enqueue(context, itemId, item.getSeller().getId(), userToken, data, alert);
	}
	
	public boolean enqueue(Context context, String itemId, String userId, String userToken, JsonObject data, String alertMsg) {
		//
		String url = ServerUtils.buildEnqueueUrl(userToken);
		
		JsonObject qObj = new JsonObject();
		qObj.addProperty("userId", userId);
		
		if (!Utils.isEmpty(itemId)) {
			qObj.addProperty("itemId", itemId);
		}
		
		qObj.addProperty("srcId", dateFormat.format(new Date()));
		qObj.addProperty("alertMsg", alertMsg);
		qObj.add("data", data);
		
		Gson gson = new GsonBuilder().create();
		String body = gson.toJson(qObj);
		
		Log.i(TAG, "Enqueueing data: " + body + " via url: " + url);
		
		try {
			String jsonString = Utils.trim(post(url, createAuthHeader(userToken), body));
			JsonElement jsonItem = new JsonParser().parse(jsonString);
			Log.i(TAG, "Got back " + jsonItem);
		} catch (Exception ex) {
			Log.e(TAG, "Unable to enqueue item ", ex);
			return false;
		}
		
		return true;
	}
	
	public ArrayList<Notification> getNotifications(Context context, String userToken, boolean purge) {
		//
		ArrayList<Notification> notifs = new ArrayList<Notification>();
		
		String url = ServerUtils.buildDequeueUrl(userToken);
		
		String body = "{\"delete\" : \"" + (purge ? "Y" : "N") + "\"}";
		
		try {
			String jsonString = Utils.trim(post(url, createAuthHeader(userToken), body));
			JsonElement jsonItem = new JsonParser().parse(jsonString);
			Log.i(TAG, "Got back " + jsonItem);
			
			// Parse notifications
			notifs = parseNotifications(jsonItem);
		} catch (Exception ex) {
			Log.e(TAG, "Unable to dequeue item ", ex);
		}
		
		return notifs;
	}
	
	public ArrayList<Notification> parseNotifications(JsonElement notifElem) throws Exception {
		//
		ArrayList<Notification> notifications = new ArrayList<Notification>();
		JsonArray jsonItems = notifElem.getAsJsonObject().getAsJsonArray("result");
		
		if (jsonItems != null) {
			//
			for (JsonElement e : jsonItems) {
				//
				JsonObject eObj = e.getAsJsonObject();
				Notification notif = new Notification();
				
				if (Utils.has(eObj, "itemId")) {
					String itemId = eObj.get("itemId").getAsString();
					notif.setItemId(itemId);
				}
				
				Date notifDate = null;
				
				if (Utils.has(eObj, "createDate")) {
					String date = eObj.get("createDate").getAsString();
					
					try {
						notifDate = dateFormat.parse(date);
					} catch (Exception ex) {
						Log.e(TAG, "Unable to parse date " + date);
					}
				}
				
				notif.setDateReceived(notifDate);
				
				if (Utils.has(eObj, "fromUser")) {
					JsonElement userElement = eObj.get("fromUser");
					User user = parseUser(userElement.getAsJsonObject());
					notif.setFrom(user);
				}
				
				if (Utils.has(eObj, "data")) {
					JsonObject data = eObj.get("data").getAsJsonObject();
					
					if (Utils.has(data, "action")) {
						String action = data.get("action").getAsString();
						notif.setAction(Action.valueOf(action));
					}
					
					if (Utils.has(data, "display")) {
						String msg = data.get("display").getAsString();
						notif.setNotification(msg);
					}
					
					if (Utils.has(data, "itemId")) {
						String itemId = data.get("itemId").getAsString();
						notif.setOffer(itemId);
					}
					
					if (Utils.has(data, "message")) {
						String msg = data.get("message").getAsString();
						notif.setMessage(msg);
					}
					
					if (Utils.has(data, "messageId")) {
						String uuid = data.get("messageId").getAsString();
						notif.setUniqueId(uuid);
					}
					
					if (Utils.has(data, "offerPrice")) {
						String price = data.get("offerPrice").getAsString();
						notif.setOffer(price);
					}
				}

				notifications.add(notif);
			}
		}
		
		return notifications;
	}
	
	public boolean unlikeItem(Context context, String itemId, String userToken) {
		// 
		String url = ServerUtils.buildLikeItemUrl(itemId, userToken);
		
		Log.i(TAG, "Unliking item with id " + itemId + " from url " + url);
		
		try {
			String jsonString = Utils.trim(delete(url, createAuthHeader(userToken)));
			JsonElement jsonItem = new JsonParser().parse(jsonString);
			Log.i(TAG, "Got back " + jsonItem);
		} catch (Exception ex) {
			Log.e(TAG, "Unable to process item ", ex);
			return false;
		}
		
		return true;
	}
	
	public boolean buyItem(Context context, Item item, double amount, User user) {
		//
		String alert = user.getNickname() + " wants to buy " + item.getTitle();
		String msg = "I would like to buy your " + item.getTitle() + " for " + Utils.formatPrice(amount);
		
		JsonObject data = new JsonObject();
		data.addProperty("action", "buy");
		data.addProperty("display", msg);
		data.addProperty("offerPrice", amount + "");
		
		boolean success = enqueue(context, item.getId(), item.getSeller().getId(), user.getToken(), data, alert);
		
		if (success) {
			// save offer locally
			com.gogoplay.data.Message offer = new com.gogoplay.data.Message();
			
			offer.setAction(Action.buy);
			offer.setDateReceived(new Date());
			offer.setFrom(user);
			offer.setTo(item.getSeller());
			offer.setItemId(item.getId());
			offer.setMessage(msg);
			offer.setNotification(alert);
			offer.setOffer(amount + "");
			offer.setUniqueId(user.getId());
			
			offer.store(context);
		}
		
		return success;
	}
	
	public boolean tradeItem(Context context, Item item, Item otherItem, User user) {
		//
		String alert = user.getNickname() + " wants to trade something for " + item.getTitle();
		String msg = "I would like to trade my " + otherItem.getTitle() + " for your " + item.getTitle();
		
		JsonObject data = new JsonObject();
		data.addProperty("action", "trade");
		data.addProperty("itemId", otherItem.getId());
		data.addProperty("display", msg);
		
		boolean success = enqueue(context, item.getId(), item.getSeller().getId(), user.getToken(), data, alert);
		
		if (success) {
			// save offer locally
			com.gogoplay.data.Message offer = new com.gogoplay.data.Message();
			
			offer.setAction(Action.trade);
			offer.setDateReceived(new Date());
			offer.setFrom(user);
			offer.setTo(item.getSeller());
			offer.setItemId(item.getId());
			offer.setMessage(msg);
			offer.setNotification(alert);
			offer.setOffer(otherItem.getId());
			offer.setUniqueId(user.getId());
			
			offer.store(context);
		}
		
		return success;
	}
	
	public Message sendMessage(Context context, Item item, String message, String messageId, User user) {
		//
		String alert = user.getNickname() + " messaged you about " + item.getTitle();
		
		JsonObject data = new JsonObject();
		data.addProperty("action", "message");
		data.addProperty("messageId", messageId);
		data.addProperty("message", message);
		data.addProperty("display", alert);
		
		boolean success = enqueue(context, item.getId(), item.getSeller().getId(), user.getToken(), data, alert);
		
		com.gogoplay.data.Message msg = null;
		
		if (success) {
			// save offer locally
			msg = new com.gogoplay.data.Message();
			
			msg.setAction(Action.message);
			msg.setDateReceived(new Date());
			msg.setFrom(user);
			msg.setTo(item.getSeller());
			msg.setItemId(item.getId());
			msg.setMessage(message);
			msg.setNotification(alert);
			msg.setOffer(null);
			msg.setUniqueId(user.getId());
			
			msg.store(context);
		}
		
		return msg;
	}
	
	public boolean watchItem(Context context, Item item, User user) {
		// 
		String userToken = user.getToken();
		String itemId = item.getId();
		String url = ServerUtils.buildWatchItemUrl(itemId, userToken);
		
		Log.i(TAG, "Watching item with id " + itemId + " from url " + url);
		
		try {
			String jsonString = Utils.trim(post(url, createAuthHeader(userToken), null));
			JsonElement jsonItem = new JsonParser().parse(jsonString);
			Log.i(TAG, "Got back " + jsonItem);
		} catch (Exception ex) {
			Log.e(TAG, "Unable to process item ", ex);
			return false;
		}
		
		String alert = user.getNickname() + " is watching " + item.getTitle();
		
		JsonObject data = new JsonObject();
		data.addProperty("action", "watch");
		data.addProperty("display", alert);
		
		return enqueue(context, itemId, item.getSeller().getId(), userToken, data, alert);
	}
	
	public boolean unwatchItem(Context context, String itemId, String userToken) {
		// 
		String url = ServerUtils.buildWatchItemUrl(itemId, userToken);
		
		Log.i(TAG, "Unwatching item with id " + itemId + " from url " + url);
		
		try {
			String jsonString = Utils.trim(delete(url, createAuthHeader(userToken)));
			JsonElement jsonItem = new JsonParser().parse(jsonString);
			Log.i(TAG, "Got back " + jsonItem);
		} catch (Exception ex) {
			Log.e(TAG, "Unable to process item ", ex);
			return false;
		}
		
		return true;
	}
	
	public boolean followUser(Context context, String followUserId, User user) {
		// 
		String userToken = user.getToken();
		String url = ServerUtils.buildFollowUserUrl(followUserId);
		
		Log.i(TAG, "Following user with id " + followUserId + " from url " + url);
		
		try {
			String jsonString = Utils.trim(post(url, createAuthHeader(userToken), null));
			JsonElement jsonItem = new JsonParser().parse(jsonString);
			Log.i(TAG, "Got back " + jsonItem);
		} catch (Exception ex) {
			Log.e(TAG, "Unable to process item ", ex);
			return false;
		}
		
		String alert = user.getNickname() + " is following you";
		
		JsonObject data = new JsonObject();
		data.addProperty("action", "follow");
		data.addProperty("display", alert);
		
		return enqueue(context, null, followUserId, userToken, data, alert);
	}
	
	public boolean unfollowUser(Context context, String userId, String userToken) {
		// 
		String url = ServerUtils.buildFollowUserUrl(userId);
		
		Log.i(TAG, "Unfollowing user with id " + userId + " from url " + url);
		
		try {
			String jsonString = Utils.trim(delete(url, createAuthHeader(userToken)));
			JsonElement jsonItem = new JsonParser().parse(jsonString);
			Log.i(TAG, "Got back " + jsonItem);
		} catch (Exception ex) {
			Log.e(TAG, "Unable to process item ", ex);
			return false;
		}
		
		return true;
	}
	
	public boolean sendPushToken(Context context, String userToken, PushToken pushToken) {
		//
		try {
			String url = ServerUtils.buildPushTokenUrl();
			String pushJson = objectMapper.writeValueAsString(pushToken);
			
			String jsonString = Utils.trim(post(url, createAuthHeader(userToken), pushJson));
			
			Log.i(TAG, "Got back " + jsonString);
			return true;
		} catch(Exception ex) {
			Log.e(TAG, "Unable to push user token: " + ex);
		}
		
		return false;
	}
	
	/**
	 * https://gogosing.org/api/preference/:id/
      POST HTTP request
      :id is the user's _id.
      HTTP body has preference param (key/value combos can be whatever we want):
        {
       "notification" : { "like" : true, "comment" : true, "email" : true }
        }
	 */
	public boolean updateNotificationPreference(Context context, NotificationPreference pref, String userToken) {
		// 
		try {
			String url = ServerUtils.buildPreferenceUrl();
			Map<String, NotificationPreference> map = Collections.singletonMap("notification", pref);
			String pushJson = objectMapper.writeValueAsString(map);
			
			String jsonString = Utils.trim(post(url, createAuthHeader(userToken), pushJson));
			
			Log.i(TAG, "Got back " + jsonString);
			return true;
		} catch(Exception ex) {
			Log.e(TAG, "Unable to push user token: " + ex);
		}
		
		return false;
	}

	public String sendPushNotification(Context context, String registrationId, String message) {
		// 
		String url = "https://android.googleapis.com/gcm/send";
		
		try {
			// send to server
			JsonObject pushObj = new JsonObject();
			
			JsonObject dataObj = new JsonObject();
			dataObj.addProperty("Message", message);
			
			pushObj.add("data", dataObj);

			JsonArray regIdsArr = new JsonArray();
			JsonPrimitive regIds = new JsonPrimitive(registrationId);
			regIdsArr.add(regIds);
			
			pushObj.add("registration_ids", regIdsArr);

			Gson gson = new GsonBuilder().create();
			String pushBody = gson.toJson(pushObj);
			
			Log.i(TAG, "Sending " + pushBody + " to: " + url);
			
			HashMap<String, String> headers = new HashMap<String, String>();
			headers.put("Authorization", "key=AIzaSyCKEHsYHl3ZjMrDLwm4jJQ_vwmFIDa0UT4");
			headers.put("Content-Type", "application/json");
			
			String jsonString = Utils.trim(post(url, headers, pushBody));
			
			if (!Utils.isEmpty(jsonString)) {
				//
				Log.i(TAG, "Received push notification response: " + jsonString);
				return jsonString;
			}
			
		} catch (Exception ex) {
			Log.e(TAG, "Unable to send push notification", ex);
		}
		
		return null;
	}
	
	public boolean deleteItem(Context context, String itemId, String userToken) {
		//
		String url = ServerUtils.buildEditItemUrl(itemId, userToken);
		
		try {
			String jsonString = Utils.trim(delete(url, createAuthHeader(userToken)));
			JsonElement jsonItem = new JsonParser().parse(jsonString);
			Log.i(TAG, "Got back " + jsonItem);
		} catch (Exception ex) {
			Log.e(TAG, "Unable to process item ", ex);
			return false;
		}
		
		return true;
	}
	
	public boolean removeComment(Context context, String itemId, String commentId, String userToken) {
		//
		// DELETE /api/item/:id/comment/:commentId
		String url = ServerUtils.buildRemoveCommentUrl(itemId, commentId, userToken);
		
		Log.i(TAG, "Removing comment with id " + commentId + " from url " + url);
		
		try {
			String jsonString = Utils.trim(delete(url, createAuthHeader(userToken)));
			JsonElement jsonItem = new JsonParser().parse(jsonString);
			Log.i(TAG, "Got back " + jsonItem);
		} catch (Exception ex) {
			Log.e(TAG, "Unable to process item ", ex);
			return false;
		}
		
		return true;
	}
	
	public boolean addComment(Context context, Comment comment, Item item, User user) {
		// 
		String userToken = user.getToken();
		String itemId = item.getId();
		String url = ServerUtils.buildAddCommentUrl(itemId, userToken);
		
		Log.i(TAG, "Adding comment for item " + itemId + " from url " + url);
		
		try {
			JsonObject commentObj = new JsonObject();
			commentObj.addProperty("comment", comment.getText());
			commentObj.addProperty("listingId", itemId);

			Gson gson = new GsonBuilder().create();
			String commentBody = gson.toJson(commentObj); //"{\"comment\":\"" + comment.getText() + "\",\"listingId\":\"" + itemId + "\"}";
			
			String jsonString = Utils.trim(post(url, createAuthHeader(userToken), commentBody));//getServerAuthToken(context), ServerUtils.generateCryptographicNonce()));
			
			if (!Utils.isEmpty(jsonString)) {
				//
				JsonObject obj = new JsonParser().parse(jsonString).getAsJsonObject();
				
				if ("ok".equalsIgnoreCase(obj.getAsJsonPrimitive("status").getAsString()) &&
					Utils.has(obj, "commentId")) {
					comment.setId(obj.getAsJsonPrimitive("commentId").getAsString());
				}
			}
			
			Log.i(TAG, "Got back " + jsonString);
		} catch (Exception ex) {
			Log.e(TAG, "Unable to process item ", ex);
			return false;
		}
		
		String alert = user.getNickname() + " commented on " + item.getTitle();
		
		JsonObject data = new JsonObject();
		data.addProperty("action", "comment");
		data.addProperty("display", alert);
		
		return enqueue(context, itemId, item.getSeller().getId(), userToken, data, alert);
	}
	
	/*private ArrayList<Item> getTestItems() {
		//
		ArrayList<Item> items = new ArrayList<Item>();
		
		Item item = new Item();
		item.setTitle("Test");
		item.addImage("https://s3.amazonaws.com/gogosing-images/4/59/459435366dc66d9f025d397ec80542aa");
		item.setLocation(new double[]{38.873257, -77.295729});
		item.setAddress("Rockville, MD");
		item.setPrice(59.99);
		item.setStatus("active");
		item.setBrand("Teletubbies");
		item.setCondition("Used");
		item.setQuantity(10);
		item.setDescription("This is simply a test object!");
		
		items.add(item);
		
		return items;
	}*/
	
	public ArrayList<Item> searchItems(Context context, String searchString, String lastItemId, Sorter sort, int limit, String userToken) {
		// TODO: Implement search item logic
		ArrayList<Item> items = new ArrayList<Item>();
		String jsonString = null;

		try {
			if (sort == null) {
				sort = new Sorter();
			}
			
			String url = ServerUtils.buildSearchItemsUrl(searchString, sort, limit, lastItemId, userToken);
			
			Log.i(TAG, "Retreiving shop data from: " + url);
			
			//jsonString = "[{\"otype\":\"listing\",\"data\":{\"status\":\"active\",\"location\":[38.808274,-76.831996],\"address\":\"Upper Marlboro, 20772\",\"LIKE-count\":0,\"COMMENT-count\":0,\"creation\":\"2014-06-20T02:49:03.635Z\"},\"_id\":\"53a3a11f49d53ddb795f2240\",\"assocs\":[{\"id1\":\"53a3a11f49d53ddb795f2240\",\"atype\":\"LIST\",\"id2\":\"53a3a11f49d53ddb795f223f\",\"_id\":\"53a3a11f49d53ddb795f2242\",\"object\":{\"otype\":\"item\",\"data\":{\"title\":\"test\",\"images\":[{\"origFileName\":\"/mnt/sdcard/DCIM/Camera/1386398470809.jpg\",\"uri\":\"https://s3.amazonaws.com/gogosing-images/2/9b/29b4b1e03d22841b20a4888385bdced6\"}],\"price\":109.88,\"condition\":null,\"size\":null,\"color\":null,\"quantity\":1,\"tags\":[\"toy\"],\"detail\":\"test toy\",\"shareOn\":null,\"creation\":\"2014-06-20T02:49:03.633Z\"},\"_id\":\"53a3a11f49d53ddb795f223f\"}}]}]";//getString(url, getServerAuthToken(context), ServerUtils.generateCryptographicNonce());
			jsonString = getString(url, createAuthHeader(userToken), null, null); //getServerAuthToken(context), ServerUtils.generateCryptographicNonce());
			
			//Log.i(TAG, "Received json: " + jsonString);
			
			items = parseItems(jsonString);
			
			//items = getTestItems();
			
			//Log.i(TAG, "Server " + url + " returned string: " + jsonString + "\nParsed:" + items);
		} catch (Exception ex) {
			Log.e(TAG, "Unable to process json " + jsonString, ex);
		}
		
		// TODO: remove once array of images available
		//for (Item item : items) {
		//	item.setImage1(item.getImage1());
		//}

		return items;
	}
	
	/**
	 * api/user/:id/watchLists
	 */
	public ArrayList<Item> getWatchedItems(Context context, String lastItemId, Sorter sort, int limit, String userToken) {
		// 
		ArrayList<Item> items = new ArrayList<Item>();
		String jsonString = null;

		try {
			if (sort == null) {
				sort = new Sorter();
			}
			
			String url = ServerUtils.buildGetWatchedItemsUrl(sort, limit, lastItemId, userToken);
			
			Log.i(TAG, "Retreiving watched items from: " + url);
			
			jsonString = getString(url, createAuthHeader(userToken), null, null);
			
			//Log.i(TAG, "Received json: " + jsonString);
			
			items = parseItems(jsonString);
			//items = getTestItems();
			
			for (Item item : items) {
				item.setWatched(true);
			}
			
			//Log.i(TAG, "Server " + url + " returned string: " + jsonString + "\nParsed:" + items);
		} catch (Exception ex) {
			Log.e(TAG, "Unable to process json " + jsonString, ex);
		}

		return items;
	}
	
	public ArrayList<Item> getUserItems(Context context, String userId, String lastItemId, int limit, String userToken) {
		//
		ArrayList<Item> items = new ArrayList<Item>();
		String jsonString = null;

		try {
			//
			String url = ServerUtils.buildUserItemsUrl(userId, limit, lastItemId, userToken);
			
			Log.i(TAG, "Retreiving shop data from: " + url);
			
			jsonString = getString(url, createAuthHeader(userToken), null, null);
			
			items = parseItems(jsonString);
		} catch (Exception ex) {
			Log.e(TAG, "Unable to process json " + jsonString, ex);
		}

		return items;
	}

	public ArrayList<Item> getItems(Context context, String lastItemId, Sorter sort, int limit, String userToken) {
		//
		ArrayList<Item> items = new ArrayList<Item>();
		String jsonString = null;

		try {
			if (sort == null) {
				sort = new Sorter();
			}
			
			String url = ServerUtils.buildGetItemsUrl(sort, limit, lastItemId, userToken);
			
			Log.i(TAG, "Retreiving shop data from: " + url);
			
			//jsonString = "[{\"otype\":\"listing\",\"data\":{\"status\":\"active\",\"location\":[38.808274,-76.831996],\"address\":\"Upper Marlboro, 20772\",\"LIKE-count\":0,\"COMMENT-count\":0,\"creation\":\"2014-06-20T02:49:03.635Z\"},\"_id\":\"53a3a11f49d53ddb795f2240\",\"assocs\":[{\"id1\":\"53a3a11f49d53ddb795f2240\",\"atype\":\"LIST\",\"id2\":\"53a3a11f49d53ddb795f223f\",\"_id\":\"53a3a11f49d53ddb795f2242\",\"object\":{\"otype\":\"item\",\"data\":{\"title\":\"test\",\"images\":[{\"origFileName\":\"/mnt/sdcard/DCIM/Camera/1386398470809.jpg\",\"uri\":\"https://s3.amazonaws.com/gogosing-images/2/9b/29b4b1e03d22841b20a4888385bdced6\"}],\"price\":109.88,\"condition\":null,\"size\":null,\"color\":null,\"quantity\":1,\"tags\":[\"toy\"],\"detail\":\"test toy\",\"shareOn\":null,\"creation\":\"2014-06-20T02:49:03.633Z\"},\"_id\":\"53a3a11f49d53ddb795f223f\"}}]}]";//getString(url, getServerAuthToken(context), ServerUtils.generateCryptographicNonce());
			jsonString = getString(url, createAuthHeader(userToken), null, null); //getServerAuthToken(context), ServerUtils.generateCryptographicNonce());
			
			//Log.i(TAG, "Received json: " + jsonString);
			
			items = parseItems(jsonString);
			//items = getTestItems();
			
			//Log.i(TAG, "Server " + url + " returned string: " + jsonString + "\nParsed:" + items);
		} catch (Exception ex) {
			Log.e(TAG, "Unable to process json " + jsonString, ex);
		}
		
		// TODO: remove once array of images available
		//for (Item item : items) {
		//	item.setImage1(item.getImage1());
		//}

		return items;
	}
	
	// Create authentication header
	private Map<String, String> createAuthHeader(String userToken) {
		//
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", "Bearer " + userToken);
		
		return headers;
	}
	
	private ArrayList<Item> parseItems(String jsonString) throws Exception {
		//
		JsonArray jsonItems = new JsonParser().parse(jsonString).getAsJsonArray();
		
		//Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDateDeserializer()).create();
		//Type type = new TypeToken<List<Item>>() {}.getType();
		ArrayList<Item> items = new ArrayList<Item>();//(List<Item>)gson.fromJson(jsonString, type));
		ArrayList<Comment> comments = new ArrayList<Comment>(); // TODO: load any comments
		
		//Log.i(TAG, "Parsed Items: " + jsonItems);
		
		for (JsonElement e : jsonItems) {
			//Log.i("Tag", "Elem: " + e);
			JsonObject obj = e.getAsJsonObject();
			
			if ("item".equalsIgnoreCase(obj.get("otype").getAsString())) {
				//
				Item currItem = new Item();
				items.add(currItem);

				/*if (Utils.has(obj, "updated")) {
					//
					currItem.setDateCreated(parseDate(obj.get("updated").getAsString()));
				} else*/ if (Utils.has(obj, "created")) {
					//
					currItem.setDateCreated(parseDate(obj.get("created").getAsString()));
				}
				
				// Parse listing data
				parseElement(obj, currItem);
				
				if (obj.has("assocs")) {
					JsonArray assocs = obj.getAsJsonArray("assocs");
					
					for (JsonElement assocElem : assocs) {
						// parse associations
						JsonObject assocObj = assocElem.getAsJsonObject().getAsJsonObject("object");
						
						Object value = parseElement(assocObj, currItem);
						
						if (value != null) {
							if (value instanceof Comment) {
								//
								if (currItem == null) {
									comments.add((Comment)value);
								} else {
									//
									if (!Utils.isEmpty(comments)) {
										currItem.getComments().addAll(comments);
									}
									
									currItem.addComment((Comment) value);
								}
							}
						}
					}
				}
			}
		}
		
		if (!Utils.isEmpty(comments)) {
			// assign comments into their associated item
		}
		
		return items;
	}
	
	private static Object parseElement(JsonObject assocObj, Item item) {
		//
		String oType = assocObj.get("otype").getAsString();
		if ("item".equalsIgnoreCase(oType)) {
			return parseItem(assocObj, item);
		} else if ("comment".equalsIgnoreCase(oType)) {
			// TODO: Parse comment 
		} else if ("listing".equalsIgnoreCase(oType)) {
			//
			HashMap<String, Object> listing = new HashMap<String, Object>();
			JsonObject object = assocObj.getAsJsonObject("data");
			
			if (object.has("status") && !object.get("status").isJsonNull()) {
				listing.put("status", object.get("status").getAsString());
			}
			
			if (object.has("location") && !object.get("location").isJsonNull()) {
				JsonArray locArr = object.get("location").getAsJsonArray();

				double lat = locArr.get(0).getAsDouble();
				double lon = locArr.get(1).getAsDouble();
				double[] locationArr = new double[]{lat, lon};
				
				listing.put("location", locationArr);
			}
			
			if (object.has("address") && !object.get("address").isJsonNull()) {
				listing.put("address", object.get("address").getAsString());
			}
			
			if (object.has("likeCount") && !object.get("likeCount").isJsonNull()) {
				listing.put("likeCount", object.get("likeCount").getAsInt());
			}
			
			if (object.has("commentCount") && !object.get("commentCount").isJsonNull()) {
				listing.put("commentCount", object.get("commentCount").getAsInt());
			}
			
			if (item != null) {
				item.setStatus(Status.valueOf((String)listing.get("status")));
				item.setLocation((double[]) listing.get("location"));
				item.setAddress((String) listing.get("address"));
				item.setLikeCount((Integer) listing.get("likeCount"));
				item.setCommentCount((Integer) listing.get("commentCount"));
			}
			
			return listing;
		}
		
		return null;
	}
	
	@SuppressWarnings("rawtypes")
	public static Item parseItem(JsonObject itemObj, Item item) {
		//
		if (item == null) {
			item = new Item();
		}

		item.setId(itemObj.get("_id").getAsString());

		JsonObject object = itemObj.getAsJsonObject("data");

		if (Utils.has(object, "title"))
			item.setTitle(object.get("title").getAsString());
		
		if (Utils.has(object, "brand"))
			item.setBrand(object.get("brand").getAsString());
		
		if (Utils.has(object,  "category")) 
			item.setCategory(object.get("category").getAsString());
		
		if (Utils.has(object, "age")) {
			//5�7yrs
			item.setAge(object.get("age").getAsString().replace("?", "�"));
		}
		
		if (Utils.has(object, "gender")) {
			try {
				item.setGender(Gender.valueOf(object.get("gender").getAsString()));
			} catch (Exception ex) {
				item.setGender(Gender.A);
			}
		}
		
		if (Utils.has(object, "price"))
			item.setPrice(object.get("price").getAsDouble());
		
		if (Utils.has(object, "condition"))
			item.setCondition(object.get("condition").getAsString());
		
		if (Utils.has(object, "size"))
			item.setSize(object.get("size").getAsString());
		
		if (Utils.has(object, "color"))
			item.setColor(object.get("color").getAsString());
		
		if (Utils.has(object, "quantity"))
			item.setQuantity(object.get("quantity").getAsInt());
		
		if (Utils.has(object, "detail"))
			item.setDetail(object.get("detail").getAsString());
		
		if (Utils.has(object, "distance"))
			item.setDistance(object.get("distance").getAsString());
		
		if (Utils.has(object, "isLiked"))
			item.setLiked(object.get("isLiked").getAsBoolean());
		
		if (Utils.has(object, "isWatched"))
			item.setWatched(object.get("isWatched").getAsBoolean());
		
		if (Utils.has(object, "status")) {
			try {
				item.setStatus(Status.valueOf(object.get("status").getAsString()));
			} catch (Exception ex) {
				item.setStatus(Status.active);
			}
		}
		
		if (Utils.has(object, "location")) {
			JsonArray locArr = object.get("location").getAsJsonArray();

			double lat = locArr.get(0).getAsDouble();
			double lon = locArr.get(1).getAsDouble();
			double[] locationArr = new double[]{lat, lon};
			
			item.setLocation(locationArr);
		}
		
		if (Utils.has(object, "address"))
			item.setAddress(object.get("address").getAsString());
		
		if (Utils.has(object, "likeCount"))
			item.setLikeCount(object.get("likeCount").getAsInt());
		
		if (Utils.has(object, "commentCount"))
			item.setCommentCount(object.get("commentCount").getAsInt());

		JsonArray data = null;
		
		if (Utils.has(object, "images") && object.get("images").isJsonArray()) {
			data = object.getAsJsonArray("images");
			item.setImages(null);
			// Parse images
			for (JsonElement dataEl : data) {
				JsonObject dataObj = dataEl.getAsJsonObject();
				if (Utils.has(dataObj, "uri"))
					item.addImage(dataObj.get("uri").getAsString() + "_" + s_imageSize + "x" + s_imageSize);
			}
		}

		// Parse tags
		if (Utils.has(object, "tags") && object.get("tags").isJsonArray()) {
			data = object.getAsJsonArray("tags");
			for (JsonElement dataEl : data) {
				if (!(dataEl instanceof JsonNull))
					item.addTag(dataEl.getAsString());
			}
		}

		// Parse Shared on
		if (Utils.has(object, "shareOn") && object.get("shareOn").isJsonArray()) {
			data = object.getAsJsonArray("shareOn");
			for (JsonElement dataEl : data) {
				if (!(dataEl instanceof JsonNull))
					item.addShareOn(dataEl.getAsString());
			}
		}
		
		// Check for associated objects
		if (itemObj.has("assocs") && itemObj.get("assocs").isJsonArray()) {
			JsonArray assocs = itemObj.getAsJsonArray("assocs");
		
			if (assocs != null) {
				for (JsonElement e : assocs) {
					JsonObject assocObj = e.getAsJsonObject().getAsJsonObject("object");
	
					Object value = parseElement(assocObj, item);
	
					if (value instanceof Map) {
						// listing
						Map itemListing = (Map) value;
	
						item.setStatus(Status.valueOf((String) itemListing.get("status")));
						item.setLocation((double[]) itemListing.get("location"));
						item.setAddress((String) itemListing.get("address"));
						item.setLikeCount((Integer) itemListing.get("likeCount"));
						item.setCommentCount((Integer) itemListing.get("commentCount"));
					}
				}
			}
		}
		
		// parse user
		if (Utils.has(itemObj, "assoc-list") && itemObj.get("assoc-list").isJsonArray()) {
			//
			JsonArray assocs = itemObj.getAsJsonArray("assoc-list");
			
			for (JsonElement e : assocs) {
				//
				JsonObject dataObj = e.getAsJsonObject();
				if (Utils.has(dataObj, "entity") && Utils.has(dataObj.get("entity").getAsJsonObject(), "data")) {
					//
					JsonObject entity = dataObj.get("entity").getAsJsonObject();
					User user = parseUser(entity);
					
					if (user != null) {
						item.setSeller(user);
						break;
					}
				}
			}
		}
		
		// Parse comments
		if (Utils.has(itemObj, "assoc-comment") && itemObj.get("assoc-comment").isJsonArray()) {
			//
			JsonArray comments = itemObj.getAsJsonArray("assoc-comment");
			
			for (JsonElement e : comments) {
				Comment c = parseComment(e);
				
				if (c != null) {
					item.addComment(c);
				}
			}
		}

		return item;
	}
	
	/**
	 * "assoc-comment": [{
        "_id": "53d3fbcc7f8a373e197fde12",
        "id1": "53d3fb790ae5b731191653c2",
        "atype": "comment",
        "id2": "53d3fbb30ae5b731191653c3",
        "created": "2014-07-26T19:04:44.617Z",
        "updated": "2014-07-26T19:04:44.617Z",
        "data": {
            "comment": "test"
        },
        "entity": {
            "_id": "53d3fb790ae5b731191653c2",
            "otype": "user",
            "created": "2014-07-26T19:03:21.392Z",
            "updated": "2014-07-26T19:03:21.392Z",
            "data": {
                "firstName": "Stone",
                "lastName": "Bat",
                "nickname": "stonebat",
                "photo": "http://graph.facebook.com/10203456970201558/picture?type=large",
                "pushToken": {
                    "device-type": "ios",
                    "device-id": "12345",
                    "device-token": "3366e2f4fb491714a3d4b109ec8c0f95fd549a30daa0c7ebe1116c7eedf4a5a0"
                }
            }
        }
    }
	 * @param e
	 * @return
	 */
	private static Comment parseComment(JsonElement e) {
		//
		Comment c = new Comment();
		
		JsonObject dataObj = e.getAsJsonObject();
		if (Utils.has(dataObj, "_id"))
			c.setId(dataObj.get("_id").getAsString());
		
		if (Utils.has(dataObj, "updated")) {
			//
			c.setDate(parseDate(dataObj.get("updated").getAsString()));
		} else if (Utils.has(dataObj, "created")) {
			//
			c.setDate(parseDate(dataObj.get("created").getAsString()));
		}
		
		if (Utils.has(dataObj, "data") && Utils.has(dataObj.get("data").getAsJsonObject(), "comment")) {
			//
			c.setText(dataObj.get("data").getAsJsonObject().get("comment").getAsString());
		}
		
		if (Utils.has(dataObj, "entity") && Utils.has(dataObj.get("entity").getAsJsonObject(), "data")) {
			//
			JsonObject entity = dataObj.get("entity").getAsJsonObject();
		
			c.setUser(parseUser(entity));
		}
		
		return c;
	}
	
	public static User parseUser(JsonObject entity) {
		return parseUser(entity, null);
	}
	
	public static Set<User> parseFollowing(JsonArray jsonUsers, User user) {
		//
		HashSet<User> following = new HashSet<User>();
		
		if (jsonUsers != null) {
			//
			for (JsonElement e : jsonUsers) {
				//
				User u = parseUser(e.getAsJsonObject(), null);
				
				if (u != null) {
					following.add(u);
					
					if (user != null) {
						user.follow(u);
					}
				}
			}
		}
		
		return following;
	}
	
	public static User parseUser(JsonObject entity, User user) {
		//
		if (Utils.has(entity, "data")) {
			//
			if (user == null) {
				user = new User();
			}

			if (Utils.has(entity, "_id")) {
				user.setId(entity.get("_id").getAsString());
			}
			
			if (Utils.has(entity, "created")) {
				// parse create date
				user.setDateCreated(parseDate(entity.get("created").getAsString()));
			}

			//JsonObject entity = entityParent.get("entity").getAsJsonObject();			
			JsonObject entityData = entity.get("data").getAsJsonObject();
			
			if (Utils.has(entityData, "firstName")) {
				user.setFirstName(entityData.get("firstName").getAsString());
			}
			
			if (Utils.has(entityData, "lastName")) {
				user.setLastName(entityData.get("lastName").getAsString());
			}
			
			if (Utils.has(entityData, "nickname")) {
				user.setNickname(entityData.get("nickname").getAsString());
			}
			
			if (Utils.has(entityData, "photo")) {
				user.setPhoto(entityData.get("photo").getAsString());
			}
			
			if (Utils.has(entityData, "token")) {
				user.setToken(entityData.get("token").getAsString());
			}
			
			if (Utils.has(entityData, "pushToken")) {
				JsonObject pushToken = entityData.get("pushToken").getAsJsonObject();
				
				PushToken token = new PushToken();
				
				if (Utils.has(entityData, "device-type")) {
					token.setDeviceType(PushToken.DeviceType.valueOf(pushToken.get("device-type").getAsString()));
				}
				
				if (Utils.has(entityData, "device-id")) {
					token.setDeviceId(pushToken.get("device-id").getAsString());
				}
				
				if (Utils.has(entityData, "device-token")) {
					token.setDeviceToken(pushToken.get("device-token").getAsString());
				}
			}
			
			return user;
		}
		
		return null;
	}
	
	private static Date parseDate(String dateStr) {
		//
		Date date = null;
		
		try {
			dateStr = dateStr.substring(0, dateStr.indexOf("."));
			date = dateFormat.parse(dateStr);
		} catch (ParseException ex) {
			Log.e(TAG, "Unable to parse date", ex);
		}
		
		return date;
	}
	
	public ArrayList<Seller> getSellers(Context context, String lastSellerId, int limit) {
		ArrayList<Seller> sellers = new ArrayList<Seller>();
		
		for (int i = 0; i < limit; i++) {
			Seller s = new Seller();
			s.setId("Seller" + i);
			s.setFollow(false);
			s.setFollowCount(100);
			s.setItemCount(150);
			sellers.add(s);
		}
		
		return sellers;
	}
	
	public static InputStream getImageInBytes(Context context, String url, int width, int height) {
		//
		if (url == null) {
			return null;
		}
		
		InputStream in = null;
		
		try {
        	Log.i(TAG, "-------- Start Download from " + url + " ---------");
			in = getInputStream(url/*ServerUtils.buildGetImageUrl(url, height, width)*/, getServerAuthToken(context), ServerUtils.generateCryptographicNonce());
		} catch(Exception ex) {
			//
			Log.e(TAG, "Unable to download image", ex);
		}
		
		return in;
	}
	
	public static Bitmap getImage(Context context, String url, int width, int height) {
		//
		if (url == null) {
			return null;
		}
		
		Bitmap img = null;
		
		try {
			Log.i(TAG, "Loading image from url: " + url);
			InputStream in = getInputStream(url, getServerAuthToken(context), ServerUtils.generateCryptographicNonce());
			
			img = ImageUtil.getScaledBitmap(in, width, height);
		} catch(Exception ex) {
			//
			Log.e(TAG, "Unable to download image", ex);
		}
		
		return img;
	}
	
	public ArrayList<Notification> getNotifications(Context context, String lastNotificationId, Sorter sort, int limit) {
		//
		List<Item> items = getItems(context, lastNotificationId, sort, limit, null);
		ArrayList<Notification> notifications = new ArrayList<Notification>(items.size());
		int like = 0;
		for (Item item : items) {
			//
			item.setLikeCount(like++);
			Notification n = new Notification();
			n.setItemId(item.getId());
			//n.setSeller(ShopApplication.getCurrentUser());
			n.setNotification("Sold Out!");
			notifications.add(n);
		}
		
		return notifications;
	}
	
	private static String getServerAuthToken(Context context) {
		//
		String token = ServerUtils.getServerToken(context);
		
		if (token == null) {
			// generate new key and store
			InputStream tis = getInputStream(ServerUtils.buildAuthTokenUrl(), null, ServerUtils.generateCryptographicNonce());
			
			if (tis != null) {
				JsonReader tokenReader = new JsonReader(new InputStreamReader(tis));
				
				try {
					tokenReader.beginObject();
					
					while (tokenReader.hasNext()) {
						//
						String name = tokenReader.nextName();

						if ("result".equals(name)) {
							token = tokenReader.nextString();

							Log.i(TAG, "Received token from server: " + token);
							ServerUtils.storeServerToken(context, token);
							break;
						}
					}
				} catch (Exception ex) {
					Log.e(TAG, "Unable to get server token", ex);
				} finally {
					try {
						tokenReader.close();
					} catch (Exception ex) {}
				}
			}
		}
		
		return token;
	}

	/*private class JsonDateDeserializer implements JsonDeserializer<Date> {

		@SuppressLint("SimpleDateFormat")
		@Override
		public Date deserialize(JsonElement json, Type typeOfT,
				JsonDeserializationContext context) throws JsonParseException {
			//
			String s = json.getAsJsonPrimitive().getAsString();
			s = s.substring(0, s.indexOf('.'));

			try {
				Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(s);
				return date;
			} catch (Exception ex) {
				Log.e(TAG, "Unable to parase date " + s, ex);
			}
			
			return null;
		}
	}*/
}
