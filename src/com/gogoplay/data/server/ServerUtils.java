package com.gogoplay.data.server;

import android.content.Context;
import android.net.Uri;
import android.net.Uri.Builder;
import android.util.Log;

import com.gogoplay.pref.Sorter;
import com.gogoplay.pref.Sorter.SortType;
import com.gogoplay.util.Utils;


public abstract class ServerUtils {
	//
	private static final String TAG = "ServerUtils";
	//private static Context context;
	private static final String s_BETA_DEMO_SERVER_URL = "http://162.243.48.226:3000";
	private static final String s_DEV_SERVER_URL = "http://gogo-play.com:3000"; //"http://104.131.186.21:3000"; //"http://104.131.237.166:3000"; //"http://97.107.141.249:3000";//"http://104.131.237.166:3000";//"http://174.129.41.115:3000";//"https://www.gogosing.org";//"http://192.168.8.107:3000";//"http://gogoplay.hopto.org:3000"; //"http://192.168.11.137:3000";//"http://web4.dcapp.org:3000";//"http://192.168.8.101:3000"; //http://gogoplay.hopto.org:3000";//"http://web4.dcapp.org:3000";
	public static final String s_DEFAULT_SERVER_URL = s_DEV_SERVER_URL;
	
	public static final String s_FACEBOOK_APP_ID = "447144942055395";
	public static final String s_FACEBOOK_APP_SECRET = "f6d55024bbfcd5ee404890853bdc6ae4";
	
	public static final int s_NUM_ITEMS_PER_PAGE = 24;
	
	public static final String s_SLASH = "/";
	
	// Rest Api Methods
	public static final String s_SIGN_UP_METHOD = "/api/user";
	public static final String s_UPDATE_TOKEN_METHOD = "/api/token";
	public static final String s_LOG_IN_METHOD = "/api/login";
	public static final String s_ME_METHOD = "/api/me";
	public static final String s_PUSH_TOKEN_METHOD = "/api/pushtoken";
	public static final String s_PREFERENCE_METHOD = "/api/preference";
	public static final String s_PUBLIC_ITEM_METHOD = "/api/public/item";
	public static final String s_ITEM_METHOD = "/api/item";
	public static final String s_ADD_ITEM_PARAM = "newProduct";
	public static final String s_LIKE_ITEM_METHOD = "/%%/like";
	public static final String s_ADD_COMMENT_METHOD = "/%%/comment";
	public static final String s_GET_WATCHED_ITEMS_METHOD = "/api/user/watchLists";
	public static final String s_GET_USER_ITEMS_METHOD = "/api/user/%%/items";
	public static final String s_WATCH_ITEM_METHOD = "/%%/watch";
	public static final String s_REPORT_ITEM_METHOD = "/api/abuse/%%";
	public static final String s_REPORT_USER_METHOD = "/api/abuse/%%";
	public static final String s_FOLLOW_METHOD = "/api/follow";
	public static final String s_FOLLOWER_USER_METHOD = s_FOLLOW_METHOD + "/%%";
	public static final String s_ENQUEUE = "/api/enqueueNotify";
	public static final String s_DEQUEUE = "/api/dequeue";
	public static final String s_TOKEN_METHOD = "/api/v1/token/generate";
	
	// Parameters
	public static final String s_USER_PARAM = "user";
	//public static final String s_USER_TOKEN_PARAM = "access_token";
	public static final String s_SORT_PARAM = "sort";
	public static final String s_LATITUDE_PARAM = "latitude";
	public static final String s_LONGITUDE_PARAM = "longitude";
	public static final String s_MAX_DISTANCE_PARAM = "maxDistance";
	public static final String s_LIMIT_PARAM = "limit";
	public static final String s_BOTTOM_PARAM = "bottom";//"page";
	public static final String s_SEARCH_PARAM = "textSearch";
	public static final String s_IMAGES_PARAM = ""; //"/images";
	
	// Authentication
	public static final String AUTH_HEADER_NAME = "Authorization";
	public static final String SERVER_TOKEN_NAME = "Token";
	public static final String CRYPTO_TOKEN_NAME = "GogoPlayAPI";
	public static final String TOKEN_FILENAME = "app_token";
	
	public static void init(Context context) {
		//
		//ServerUtils.context = context;
	}
	
	public static String getServerUrl() {
		//SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		
		String url = s_DEFAULT_SERVER_URL; //prefs.getString(context.getString(R.string.pref_server_url), s_DEFAULT_SERVER_URL);
		
		Log.i(TAG, "Server URL: " + url);
		return url;
	}
	
	public static String buildMeUrl() {
		//
		Uri serverUri = Uri.parse(getServerUrl() + s_ME_METHOD);
		
		return serverUri.toString();
	}
	
	public static String buildFollowingUrl() {
		//
		Uri serverUri = Uri.parse(getServerUrl() + s_FOLLOW_METHOD);
		
		return serverUri.toString();
	}
	
	public static String buildPushTokenUrl() {
		//
		Uri serverUri = Uri.parse(getServerUrl() + s_PUSH_TOKEN_METHOD);
		
		return serverUri.toString();
	}
	
	public static String buildPreferenceUrl() {
		//
		Uri serverUri = Uri.parse(getServerUrl() + s_PREFERENCE_METHOD);
		
		return serverUri.toString();
	}
	
	public static String buildTokenUrl() {
		Uri serverUri = Uri.parse(getServerUrl() + s_UPDATE_TOKEN_METHOD);
		
		return serverUri.toString();
	}
	
	public static String buildSignUpUrl(String userInfo) {
		Uri serverUri = Uri.parse(getServerUrl() + s_SIGN_UP_METHOD);
		
		if (!Utils.isEmpty(userInfo)) {
			Builder builder = serverUri.buildUpon();
			
			builder.appendQueryParameter(s_USER_PARAM, userInfo);
			
			serverUri = builder.build();
		}
		
		return serverUri.toString();
	}
	
	public static String buildLoginUrl(String userInfo) {
		Uri serverUri = Uri.parse(getServerUrl() + s_LOG_IN_METHOD);
		
		if (!Utils.isEmpty(userInfo)) {
			Builder builder = serverUri.buildUpon();
			
			builder.appendQueryParameter(s_USER_PARAM, userInfo);
			
			serverUri = builder.build();
		}
		
		return serverUri.toString();
	}
	
	public static String buildEnqueueUrl(String userToken) {
		//
		Uri serverUri = Uri.parse(getServerUrl() + s_ENQUEUE);
		
		return serverUri.toString();
	}
	
	public static String buildDequeueUrl(String userToken) {
		Uri serverUri = Uri.parse(getServerUrl() + s_DEQUEUE);
		
		return serverUri.toString();
	}
	
	public static String buildSearchItemsUrl(String searchText, Sorter sort, int limit, String lastItemId, String userToken) {
		//
		Uri serverUri = Uri.parse(getServerUrl() + buildItemUrl(userToken, ""));

		if (searchText != null || sort != null || limit > 0 || lastItemId != null) {
			Builder builder = serverUri.buildUpon();

			if (searchText != null) {
				builder.appendQueryParameter(s_SEARCH_PARAM, searchText);
			}
			
			if (lastItemId != null) {
				builder.appendQueryParameter(s_BOTTOM_PARAM, lastItemId);
			}
			
			if (sort != null) {
				// TODO - resolve this, search is not working with distance sort
				buildSort(builder, sort);
			}

			if (limit > 0) {
				builder.appendQueryParameter(s_LIMIT_PARAM, limit + "");
			}
			

			serverUri = builder.build();
		}
		
		return serverUri.toString();
	}
	
	public static String buildUserItemsUrl(String userId, int limit, String lastItemId, String userToken) {
		//
		Uri serverUri = getMethodUri(s_GET_USER_ITEMS_METHOD, userId);

		if (limit > 0 || lastItemId != null) {
			Builder builder = serverUri.buildUpon();

			if (lastItemId != null) {
				builder.appendQueryParameter(s_BOTTOM_PARAM, lastItemId);
			}

			if (limit > 0) {
				builder.appendQueryParameter(s_LIMIT_PARAM, limit + "");
			}

			serverUri = builder.build();
		}
		
		return serverUri.toString();
	}
	
	public static String buildGetItemsUrl(Sorter sort, int limit, String lastItemId, String userToken) {
		//
		Uri serverUri = Uri.parse(getServerUrl() + buildItemUrl(userToken, ""));

		if (sort != null || limit > 0 || lastItemId != null) {
			Builder builder = serverUri.buildUpon();

			if (lastItemId != null) {
				builder.appendQueryParameter(s_BOTTOM_PARAM, lastItemId);
			}
			
			if (sort != null) {
				buildSort(builder, sort);
			}

			if (limit > 0) {
				builder.appendQueryParameter(s_LIMIT_PARAM, limit + "");
			}

			serverUri = builder.build();
		}
		
		return serverUri.toString();
	}
	
	public static String buildGetWatchedItemsUrl(Sorter sort, int limit, String lastItemId, String userToken) {
		//
		Uri serverUri = Uri.parse(getServerUrl() + s_GET_WATCHED_ITEMS_METHOD);

		if (sort != null || limit > 0 || lastItemId != null) {
			Builder builder = serverUri.buildUpon();

			if (lastItemId != null) {
				builder.appendQueryParameter(s_BOTTOM_PARAM, lastItemId);
			}
			
			if (sort != null) {
				buildSort(builder, sort);
			}

			if (limit > 0) {
				builder.appendQueryParameter(s_LIMIT_PARAM, limit + "");
			}

			serverUri = builder.build();
		}
		
		return serverUri.toString();
	}
	
	private static void buildSort(Builder builder, Sorter sort) {
		//
		builder.appendQueryParameter(s_SORT_PARAM, sort.type.toString());
		
		if (sort.type == SortType.distance) {
			// add distance params
			builder.appendQueryParameter(s_LATITUDE_PARAM, sort.location.getLatitude() + "");
			builder.appendQueryParameter(s_LONGITUDE_PARAM, sort.location.getLongitude() + "");
			builder.appendQueryParameter(s_MAX_DISTANCE_PARAM, sort.radius + "");
		}
	}
	
	public static String buildGetItemUrl(String itemId, String userToken) {
		//
		Uri serverUri = Uri.parse(getServerUrl() + buildItemUrl(userToken, "") + "/" + itemId);
		return serverUri.toString();
	}
	
	public static String buildAddCommentUrl(String itemId, String userToken) {
		//
		Uri serverUri = getMethodUri(buildItemUrl(userToken, s_ADD_COMMENT_METHOD), itemId);
		
		/*if (!Utils.isEmpty(userToken)) {
			Builder builder = serverUri.buildUpon();
			
			builder.appendQueryParameter(s_USER_TOKEN_PARAM, userToken);
			serverUri = builder.build();
		}*/
		
		return serverUri.toString();
	}
	
	public static String buildRemoveCommentUrl(String itemId, String commentId, String userToken) {
		//
		Uri serverUri = getMethodUri(buildItemUrl(userToken, s_ADD_COMMENT_METHOD), itemId);
		
		serverUri = serverUri.buildUpon().appendPath(commentId).build();
		
		return serverUri.toString();
	}
	
	private static Uri getMethodUri(String type, String value) {
		return Uri.parse(getServerUrl() + type.replace("%%", value));
	}
	
	/**
	 * Determines whether to build the public or the 
	 * private uri for the item
	 * 
	 * @param userToken - the users token
	 * @param itemMethod - the lower-level item method to call
	 * @return the appropriate url for the item
	 */
	public static String buildItemUrl(String userToken, String itemMethod) {
		//
		StringBuilder url = new StringBuilder();
		
		if (!Utils.isEmpty(userToken)) {
			url.append(s_ITEM_METHOD);
		} else {
			url.append(s_PUBLIC_ITEM_METHOD);
		}
		
		return url.append(itemMethod).toString();
	}
	
	public static String buildReportItemUrl(String itemId) {
		//
		Uri serverUri = getMethodUri(s_REPORT_ITEM_METHOD, itemId);
		
		return serverUri.toString();
	}
	
	public static String buildReportUserUrl(String userId) {
		//
		Uri serverUri = getMethodUri(s_REPORT_USER_METHOD, userId);
		
		return serverUri.toString();
	}
	
	public static String buildFollowUserUrl(String userId) {
		//
		Uri serverUri = getMethodUri(s_FOLLOWER_USER_METHOD, userId);
		
		return serverUri.toString();
	}
	
	public static String buildLikeItemUrl(String itemId, String userToken) {
		//
		Uri serverUri = getMethodUri(buildItemUrl(userToken, s_LIKE_ITEM_METHOD), itemId);
		
		/*if (!Utils.isEmpty(userToken)) {
			Builder builder = serverUri.buildUpon();
			
			builder.appendQueryParameter(s_USER_TOKEN_PARAM, userToken);
			serverUri = builder.build();
		}*/
		
		return serverUri.toString();
	}
	
	public static String buildWatchItemUrl(String itemId, String userToken) {
		//
		Uri serverUri = getMethodUri(buildItemUrl(userToken, s_WATCH_ITEM_METHOD), itemId);
		
		/*if (!Utils.isEmpty(userToken)) {
			Builder builder = serverUri.buildUpon();
			
			builder.appendQueryParameter(s_USER_TOKEN_PARAM, userToken);
			serverUri = builder.build();
		}*/
		
		return serverUri.toString();
	}
	
	public static String buildGetImageUrl(String uri, int width, int height) {
		//
		Uri serverUri = Uri.parse(/*getServerUrl() + s_IMAGES_PARAM + "/" +*/ uri);
		String theUri = serverUri.toString();
		
		if (height > 0 && width > 0) {
			theUri = serverUri.toString() + "_" + width;
		}
		
		return theUri;
	}
	
	public static String buildAddItemUrl(String userToken) {
		//
		Uri serverUri = Uri.parse(getServerUrl() + buildItemUrl(userToken, ""));
		
		/*if (!Utils.isEmpty(json)) {
			Builder builder = serverUri.buildUpon();
		 
			//builder.appendQueryParameter(s_USER_TOKEN_PARAM, userToken);
			builder.appendQueryParameter(s_ADD_ITEM_PARAM, json);
			serverUri = builder.build();
		}*/
		
		return serverUri.toString();// + "?" + s_ADD_ITEM_PARAM + "=\"" + json + "\"";
	}
	
	public static String buildEditItemUrl(String itemId, String userToken) {
		//
		//getMethodUri(buildItemUrl(userToken, s_ADD_COMMENT_METHOD), itemId);
		Uri serverUri = Uri.parse(getServerUrl() + buildItemUrl(userToken, "") + s_SLASH + itemId);
		
		return serverUri.toString();
	}

	public static String buildAuthTokenUrl() {
		//
		Uri serverUri = Uri.parse(getServerUrl() + s_TOKEN_METHOD);
		return serverUri.toString();
	}

	public static String buildTokenHeader(String serverToken) {
		// 
		return ("Token token=\"" + serverToken + "\"");
	}
	
	public static String generateCryptographicNonce() {
		//
		/*String add = "aqrxv";
	    String sub = "znmdc";
	    String mul = "bwyps";
		
	    long currentTime = new Date().getTime();
	    long originCurrentTime = currentTime;
	    
	    StringBuilder command = new StringBuilder();
	    
	    Random random = new Random();
	    int opLength = 5 + random.nextInt(6);
	    
	    for (int i = 0; i < opLength; i++) {
	    	char opChar = 0;
	        int operand = 0;
	        int opType = random.nextInt() % 3;
	        
	        switch (opType)
	        {
	        	case 0:
	        		
	        		opChar = add.charAt(random()%5);
	                operand = random() % 9000000;
	                Log.i(TAG, "OP: " + currentTime + " += " + operand);
	                currentTime += operand;
	        		break;
	        	case 1:
	        		
	        		opChar = sub.charAt(random()%5);
	                operand = (int) (random() % (originCurrentTime / 10 - 1));
	                Log.i(TAG, "OP: " + currentTime + " -= " + operand);
	                currentTime -= operand;
	        		break;
	        	case 2:
	        		
	        		opChar = mul.charAt(random()%5);
	                operand = 1 + random() % 100;
	                Log.i(TAG, "OP: " + currentTime + " *= " + operand);
	                currentTime *= operand;
	        		break;
	        }
	        
	        command.append(opChar).append(operand);
	    }
	    
	    long randomNumber = (1000000 + Math.abs(random.nextLong() % 8000000)) * 402 + 91827443;
	    
	    String toMD5 = command + "GogoPlayYayer#!4^&" + currentTime;
	    MessageDigest md5;
	    String md5Hash = "";
	    
		try {
			md5 = MessageDigest.getInstance("MD5");
			md5.update(toMD5.getBytes());
			byte[] digest = md5.digest();
			StringBuffer sb = new StringBuffer();
			
	        for (int i = 0; i < digest.length; ++i) {
	          sb.append(Integer.toHexString((digest[i] & 0xFF) | 0x100).substring(1,3));
	        }
	        
			md5Hash = sb.toString();
		} catch (NoSuchAlgorithmException e) {
			Log.e(TAG, "Unable to generate md5 hash", e);
		}
	    
	    String token = originCurrentTime + "-" + command + "-" + md5Hash + "-" + randomNumber;
	    
	    Log.i(TAG, "token = " + token + " currentTime=" + currentTime);
	    return token;*/
		
		return "";
	}
	
	/*private static int random() {
		//
		Random random = new Random();
		return Math.abs(random.nextInt());
	}*/
	
	public static void storeServerToken(Context context, String key) {
		//
		//FileUtil.store(context, TOKEN_FILENAME, key);
	}
	
	public static String getServerToken(Context context) {
		//
		//return FileUtil.load(context, TOKEN_FILENAME);
		return "";
	}
}
