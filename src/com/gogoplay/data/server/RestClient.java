package com.gogoplay.data.server;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.util.Log;

import com.gogoplay.util.Utils;

/**
 * Generic Rest client
 * 
 * @author iudoka
 */
public class RestClient {
	private static final String TAG = "RestClient";

	private static final int TIMEOUT = 50000;
	
	public static String convertStreamToString(InputStream is) {
		if (is == null)
			return "";

		/*
		 * To convert the InputStream to String we use the
		 * BufferedReader.readLine() method. We iterate until the BufferedReader
		 * return null which means there's no more data to read. Each line will
		 * appended to a StringBuilder and returned as String.
		 */
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}
	
	public static InputStream getInputStream(String url, String serverToken, String cryptoKey) {
		return getInputStream(url, null, serverToken, cryptoKey);
	}
	
	public static InputStream getInputStream(String url, Map<String, String> headers, String serverToken, String cryptoKey) {
		//
		InputStream instream = null;
		HttpClient httpclient = new DefaultHttpClient(getParams());
		
		// Prepare a request object
		HttpGet getRequest = new HttpGet(url);
		
		// Old Rails Server Security Code
		/*if (serverToken != null) {
			getRequest.setHeader(ServerUtils.AUTH_HEADER_NAME, ServerUtils.buildTokenHeader(serverToken));
		}
		
		getRequest.setHeader(ServerUtils.CRYPTO_TOKEN_NAME, cryptoKey);*/
		
		if (headers != null) {
			for (Map.Entry<String, String> header : headers.entrySet()) {
				//
				getRequest.setHeader(header.getKey(), header.getValue());
			}
		}

		// Execute the request
		HttpResponse response;
		try {
			response = httpclient.execute(getRequest);
			
			// Examine the response status
			Log.i(TAG, response.getStatusLine().toString());
			if (response.getStatusLine().getStatusCode() == HttpURLConnection.HTTP_OK) {
				// Get hold of the response entity
				HttpEntity entity = response.getEntity();
				
				// If the response does not enclose an entity, there is no need
				// to worry about connection release
				if (entity != null) {
					instream = entity.getContent();
				}
			}
		} catch (ClientProtocolException e) {
			Log.e(TAG, "Unable to process url " + url, e);
		} catch (IOException e) {
			Log.e(TAG, "Unable to process url " + url, e);
		}

		return instream;
	}
	
	public static String getString(String url, String serverToken, String cryptoKey) {
		//
		return getString(url, null, serverToken, cryptoKey);
	}
	
	public static String getString(String url, Map<String, String> headers, String serverToken, String cryptoKey) {
		return convertStreamToString(getInputStream(url, headers, serverToken, cryptoKey));
	}
	
	public class FormBodyPart {
		String key;
		String filename;
		Object content;
		ContentType contentType;
		
		public FormBodyPart(String key, String filename, Object content, ContentType contentType) {
			super();
			this.key = key;
			this.filename = filename;
			this.content = content;
			this.contentType = contentType;
		}
	}
	
	public static String delete(String serverUrl, Map<String, String> headers) {
		//
		HttpClient httpclient = new DefaultHttpClient(getParams());
		HttpDelete httpdelete = new HttpDelete(serverUrl);
		
		try {
			//
			if (headers != null) {
				for (Map.Entry<String, String> header : headers.entrySet()) {
					//
					httpdelete.setHeader(header.getKey(), header.getValue());
				}
			}
			
			HttpResponse response = httpclient.execute(httpdelete);

			if (response.getStatusLine().getStatusCode() == HttpURLConnection.HTTP_OK) {
				//
				HttpEntity entity = response.getEntity();
				return IOUtils.toString(entity.getContent(), "UTF-8");
			} else {
				Log.e(TAG, "Unable to delete to " + serverUrl + " : " + response.getStatusLine());
			}
		} catch (ClientProtocolException e) {
			Log.e(TAG, "Unable to delete", e);
		} catch (IOException e) {
			Log.e(TAG, "Unable to delete", e);
		}
		
		return null; //"{\"error\": {\"message\": \"Message describing the error\",\"type\": \"OAuthException\",\"code\": \"190\",\"error_subcode\": 460 } }";
		
		//return null;
	}
	
	public static HttpParams getParams() {
		//
		HttpParams httpParameters = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParameters, TIMEOUT);
		HttpConnectionParams.setSoTimeout(httpParameters, TIMEOUT);
		
		return httpParameters;
	}
	
	public static String put(String serverUrl, Map<String, String> headers, String body) {
		//
		HttpClient httpclient = new DefaultHttpClient(getParams());
		HttpPut httpput = new HttpPut(serverUrl);
		
		try {
			//
			if (headers != null) {
				for (Map.Entry<String, String> header : headers.entrySet()) {
					//
					httpput.setHeader(header.getKey(), header.getValue());
				}
			}
			
			if (!Utils.isEmpty(body)) {
				//
				BasicHttpEntity entity = new BasicHttpEntity();
				
				byte[] bytes = body.getBytes();
				ByteArrayInputStream stream = new ByteArrayInputStream(bytes);
				entity.setContent(stream);
				entity.setContentLength(bytes.length);
				entity.setContentType("application/json");
				entity.setChunked(false);
			
				httpput.setEntity(entity);
			}
			
			HttpResponse response = httpclient.execute(httpput);

			if (response.getStatusLine().getStatusCode() == HttpURLConnection.HTTP_OK) {
				//
				HttpEntity entity = response.getEntity();
				return IOUtils.toString(entity.getContent(), "UTF-8");
			} else {
				Log.e(TAG, "Unable to put to " + serverUrl + " : " + response.getStatusLine());
			}
		} catch (ClientProtocolException e) {
			Log.e(TAG, "Unable to put", e);
		} catch (IOException e) {
			Log.e(TAG, "Unable to put", e);
		}
		
		//return "{\"error\": {\"message\": \"Message describing the error\",\"type\": \"OAuthException\",\"code\": \"190\",\"error_subcode\": 460 } }";
		
		return null;
	}
	
	public static String post(String serverUrl, Map<String, String> headers, String body) {
		//
		HttpClient httpclient = new DefaultHttpClient(getParams());
		HttpPost httppost = new HttpPost(serverUrl);
		
		try {
			//
			if (headers != null) {
				for (Map.Entry<String, String> header : headers.entrySet()) {
					//
					httppost.setHeader(header.getKey(), header.getValue());
				}
			}
			
			if (!Utils.isEmpty(body)) {
				//
				BasicHttpEntity entity = new BasicHttpEntity();
				
				byte[] bytes = body.getBytes();
				ByteArrayInputStream stream = new ByteArrayInputStream(bytes);
				entity.setContent(stream);
				entity.setContentLength(bytes.length);
				entity.setContentType("application/json");
				entity.setChunked(false);
			
				httppost.setEntity(entity);
			}
			
			HttpResponse response = httpclient.execute(httppost);

			if (response.getStatusLine().getStatusCode() == HttpURLConnection.HTTP_OK) {
				//
				HttpEntity entity = response.getEntity();
				return IOUtils.toString(entity.getContent(), "UTF-8");
			} else {
				Log.e(TAG, "Unable to post to " + serverUrl + " : " + response.getStatusLine());
			}
		} catch (ClientProtocolException e) {
			Log.e(TAG, "Unable to post", e);
		} catch (IOException e) {
			Log.e(TAG, "Unable to post", e);
		}
		
		//return "{\"error\": {\"message\": \"Message describing the error\",\"type\": \"OAuthException\",\"code\": \"190\",\"error_subcode\": 460 } }";
		
		return null;
	}
	
	public static String uploadMultipart(String serverUrl, Map<String, String> headers, List<FormBodyPart> parts, String serverToken, String cryptoKey, boolean update) {
		//

		HttpClient httpclient = new DefaultHttpClient(getParams());
		HttpEntityEnclosingRequestBase httpMethod = null;
		
		if (!update) {
			httpMethod = new HttpPost(serverUrl);
		} else {
			httpMethod = new HttpPut(serverUrl);
		}

		try {
			if (headers != null) {
				for (Map.Entry<String, String> header : headers.entrySet()) {
					//
					httpMethod.setHeader(header.getKey(), header.getValue());
				}
			}
			
			MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
			entityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			
			for (FormBodyPart part : parts) {
				//
				String key = part.key;
				Object content = part.content;
				
				if (content instanceof String) {
					entityBuilder.addTextBody(key, (String) content);
				} else if (content instanceof File) {
					entityBuilder.addBinaryBody(key, (File)content);
				} else if (content instanceof InputStream) {
					entityBuilder.addBinaryBody(key, (InputStream) content, part.contentType, part.filename);
				} else if (content instanceof byte[]) {
					entityBuilder.addBinaryBody(key, (byte[])content, part.contentType, part.filename);
				}
			}
			
			HttpEntity entity = entityBuilder.build();
			httpMethod.setEntity(entity);
			
			/* Old Rails Server Key
			if (serverToken != null) {
				httppost.setHeader(ServerUtils.AUTH_HEADER_NAME, ServerUtils.buildTokenHeader(serverToken));
			}
			
			httppost.setHeader(ServerUtils.CRYPTO_TOKEN_NAME, cryptoKey);*/
			
			HttpResponse response = httpclient.execute(httpMethod);

			if (response.getStatusLine().getStatusCode() == HttpURLConnection.HTTP_OK) {
				//
				HttpEntity resEntity = response.getEntity();
	
				BufferedReader reader = new BufferedReader(new InputStreamReader(
						resEntity.getContent(), "UTF-8"));
				String sResponse;
				StringBuilder newItemId = new StringBuilder();
	
				while ((sResponse = reader.readLine()) != null) {
					newItemId = newItemId.append(sResponse);
				}
				Log.i("test", "New Item Id: " + newItemId);
				return newItemId.toString();
			} else {
				Log.e(TAG, "Unable to upload: " + response.getStatusLine());
			}
			
		} catch (ClientProtocolException e) {
			Log.e(TAG, "Unable to upload", e);
		} catch (IOException e) {
			Log.e(TAG, "Unable to upload", e);
		}
		
		return null;
	}
}