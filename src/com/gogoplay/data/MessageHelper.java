package com.gogoplay.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Persists Notifications
 * @author iudoka
 *
 */
public class MessageHelper extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 2;
	
	public static final String MESSAGE_DATABASE_NAME = "message.db";
	public static final String MESSAGE_ID_COLUMN = "_id";
	public static final String MESSAGE_UUID_COLUMN = "uuid";
	public static final String MESSAGE_ITEM_ID_COLUMN = "item_id";
	public static final String MESSAGE_OFFER_COLUMN = "offer";
	public static final String MESSAGE_FROM_ID_COLUMN = "from_id";
	public static final String MESSAGE_FROM_COLUMN = "from_user";
	public static final String MESSAGE_TO_COLUMN = "to_user";
	public static final String MESSAGE_ACTION_COLUMN = "action";
	public static final String MESSAGE_NOTIF_COLUMN = "notif";
	public static final String MESSAGE_MSG_COLUMN = "msg";
	public static final String MESSAGE_DATE_COLUMN = "date";
	
	public static final String MESSAGE_TABLE = "offers";

	private static final String TABLE_CREATE = "CREATE TABLE "
			+ MESSAGE_TABLE + " (" 
			+ MESSAGE_ID_COLUMN + " INTEGER PRIMARY KEY AUTOINCREMENT,"
			+ MESSAGE_UUID_COLUMN + " TEXT,"
			+ MESSAGE_ITEM_ID_COLUMN + " TEXT,"
			+ MESSAGE_OFFER_COLUMN + " TEXT,"
			+ MESSAGE_FROM_ID_COLUMN + " TEXT,"
			+ MESSAGE_FROM_COLUMN + " TEXT,"
			+ MESSAGE_TO_COLUMN + " TEXT,"
			+ MESSAGE_ACTION_COLUMN + " TEXT,"
			+ MESSAGE_NOTIF_COLUMN + " TEXT,"
			+ MESSAGE_MSG_COLUMN + " TEXT,"
			+ MESSAGE_DATE_COLUMN + " TEXT);";

	public MessageHelper(Context context) {
		super(context, MESSAGE_DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		//
		db.execSQL(TABLE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(MessageHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + MESSAGE_TABLE);
		onCreate(db);
	}

	public void emptyTable() {
		//
		getWritableDatabase().execSQL("delete from "+ MESSAGE_TABLE);
	}
	
	
}
