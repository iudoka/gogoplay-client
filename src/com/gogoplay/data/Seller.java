package com.gogoplay.data;

import java.io.Serializable;
import java.util.List;

public class Seller implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9011145142830094131L;

	private String id;
	private User user;
	private List<Item> items;
	private boolean follow;
	private int followCount;
	private int itemCount;

	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public boolean isFollow() {
		return follow;
	}

	public void setFollow(boolean follow) {
		this.follow = follow;
	}

	public int getFollowCount() {
		return followCount;
	}

	public void setFollowCount(int followCount) {
		this.followCount = followCount;
	}

	public int getItemCount() {
		return itemCount;
	}

	public void setItemCount(int itemCount) {
		this.itemCount = itemCount;
	}
}
