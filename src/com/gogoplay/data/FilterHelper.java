package com.gogoplay.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class FilterHelper extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 2;

	public static final String FILTER_DATABASE_NAME = "filters.db";
	public static final String FILTER_ID_COLUMN = "_id";
	public static final String FILTER_NAME_COLUMN = "name";
	public static final String FILTER_AGE_COLUMN = "age";
	public static final String FILTER_GENDER_COLUMN = "gender";
	public static final String FILTER_BRAND_COLUMN = "brand";
	public static final String FILTER_LOCATION_COLUMN = "location";
	public static final String FILTER_PRICE_MIN_COLUMN = "price_min";
	public static final String FILTER_PRICE_MAX_COLUMN = "price_max";
	public static final String FILTER_CATEGORY_COLUMN = "category";
	public static final String FILTER_CONDITION_COLUMN = "condition";
	public static final String FILTER_SELECTED_COLUMN = "selected";
	
	public static final String FILTER_TABLE = "filters";

	private static final String TABLE_CREATE = "CREATE TABLE "
			+ FILTER_TABLE + " (" 
			+ FILTER_ID_COLUMN + " INTEGER PRIMARY KEY AUTOINCREMENT,"
			+ FILTER_NAME_COLUMN + " TEXT,"
			+ FILTER_AGE_COLUMN + " TEXT,"
			+ FILTER_GENDER_COLUMN + " TEXT,"
			+ FILTER_BRAND_COLUMN + " TEXT,"
			+ FILTER_LOCATION_COLUMN + " TEXT,"
			+ FILTER_PRICE_MIN_COLUMN + " INTEGER,"
			+ FILTER_PRICE_MAX_COLUMN + " INTEGER,"
			+ FILTER_CATEGORY_COLUMN + " TEXT,"
			+ FILTER_CONDITION_COLUMN + " TEXT,"
			+ FILTER_SELECTED_COLUMN + " INTEGER);";

	public FilterHelper(Context context) {
		super(context, FILTER_DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		//
		db.execSQL(TABLE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(FilterHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + FILTER_TABLE);
		onCreate(db);
	}

	public void emptyTable() {
		//
		getWritableDatabase().execSQL("delete from "+ FILTER_TABLE);
	}
	
	
}
