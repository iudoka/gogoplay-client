package com.gogoplay;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Configuration;
import android.location.Location;
import android.location.LocationListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gogoplay.data.DataService.UserStateListener;
import com.gogoplay.data.User;
import com.gogoplay.notification.PushNotificationHelper;
import com.gogoplay.pref.NotificationPreference;
import com.gogoplay.pref.Sorter;
import com.gogoplay.ui.MutableDisplay;
import com.gogoplay.ui.MutableDisplay.DisplayType;
import com.gogoplay.ui.NavAdapter;
import com.gogoplay.ui.ShopActionBarActivity;
import com.gogoplay.ui.ShopApplication;
import com.gogoplay.ui.ShopFragment;
import com.gogoplay.ui.widget.ListItem;
import com.gogoplay.ui.widget.ListItem.Type;
import com.gogoplay.util.LocationUtil;

@SuppressLint("UseSparseArrays")
public class Home extends ShopActionBarActivity {
	//
	private static final String s_NEW_USER_PREF = "newUser";
	private static final String s_LEARNED_DRAWER_NAV = "learnedDrawer";
	private static final int NAVIGATE_TO = 1;
	
	private static final int SEARCH = 1;
	private static final int HOME = 2;
	private static final int NOTIFICATION = 3;
	private static final int WISH_LIST = 4;
	private static final int ADD_LISTING = 5;
	//private static final int SORTER_REQUEST = 99;

	private boolean clearCache = true;
	
	// Navigation Drawer
	private DrawerLayout drawerLayout;
	private ArrayList<ListItem> navItems;
	private ListView drawerList;
	private ActionBarDrawerToggle drawerToggle;
	private int currSelection = HOME;

	// Fragments
	private Search searchView;
	private Account accountView;
	private Products productsView;
	private Notifications notificationsView;
	private Sellers sellersView;
	private WishList wishListView;
	private AddProduct addProductView;
	private ShopFragment currentFragment;
	private Handler pushRegisterHandler;
	private NavHandler handler;
	private NavAdapter navAdapter;
	
	// Notification
	private PushNotificationHelper notificationHelper;
	private OnSharedPreferenceChangeListener prefListener;
	// Location
	private LocationUtil locationUtil;
	private boolean waitingForService;
	//private boolean checkSorter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home);

		//checkSorter = true;
		createUI(savedInstanceState);
		setupListeners();
	}

	public void onResume() {
		super.onResume();

		notificationHelper.checkPlayServices();
	}

	@SuppressLint("HandlerLeak")
	private void createUI(Bundle savedInstanceState) {
		//super.createUI();

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		
		prefListener = new OnSharedPreferenceChangeListener() {
			
			@Override
			public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
				// 
				if (key.contains("notif")) {
					//
					if (isDataServiceConnected()) {
						//
						boolean like = sharedPreferences.getBoolean("pref_like_notif", true);
						boolean comment = sharedPreferences.getBoolean("pref_comment_notif", true);
						boolean email = sharedPreferences.getBoolean("pref_buy_notif", true);
						boolean watch = sharedPreferences.getBoolean("pref_watch_notif", true);
						boolean follow = sharedPreferences.getBoolean("pref_follow_notif", true);
						
						NotificationPreference notif = new NotificationPreference(like, comment, email, watch, follow);
						getDataService().updateNotificationPreferences(Home.this, notif, new Handler() {
							@Override
							public void handleMessage(Message msg) {
								//
								if (msg.getData() != null) {
									//
									boolean updated = msg.getData().getBoolean("updated");
									
									if (!updated) {
										Toast.makeText(Home.this, "Unable to update preferences",
												Toast.LENGTH_LONG).show();
									}
								}
							}
						});
					}
				}
			}
		};
		
		prefs.registerOnSharedPreferenceChangeListener(prefListener);
		
		handler = new NavHandler(Looper.getMainLooper());
		pushRegisterHandler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				//
				String regId = msg.getData().getString("registrationId");
				Log.i(getTag(), "Received push notification registration id: " + regId);


                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE); 
                ClipData clip = ClipData.newPlainText("gcmId", regId);
                clipboard.setPrimaryClip(clip);
			}
		};

		// load the fragments
		restoreFragments(savedInstanceState);
		createDrawer(savedInstanceState);
	}

	@SuppressLint("HandlerLeak")
	private void createDrawer(Bundle savedInstanceState) {
		//
		drawerLayout = (DrawerLayout) findViewById(R.id.nav_drawer);
		drawerList = (ListView) findViewById(R.id.drawer_list);

		navItems = new ArrayList<ListItem>();
		
		navItems.add(new ListItem("", R.drawable.anonymous, R.drawable.ic_menu_settings, ListItem.Type.Profile, accountView));
		//navItems.add(new NavItem("Buy", -1, -1, Type.Header, null));
		navItems.add(new ListItem(searchView.getTitle(), R.drawable.ic_menu_search, -1, ListItem.Type.Navigation, searchView));
		navItems.add(new ListItem(productsView.getTitle(), R.drawable.ic_action_picture, -1, Type.Navigation, productsView));
		navItems.add(new ListItem(notificationsView.getTitle(), R.drawable.ic_action_labels, -1, Type.Navigation, notificationsView));
		//navItems.add(new ListItem(sellersView.getTitle(), R.drawable.ic_action_group, -1, Type.Navigation, sellersView));
		navItems.add(new ListItem(wishListView.getTitle(), R.drawable.ic_menu_view, -1, Type.Navigation, wishListView));

		//navItems.add(new NavItem("Sell", -1, -1, Type.Header, null));
		navItems.add(new ListItem(addProductView.getTitle(), R.drawable.ic_menu_camera, -1, Type.Navigation, addProductView));
		navItems.add(new ListItem(Account.ORDER_HISTORY, R.drawable.ic_action_view_as_list, -1, Type.Action, null));
		/*navItems.add(new NavItem("My Orders", -1, -1, Type.Item, myOrdersView));*/

		// Set the adapter for the list view
		navAdapter = new NavAdapter(this, this, navItems);
		drawerList.setAdapter(navAdapter);
		
		// Set the list's click listener
		drawerList.setOnItemClickListener(new DrawerItemClickListener());

		drawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
		drawerLayout, /* DrawerLayout object */
		R.drawable.ic_drawer, /* nav drawer icon to replace 'Up' caret */
		R.string.drawer_open, /* "open drawer" description */
		R.string.drawer_close /* "close drawer" description */) {

			/** Called when a drawer has settled in a completely closed state. */

			public void onDrawerClosed(View view) {
				//
				
				if (currentFragment != null) {
					currentFragment.onNavShowing(false);
				}
			}

			/** Called when a drawer has settled in a completely open state. */

			public void onDrawerOpened(View drawerView) {
				// 
				SharedPreferences prefs = getSharedPreferences(
						getString(R.string.pref_file), MODE_PRIVATE);
				prefs.edit().putBoolean(s_LEARNED_DRAWER_NAV, true).commit();
				
				if (currentFragment != null) {
					currentFragment.onNavShowing(true);
				}
			}
			
			@Override
			public void onDrawerSlide(View drawerView, float slideOffset) {
				// 
				super.onDrawerSlide(drawerView, slideOffset);
			}
		};

		// Set the drawer toggle as the DrawerListener
		drawerLayout.setDrawerListener(drawerToggle);
		
		// Setup Push Notifications registration key
		notificationHelper = new PushNotificationHelper(this, this);
		
		new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {
				// 
				// Setup Location Utility
				locationUtil = new LocationUtil(Home.this);
				updateSorter(locationUtil.getCurrentLocation());
				locationUtil.requestLocationUpdates(10*60*1000, 10, new LocationListener() {
					
					@Override
					public void onStatusChanged(String provider, int status, Bundle extras) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void onProviderEnabled(String provider) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void onProviderDisabled(String provider) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void onLocationChanged(Location location) {
						// 
						updateSorter(location);
					}
				});
				
				return null;
			}
		};
	}

	private void updateSorter(Location location) {
		//
		if (isDataServiceConnected()) {
			/*Sorter sorter = getDataService().getSorter(this);
			
			// Use current location  only on initial setting
			if (sorter != null && sorter.useCurrentLocation && location != null) {
				sorter.location = location;
			}*/
		}
	}
	
	private void restoreFragments(Bundle savedInstanceState) {
		// 
		FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        
        if (savedInstanceState != null) {
        	// reload fragment state
        	accountView = (Account) manager.getFragment(savedInstanceState, Account.class.getName());
        	searchView = (Search) manager.getFragment(savedInstanceState, Search.class.getName());
        	productsView = (Products) manager.getFragment(savedInstanceState, Products.class.getName());
        	notificationsView = (Notifications) manager.getFragment(savedInstanceState, Notifications.class.getName());
        	sellersView = (Sellers) manager.getFragment(savedInstanceState, Sellers.class.getName());
        	wishListView = (WishList) manager.getFragment(savedInstanceState, WishList.class.getName());
        	addProductView = (AddProduct) manager.getFragment(savedInstanceState, AddProduct.class.getName());
        }
        
        if (accountView == null) {
        	accountView = new Account();
        	transaction.add(R.id.root, accountView, Account.class.getName());//.detach(accountView);
        }
        
        if (productsView == null) {
        	productsView = new Products();
        	transaction.add(R.id.root, productsView, Products.class.getName());//.detach(productsView);
        }
        
        if (searchView == null) {
        	searchView = new Search(); 
        	transaction.add(R.id.root, searchView, Search.class.getName());//.detach(searchView);
        }
        
        if (notificationsView == null) {
        	notificationsView = new Notifications();
        	transaction.add(R.id.root, notificationsView, Notifications.class.getName());//.detach(notificationsView);
        }
        
        if (addProductView == null) {
        	addProductView = new AddProduct();
        	transaction.add(R.id.root, addProductView, AddProduct.class.getName());//.detach(addProductView);
        }
        
        if (sellersView == null) {
        	sellersView = new Sellers();
        	transaction.add(R.id.root, sellersView, Sellers.class.getName());//.detach(sellersView);
        }
        
        if (wishListView == null) {
        	wishListView = new WishList();
        	transaction.add(R.id.root, wishListView, WishList.class.getName());//.detach(wishListView);
        }
        
        transaction.hide(accountView);
        transaction.hide(productsView);
        transaction.hide(searchView);
        transaction.hide(notificationsView);
        transaction.hide(addProductView);
        transaction.hide(sellersView);
        transaction.hide(wishListView);

		transaction.commit();
	}
	
	@Override
    protected void onSaveInstanceState(Bundle outState) {
        
        FragmentManager manager = getSupportFragmentManager();
        
        manager.putFragment(outState, Account.class.getName(), accountView);
        manager.putFragment(outState, Search.class.getName(), searchView);
        manager.putFragment(outState, Products.class.getName(), productsView);
        manager.putFragment(outState, Notifications.class.getName(), notificationsView);
        manager.putFragment(outState, Sellers.class.getName(), sellersView);
        manager.putFragment(outState, WishList.class.getName(), wishListView);
        manager.putFragment(outState, AddProduct.class.getName(), addProductView);

        super.onSaveInstanceState(outState);
	}

	private void setupListeners() {
		//
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home, menu);

		boolean retVal = super.onCreateOptionsMenu(menu);
		
		productsView.setMenu(menu);
		searchView.setMenu(menu);
		
		setVisibleMenuOptions(currentFragment);
		
		return retVal;
	}

	public void goBack() {
		// NavUtils.navigateUpFromSameTask(this);
		onBackPressed();
	}
	
	@Override
	public void onBackPressed() {
		// 
		if (currentFragment == navItems.get(HOME).getTarget()) {
			super.onBackPressed();
		} else {
			goHome();
		}
	}
	
	public void goHome() {
		goHome(false);
	}
	
	public void goHome(boolean refresh) {
		selectItem(HOME, true, refresh);
	}
	
	public void goToNotification() {
		selectItem(NOTIFICATION);
	}
	
	public void goToSearch() {
		selectItem(SEARCH);
	}
	
	public void goToWishList() {
		selectItem(WISH_LIST);
	}
	
	public void goToAddItem() {
		((AddProduct)addProductView).setShowCameraOnStart();
		selectItem(ADD_LISTING);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

		if (drawerToggle != null) {
			// Sync the toggle state after onRestoreInstanceState has occurred.
			drawerToggle.syncState();
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);

		if (drawerToggle != null) {
			drawerToggle.onConfigurationChanged(newConfig);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Hide action bar items
		if (drawerToggle != null && drawerToggle.onOptionsItemSelected(item)) {
			return true;
		}

		// Handle presses on the action bar items
		switch (item.getItemId()) {
			case R.id.action_search:
				goToSearch();
				return true;
			case R.id.action_refresh:
				//
				currentFragment.refresh();
				return true;
			case R.id.action_add_item:
				goToAddItem();
				return true;
			case R.id.action_list_or_map: {
				((Search)currentFragment).toggleDisplayType();
				int icon = R.drawable.ic_menu_mapmode;
				
				if (((Search)currentFragment).getDisplayType() == Search.DisplayType.MAP) {
					icon = R.drawable.ic_action_view_as_grid;
				}
				
				item.setIcon(getResources().getDrawable(icon));
				return true;
			}
			case R.id.action_clear: {
				((Notifications)currentFragment).clearNotifications();
				return true;
			}
			case R.id.log_out: {
				//
				accountView.logout();
				return true;
			}
			default:
				break;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onDestroy() {
		//
		super.onDestroy();
	}
	
	@Override
	protected void onResumeFragments() {
		//
		super.onResumeFragments();

		// check authentication
		// if first time - send user to sign up screen
		SharedPreferences prefs = getSharedPreferences(
				getString(R.string.pref_file), MODE_PRIVATE);
		boolean newUser = prefs.getBoolean(s_NEW_USER_PREF, true);
		boolean showDrawer = !prefs.getBoolean(s_LEARNED_DRAWER_NAV, false);

		if (newUser) {
			prefs.edit().putBoolean(s_NEW_USER_PREF, false).commit();

			ShopApplication.goToSignUp(this);
		} else {
			//
			waitingForService = false;

			final Intent intent = getIntent();
			
			if (intent != null) {
			    final String action = intent.getAction();
			    
			    if (Intent.ACTION_SYNC.equals(action)) {
			    	// Go to Notifications and sync
			    	currentFragment = notificationsView;
			    	drawerList.setItemChecked(NOTIFICATION, true);
					drawerList.setSelection(NOTIFICATION);
					intent.setAction(null);
			    }
			}
			
			if (isDataServiceConnected()) {
				//
				showFragment(currentFragment);
			} else {
				waitingForService = true;
			}

			if (showDrawer) {
				drawerLayout.openDrawer(drawerList);
			}
		}
	}
	
	/*private void showSortLocationDialog() {
		//
		Intent intent = new Intent(this, LocationCheck.class);
		//intent.putExtra(LocationCheck.EXTRA_SORTER, getSorter());
		
		//startActivityForResult(intent, SORTER_REQUEST);
	}*/
	
	/*@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// 
		if (data != null) {
			if (resultCode == RESULT_OK) {
				if (requestCode == SORTER_REQUEST) {
					//
					Sorter sorter = (Sorter) data.getExtras().get(LocationCheck.EXTRA_SORTER);
					if (isDataServiceConnected()) {
						Sorter oldSorter = getDataService().getSorter(this);
						
						if (sorter == null && oldSorter == null) {
							//
							sorter = new Sorter();
						}
						
						if (sorter != null) {
							getDataService().setSorter(this, sorter);
						}
					}
				}
			}
		} else {
			//
			if (requestCode == SORTER_REQUEST) {
				if (isDataServiceConnected()) {
					Sorter oldSorter = getDataService().getSorter(this);
					Sorter sorter = null;
					
					if (oldSorter == null) {
						//
						sorter = new Sorter();
					}

					if (sorter != null) {
						getDataService().setSorter(this, sorter);
					}
				}
			}
		}
	}*/
	
	public Sorter getSorter() {
		//
		if (isDataServiceConnected()) {
			return getDataService().getSorter(this);
		} else {
			return null;
		}
	}

	@Override
	protected void onStop() {
		super.onStop();
		waitingForService = false;
	}

	@Override
	public String getViewTitle() {
		return getString(R.string.app_name);
	}
	
	@Override
	public String getViewFooter() {
		// 
		return null;
	}

	@Override
	public String getTag() {
		return "HomeActivity";
	}

	@SuppressLint("HandlerLeak")
	@Override
	protected void dataServiceConnected(boolean connected) {
		// determine the current tab and inform fragment of service connection
		
		if (connected) {
			// 
			if (clearCache) {
				//
				clearCache = false;
				getDataService().clearCache();
			}
			getDataService().addUserStateListener(new UserStateListener() {
				
				@Override
				public void userLoggedOut() {
					//
					navAdapter.notifyDataSetChanged();
				}
				
				@Override
				public void userLoggedIn(User user) {
					// TODO Auto-generated method stub
					
				}
			});
			
			if (navAdapter != null) {
				navAdapter.notifyDataSetChanged();
			}
			
			if (waitingForService) {
				//
				waitingForService = false;
				showFragment(currentFragment);
			}
			
			// Send Push notification Registration Id 
			notificationHelper.register(pushRegisterHandler, true);
		}
		
		if (currentFragment != null) {
			currentFragment.dataServiceConnected(connected);
		}
		/*else {
			//
			if (productsView != null)
				productsView.dataServiceConnected(connected);
		}*/
	}

	private class DrawerItemClickListener implements
			ListView.OnItemClickListener {
		@SuppressWarnings("rawtypes")
		@Override
		public void onItemClick(AdapterView parent, View view, final int position,
				long id) {
			
			Message navMessage = handler.obtainMessage(NAVIGATE_TO, position);
            navMessage.sendToTarget();
		}
	}
	
	private void selectItem(int position) {
		selectItem(position, true);
	}
	
	private void selectItem(int position, boolean showFragment) {
		//
		selectItem(position, showFragment, false);
	}

	private void selectItem(int position, boolean showFragment, boolean refresh) {
		// 
		ListItem navItem = navItems.get(position);
		ShopFragment fragment = navItem.getTarget();

		if (navItem.getType() != Type.Action) {
			if (fragment != null) {
				// Highlight the selected item, update the title, and close the drawer
				drawerList.setItemChecked(position, true);
				drawerList.setSelection(position);
				drawerLayout.closeDrawer(drawerList);
				currSelection = position;
				
				if (showFragment) {
					showFragment(fragment, refresh);
				}
			} else {
				drawerList.setItemChecked(-1, true);
			}
		} else if (navItem.getType() == Type.Action) {
			//
			if (Account.ORDER_HISTORY.equalsIgnoreCase(navItem.getTitle())) {
				// don't change nav list selection
				drawerList.setItemChecked(currSelection, true);
				drawerList.setSelection(currSelection);
				
				if (isDataServiceConnected() && getDataService().checkAuthentication(this)) {
					ShopApplication.goToOrderHistory(this, getDataService().getUser(this));
				}	
			}
			
			drawerLayout.closeDrawer(drawerList);
		}
	}
	
//	private void checkSorter() {
//		if (!DataService.isSorterSet(this)) {
//			//
//			showSortLocationDialog();
//		}
//	}
	
	private void showFragment(ShopFragment fragment) {
		showFragment(fragment, false);
	}

	private void showFragment(ShopFragment fragment, boolean refresh) {
		//
		/*if (checkSorter) {
			checkSorter = false;
			if (getSorter() == null) {
				//
				showSortLocationDialog();
			}
		}*/
		
		if (fragment == null) {
			fragment = navItems.get(HOME).getTarget();
			// 
			selectItem(HOME, false);
		}
		
		if (fragment == currentFragment) {
			if (!fragment.isHidden()) {
				if (!fragment.isInit() || refresh) {
					fragment.refresh();
				} else {
					//fragment.update();
				}
			
				return;
			}
		}
		
		// Insert the fragment by replacing any existing fragment
		//fragment.setMenu(getMenu());
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		
		for (ListItem item : navItems) {
			if (item.getTarget() != null &&
				fragment != item.getTarget() && 
				!item.getTarget().isHidden()) {
				transaction.hide(item.getTarget());
			}
		}
		
		if (currentFragment != null && fragment != currentFragment) {
			currentFragment.onClose();
		}
		
		if (fragment.canShow()) {
			transaction.show(fragment);
			
			if (!fragment.isInit() || refresh) {
				fragment.refresh();
			} else {
				fragment.update();
			}
			
			currentFragment = fragment;
			setViewTitle(currentFragment.getTitle(), currentFragment.getFooter());
			
			setVisibleMenuOptions(currentFragment);
		} else {
			transaction.hide(fragment);
			currentFragment = fragment;
		}
		transaction.commit();

//		currentFragment = fragment;
//		setViewTitle(currentFragment.getTitle(), currentFragment.getFooter());
//		
//		setVisibleMenuOptions(currentFragment);
	}
	
	private void setVisibleMenuOptions(final ShopFragment fragment) {
		// 
		Menu menu = getMenu();

		if (menu == null) {
			return;
		}
			
		// Set up and show grid mutable menu item
		final MenuItem gridType = menu.findItem(R.id.action_grid_type);
		boolean visible = false;

		if (fragment instanceof MutableDisplay && fragment != wishListView) {
			visible = true;
			gridType.setTitle(((MutableDisplay)fragment).getNextDisplayTypeName());
			gridType.setIcon(((MutableDisplay)fragment).getNextDisplayTypeIcon());
		}

		gridType.setVisible(visible);
		
		if (visible) {
			gridType.setOnMenuItemClickListener(new OnMenuItemClickListener() {
	
				@Override
				public boolean onMenuItemClick(MenuItem item) {
					//
					/*((MutableDisplay) currentFragment)
								.setDisplayType(getNextDisplayType(((MutableDisplay) currentFragment)
										.getCurrentDisplayType()));*/
					((MutableDisplay) currentFragment).toggleDisplay();
					if (fragment instanceof MutableDisplay) {
						gridType.setTitle(((MutableDisplay)fragment).getNextDisplayTypeName());
						gridType.setIcon(((MutableDisplay)fragment).getNextDisplayTypeIcon());
					}
					return true;
				}
			});
		}
			
		MenuItem refresh = menu.findItem(R.id.action_refresh);
		MenuItem search = menu.findItem(R.id.action_search);
		MenuItem search2 = menu.findItem(R.id.action_search_2);
		MenuItem listOrMap = menu.findItem(R.id.action_list_or_map);
		MenuItem addItem = menu.findItem(R.id.action_add_item);
		MenuItem overflow = menu.findItem(R.id.action_overflow);
		MenuItem logout = menu.findItem(R.id.log_out);
		MenuItem clearAll = menu.findItem(R.id.action_clear);
			
		String title = getString(R.string.logout_user);
		SpannableStringBuilder logoutTitle = new SpannableStringBuilder(title);
		ForegroundColorSpan fcs = new ForegroundColorSpan(getResources().getColor(R.color.red));
		logoutTitle.setSpan(fcs, 0, title.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
			
		TextView logoutView = new TextView(this);
		logoutView.setText(logoutTitle);
		logoutView.setPadding(0, 0, 10, 0);
		logoutView.setOnClickListener(new OnClickListener() {
				
			@Override
			public void onClick(View v) {
				// 
				accountView.logout();
			}
		});
			
		logout.setActionView(logoutView);
			
		visible = !(fragment instanceof Search) && !(fragment instanceof AddProduct) && !(fragment instanceof Account) && !(fragment instanceof Notifications);
			
		listOrMap.setVisible((fragment instanceof Search));
		search2.setVisible((fragment instanceof Search));
		refresh.setVisible(!(fragment instanceof Search) && !(fragment instanceof AddProduct) && !(fragment instanceof Account));
		search.setVisible(visible);
		addItem.setVisible(!(fragment instanceof Search) && !(fragment instanceof AddProduct) && !(fragment instanceof Account));
		logout.setVisible((fragment instanceof Account));
		clearAll.setVisible((fragment instanceof Notifications));
		
		boolean overflowVisible = !(fragment instanceof Search) && !(fragment instanceof AddProduct) && !(fragment instanceof Account);
		overflow.setVisible(overflowVisible);
		
		if (overflowVisible) {
			//
			MenuItem sortLocation = menu.findItem(R.id.sort_by_location);
			MenuItem sortDate = menu.findItem(R.id.sort_by_date);
			
			sortLocation.setVisible(!(fragment instanceof Notifications) && !(fragment instanceof WishList));
			sortDate.setVisible(!(fragment instanceof Notifications) && !(fragment instanceof WishList)); 
		}
	}
	
	@Override
	public boolean onKeyUp(int keycode, KeyEvent e) {
	    switch(keycode) {
	        case KeyEvent.KEYCODE_MENU:
	            if (getMenu() !=null) {
	                getMenu().performIdentifierAction(R.id.action_overflow, 0);
	            }
	    }
	 
	    return super.onKeyUp(keycode, e);
	}
	
	public void toggleSearchDisplayType() {
		onOptionsItemSelected(getMenu().findItem(R.id.action_list_or_map));
	}
	
	@SuppressWarnings("unused")
	private DisplayType getNextDisplayType(DisplayType type) {
		//
		DisplayType nextType = DisplayType.SINGLE_ITEM;

		if (type == DisplayType.SINGLE_ITEM) {
			nextType = DisplayType.TWO_PER_LINE;
		} else if (type == DisplayType.TWO_PER_LINE) {
			nextType = DisplayType.THREE_PER_LINE;
		}

		return nextType;
	}

	private class NavHandler extends Handler {

		public NavHandler(Looper mainLooper) {
			super(mainLooper);
		}
		
		public void handleMessage(Message inputMessage) {
			//
			Integer itemIndex = (Integer)inputMessage.obj;
			
			if (itemIndex != null) {
				selectItem(itemIndex);
			}
		}
	}
}
