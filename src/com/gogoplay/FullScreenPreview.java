package com.gogoplay;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;

import com.gogoplay.data.Image;
import com.gogoplay.ui.ShopFragmentActivity;
import com.gogoplay.ui.widget.ImagePreviewList;
import com.gogoplay.ui.widget.ImagePreviewList.ImagePreviewParent;

public class FullScreenPreview extends ShopFragmentActivity implements ImagePreviewParent {

	public static final String s_EXTRA_IMAGE_GALLERY = "Images";
	public static final String s_EXTRA_START_INDEX = "ImageIndex";
	
	private ImagePreviewList previewPopup;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// remove title

		setContentView(R.layout.fullscreen);

		previewPopup = (ImagePreviewList) findViewById(R.id.previewPopup);
		previewPopup.setShopFragmentActivity(this);
	}
	
	@Override
	protected void onResume() {
		// 
		super.onResume();
	}

	@Override
	public String getTag() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getViewTitle() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public String getViewFooter() {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void serviceConnected() {
		// 
		Intent intent = getIntent();
		ArrayList<Image> images = (ArrayList<Image>)intent.getExtras().get(s_EXTRA_IMAGE_GALLERY);
		int position = intent.getExtras().getInt(s_EXTRA_START_INDEX);
		
		previewPopup.show(images, position, getDataService());
	}

	@Override
	public void popupClosed() {
		// 
		//onBackPressed();
		finish();
	}
}
