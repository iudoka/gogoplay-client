package com.gogoplay.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import com.gogoplay.R;
import com.gogoplay.ui.ShopApplication;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

/**
 * Address and location Utility
 * We should consider using http://ziptasticapi.com/<zipcode>
 *  
 * @author iudoka
 *
 */
public class LocationUtil implements LocationListener {
	//
	private static final String TAG = "LocationUtil";
	private static boolean checkLocationEnabled = true;
	private static final Object s_LOCATION_LOCK = new Object();
	private static final boolean s_allowOnlyUsAddresses = true; 

	private static final long s_EARTH_RADIUS_MILES = 3959;
	private static final long s_EARTH_RADIUS_KM = 6371;
	private static final int s_MILES_PER_MONGO_UNIT = 69;
	
	//private static final long MIN_TIME_BW_UPDATES = 20000;

	//private static final float MIN_DISTANCE_CHANGE_FOR_UPDATES = 0;
	
	//private static Location currentLocation;
	
	private static final HashMap<String, String> usStateCodes = new HashMap<String, String>();
	
	static {
		//
		usStateCodes.put("Alabama", "AL");
		usStateCodes.put("Alaska", "AK");
		usStateCodes.put("American Samoa", "AS");
		usStateCodes.put("Arizona", "AZ");
		usStateCodes.put("Arkansas", "AR");
		usStateCodes.put("California", "CA");
		usStateCodes.put("Colorado", "CO");
		usStateCodes.put("Connecticut", "CT");
		usStateCodes.put("Delaware", "DE");
		usStateCodes.put("District of Columbia", "DC");
		usStateCodes.put("Florida", "FL");
		usStateCodes.put("Georgia", "GA");
		usStateCodes.put("Guam", "GU");
		usStateCodes.put("Hawaii", "HI");
		usStateCodes.put("Idaho", "ID");
		usStateCodes.put("Illinois", "IL");
		usStateCodes.put("Indiana", "IN");
		usStateCodes.put("Iowa", "IA");
		usStateCodes.put("Kansas", "KS");
		usStateCodes.put("Kentucky", "KY");
		usStateCodes.put("Louisiana", "LA");
		usStateCodes.put("Maine", "ME");
		usStateCodes.put("Maryland", "MD");
		usStateCodes.put("Marshall Islands", "MH");
		usStateCodes.put("Massachusett", "MA");
		usStateCodes.put("Michigan", "MI");
		usStateCodes.put("Micronesia", "FM");
		usStateCodes.put("Minnesota", "MN");
		usStateCodes.put("Mississippi", "MS");
		usStateCodes.put("Missouri", "MO");
		usStateCodes.put("Montana", "MT");
		usStateCodes.put("Nebraska", "NE");
		usStateCodes.put("Nevada", "NV");
		usStateCodes.put("New Hampshire", "NH");
		usStateCodes.put("New Jersey", "NJ");
		usStateCodes.put("New Mexico", "NM");
		usStateCodes.put("New York", "NY");
		usStateCodes.put("North Carolina", "NC");
		usStateCodes.put("North Dakota", "ND");
		usStateCodes.put("Northern Marianas", "MP");
		usStateCodes.put("Ohio", "OH");
		usStateCodes.put("Oklahoma", "OK");
		usStateCodes.put("Oregon", "OR");
		usStateCodes.put("Palau", "PW");
		usStateCodes.put("Pennsylvania", "PA");
		usStateCodes.put("Puerto Rico", "PR");
		usStateCodes.put("Rhode Island", "RI");
		usStateCodes.put("South Carolina", "SC");
		usStateCodes.put("South Dakota", "SD");
		usStateCodes.put("Tennessee", "TN");
		usStateCodes.put("Texas", "TX");
		usStateCodes.put("Utah", "UT");
		usStateCodes.put("Vermont", "VT");
		usStateCodes.put("Virginia", "VA");
		usStateCodes.put("Virgin Islands", "VI");
		usStateCodes.put("Washington", "WA");
		usStateCodes.put("West Virginia", "WV");
		usStateCodes.put("Wisconsi", "WI");
		usStateCodes.put("Wyoming", "WY");

		//Read more: U.S. State Abbreviations & State Postal Codes | Infoplease.com http://www.infoplease.com/ipa/A0110468.html#ixzz39Id0DzBP
	}
	
	public static final String EXTRA_CITY = "city";
	public static final String EXTRA_STATE = "state";
	public static final String EXTRA_ZIPCODE = "zipcode";
	public static final String EXTRA_CITY_AND_ZIP = "cityAndZip";
	public static final String EXTRA_CITY_AND_STATE_CODE = "cityAndStateCode";
	public static final String EXTRA_SHORT_ADDRESS = "shortAddress";
	public static final String EXTRA_LONG_ADDRESS = "longAddress";
	
	private Activity activity;
	private LocationManager locationManager;
	private String provider;
	
	public LocationUtil(Activity activity) {
		//
		this.activity = activity;
		
		// initialize
		init();
	}
	
	private void init() {

		synchronized(s_LOCATION_LOCK) {
			locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
			
			boolean enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
	
			// check if enabled and if not send user to the GSP settings
			// Better solution would be to display a dialog and suggesting to
			// go to the settings
			if (checkLocationEnabled && !enabled) {
				//
				checkLocationEnabled = false;
				showSettingsDialog(activity.getString(R.string.turn_on_location), null);
			} else {
				// Define the criteria how to select the location provider -> use default
				Criteria criteria = new Criteria();
				provider = locationManager.getBestProvider(criteria, false);
			}
		}
	}
	
	@SuppressLint("HandlerLeak")
	public void showSettingsDialog(String msg, String negativeButton) {
		//
		if (negativeButton == null) {
			negativeButton = activity.getString(R.string.maybe_later);
		}
		
		ShopApplication.showConfirmation(activity, msg, activity.getString(R.string.turn_on_location_msg), activity.getString(R.string.turn_on), negativeButton, new Handler() {
			@Override
			public void handleMessage(Message msg) {
				//
				boolean confirmed = msg.getData().getBoolean(ShopApplication.CONFIRM_MSG);
				
				if (confirmed) {
					//
					Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					activity.startActivity(intent);
				}
			}
		});
	}
	
	public Location getLocation(final double latitude, final double longitude) {
		//
		List<Address> addresses = getAddress(latitude, longitude);
		
		if (Utils.isEmpty(addresses)) {
			return null;
		}

		Location loc = new Location("");
		loc.setLatitude(latitude);
		loc.setLongitude(longitude);
		
		addLocationExtras(loc, addresses);
		
		return loc;
	}
	
	/*private void handleCallback(Serializable result, Handler handler) {
		//
		Message m = new Message();
		Bundle b = new Bundle();
		b.putString("location", result.toString());

		m.setData(b);

		handler.sendMessage(m);
	}
	
	private void handleCallback(Parcelable result, Handler handler) {
		//
		Message m = new Message();
		Bundle b = new Bundle();
		b.putString("location", ((Location)result).getProvider());
		b.putDouble("lat", ((Location)result).getLatitude());
		b.putDouble("lon", ((Location)result).getLongitude());

		m.setData(b);

		handler.sendMessage(m);
	}
	
	public void getCityAndZip(final double latitude, final double longitude, final Handler handler) {
		//
		new Thread() {
			public void run() {
				String locationStr = "";

				List<Address> addresses = getAddress(latitude, longitude);
				
				if (!Utils.isEmpty(addresses)) {
					locationStr = addresses.get(0).getLocality() + " " + addresses.get(0).getPostalCode();
				}
				
				Location loc = new Location("");
				loc.setProvider(locationStr);
				loc.setLatitude(latitude);
				loc.setLongitude(longitude);
				
				handleCallback(loc, handler);
			}
		}.start();
	}*/
	
	public Location getCurrentLocation() {
		//
		return getCurrentLocation(false, null, null);
	}
	
	public Location getCurrentLocation(boolean showLocationMessage, String locationOffMsg, String locationOffAction) {
		//
    	Location location = null;
    	
	    try {
	        // getting GPS status
	        boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

	        // getting network status
	        boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

	        if (!isGPSEnabled && !isNetworkEnabled) {
	            // no network provider is enabled
	        	
	        	if (showLocationMessage) {
	        		showSettingsDialog(locationOffMsg, locationOffAction);
	        	}
	        } else {
	            //this.canGetLocation = true;
	            if (isNetworkEnabled) {
	                /*locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
	                        							   MIN_TIME_BW_UPDATES,
	                        							   MIN_DISTANCE_CHANGE_FOR_UPDATES, this);*/
	                Log.d("Network", "Network Enabled");
	                if (locationManager != null) {
	                    location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
	                }
	            }
	            // if GPS Enabled get lat/long using GPS Services
	            if (isGPSEnabled) {
	                if (location == null) {
	                    /*locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
	                            							   MIN_TIME_BW_UPDATES,
	                            							   MIN_DISTANCE_CHANGE_FOR_UPDATES, this);*/
	                    Log.d("GPS", "GPS Enabled");
	                    if (locationManager != null) {
	                        location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
	                    }
	                }
	            }
	        }

	    } catch (Exception e) {
	        e.printStackTrace();
	    }

	    if (location != null) {
	    	addLocationExtras(location);
	    }
	    
	    return location;
	}
	
	private void addLocationExtras(Location loc) {
		addLocationExtras(loc, null);
	}
	
	private void addLocationExtras(Location loc, List<Address> addresses) {
		//
		Geocoder geoCoder = new Geocoder(activity.getBaseContext(), Locale.getDefault());
		//List<Address> addresses = new ArrayList<Address>();
		//StringBuilder locationStr = new StringBuilder();
		
		try {
			
			if (addresses == null) {
				addresses = geoCoder.getFromLocation(loc.getLatitude(), loc.getLongitude(), 1);
			}
			
			if (!Utils.isEmpty(addresses)) {
				//
				Address addr = addresses.get(0);
				
				/*locationStr.append(addr.getLocality());
				
				String addr2 = addr.getPostalCode();
				
				if (addr2 == null) {
					//
					if (!Utils.isEmpty(addr.getAdminArea())) {
						//
						addr2 = addr.getAdminArea();
					}
					
					if (addr2 == null && !Utils.isEmpty(addr.getCountryCode())) {
						//
						addr2 = addr.getCountryCode();
					}
				}
				
				if (addr2 != null) {
					locationStr.append(", " + addr2);
				}*/
				
				Bundle extras = new Bundle();
				extras.putString(EXTRA_CITY, addr.getLocality());
				extras.putString(EXTRA_STATE, addr.getAdminArea());
				extras.putString(EXTRA_ZIPCODE, addr.getPostalCode());
				extras.putString(EXTRA_CITY_AND_ZIP, addr.getLocality() + (Utils.isEmpty(addr.getPostalCode()) ? "" : ", " + addr.getPostalCode()));
				
				String stateCode = usStateCodes.get(addr.getAdminArea());
				
				extras.putString(EXTRA_CITY_AND_STATE_CODE, addr.getLocality() + (Utils.isEmpty(stateCode) ? (Utils.isEmpty(addr.getAdminArea()) ? "" : ", " + addr.getAdminArea()) : ", " + stateCode));
				extras.putString(EXTRA_SHORT_ADDRESS, addr.getAddressLine(0));
				
				StringBuilder longAddrs = new StringBuilder();
				
				for (int i=0; i <= addr.getMaxAddressLineIndex(); i++) {
					longAddrs.append(addr.getAddressLine(i) + " ");
				}
				
				extras.putString(EXTRA_LONG_ADDRESS, Utils.trim(longAddrs.toString()));
				loc.setExtras(extras);
			}
		} catch (Exception ex) {
			Log.e(TAG, "Unable to locate user!", ex);
		}
	}
	
	public String getCurrentCityAndStateCode() {
		//
		String locationStr = "";

		try {
			// if (provider != null) {
			Location location = getCurrentLocation();
			List<Address> addresses = getAddress(location.getLatitude(),
					location.getLongitude());

			if (!Utils.isEmpty(addresses)) {
				Address addr = addresses.get(0);
				
				String stateCode = usStateCodes.get(addr.getAdminArea());
				locationStr = addr.getLocality() + (Utils.isEmpty(stateCode) ? (Utils.isEmpty(addr.getAdminArea()) ? "" : ", " + addr.getAdminArea()) : ", " + stateCode);
			}
			// }
		} catch (Exception ex) {
			Log.e(TAG, "Unable to get current city and state!", ex);
		}

		return locationStr;
	}
	
	public String getCurrentCityAndState() {
		//
		String locationStr = "";

		try {
			// if (provider != null) {
			Location location = getCurrentLocation();
			List<Address> addresses = getAddress(location.getLatitude(),
					location.getLongitude());

			if (!Utils.isEmpty(addresses)) {
				Address addr = addresses.get(0);
				locationStr = addr.getLocality() + (Utils.isEmpty(addr.getAdminArea()) ? "" : ", " + addr.getAdminArea());
			}
			// }
		} catch (Exception ex) {
			Log.e(TAG, "Unable to get current city and state!", ex);
		}

		return locationStr;
	}
	
	public String getCurrentCityAndZip() {
		//
		String locationStr = "";
		String zipcode = "";

		try {
			// if (provider != null) {
			Location location = getCurrentLocation();
			List<Address> addresses = getAddress(location.getLatitude(),
					location.getLongitude());

			if (!Utils.isEmpty(addresses)) {
				Address addr = addresses.get(0);
				zipcode = addr.getPostalCode();
				locationStr = addr.getLocality() + (Utils.isEmpty(zipcode) ? "" : ", " + zipcode);
			}
			// }
		} catch (Exception ex) {
			Log.e(TAG, "Unable to get current city and zip!", ex);
		}

		return locationStr;
	}
	
	public String getCurrentZipcode() {
		//
		String locationStr = null;
		
		try {
			Location location = getCurrentLocation();
			List<Address> addresses = getAddress(location.getLatitude(), location.getLongitude());
			
			if (!Utils.isEmpty(addresses)) {
				locationStr = addresses.get(0).getPostalCode();
			}
		} catch(Exception ex) {
			Log.e(TAG, "Unable to get current zipcode!", ex);
		}
		
		return locationStr;
	}
	
	private List<Address> getAddress(double latitude, double longitude) {
		//
		Geocoder geoCoder = new Geocoder(activity.getApplicationContext(), Locale.getDefault());
		List<Address> addresses = new ArrayList<Address>();
		
		try {
			addresses = geoCoder.getFromLocation(latitude, longitude, 1);
			
			if (s_allowOnlyUsAddresses) {
				if (!Utils.isEmpty(addresses)) {
					if (!"US".equalsIgnoreCase(addresses.get(0).getCountryCode())) {
						try {
							String message = "Please enter a location in the U.S.";
				        	Toast toast = Toast.makeText(getContext(), message, Toast.LENGTH_LONG);
							toast.show();
						} catch (Exception ex) {}
						return null;
					}
				}
			}
			
		} catch (Exception ex) {
			Log.e(TAG, "Unable to locate user!", ex);
		}
		
		return addresses;
	}
	
	public Context getContext() {
		return activity;
	}
	
	public Location getLocation(String addrs) {
		//
		Geocoder geoCoder = new Geocoder(activity.getBaseContext(), Locale.getDefault());
		List<Address> addresses = new ArrayList<Address>();
		Location loc = null;
		
		try {
			addresses = geoCoder.getFromLocationName(addrs, 1);
			
			if (!Utils.isEmpty(addresses)) {
				//
				Address addr = addresses.get(0);
				
				// ONLY ALLOW U.S. ADDRESSES FOR NOW
				if (!"US".equalsIgnoreCase(addr.getCountryCode())) {
					return null;
				}

				loc = new Location("");
				loc.setLatitude(addr.getLatitude());
				loc.setLongitude(addr.getLongitude());
				
				addLocationExtras(loc, addresses);
			}
		} catch (Exception ex) {
			Log.e(TAG, "Unable to locate user!", ex);
		}
		
		return loc;
	}
	
	public void requestLocationUpdates(int minTime, float minDistance, LocationListener listener) {
		//
		locationManager.requestLocationUpdates(provider, minTime, minDistance, listener);
	}

	@Override
	public void onLocationChanged(Location location) {
		// 
		if (location != null) {
			//currentLocation = location;
		}
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}
	
	public static double radiansToMiles(double distanceInRadians) {
		// All distances use radians. This allows you to easily multiply by the radius of the earth (about 6371 km or 3959 miles) to get the distance in your choice of units. Conversely, divide by the radius of the earth when doing queries.
		return distanceInRadians * s_EARTH_RADIUS_MILES;
	}
	
	public static double radiansToKilometers(double distanceInRadians) {
		// All distances use radians. This allows you to easily multiply by the radius of the earth (about 6371 km or 3959 miles) to get the distance in your choice of units. Conversely, divide by the radius of the earth when doing queries.
		return distanceInRadians * s_EARTH_RADIUS_KM;
	}
	
	public static double mongoUnitsToMiles(double mongoUnit) {
		//
		return mongoUnit * s_MILES_PER_MONGO_UNIT;
	}
}
