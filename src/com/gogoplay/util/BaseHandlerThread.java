package com.gogoplay.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;

public class BaseHandlerThread extends HandlerThread{
	//
	private static final int RUN_THREAD = 0;
	
	private long uid = 0;
	private Handler handler;
	private Map<Long, Runnable> requestMap = Collections.synchronizedMap(new HashMap<Long, Runnable>());
	
	
	public BaseHandlerThread(String tag) {
		super(tag);
	}
	
	@SuppressLint("HandlerLeak")
	protected void onLooperPrepared() {
		handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				if (msg.what == RUN_THREAD) {
					Long id = (Long)msg.obj;
					handleTask(id);
				}
			}
		};
	}
	
	public void queueTask(Runnable task) {
		long id = getUid();
		
		requestMap.put(id, task);
		handler.obtainMessage(RUN_THREAD, id).sendToTarget();
		Log.i(getName(), "Queued task " + id + " to be handled ");
	}
	
	private void handleTask(final Long id) {
		//
		try {
			final Runnable task = requestMap.remove(id);
			Log.i(getName(), "Handling task " + task.toString());
			task.run();
		} catch (Exception ex) {
			Log.e(getName(), "Unable to execute queued task", ex);
		}
	}
	
	private long getUid() {
		return uid++;
	}

	public void clearQueue() {
		Log.i(getName(), "Clearing queue of " + requestMap.size() + " items");
		handler.removeMessages(RUN_THREAD);
		requestMap.clear();
	}
	
	public void lowMemory() {
		clearQueue();
	}
	
	public boolean quit() {
		clearQueue();
		
		return super.quit();
	}
}
