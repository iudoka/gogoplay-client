package com.gogoplay.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.dom4j.Node;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.StrictMode;
import android.os.Build.VERSION_CODES;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.text.style.ForegroundColorSpan;

import com.gogoplay.Home;
import com.gogoplay.ProductDetail;
import com.gogoplay.R;
import com.gogoplay.data.Item;
import com.gogoplay.data.User;
import com.gogoplay.data.server.ServerUtils;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;

/**
 * Utility class to use by views
 * 
 * @author Ison Udoka
 */
public abstract class Utils {
	//
	
	public static final String s_MAP_API_KEY = "0rFnwnUr_vPRBwHMi1pkUe0vxCGZAcZrPHRb6Ew";
	public static final String s_FLICKR_KEY = "bf0c874636d5a34b3afb29e22796e985";
	public static final String s_FLICKR_SECRET = "032628fe708f338f";
	private static final NumberFormat priceFormat = NumberFormat.getCurrencyInstance();
	private static final String EMPTY = "";

	public static final String formatPrice(Number price) {
		//
		String priceTxt = priceFormat.format(price);
			
		return priceTxt.replace(".00", EMPTY);
	}
	
	public static final String formatPrice(Context context, Number price) {
		//
		if (price.doubleValue() > 0) {
			return formatPrice(price);
		} else {
			return context.getString(R.string.free);
		}
	}
	
	public static boolean isSameUser(User user1, User user2) {
		//
		if (user1 == null || user2 == null) {
			return false;
		}
		
		return Utils.equals(user1.getId(), user2.getId());
	}
	
	public static final String emptyIfNull(String str) {
		return ((str != null) ? str : EMPTY);
	}
	
	public static final CharSequence emptyIfNull(CharSequence str) {
		return ((str != null) ? str : EMPTY);
	}
	
	public static final boolean isEmpty(CharSequence str) {
		return str == null || isEmpty(str.toString());
	}

	public static final boolean isEmpty(String str) {
		return TextUtils.isEmpty(str);
	}
	
	public static final String nullEmptyString(String str) {
		//
		if (isEmpty(str)) {
			return null;
		}
		
		return str;
	}

	public static final boolean isDigitsOnly(String str) {
		return TextUtils.isDigitsOnly(str);
	}

	@SuppressWarnings("rawtypes")
	public static final boolean isEmpty(Collection c) {
		return c == null || c.size() == 0;
	}

	public static final String toString(Object o) {
		if (o != null)
			return o.toString();

		return EMPTY;
	}
	
	public static final String trim(String str) {
		if (str != null) {
			return str.trim();
		}
		
		return str;
	}

	public static byte[] toByteArray(Object obj) {
		byte[] arr = null;

		try {
			ByteArrayOutputStream bis = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(bis);

			oos.writeObject(obj);

			arr = bis.toByteArray();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return arr;
	}

	public static Object toObject(byte[] bytes) {
		Object obj = null;

		try {
			ByteArrayInputStream sis = new ByteArrayInputStream(bytes);
			ObjectInputStream ois = new ObjectInputStream(sis);
			obj = ois.readObject();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return obj;
	}

	public static String serialize(Object obj) {
		String bytes = null;
		ObjectOutputStream oos = null;

		try {
			StringOutputStream sos = new StringOutputStream();
			oos = new ObjectOutputStream(sos);
			oos.writeObject(obj);

			bytes = sos.toString();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			//
			if (oos != null) {
				try {
					oos.close();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return bytes;
	}

	public static Object deserialize(String bytes) {
		Object obj = null;
		ObjectInputStream ois = null;

		try {
			StringInputStream sis = new StringInputStream(bytes);
			ois = new ObjectInputStream(sis);
			obj = ois.readObject();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (ois != null) {
					ois.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return obj;
	}

	public static Bitmap getRoundedCornerBitmap(Context context, Bitmap bitmap) {
		Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
				bitmap.getHeight(), Config.ARGB_8888);
		Canvas canvas = new Canvas(output);

		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		final RectF rectF = new RectF(rect);
		final float roundPx = convertDipsToPixels(context, 15);

		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);

		return output;
	}

	public static int convertDipsToPixels(Context context, int dips) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dips * scale + 0.5f);
	}

	public static String signUrl(String inputUrl) throws IOException,
			InvalidKeyException, NoSuchAlgorithmException, URISyntaxException {
		// Convert the string to a URL so we can parse it
		URL url = new URL(inputUrl);

		UrlSigner signer = new UrlSigner(s_MAP_API_KEY);
		String request = signer.signRequest(url.getPath(), url.getQuery());

		String signedUrl = url.getProtocol() + "://" + url.getHost() + request;

		// System.out.println("Signed URL :" + signedUrl);

		return signedUrl;
	}

	public static double avg(List<? extends Number> values) {
		double sum = 0;

		for (Number nbr : values) {
			sum += nbr.doubleValue();
		}

		return sum / values.size();
	}

	public static String getText(Node node) {
		if (node != null) {
			return emptyIfNull(node.getText());
		}

		return EMPTY;
	}

	public static long parseLong(Node node, long defaultVal) {
		long longVal = defaultVal;
		String strVal = getText(node);

		if (isEmpty(strVal)) {
			return -1;
		}

		try {
			longVal = Long.parseLong(strVal);
		} catch (NumberFormatException ex) {
			longVal = defaultVal;
		}

		return longVal;
	}

	public static int parseInt(Node node, int defaultVal) {
		int numVal = defaultVal;
		String strVal = getText(node);

		if (isEmpty(strVal)) {
			return -1;
		}

		try {
			numVal = Integer.parseInt(strVal);
		} catch (NumberFormatException ex) {
			numVal = defaultVal;
		}

		return numVal;
	}

	public static double parseDouble(String strVal) {
		double numVal;

		try {
			numVal = Double.parseDouble(strVal);
		} catch (NumberFormatException ex) {
			numVal = 0;
		}

		return numVal;
	}
	
	public static boolean has(JsonObject element, String name) {
		//
		return (element.has(name) && !(element.get(name) instanceof JsonNull));
	}

	public static float parseFloat(Node node, float defaultVal) {
		float numVal = defaultVal;
		String strVal = getText(node);

		if (isEmpty(strVal)) {
			return -1;
		}

		try {
			numVal = Float.parseFloat(strVal);
		} catch (NumberFormatException ex) {
			numVal = defaultVal;
		}

		return numVal;
	}

	public static boolean equals(Object obj1, Object obj2) {
		if (obj1 == null)
			return obj2 == null;

		return obj1.equals(obj2);
	}

	public static String formatPhoneNumber(String number) {
		String retVal = EMPTY;
		PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
		String countryCode = "US"; // TODO - get country code on the fly

		try {
			PhoneNumber pn = phoneUtil.parse(number, countryCode);
			retVal = phoneUtil.format(pn, PhoneNumberFormat.E164);
		} catch (NumberParseException e) {
			//
			retVal = emptyIfNull(number);
		}

		return retVal;
	}

	public static boolean getBoolean(String string) {
		try {
			return Boolean.valueOf(string);
		} catch (Throwable t) {
			return false;
		}
	}

	public static String commaSeparate(List<String> values) {
		StringBuilder b = new StringBuilder();

		for (String value : values) {
			b.append(value + ",");
		}

		return b.substring(0, b.length() - 1);
	}

	public static String encodeHttp(String name) {
		return name.replace(' ', '+');
	}
	
	@TargetApi(VERSION_CODES.HONEYCOMB)
    public static void enableStrictMode() {
        if (Utils.hasGingerbread()) {
            StrictMode.ThreadPolicy.Builder threadPolicyBuilder =
                    new StrictMode.ThreadPolicy.Builder()
                            .detectAll()
                            .penaltyLog();
            StrictMode.VmPolicy.Builder vmPolicyBuilder =
                    new StrictMode.VmPolicy.Builder()
                            .detectAll()
                            .penaltyLog();

            if (Utils.hasHoneycomb()) {
                threadPolicyBuilder.penaltyFlashScreen();
                vmPolicyBuilder
                        .setClassInstanceLimit(Home.class, 1)
                        .setClassInstanceLimit(ProductDetail.class, 1);
            }
            StrictMode.setThreadPolicy(threadPolicyBuilder.build());
            StrictMode.setVmPolicy(vmPolicyBuilder.build());
        }
    }

    public static boolean hasFroyo() {
        // Can use static final constants like FROYO, declared in later versions
        // of the OS since they are inlined at compile time. This is guaranteed behavior.
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO;
    }

    public static boolean hasGingerbread() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD;
    }

    public static boolean hasHoneycomb() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
    }

    public static boolean hasHoneycombMR1() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1;
    }

    public static boolean hasJellyBean() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
    }

    public static boolean hasKitKat() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
    }

	public static String trimToFit(String text, int len, String ellipsis) {
		text = trim(emptyIfNull(text));
		
		if (text.length() >= len) {
			int ellipsisLen = 0;
			
			if (!isEmpty(ellipsis)) {
				ellipsisLen = ellipsis.length();
			}
			
			text = text.substring(0, len - ellipsisLen);
			text = text + emptyIfNull(ellipsis);
		}
		
		return text;
	}
	
	public static void shareProduct(Context context, Activity activity, Item item, User user) {
		//
		String shareSubj = getShareHeader(context, user);
		String shareText = context.getResources().getString(R.string.shareMsg) + " " + getProductUri(item);
	 
	    /*PackageManager pm = getPackageManager();
	    Intent sendIntent = new Intent(Intent.ACTION_SEND);    
	    sendIntent.setType("text/plain");
	 
	    // Pick specific intents you want!
	    List<ResolveInfo> resInfo = pm.queryIntentActivities(sendIntent, 0);
	    List<LabeledIntent> intentList = new ArrayList<LabeledIntent>();       
	    for (int i = 0; i < resInfo.size(); i++) {
	        // Extract the label, append it, and repackage it in a LabeledIntent
	        ResolveInfo ri = resInfo.get(i);
	        String packageName = ri.activityInfo.packageName;
	        
	        if(packageName.contains("android.email") || 
	           packageName.contains("twitter") || 
	           packageName.contains("facebook") || 
	           packageName.contains("mms") || 
	           packageName.contains("kakao") || 
	           packageName.contains("com.google.android.apps.plus") || 
	           packageName.contains("android.gm")) {
	        	//
	            Intent intent = new Intent();
	            intent.setComponent(new ComponentName(packageName, ri.activityInfo.name));
	            intent.setAction(Intent.ACTION_SEND);
	            intent.setType("text/plain");
	            
	            if(packageName.contains("twitter")) {
	                intent.putExtra(Intent.EXTRA_TEXT, shareText);
	            } else if(packageName.contains("facebook")) {
	                // Warning: Facebook IGNORES our text. They say "These fields are intended for users to express themselves. Pre-filling these fields erodes the authenticity of the user voice."
	                // One workaround is to use the Facebook SDK to post, but that doesn't allow the user to choose how they want to share. We can also make a custom landing page, and the link
	                // will show the <meta content ="..."> text from that page with our link in Facebook.
	                intent.putExtra(Intent.EXTRA_TEXT, shareText);
	            } else if(packageName.contains("mms")) {
	                intent.putExtra(Intent.EXTRA_TEXT, shareText);
	            } else if(packageName.contains("android.gm") || packageName.contains("android.email")) {
	                intent.putExtra(Intent.EXTRA_TEXT, shareText);
	                intent.putExtra(Intent.EXTRA_SUBJECT, shareSubj);              
	                intent.setType("message/rfc822");
	            } else {
	            	//
	            	intent.putExtra(Intent.EXTRA_TEXT, shareText);
	            }
	 
	            intentList.add(new LabeledIntent(intent, packageName, ri.loadLabel(pm), ri.icon));
	        }
	    }
	    
	    
	    
	    if (!Utils.isEmpty(intentList)) {
	    	// sort options by label
		    Collections.sort(intentList, new Comparator<LabeledIntent>() {
	
				@Override
				public int compare(LabeledIntent lhs, LabeledIntent rhs) {
					// 
					return lhs.getNonLocalizedLabel().toString().compareTo(rhs.getNonLocalizedLabel().toString());
				}
			});
		 
		    // Show sorted intents in the list as extra's
		    LabeledIntent[] extraIntents = intentList.toArray( new LabeledIntent[ intentList.size() ]);
	
		    sendIntent = new Intent(Intent.ACTION_SEND);
		    
		    Intent openInChooser = Intent.createChooser(sendIntent, "Share using");
		    openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
		    startActivity(openInChooser);
	    } else {*/
		    // Show All send intents!
		Intent sendIntent = new Intent(Intent.ACTION_SEND);    
	    sendIntent.setType("text/plain");
	    sendIntent.putExtra(Intent.EXTRA_TEXT, shareText);
		sendIntent.putExtra(Intent.EXTRA_SUBJECT, shareSubj);
		    
		Intent openInChooser = Intent.createChooser(sendIntent, "Share using");
		activity.startActivity(openInChooser);
	    //}
	}
	
	public static String getProductUri(Item item) {
		return ServerUtils.buildGetItemUrl(item.getId(), null);
	}
	
	public static String getShareHeader(Context context, User user) {
		String hdr = null;
		
		if (user != null) {
			//
			hdr = context.getResources().getString(R.string.shareByUserHeader).replace("user_name", user.getName());
		} else {
			//
			hdr = context.getResources().getString(R.string.shareMsg);
		}
		
		return hdr;
	}
	
	/**
	 * Parses special characters including emoticons/emoji/twitter-like @ statements
	 * 
	 * @param text - the text
	 * @param context - the context
	 * @return spannable string if any special chars are found or just a plain text otherwise
	 */
	public static CharSequence parseSpecialChars(String text, Context context) {
		//
		text = Utils.emptyIfNull(text);
		CharSequence seq = AndroidEmoji.ensure(text, context);
		SpannableStringBuilder ssb = null;
		
		if (seq instanceof SpannableStringBuilder) {
			ssb = (SpannableStringBuilder) seq;
		} else {
			//
			ssb = new SpannableStringBuilder(text);
		}
		
		// Find twitter like @ patterns
		ArrayList<int[]> atIndices = findAtIndices(text);
		
		if (!isEmpty(atIndices)) {
			//
			for (int [] index : atIndices) {
				// Span to linkify text
				ForegroundColorSpan fcs = new ForegroundColorSpan(context.getResources().getColor(R.color.light_blue));
				ssb.setSpan(fcs, index[0], index[1], Spannable.SPAN_INCLUSIVE_INCLUSIVE);
			}
		}
		
		return ssb;
	}

	public static ArrayList<int[]> findAtIndices(String text) {
		//
		Pattern p = Pattern.compile("(^|[^@\\w])@(\\w{1,15})\\b");
		Matcher m = p.matcher(text);
		ArrayList<int[]> indices = new ArrayList<int[]>();
		
		while (m.find()) {
			//
			indices.add(new int[]{m.start(), m.end()});
		}
		
		return indices;
	}
	
	public static byte[] marshall(Parcelable parceable) {
        Parcel parcel = Parcel.obtain();
        parceable.writeToParcel(parcel, 0);
        byte[] bytes = parcel.marshall();
        parcel.recycle(); // not sure if needed or a good idea
        return bytes;
    }

    public static Parcel unmarshall(byte[] bytes) {
        Parcel parcel = Parcel.obtain();
        parcel.unmarshall(bytes, 0, bytes.length);
        parcel.setDataPosition(0); // this is extremely important!
        return parcel;
    }

    public static <T> T unmarshall(byte[] bytes, Parcelable.Creator<T> creator) {
        Parcel parcel = unmarshall(bytes);
        return creator.createFromParcel(parcel);
    }

	public static String toUpperCase(String str) {
		// 
		return emptyIfNull(str).toUpperCase(Locale.US);
	}
	
	public static String toLowerCase(String str) {
		//
		return emptyIfNull(str).toLowerCase(Locale.US);
	}
	
	public static String printDate(Date date) {
		//
		if (date == null) {
			return "";
		}
		
		return "" + DateUtils.getRelativeTimeSpanString(date.getTime());
	}
}
