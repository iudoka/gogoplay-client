package com.gogoplay.util;

import com.gogoplay.R;

import android.content.Context;

public class FlurryUtils {
	//
	
	// Events
	public static final String s_LOGIN = "LOGIN";
	public static final String s_LOGOUT = "LOGOUT";
	public static final String s_BUY = "BUY";
	public static final String s_TRADE = "TRADE";
	public static final String s_ADD_COMMENT = "ADD_COMMENT";
	public static final String s_REMOVE_COMMENT = "REMOVE_COMMENT";
	public static final String s_LIKE = "LIKE";
	public static final String s_UNLIKE = "UNLIKE";
	public static final String s_SEARCH = "SEARCH";
	public static final String s_FILTER = "FILTER";
	public static final String s_WATCH = "WATCH";
	public static final String s_UNWATCH = "UNWATCH";
	public static final String s_REPORT_USER = "REPORT_USER";
	public static final String s_REPORT_ITEM = "REPORT_ITEM";
	public static final String s_FOLLOW = "FOLLOW";
	
	public static final String getFlurryKey(Context context) {
		//
		return context.getString(R.string.flurry_key);
	}
}
