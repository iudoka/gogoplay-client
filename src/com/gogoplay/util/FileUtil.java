package com.gogoplay.util;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
//import java.io.InputStreamReader;

import android.content.Context;
import android.util.Log;

public abstract class FileUtil {
	private static final String TAG = "FileUtil";
	
	public static boolean store(Context context, String uniqueName, String data) {
		//
		byte[] bytes = null;
		
		if (data == null) {
			bytes = new byte[]{};
		} else {
			bytes = data.getBytes();
		}
		
		return store(context, uniqueName, bytes);
	}
	
	public static boolean store(Context context, String uniqueName, byte[] data) {
		//
		FileOutputStream fos = null;
		try {
			fos = context.openFileOutput(uniqueName, Context.MODE_PRIVATE);
			fos.write(data);
		} catch (Exception e) {
			Log.e(TAG, "Unable to store data (" + uniqueName + "): " + data, e);
			return false;
		} finally {
			
			if (fos != null) {
				try {
					fos.close();
				} catch (Exception ex) {}
			}
		}
		
		return true;
	}
	
	public static String load(Context context, String uniqueName) {
		//
		byte [] data = loadBytes(context, uniqueName);
		
		if (data != null) {
			return new String(data);
		} else {
			return null;
		}
	}
		
	public static byte[] loadBytes(Context context, String uniqueName) {
		//
		byte[] data = null;
		FileInputStream fis = null;
		BufferedReader br = null;
		
		try {
			//
			fis = context.openFileInput(uniqueName);
//			InputStreamReader inputStreamReader = new InputStreamReader(fis);
//		    br = new BufferedReader(inputStreamReader);
//		    StringBuilder sb = new StringBuilder();
//		    String line;
//		    while ((line = br.readLine()) != null) {
//		        sb.append(line);
//		    }
//		    data = sb;
			
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			byte[] b = new byte[1024];
			int bytesRead;
			while ((bytesRead = fis.read(b)) != -1) {
			   bos.write(b, 0, bytesRead);
			}
			data = bos.toByteArray();
		} catch (Exception ex) {
			Log.e(TAG, "Unable to read data from disk", ex);
		} finally {
			
			if (br != null) {
				try {
					br.close();
				} catch (Exception ex) {}
			}
			
			if (fis != null) {
				try {
					fis.close();
				} catch (Exception ex) {}
			}
		}
		
		return data;
	}
}
