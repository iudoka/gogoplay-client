/**
 * 
 */
package com.gogoplay.util;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class StringOutputStream extends OutputStream
{
	private StringBuffer m_stringBuffer = new StringBuffer("");
	private int m_maxBuffer = 1024;
	
	int m_index = 0;
	List<Byte> m_bufferList = new ArrayList<Byte>(); 

	public StringOutputStream()
	{
		// do nothing
	}
	
	public StringOutputStream(int bufferSize)
	{
		if(bufferSize > 0)
		{
			m_maxBuffer = bufferSize;
			m_bufferList = new ArrayList<Byte>(bufferSize + 1);
			flush();
		}
	}
	
	public void write(int b) throws IOException
	{
		m_index++;
		
		if(m_index >= m_maxBuffer)
		{
			flush();
		}
		
		m_bufferList.add((byte)b);
	}
	
	public void flush()
	{
		byte[] buff = new byte[m_bufferList.size()];
		int index = 0;
		
		for(Byte aByte : m_bufferList)
		{
			buff[index++] = aByte;
		}
		
		m_stringBuffer.append(new String(buff));
		m_index = 0;
		m_bufferList.clear();
	}
	
	public String toString()
	{
		flush();
		return m_stringBuffer.toString();
	}
}