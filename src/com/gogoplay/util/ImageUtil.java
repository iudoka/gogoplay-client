package com.gogoplay.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import org.apache.commons.io.IOUtils;

import com.gogoplay.R;

public abstract class ImageUtil {
	//
	public static final int IMAGE_SIZE = 320;
	
	private static final String TAG = "ImageUtil";
	
	public static final String RESOURCE_ID = "resource";
	
	public static final String getAviaryKey(Context context) {
		//
		return context.getString(R.string.aviary_key);
	}
	
	public static Bitmap getScaledBitmap(InputStream in, int width, int height) throws IOException {
		//
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		IOUtils.copy(in, baos);
		byte[] bytes = baos.toByteArray();
		
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inSampleSize = ImageUtil.calculateSampleSize(new ByteArrayInputStream(bytes), width, height);
		
		return BitmapFactory.decodeStream(new ByteArrayInputStream(bytes), null, o);
	}
	
	public static int calculateSampleSize(InputStream in, int reqWidth, int reqHeight) {
		//
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(in, null, options);
		
		final int height = options.outHeight;
	    final int width = options.outWidth;
	    int inSampleSize = 1;

	    if (height > reqHeight || width > reqWidth) {

	        final int halfHeight = height / 2;
	        final int halfWidth = width / 2;

	        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
	        // height and width larger than the requested height and width.
	        while ((halfHeight / inSampleSize) > reqHeight
	                && (halfWidth / inSampleSize) > reqWidth) {
	            inSampleSize *= 2;
	        }
	    }

	    Log.i(TAG, "Sample size of image set to " + inSampleSize);
	    return inSampleSize;
	}

	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}
	
	public static InputStream getBitmapInputStream(String imageUrl) {
		try {
			URL url = new URL(imageUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoInput(true);
			connection.connect();
			return connection.getInputStream();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Bitmap getBitmapFromURL(String imageUrl) {
		try {
			URL url = new URL(imageUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoInput(true);
			connection.connect();
			InputStream input = connection.getInputStream();
			Bitmap myBitmap = BitmapFactory.decodeStream(input);
			return myBitmap;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void loadFromResource(Context context, int resId, Handler handler) {
		//
		new FetchImageTask(context, resId, handler).execute();
	}
	
	private static class FetchImageTask extends AsyncTask<Void, Void, Bitmap> {
		private Context context;
		private Handler uiCallback;
		private int resId;

		public FetchImageTask(Context context, int resId, Handler uiCallback) {
			this.context = context;
			this.uiCallback = uiCallback;
			this.resId = resId;
		}

		@Override
		protected Bitmap doInBackground(Void... params) {
			//
			return BitmapFactory.decodeResource(context.getResources(), resId);
		}

		@Override
		protected void onPreExecute() {
		}

		@Override
		protected void onProgressUpdate(Void... values) {
		}

		@Override
		protected void onPostExecute(Bitmap img) {
			//
			Message m = new Message();
			Bundle b = new Bundle();
			b.putParcelable(RESOURCE_ID, img);

			m.setData(b);

			uiCallback.sendMessage(m);
		}
	}
}
