/**
 * 
 */
package com.gogoplay.util;

import java.io.IOException;
import java.io.InputStream;

public class StringInputStream extends InputStream
{
	private int m_index = 0;
	private byte[] m_bytes;
	
	public StringInputStream(String str)
	{
		m_bytes = str.getBytes();
	}
	
	public int read() throws IOException
	{
		if(m_index >= m_bytes.length)
		{
			return -1;
		}
		
		int value = (int)m_bytes[m_index];
		m_index++;
		
		return value;
	}
}