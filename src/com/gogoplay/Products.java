package com.gogoplay;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.gogoplay.data.Comment;
import com.gogoplay.data.Item;
import com.gogoplay.pref.Sorter;
import com.gogoplay.pref.Sorter.SortType;
import com.gogoplay.ui.MutableDisplay;
import com.gogoplay.ui.ProductAdapter;
import com.gogoplay.ui.ShopActivity;
import com.gogoplay.ui.ShopApplication;
import com.gogoplay.ui.ShopFragment;
import com.gogoplay.ui.widget.ProgressDisplay;
import com.gogoplay.ui.widget.ShopGridView;
import com.gogoplay.ui.widget.ShopScrollView.ShopScrollViewListener;
import com.gogoplay.ui.widget.StaggeredGridView;
import com.gogoplay.ui.widget.TextEntryFragment;
import com.gogoplay.util.LocationUtil;
import com.gogoplay.util.Utils;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshGridView;

public class Products extends ShopFragment implements MutableDisplay, ShopScrollViewListener, OnRefreshListener<StaggeredGridView>, OnLastItemVisibleListener {
	//
	public final static int DISPLAY_DETAILS = 99;
	public final static int SORTER_REQUEST = 100;
	public final static int ADD_COMMENT = 101;
	
	private DisplayType currentDisplayType = DisplayType.SINGLE_ITEM;
	private int currentOrientation;
	private ProductFetcher defaultFetcher;
	private ProductFetcher productFetcher;
	private View root;
	private ViewGroup emptyArea;
	//private ShopSimpleScrollView scroller;
	private ProgressDisplay progress;
	//private NonScrollableGridView grid;
	//private PullToRefreshGridView ptrGrid;
	private ShopGridView ptrGrid;
	private StaggeredGridView grid;
	private PullToRefreshGridView imageGrid;
	
	private TextView footer;
	private ProductAdapter adapter;
	private ProductsHandler handler;
	private ItemDetailsHandler itemDetailsHandler;
	private TextView emptyTitle;
	private TextView empty;
	private int page;
	private Item lastItem;
	private boolean feedDisplay = true;
	//private boolean viewingDetails = false;
	private boolean initialized = false;
	private boolean stopped = false;
	private boolean checkSorter = true;
	private ViewGroup locationArea;
	private TextView location;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i(getTag(), "Just created Popular Products Fragment " + this);
	}
	
	public void setVisibility(int visibility) {
		
		if (root != null) {
			root.setVisibility(visibility);
		}
	}
	
	protected ShopGridView getPtrGrid() {
		return ptrGrid;
	}
	
	public Handler getHandler() {
		return handler;
	}
	
	public Item getLastItem() {
		return lastItem;
	}
	
	public void setLastItem(Item item) {
		this.lastItem = item;
	}
	
	public int getPage() {
		return page;
	}
	
	public void setPage(int page) {
		this.page = page;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		//
		return inflater.inflate(R.layout.products, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (savedInstanceState != null)
			return;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		defaultFetcher = new ProductFetcher() {
			
			@Override
			public void fetchItems(String lastItemId, Handler handler, boolean refresh) {
				// 
				Log.i(getTag(), "Fetching items " + (refresh ? " - for refresh" : ""));
				getDataService().getItems(getActivity(), lastItemId, handler, refresh);
			}
			
			public boolean canFetch() {
				//
				return true;
			}
		};
		
		createUI();
		setupListeners();
	}
	
	protected boolean checkSorter() {
		//
		boolean sorterSet = true;
		
		if (isSortingRequired() && checkSorter && isDataServiceConnected()) {
			Sorter sorter = getSorter();
			
			if (sorter == null) {
				// throw up location checker
				sorterSet = false;
				showSortLocationDialog();
			}
		}
		
		checkSorter = false;
		
		return sorterSet;
	}
	
	private void showSortLocationDialog() {
		//
		Intent intent = new Intent(getActivity(), LocationDialog.class);
		intent.putExtra(LocationDialog.EXTRA_SORTER, getSorter());
		
		startActivityForResult(intent, SORTER_REQUEST);
	}
	
	@Override
    public void onStart() {
        super.onStart();
		
		//if (isDataServiceBound()) {
			//
		//	attachDataService();
		//}
    }
	
	@Override
	public void onResume() {
		super.onResume(); 
	}
	
	public boolean isFragmentHidden() {
		return isHidden();
	}
	
	private void attachDataService() {
		//
		
		if (adapter != null) {
			// adapter.setDataService(getDataService());

			if (!isFragmentHidden()) {

				if (!isInit() || stopped) {
					//refresh();
				}/*else {
					if (itemDisplayed != null) {
						//update();
					} else {
						//populate();
						//update();
						refresh();
					}
					//refresh();
					//handler.displayItems(new ArrayList<Item>(adapter.getItems()), null, false);
				}*/
			}
		}
	}

    @Override
    public void onStop() {
        super.onStop();
        
        //if (!viewingDetails) {
        	stopped = true;
        //} else {
        	//viewingDetails = false;
        //}
    }
    
    @Override
    public void onPause() {
    	super.onPause();
    }
    
    @Override
    public void setMenu(Menu menu) {
    	super.setMenu(menu);
    	
    	//MenuItem overflow = menu.findItem(R.id.action_overflow);
    	MenuItem item = menu.findItem(R.id.sort_by_location);
    	item.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				//
				Sorter sorter = getDataService().getSorter(getActivity());
				
				if (sorter == null || sorter.location == null) {
					showSortLocationDialog();
				} else {
					sorter.setType(SortType.distance);
					getDataService().setSorter(getActivity(), sorter);
					refresh();
				}
				
				return true;
			}
		});
    	
    	item = menu.findItem(R.id.sort_by_date);
    	item.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				//
				Sorter sorter = getDataService().getSorter(getActivity());
				
				if (sorter == null) {
					sorter = new Sorter(); // sort by date 
				} else {
					sorter.setType(SortType.date);
				}
				
				getDataService().setSorter(getActivity(), sorter);
				refresh();
				
				return true;
			}
		});
    	
    }

	@Override
	public void onDestroy() {
		//
		super.onDestroy();
		
		ShopApplication.unbindDrawables(root);
	}

	private void createUI() {
		//
		root = getView().findViewById(R.id.root);
		locationArea = (ViewGroup) getView().findViewById(R.id.sortArea);
		location = (TextView) getView().findViewById(R.id.sortNear);
		emptyArea = (ViewGroup) getView().findViewById(R.id.emptyArea);
		
		OnClickListener addItemListener = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 
				onEmptyMessageClicked();
			}
		};
		
		/*ImageView img = (ImageView) getView().findViewById(R.id.emptyIcon);
		if (getEmptyIcon() != null) {
			//
			img.setImageDrawable(getEmptyIcon());
			img.setOnClickListener(addItemListener);
		} else {
			//
			img.setVisibility(View.GONE);
		}*/
		
		emptyTitle = (TextView) getView().findViewById(R.id.emptyTextTitle);
		emptyTitle.setVisibility(View.GONE);
		emptyTitle.setText(getEmptyTitle());
		
		empty = (TextView) getView().findViewById(R.id.emptyText);
		empty.setVisibility(View.GONE);
		empty.setText(getEmptyText());
		empty.setOnClickListener(addItemListener);
		
		//scroller = (ShopSimpleScrollView) getView().findViewById(R.id.scroller);
		//scroller.setScrollViewListener(this);
		
		progress = (ProgressDisplay) getView().findViewById(R.id.progress);

		adapter = new ProductAdapter(this, getActivity(), null);
		//grid = (NonScrollableGridView) getView().findViewById(R.id.itemGrid);
		ptrGrid = (ShopGridView) getView().findViewById(R.id.itemGrid);
		ptrGrid.setEmptyView(emptyArea);
		ptrGrid.setOnRefreshListener(this);
		ptrGrid.setOnLastItemVisibleListener(this);
		
		grid = ptrGrid.getRefreshableView();
		
		imageGrid = (PullToRefreshGridView) getView().findViewById(R.id.imageGrid);
		imageGrid.setEmptyView(emptyArea);
		imageGrid.setOnRefreshListener(new GridRefreshListener());
		imageGrid.setOnLastItemVisibleListener(this);
		imageGrid.setVisibility(isFeedDisplay() ? View.GONE : View.VISIBLE);

		//
		footer = new TextView(getActivity());
		footer.setGravity(Gravity.CENTER);
		footer.setText(getString(R.string.end_of_list));
		footer.setTextColor(getResources().getColor(R.color.light_blue));
		footer.setTextSize(16);
		footer.setTypeface(footer.getTypeface(), Typeface.BOLD);
		int padding = (int) getResources().getDimensionPixelSize(R.dimen.padding);
		footer.setPadding(padding, padding, padding, padding);
		footer.setBackgroundResource(R.drawable.solid_rectangle);
		
		//
		LinearLayout footerLayout = new LinearLayout(getActivity());
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		footerLayout.setLayoutParams(params);
		params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		footerLayout.addView(footer, params);
		
		grid.addFooterView(footerLayout);
		
		grid.setAdapter(adapter);
		imageGrid.setAdapter(adapter);

		lastItem = null;
		page = 0;
	}

	private void setupListeners() {
		// Set a listener to be invoked when the list should be refreshed.
		// DO NOT UNCOMMENT
		//scroller.setOnRefreshListener(new RefreshListener());

		// create handlers on UI main thread
		handler = new ProductsHandler();
		
		itemDetailsHandler = new ItemDetailsHandler();
		
		locationArea.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//
				showSortLocationDialog();
			}
		});
	}
	
	protected String getEmptyTitle() {
		//
		return getString(R.string.emptyProductsTitle);
	}
	
	protected String getNoResultsTitle() {
		return getString(R.string.noSearchResultsTitle);
	}
	
	protected String getEmptyText() {
		//
		return getString(R.string.emptyProducts);
	}
	
	protected String getNoResultsText() {
		return getString(R.string.noSearchResults);
	}
	
	protected Drawable getEmptyIcon() {
		//
		return getResources().getDrawable(R.drawable.ic_menu_camera);
	}

	public void refresh() {
		refresh(true);
	}
	
	protected void hideGrid() {
		grid.setVisibility(View.INVISIBLE);
	}
	
	protected void showGrid() {
		grid.setVisibility(View.VISIBLE);
	}
	
	private void refresh(boolean showMessage) {
		//
		if (stopped) {
			//
			stopped = false;
			
			/*if (!viewingDetails) {
				clear();
			} else {
				viewingDetails = false;
				return;
			}*/
		}
		
		if (isDataServiceConnected()) {

			if (checkSorter()) {

				if (showMessage) {
					showMessage("Refreshing...");
				}

				if (isSortingRequired()) {
					Sorter s = getSorter();
					
					if (s != null && s.type == SortType.distance) {
						locationArea.setVisibility(View.VISIBLE);
						location.setText(s.location.getExtras().getString(
								LocationUtil.EXTRA_CITY_AND_STATE_CODE));
					} else {
						locationArea.setVisibility(View.GONE);
					}
				} else {
					locationArea.setVisibility(View.GONE);
				}

				initialized = true;
				page = 0;
				clear();
				fetchItems(null, handler, true);
			}
		}
	}
	
	private class GridRefreshListener implements OnRefreshListener<GridView> {

		@Override
		public void onRefresh(PullToRefreshBase<GridView> refreshView) {
			// 
			refresh();
		}
	}
	
	public Sorter getSorter() {
		//
		if (isSortingRequired() && isDataServiceConnected()) {
			return getDataService().getSorter(getActivity());
		} else {
			return null;
		}
	}
	
	public boolean isInit() {
		//
		return initialized; //adapter.getCount() > 0;
	}
	
	public void setInitialized(boolean initialized) {
		this.initialized = initialized;
	}

	private void loadNextPage() {
		// load the next page of products
		
		if (isDataServiceConnected() && lastItem != null) {
			// make sure reload is only called once as needed
			showMessage("Loading next page...");
			
			page++;
			
			String bottom = lastItem.getId();
			
			Sorter sorter = getDataService().getSorter(getActivity());
			if (sorter != null && sorter.getType() == Sorter.SortType.distance) {
				//
				bottom = lastItem.getDistance();
			}
			
			fetchItems(bottom, handler, false);
			lastItem = null;
		}
	}

	public void populate() {
		//
		if (isDataServiceConnected()) {
			Log.i(getTag(), "Fetching Items");
			showMessage("Loading...");
			
			fetchItems(null, handler, false);
		}
	}
	
	public interface ProductFetcher {
		public void fetchItems(String lastItemId, Handler handler, boolean refresh);
		
		public boolean canFetch();
	}
	
	private void fetchItems(String lastItemId, Handler handler, boolean refresh) {
		//
		if (getProductFetcher().canFetch()) {
			//emptyArea.setVisibility(View.GONE);
			footer.setText(getString(R.string.loading));
			footer.setVisibility(View.VISIBLE);
			getProductFetcher().fetchItems(lastItemId, handler, refresh);
		} else {
			hideMessage();
			//if (adapter.getCount() <= 0) {
				//emptyArea.setVisibility(View.VISIBLE);
			//}
		}
	}
	
	public ProductFetcher getProductFetcher() {
		//
		if (productFetcher == null) {
			productFetcher = defaultFetcher;
		}
		
		return productFetcher;
	}
	
	public void setProductFetcher(ProductFetcher fetcher) {
		productFetcher = fetcher;
	}
	
	public void showMessage(String message) {
		//
		progress.setVisibility(View.VISIBLE);
		progress.setText(message);
	}
	
	public void hideMessage() {
		progress.setVisibility(View.GONE);
		ptrGrid.onRefreshComplete();
		imageGrid.onRefreshComplete();
	}
	
	/**
	 * Safest place to update UI items
	 */
	@SuppressLint("HandlerLeak")
	private class ProductsHandler extends Handler {
		//
		@SuppressWarnings("unchecked")
		@Override
		public void handleMessage(Message msg) {
			//
			ArrayList<Item> items = (ArrayList<Item>) msg.getData().get("items");
			boolean refresh = msg.getData().getBoolean("refresh");
			String lastItemId = msg.getData().getString("lastItemId");
			
			displayItems(items, lastItemId, refresh);
		}
			
		public void displayItems(List<Item> items, String lastItemId, boolean refresh) {
			//
			if (!isAdded()) {
				return;
			}
			
			hideMessage();
			
			//redraw();
			//grid.setNumColumns((int)adapter.getNumColumns());

			footer.setVisibility(View.GONE);
			grid.setColumnCount((int)adapter.getNumColumns(), false);
			
			if (!Utils.isEmpty(items)) {
				//
				emptyTitle.setVisibility(View.GONE);
				empty.setVisibility(View.GONE);
				lastItem = items.get(items.size() - 1);
					
				if (refresh) {
					// refresh data
					adapter.refresh(items);
					
				} else if (lastItemId == null) {
					//
					adapter.refresh(items);
				}
				else {
					adapter.append(items);
				}
			} else {
				//
				lastItem = null;
				
				if (refresh) {
					//
					// refresh data
					clear();
					
					emptyTitle.setVisibility(View.VISIBLE);
					empty.setVisibility(View.VISIBLE);
				}

				footer.setText(getString(R.string.end_of_list));
			}
			
			toggleEmptyMessage(refresh);
			//grid.invalidateViews();
			
			//footer.setVisibility(adapter.getCount() == 1 ? View.GONE : View.VISIBLE);
		}
	}
	
	public void clear() {
		//
		adapter.refresh(new ArrayList<Item>());
	}
	
	public boolean isEmpty() {
		return adapter.getCount() <= 0;
	}
	
	public int getCount() {
		//
		return adapter.getCount();
	}
	
	protected void onEmptyMessageClicked() {
		//
		if (isEmpty()) {
			//((Home)getActivity()).goToAddItem();
		}
	}
	
	private void toggleEmptyMessage(boolean refreshed) {
		//
		if (refreshed) {
			emptyTitle.setText(getNoResultsTitle());
			empty.setText(getNoResultsText());
		} else {
			emptyTitle.setText(getEmptyTitle());
			empty.setText(getEmptyText());
		}
		
		if (adapter.getCount() <= 0) {
			//
			//emptyArea.setVisibility(View.VISIBLE);
			grid.setVisibility(View.GONE);
			imageGrid.setVisibility(View.GONE);
		} else {
			//
			//emptyArea.setVisibility(View.GONE);
			if (isFeedDisplay()) {
				grid.setVisibility(View.VISIBLE);
				imageGrid.setVisibility(View.GONE);
			} else {
				grid.setVisibility(View.GONE);
				imageGrid.setVisibility(View.VISIBLE);
			}
		}
	}
	
	@Override
	public void update() {
		//
		redraw();
	}
	
	public void redraw() {
		//
		int cc = (int) adapter.getNumColumns();
		grid.setColumnCount(cc, false);
		imageGrid.getRefreshableView().setNumColumns(cc);
		adapter.redraw();
		//grid.invalidateViews();
		toggleEmptyMessage(false);
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// 
		super.onConfigurationChanged(newConfig);
		
		if (newConfig.orientation != currentOrientation) {
			//
			currentOrientation = newConfig.orientation;
			//scroller.setVisibility(View.GONE);
			redraw();
			//refresh();
		}
	}

	@Override
	public void dataServiceConnected(boolean connected) {
		//
		if (connected) {
			
			//if (!dataServiceInit) {
				//dataServiceInit = true;
				attachDataService();
			//}
		} else {
			//itemDisplayed = null;
			hideMessage();
		}
	}

	@Override
	public void onScrollStarted() {
		// 
		if (isDataServiceConnected()) {
			getDataService().pauseThumbnailFetcher(true);
		}
	}

	@Override
	public void onScrollStopped() {
		//
		if (isDataServiceConnected()) {
			getDataService().pauseThumbnailFetcher(false);
		}
	}

	@Override
	public void onScrolledToEnd() {
		//
	    //Log.i(getTag(), "Scrolled to end");
		loadNextPage();
	}
	
	@Override
	public void setDisplayType(DisplayType type) {
		// 
		/*int displayCols = 2;
		
		if (type == DisplayType.THREE_PER_LINE) {
			displayCols = 3;
		} else if (type == DisplayType.SINGLE_ITEM) {
			displayCols = 1;
		}
		
		grid.setNumColumns(displayCols);
		grid.invalidateViews();*/
		
		currentDisplayType = type;
	}
	
	public DisplayType getCurrentDisplayType() {
		return currentDisplayType;
	}
	
	@Override
	public CharSequence getNextDisplayTypeName() {
		if (isFeedDisplay()) {
			return getString(R.string.action_image_type);
		} else {
			return getString(R.string.action_feed_type);
		}
	}
	
	@Override
	public int getNextDisplayTypeIcon() {
		// 
		if (isFeedDisplay()) {
			return R.drawable.ic_action_view_as_grid;
		} else {
			return R.drawable.ic_menu_allfriends;
		}
	}
	
	@Override
	public String getTitle() {
		return "Explore"; //getString(R.string.product_list_title);
	}

	public void displayProductDetails(Item item) {
		// 
		showMessage("");
		getDataService().getItem(getActivity(), item.getId(), null, itemDetailsHandler);
	}
	
	@SuppressLint("HandlerLeak")
	private class ItemDetailsHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			hideMessage();
			Item item = (Item) msg.getData().get("item");
			goToDetails(item);
		}
	}
	
	public void addComment(Item item) {
		//
		// check authorization
		if (isDataServiceConnected() && getDataService().checkAuthentication(getActivity())) {
			Intent intent = new Intent(getActivity(), TextEntryFragment.class);
			
			intent.putExtra(TextEntryFragment.EXTRA_ITEM, item);
			intent.putExtra(TextEntryFragment.EXTRA_TITLE, getString(R.string.comment));
			intent.putExtra(TextEntryFragment.EXTRA_MAX_CHARS, getResources().getInteger(R.integer.max_comment_length));
			
			startActivityForResult(intent, ADD_COMMENT);
		}
	}
	
	private void goToDetails(Item item) {
		//
		try {
			//viewingDetails = true;
			Intent intent = new Intent(getActivity(), ProductDetail.class);
			intent.setAction(ProductDetail.ACTION_DETAILS);
			
			intent.putExtra("params", item);
			
			startActivityForResult(intent, DISPLAY_DETAILS);
			//getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
		} catch (Exception ex) {
			//
			
		}
	}
	
	private void updateGrid(Item newItem) {
		if (newItem == null || grid.getCount() <= 0) 
			return;
		
		int start = grid.getFirstVisiblePosition();
		
		// Update the current adapter
		for (int i = start, j = grid.getLastVisiblePosition(); i <= j; i++) {
			Item oldItem = (Item) grid.getItemAtPosition(i);
			if (oldItem != null && Utils.equals(newItem.getId(),oldItem.getId())) {
				// store the new changes
				oldItem.merge(newItem);
				View view = grid.getChildAt(i - start);
				grid.getAdapter().getView(i, view, grid);
				break;
			}
		}
	}
	
	@SuppressLint("HandlerLeak")
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// 
		if (data != null && resultCode == ShopActivity.RESULT_OK) {
			if (requestCode == DISPLAY_DETAILS) {
				// update current item
				Item newItem = (Item) data.getExtras().get("item");
				
				if (newItem != null) {
					//
					updateGrid(newItem);
				}
			} else if (requestCode == SORTER_REQUEST) {
				//
				Sorter sorter = (Sorter) data.getExtras().get(LocationDialog.EXTRA_SORTER);
				
				if (sorter != null && isDataServiceConnected()) {
					getDataService().setSorter(getActivity(), sorter);
					refresh();
				}
			} else if (requestCode == ADD_COMMENT) {
				//
				String text = data.getExtras().getString(TextEntryFragment.EXTRA_TEXT);
				final Item item = (Item) data.getExtras().getSerializable(TextEntryFragment.EXTRA_ITEM);
				
				Comment comment = new Comment("", new Date(), getDataService().getUser(getActivity()), Utils.nullEmptyString(text), 0, null);
				showMessage("");
				getDataService().addComment(getActivity(), comment, item, new Handler() {
					@Override
					public void handleMessage(Message msg) {
						//
						boolean success = msg.getData().getBoolean("success");
						
						if (!success) {
							//
							hideMessage();
							Toast toast = Toast.makeText(getActivity(), "Unable to post comment!", Toast.LENGTH_LONG);
							toast.show();
						} else {
							getDataService().getItem(getActivity(), item.getId(), item, new Handler() {
								@Override
								public void handleMessage(Message msg) {
									// 
									Item updatedItem = (Item)msg.getData().get("item");
									
									if (updatedItem != null) {
										//
										updateGrid(updatedItem);
									}
									hideMessage();
								}
							});
						}
					}
				});
			}
		} else {
			if (requestCode == SORTER_REQUEST) {
				//
				if (isDataServiceConnected()) {

					Sorter oldSorter = getDataService().getSorter(getActivity());
					
					if (oldSorter == null) {
						// set default sorter to sort by date if no current sorter set
						Sorter sorter = new Sorter(); // sort by date 
						getDataService().setSorter(getActivity(), sorter);
						refresh();
					}
				}
			}
		}
		
		super.onActivityResult(requestCode, resultCode, data);
	}

	/**
	 * @return the feedDisplay
	 */
	public boolean isFeedDisplay() {
		return feedDisplay;
	}

	/**
	 * @param feedDisplay the feedDisplay to set
	 */
	public void toggleFeedDisplay() {
		//
		this.feedDisplay = !feedDisplay;
		int cc = (int)adapter.getNumColumns();
		
		if (feedDisplay) {
			//
			grid.setColumnCount(cc);
			ptrGrid.setVisibility(View.VISIBLE);
			imageGrid.setVisibility(View.GONE);
		} else {
			//
			imageGrid.getRefreshableView().setNumColumns(cc);
			ptrGrid.setVisibility(View.GONE);
			imageGrid.setVisibility(View.VISIBLE);
		}
		
		adapter.notifyDataSetChanged();
		//refresh();
	}
	
	public void toggleDisplay() {
		toggleFeedDisplay();
	}

	@Override
	public void onRefresh(PullToRefreshBase<StaggeredGridView> refreshView) {
		// 
		refresh();
	}

	@Override
	public void onLastItemVisible() {
		// 
		loadNextPage();
	}
	
	public boolean isSortingRequired() {
		return true;
	}
}
