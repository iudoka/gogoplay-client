package com.gogoplay.pref;

import java.io.Serializable;

public class NotificationPreference implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8620359614596850376L;
	
	//
	private boolean like;
	private boolean comment;
	private boolean email;
	private boolean watch;
	private boolean follow;

	public NotificationPreference(boolean like, boolean comment, boolean email, boolean watch, boolean follow) {
		//
		this.like = like;
		this.comment = comment;
		this.email = email;
		this.watch = watch;
		this.follow = follow;
	}
	
	/**
	 * @return the like
	 */
	public boolean isLike() {
		return like;
	}

	/**
	 * @param like
	 *            the like to set
	 */
	public void setLike(boolean like) {
		this.like = like;
	}

	/**
	 * @return the comment
	 */
	public boolean isComment() {
		return comment;
	}

	/**
	 * @param comment
	 *            the comment to set
	 */
	public void setComment(boolean comment) {
		this.comment = comment;
	}

	/**
	 * @return the email
	 */
	public boolean isEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(boolean email) {
		this.email = email;
	}

	/**
	 * @return the watch
	 */
	public boolean isWatch() {
		return watch;
	}

	/**
	 * @param watch the watch to set
	 */
	public void setWatch(boolean watch) {
		this.watch = watch;
	}

	/**
	 * @return the follow
	 */
	public boolean isFollow() {
		return follow;
	}

	/**
	 * @param follow the follow to set
	 */
	public void setFollow(boolean follow) {
		this.follow = follow;
	}
}
