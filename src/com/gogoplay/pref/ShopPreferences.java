package com.gogoplay.pref;

import android.os.Bundle;

import com.gogoplay.R;
import com.gogoplay.ui.ShopPreferenceActivity;

public class ShopPreferences extends ShopPreferenceActivity {
	//
	
	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		addPreferencesFromResource(R.xml.shop_preferences);

		getActionBar().setDisplayHomeAsUpEnabled(true);
	}
}
