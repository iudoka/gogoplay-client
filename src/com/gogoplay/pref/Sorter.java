package com.gogoplay.pref;

import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import android.location.Location;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

public class Sorter implements Parcelable {
	//
	public static final SortType DEFAULT_SORT = SortType.date;
	public static final int DEFAULT_RADIUS = 100; // 50 mile radius

	public enum SortType {
		date("i"), distance("d");

		private String type;

		private SortType(String type) {
			this.type = type;
		}

		public String toString() {
			return type;
		}
	}

	public SortType type = DEFAULT_SORT;
	public boolean useCurrentLocation;

	public Location location;
	public int radius = DEFAULT_RADIUS;
	
	public Sorter() {
		//
		type = DEFAULT_SORT;
		useCurrentLocation = false;
		location = null;
		radius = DEFAULT_RADIUS;
	}
	
	public Sorter(Parcel in) {
		//
		readFromParcel(in);
	}

	public String toString() {
		return type.toString();
	}

	/**
	 * @return the type
	 */
	public SortType getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(SortType type) {
		this.type = type;
	}

	/**
	 * @return the useCurrentLocation
	 */
	public boolean isUseCurrentLocation() {
		return useCurrentLocation;
	}

	/**
	 * @param useCurrentLocation
	 *            the useCurrentLocation to set
	 */
	public void setUseCurrentLocation(boolean useCurrentLocation) {
		this.useCurrentLocation = useCurrentLocation;
	}

	/**
	 * @return the location
	 */
	public Location getLocation() {
		return location;
	}

	/**
	 * @param location
	 *            the location to set
	 */
	public void setLocation(Location location) {
		this.location = location;
	}

	/**
	 * @return the radius
	 */
	public int getRadius() {
		return radius;
	}

	/**
	 * @param radius
	 *            the radius to set
	 */
	public void setRadius(int radius) {
		this.radius = radius;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int flags) {
		// 
		parcel.writeString(type.name());
		parcel.writeInt(useCurrentLocation ? 1 : 0);
		parcel.writeParcelable(location, flags);
		parcel.writeInt(radius);
	}
	
	private void readFromParcel(Parcel parcel) {    
        //
		type = SortType.valueOf(parcel.readString());
		useCurrentLocation = parcel.readInt() == 1 ? true : false;
		location = (Location) parcel.readParcelable(Location.class.getClassLoader());
		radius = parcel.readInt();
    }  
  
    public static final Parcelable.Creator<Sorter> CREATOR = new Parcelable.Creator<Sorter>() {  
    
        public Sorter createFromParcel(Parcel in) {  
            return new Sorter(in);  
        }  
   
        public Sorter[] newArray(int size) {  
            return new Sorter[size];  
        }  
          
    };
    
	public static String toJson(Sorter sorter) {
		//
		try {
			JSONObject json = new JSONObject();

			json.put("type", sorter.type.name());
			json.put("useCurrentLocation", sorter.useCurrentLocation);
			json.put("radius", sorter.radius);
			json.put("location.latitude", sorter.location.getLatitude());
			json.put("location.longitude", sorter.location.getLongitude());
			
			if (sorter.location.getExtras() != null) {
				//
				for (String key : sorter.location.getExtras().keySet()) {
					json.put("location." + key, sorter.location.getExtras().getString(key));
				}
			}

			return json.toString();
		} catch (JSONException ex) {

		}

		return null;
	}
	
	public static Sorter fromJson(String json) {
		//
		Sorter sorter = null;
		
		if (json != null) {
			sorter = new Sorter();
			
			try {
				JSONObject sorterJson = new JSONObject(json);
				
				sorter.setType(SortType.valueOf(sorterJson.getString("type")));
				sorter.setUseCurrentLocation(sorterJson.getBoolean("useCurrentLocation"));
				sorter.setRadius(sorterJson.getInt("radius"));
				
				Location location = new Location("");
				location.setLatitude(sorterJson.getInt("location.latitude"));
				location.setLongitude(sorterJson.getInt("location.longitude"));
				
				@SuppressWarnings("rawtypes")
				Iterator keys = sorterJson.keys();
				location.setExtras(new Bundle());
				
				while (keys.hasNext()) {
					//
					Object iterObj = keys.next();
					String key = iterObj.toString();
					
					if (key.contains("location.") && 
						(!key.equalsIgnoreCase("location.latitude") && !key.equalsIgnoreCase("location.longitude"))) {
						//
						location.getExtras().putString(key.replace("location.", ""), sorterJson.getString(key));
					}
				}
				sorter.setLocation(location);
			} catch (Exception ex) {
				
			}
		}
		
		return sorter;
	}
}