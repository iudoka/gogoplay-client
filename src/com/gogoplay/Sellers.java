package com.gogoplay;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gogoplay.data.Seller;
import com.gogoplay.ui.SellersAdapter;
import com.gogoplay.ui.ShopApplication;
import com.gogoplay.ui.ShopFragment;
import com.gogoplay.ui.widget.ProgressDisplay;
import com.gogoplay.ui.widget.ShopGridView;
import com.gogoplay.ui.widget.ShopScrollView.ShopScrollViewListener;
import com.gogoplay.util.Utils;

public class Sellers extends ShopFragment implements ShopScrollViewListener {
	//
	
	private View root;
	//private ShopSimpleScrollView scroller;
	private ProgressDisplay progress;
	private ShopGridView list;
	private SellersAdapter adapter;
	private SellersHandler sellersHandler;
	private int page;
	private Seller lastSeller;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		//
		return inflater.inflate(R.layout.sellers, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (savedInstanceState != null)
			return;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		createUI();
		setupListeners();
	}
	
	@Override
    public void onStart() {
        super.onStart();
		
		if (isDataServiceConnected()) {
			//
			attachDataService();
		}
    }
	
	@Override
	public void onResume() {
		super.onResume();

		list.requestFocus();
	}
	
	private void attachDataService() {
		//
		if (isDataServiceConnected()) {
	        //if (adapter.getCount() == 0) {
	        //	populate();
	        //}
		}
	}

    @Override
    public void onStop() {
        super.onStop();
    }
    
    @Override
    public void onPause() {
    	super.onPause();
    }

	@Override
	public void onDestroy() {
		//
		super.onDestroy();
		
		ShopApplication.unbindDrawables(root);
	}

	private void createUI() {
		//
		root = getView().findViewById(R.id.root);
		//scroller = (ShopSimpleScrollView) getView().findViewById(R.id.scroller);
		//scroller.setScrollViewListener(this);
		
		progress = (ProgressDisplay) getView().findViewById(R.id.progress);

		//FragmentManager manager = getActivity().getSupportFragmentManager();
		//ShopActivityBase activity = (ShopActivityBase) getActivity();
		
		adapter = new SellersAdapter(this, getActivity(), null);
		list = (ShopGridView) getView().findViewById(R.id.sellerList);
		list.setAdapter(adapter);
		
		// create handlers on UI main thread
		sellersHandler = new SellersHandler();

		lastSeller = null;
		page = 0;
		
	}
	
	private void setupListeners() {
		//
	}
	
	@Override
	public void refresh() {
		//
		if (isDataServiceConnected()) {
			showMessage("Refreshing...");
			
			page = 0;
			getDataService().getSellers(getActivity(), page + "", sellersHandler, true);
			/*gallery.requestFocus();
			getDataService().refreshFeaturedItems(getActivity(), featuredHandler);
			getDataService().refreshItems(getActivity(), popularHandler);*/
		}
	}
	
	private void loadNextPage() {
		// load the next page of products
		
		if (isDataServiceConnected() && lastSeller != null) {
			// make sure reload is only called once as needed
			showMessage("Loading next page...");
			
			page++;
			getDataService().getSellers(getActivity(), page + "", sellersHandler, false);
			lastSeller = null;
		}
	}
	
	/*private void populate() {
		//
		
		if (isDataServiceBound()) {
			Log.i(getTag(), "Just populated Popular Products Fragment " + this);
			showMessage("Loading...");
			
			getDataService().getSellers(getActivity(), page + "", sellersHandler, false);
		}
	}*/
	
	/**
	 * Safest place to update UI items
	 */
	@SuppressLint("HandlerLeak")
	private class SellersHandler extends Handler {
		//
		@SuppressWarnings("unchecked")
		@Override
		public void handleMessage(Message msg) {
			//
			ArrayList<Seller> items = (ArrayList<Seller>) msg.getData().get("sellers");
			boolean refresh = msg.getData().getBoolean("refresh");
			displaySellers(items, refresh);
		}

		private void displaySellers(ArrayList<Seller> sellers, boolean refresh) {
			//
			hideMessage();
			
			if (!Utils.isEmpty(sellers)) {
				lastSeller = sellers.get(sellers.size() - 1);
				
				if (refresh) {
					adapter.refresh(sellers);
				} else {
					//
					adapter.append(sellers);
				}
			}
		}
	}
	
	private void showMessage(String message) {
		//
		progress.setVisibility(View.VISIBLE);
		progress.setText(message);
	}
	
	private void hideMessage() {
		progress.setVisibility(View.GONE);
	}

	@Override
	public void dataServiceConnected(boolean connected) {
		//
		if (connected) {
			attachDataService();
		}
	}

	@Override
	public void onScrollStarted() {
		// 
		if (isDataServiceConnected()) {
			getDataService().pauseThumbnailFetcher(true);
		}
	}

	@Override
	public void onScrollStopped() {
		//
		if (isDataServiceConnected()) {
			getDataService().pauseThumbnailFetcher(false);
		}
	}

	@Override
	public void onScrolledToEnd() {
		//
	    //Log.i(getTag(), "Scrolled to end");
		loadNextPage();
	}
	
	@Override
	public String getTitle() {
		return "Top Sellers";//getString(R.string.sellers_list_title);
	}
}
