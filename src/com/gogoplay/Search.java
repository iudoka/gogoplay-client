package com.gogoplay;

import java.text.NumberFormat;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.MenuItemCompat.OnActionExpandListener;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnCloseListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.gogoplay.Products.ProductFetcher;
import com.gogoplay.data.Item;
import com.gogoplay.data.SearchContentProvider;
import com.gogoplay.data.SearchEntryHelper;
import com.gogoplay.ui.ShopFragment;
import com.gogoplay.ui.widget.ProgressDisplay;
import com.gogoplay.ui.widget.SlidingDrawer;
import com.gogoplay.util.LocationUtil;
import com.gogoplay.util.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.LatLngBounds.Builder;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;

/**
 * do not zoom on activity return from detail screen
 * 
 * @author iudoka
 *
 */
public class Search extends ShopFragment implements LocationListener, LoaderCallbacks<Cursor> {

	public enum DisplayType {
		LIST,
		MAP;
	}
	
	private static final int DISPLAY_DETAILS = 200;
	private static final int ZOOM = 10;
	private static final int ZOOM_DISTANCE = 709;
	private static final double EARTHRADIUS = 6366198;

	private DisplayType type = DisplayType.LIST;
	private SearchView entry;
	private SlidingDrawer filterSlider;
	private com.gogoplay.data.SearchFilter currentFilter;
	private Button filterBtn;
	private SearchEntryHelper previousHelper;
	private ViewGroup previousSearchArea;
	private ListView previousSearches;
	private SimpleCursorAdapter previousAdapter;
	private SearchProducts listResults;
	private ProductFetcher fetcher;
	private Handler handler;
	private SupportMapFragment mapResults;
	private GoogleMap googleMap;
	private LocationUtil locUtil;
	private ArrayList<Item> currentResults;
	private int currentTotal;
	private FrameLayout resultLayout;
	private boolean mapReady = false;
	private ProgressDisplay progress;
	private Item itemDisplayed;
	private TextView clearPrevious;
	private MenuItem listOrMapMenu;
	private String currentSearch;
	private SearchFilter filterView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent,
			Bundle savedInstanceState) {
		//
		return inflater.inflate(R.layout.search, parent, false);
	}
	
	public void setMenu(Menu menu) {
		//
		listOrMapMenu = menu.findItem(R.id.action_list_or_map);
		
		entry = getSearchView(menu);
		entry.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
		entry.setQueryHint(getResources().getString(R.string.search_hint));
		entry.setIconifiedByDefault(false);
		
		entry.setOnQueryTextFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				//
				if (hasFocus) {
					entry.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
					
					// default search display is list
					if (type == DisplayType.MAP) {
						((Home)getActivity()).toggleSearchDisplayType();
					}
				}
				
				showPreviousSearches(hasFocus);
			}
		});
		
		/*entry.setOnSearchClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 
				resultCountArea.setVisibility(View.GONE);
				Log.i(getTag(), "Search was clicked!");
			}
		});*/
		
		entry.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			@Override
			public boolean onQueryTextChange(String newText) {
				// 
				//showPreviousSearches(true);
				return false;
			}

			@Override
			public boolean onQueryTextSubmit(String query) {
				//
				runSearch(query, true);
				return true;
			}
		});
		

		entry.setOnCloseListener(new OnCloseListener() {
			
			@Override
			public boolean onClose() {
				//
				//resultCountArea.setVisibility(listResults.isEmpty() ? View.GONE : View.VISIBLE);
				Log.i(getTag(), "Search closed");
				return false;
			}
		});
		
		/*searchArea.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 
				entry.onActionViewExpanded();
			}
		});*/
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		//
		super.onViewCreated(view, savedInstanceState);
		
		createUI(view, savedInstanceState);
		setupListeners();
	}
	
	public void setCurrentFilter(com.gogoplay.data.SearchFilter filter) {
		// set filter and re-run search
		showFilter(false);
		currentFilter = filter;
		//runSearch(currentSearch, false);
		
		if (type == DisplayType.LIST){
			listResults.refresh();
		} else {
			// update map with results
			fetcher.fetchItems(null, handler, true);
		}
	}
	
	@SuppressLint("HandlerLeak")
	private class ResultHandler extends Handler {
		//
		@SuppressWarnings("unchecked")
		@Override
		public void handleMessage(Message msg) {
			//
			progress.setVisibility(View.VISIBLE);
			ArrayList<Item> items = (ArrayList<Item>) msg.getData().get("items");
			boolean refresh = msg.getData().getBoolean("refresh");
			int totalCount = msg.getData().getInt("totalCount");

			currentResults = items;
			currentTotal = totalCount;
			
			if (refresh) {
				// 
				showFilterButton(totalCount > 0);
				listOrMapMenu.setVisible(totalCount > 0);
				String countText = null;
				
				if (totalCount > 0) {
					countText = totalCount + " result" + (totalCount != 1 ? "s" : "");
					((Home)getActivity()).setViewTitle(entry.getQuery(), countText);
				}
			}
			
			displayItems(items, refresh);
		}
	}
	
	private void displayItems(ArrayList<Item> items, boolean refresh) {
		//
		if (type == DisplayType.LIST) {
			// forward to results table
			Message m = new Message();
			Bundle b = new Bundle();
			b.putSerializable("items", items);
			b.putBoolean("refresh", refresh);
			m.setData(b);
			
			/*if (refresh) {
				// forward response to wrapped results list
				//listResults.clear();
				listResults.getHandler().sendMessage(m);
			} else {
				Log.i(getTag(), "Displaying items refresh is false.");*/
				//listResults.hideMessage();
				listResults.getHandler().sendMessage(m);
			//}
			//listResults.showGrid(); //setVisibility(View.VISIBLE);
		} else {
			// add markers to map
			updateMarkers(items, refresh);
		}
	}
	
	private void updateMarkers(ArrayList<Item> items, boolean refresh) {
		// 
		// add markers to map
		if (refresh) {
			googleMap.clear();
		}

		if (mapReady) {
			// show markers
			showMarkers(items);
		}
	}

	protected String getEmptyTitle() {
		//
		return "";//getString(R.string.emptyWishListTitle);
	}
	
	protected String getEmptyText() {
		//
		return "";//getString(R.string.emptyWishList);
	}
	
	protected Drawable getEmptyIcon() {
		//
		return null;//getResources().getDrawable(R.drawable.ic_heart_red);
	}
	
	private void showMarkers(ArrayList<Item> items) {
		//
		if (Utils.isEmpty(items)) {
			return;
		}
		
		googleMap.setInfoWindowAdapter(new MarkerWindow());
		googleMap.setOnInfoWindowClickListener(new MarkerClickListener());
		
		boolean bounded = false;
		LatLngBounds.Builder builder = new LatLngBounds.Builder();
		for (Item item : items) {
			if (item.getLocation() != null) {
				bounded = true;
				//Location loc = locUtil.getCurrentLocation();
				double[] location = /*new double[]{loc.getLatitude(), loc.getLongitude()};*/item.getLocation();
				Marker marker = googleMap.addMarker(new MarkerOptions()
		        .position(new LatLng(location[0], location[1]))
		        .title(item.getTitle())
		        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location))
		        .flat(true)
		        .alpha(0.8f)
		        .snippet(item.getId()));
				Log.i(getTag(), "Added marker " + marker.getTitle() + " position: " + marker.getPosition());
				builder.include(marker.getPosition());
			}
		}
		
		if (bounded) {
			LatLngBounds bounds = getPaddedBounds(builder);
			int padding = 0; // offset from edges of the map in pixels
			int width = 100;
			int height = 100;
			//bounds.getCenter();
			CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
			//CameraUpdate cu = CameraUpdateFactory.newLatLngZoom(bounds.getCenter(), ZOOM);
			
			//googleMap.animateCamera(CameraUpdateFactory.zoomTo(5));
			googleMap.animateCamera(cu);
			//googleMap.animateCamera(CameraUpdateFactory.zoomBy(-5));
		} else {
			setCurrentLocation();
		}
	}
	
	private LatLngBounds getPaddedBounds(Builder builder) {
		// 
		LatLngBounds tmpBounds = builder.build();
		
	    /** Add 2 points 1000m northEast and southWest of the center.
	     * They increase the bounds only, if they are not already larger
	     * than this. 
	     * 1000m on the diagonal translates into about 709m to each direction. */
	    LatLng center = tmpBounds.getCenter();
	    LatLng norhtEast = move(center, ZOOM_DISTANCE, ZOOM_DISTANCE);
	    LatLng southWest = move(center, -ZOOM_DISTANCE, -ZOOM_DISTANCE);
	    builder.include(southWest);
	    builder.include(norhtEast);
	    return builder.build();
	}
	
	/**
	 * Create a new LatLng which lies toNorth meters north and toEast meters
	 * east of startLL
	 */
	private static LatLng move(LatLng startLL, double toNorth, double toEast) {
	    double lonDiff = meterToLongitude(toEast, startLL.latitude);
	    double latDiff = meterToLatitude(toNorth);
	    return new LatLng(startLL.latitude + latDiff, startLL.longitude
	            + lonDiff);
	}

	private static double meterToLongitude(double meterToEast, double latitude) {
	    double latArc = Math.toRadians(latitude);
	    double radius = Math.cos(latArc) * EARTHRADIUS;
	    double rad = meterToEast / radius;
	    return Math.toDegrees(rad);
	}


	private static double meterToLatitude(double meterToNorth) {
	    double rad = meterToNorth / EARTHRADIUS;
	    return Math.toDegrees(rad);
	}

	private class MarkerClickListener implements OnInfoWindowClickListener {

		@Override
		public void onInfoWindowClick(Marker marker) {
			//
			//ShopApplication.goToProductDetails(getActivity(), marker.getSnippet());
			goToDetails(marker.getSnippet(), null);
			/*getDataService().getItem(getActivity(), marker.getSnippet(), null, new Handler() {
				@Override
				public void handleMessage(Message msg) {
					// 
					Item item = (Item) msg.getData().get("item");
					
					if (item != null) {
						//
						goToDetails(item);
					}
				}
			});*/
		}
	}
	
	private void goToDetails(String itemId, Item item) {
		//
		Intent intent = new Intent(getActivity(), ProductDetail.class);
		intent.setAction(ProductDetail.ACTION_DETAILS);
		
		if (item != null) {
			intent.putExtra("params", item);
		} else {
			intent.putExtra("params", itemId);
		}
		
		itemDisplayed = item;
		startActivityForResult(intent, DISPLAY_DETAILS);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// 
		if (data != null && requestCode == DISPLAY_DETAILS) {
			// update current item
			Item newItem = (Item) data.getExtras().get("item");
			
			if (newItem != null && itemDisplayed != null) {
				itemDisplayed.setLiked(newItem.isLiked());
				itemDisplayed.setLikeCount(newItem.getLikeCount());
				itemDisplayed.setComments(newItem.getComments());
				itemDisplayed.setCommentCount(newItem.getCommentCount());
				
				// update will be called by parent
				//update();
				updateMarkers(currentResults, true);
			}
		}
	}
	
	private Item getItem(String id) {
		//
		for (Item i : currentResults) {
			//
			if (id.equalsIgnoreCase(i.getId())) {
				return i;
			}
		}
		
		return null;
	}
	
	private class MarkerWindow implements InfoWindowAdapter {
		//
		private final int resId = R.layout.product_marker;
		private final NumberFormat priceFormat = NumberFormat.getCurrencyInstance();
		
		@Override
		public View getInfoContents(Marker marker) {
			// 
			Item item = getItem(marker.getSnippet());
			
			if (item == null) {
				return null;
			}
			
			LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			View v = vi.inflate(resId, null);
			
			ImageView imgView = (ImageView) v.findViewById(R.id.itemImage);
			TextView nameView = (TextView) v.findViewById(R.id.itemTitle);
			TextView descView = (TextView) v.findViewById(R.id.itemDesc);
			TextView priceView = (TextView) v.findViewById(R.id.itemPrice);
			TextView condView = (TextView) v.findViewById(R.id.itemCondition);
			
			if (!Utils.isEmpty(item.getImages())) {
				// Fetch and set bitmap for item
				String image = item.getImages().get(0).getUri();

				getDataService().getThumbnailFetcher().loadImage(image, imgView);
			}
			
			nameView.setText(Utils.trimToFit(item.getTitle(), 25, "..."));
			nameView.setVisibility(Utils.isEmpty(item.getTitle()) ? View.GONE : View.VISIBLE);
			
			if (Utils.isEmpty(item.getDescription())) {
				//descView.setVisibility(View.GONE);
				if (nameView.getVisibility() != View.GONE){
					nameView.setVisibility(View.GONE);
					descView.setText(Utils.trimToFit(item.getTitle(), 25, "..."));
				}
			} else {
				descView.setText(Utils.trimToFit(item.getDescription(), 25, "..."));
			}
			priceView.setText(priceFormat.format(item.getPrice()).replace(".00", ""));
			
			condView.setText(Utils.trimToFit(item.getCondition(), 25, ""));
			condView.setVisibility(Utils.isEmpty(item.getCondition()) ? View.GONE : View.VISIBLE);
			
			return v;
		}

		@Override
		public View getInfoWindow(Marker marker) {
			// 
			return null;
		}
	}

	private void createUI(View v, Bundle savedInstanceState) {
		//
		mapReady = false;
		handler = new ResultHandler();
		progress = (ProgressDisplay) v.findViewById(R.id.progress);
		resultLayout = (FrameLayout) v.findViewById(R.id.root);
		filterBtn = (Button) v.findViewById(R.id.filter);
		filterSlider = (SlidingDrawer) v.findViewById(R.id.filterDrawer);
		
		previousHelper = new SearchEntryHelper(getActivity());;
		previousSearches = (ListView) v.findViewById(R.id.previous_searches);
		previousSearches.setDescendantFocusability(ListView.FOCUS_BLOCK_DESCENDANTS);
		previousSearchArea = (ViewGroup) v.findViewById(R.id.previousSearchArea);
		
		clearPrevious = (TextView) getView().findViewById(R.id.clearPrevious);
		
		loadPrevious();
		
		FragmentManager manager = getActivity().getSupportFragmentManager();
		listResults = (SearchProducts) manager.findFragmentById(R.id.list_results);
		listResults.setParent(this);
		// Pull to refresh doesn't really make sense on search results
		listResults.getPtrGrid().setMode(Mode.DISABLED);
		mapResults = (SupportMapFragment) manager.findFragmentById(R.id.map_results);
        googleMap = mapResults.getMap();
        
        filterView = (SearchFilter) manager.findFragmentById(R.id.filterContainer);
        filterView.setFilterParent(this);
        
        // location utility
		locUtil = new LocationUtil(getActivity());
		
		fetcher = new ProductFetcher() {
			
			@Override
			public void fetchItems(String lastItemId, Handler handler, boolean refresh) {
				//
				String searchString = entry.getQuery().toString();
				
				if (!Utils.isEmpty(searchString)) {
					getDataService().searchItems(getActivity(), searchString, lastItemId, refresh, Search.this.handler);
				} else {
					//
					displayItems(null, refresh);
				}
			}
			
			@Override
			public boolean canFetch() {
				return true; //searchStarted;
			}
		};
		
		listResults.setProductFetcher(fetcher);
	}
	
	private SearchView getSearchView(Menu menu) {
		//
		MenuItem menuItem = menu.findItem(R.id.action_search_2);
		
		// When using the support library, the setOnActionExpandListener() method is
	    // static and accepts the MenuItem object as an argument
	    MenuItemCompat.setOnActionExpandListener(menuItem, new OnActionExpandListener() {
	        @Override
	        public boolean onMenuItemActionCollapse(MenuItem item) {
	            // Do something when collapsed
	        	//resultCountArea.setVisibility(listResults.isEmpty() ? View.GONE : View.VISIBLE);
	            return true;  // Return true to collapse action view
	        }

	        @Override
	        public boolean onMenuItemActionExpand(MenuItem item) {
	            // Do something when expanded
	        	item.getActionView().requestFocus();
	        	showPreviousSearches(true);
	            return true;  // Return true to expand action view
	        }
	    });
	    
	    return (SearchView) MenuItemCompat.getActionView(menuItem);    
	}

	private void setupListeners() {
		//
		resultLayout.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
	        @Override
	        public void onGlobalLayout() {
	            //
	        	mapReady = true;
	        		
	        	if (type == DisplayType.MAP) {
		        	if (!Utils.isEmpty(currentResults)) {
		       			//
		        		googleMap.clear();
		       			showMarkers(currentResults);
		        	}
	        	}
	        }
	    });
		

		clearPrevious.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 
				clearPreviousSearches();
			}
		});
		
		previousSearches.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// run the previous search query again
				Cursor cursor = (Cursor) previousSearches.getItemAtPosition(position);
				if (cursor.moveToPosition(position)) {
					long theId = cursor.getLong(cursor.getColumnIndex(SearchEntryHelper.SEARCH_ID_COLUMN));
					String item = cursor.getString(cursor.getColumnIndex(SearchEntryHelper.SEARCH_TEXT_COLUMN));
					delete(theId);
					entry.setQuery(item, true);
				}
			}
		});
		
		filterBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//
				if (filterSlider.isOpened()) {
					showFilter(false);
				} else {
					showFilter(true);
				}
			}
		});
	}
	
	private void showFilterButton(boolean show) {
		//
		int vis = show ? View.VISIBLE : View.GONE;
		
		filterBtn.setVisibility(vis);
		filterSlider.setVisibility(vis);
	}
	
	private void showFilter(boolean show) {
		//
		if (show) {
			filterSlider.open();
		} else {
			filterSlider.close();
		}
		
		filterBtn.setText(!show ? getString(R.string.filter) : getString(R.string.close_filter));
		// close soft input keyboard
		onClose();
	}
	
	private void loadPrevious() {
		//
		String[] from = new String[] { SearchEntryHelper.SEARCH_TEXT_COLUMN };
		// Fields on the UI to which we map
	    int[] to = new int[] { R.id.item };
		
	    getLoaderManager().initLoader(0, null, this);
		previousAdapter = new SimpleCursorAdapter(getActivity(), R.layout.list_item, null, from, to, 0);
		previousSearches.setAdapter(previousAdapter);
		
		int count = previousAdapter.getCount();
		clearPrevious.setVisibility(count > 0 ? View.VISIBLE : View.GONE);
	}
	
	private void showPreviousSearches(boolean show) {
		//
		if (show) {
			//
			listResults.setVisibility(View.GONE);
			
			previousSearchArea.setVisibility(View.VISIBLE);
			// hide filter overlay if showing
			showFilter(false);
			showFilterButton(false);

			int count = previousAdapter.getCount();			
			clearPrevious.setVisibility(count > 0 ? View.VISIBLE : View.GONE);
		} else {
			previousSearchArea.setVisibility(View.GONE);
			listResults.setVisibility(View.VISIBLE);
		}
	}
	
	private void storeQuery(String query) {
		//
		ContentValues values = new ContentValues();
	    //values.put(SearchEntryHelper.SEARCH_ID_COLUMN, query.toUpperCase());
	    values.put(SearchEntryHelper.SEARCH_TEXT_COLUMN, query);

	    getActivity().getContentResolver().insert(SearchContentProvider.CONTENT_URI, values);
	}
	
	private void clearPreviousSearches() {
		//
		previousHelper.emptyTable();
		loadPrevious();
	}
	
	public void runSearch(String searchString, boolean store) {
		// hide previous searches
		showPreviousSearches(false);
		currentSearch = searchString;
		
		if (store) {
			// store entered search value
			storeQuery(searchString);
		}
		
		// clear search results count area
		((Home)getActivity()).setViewTitle(getTitle(), null);
		// hide map option until results come back
		listOrMapMenu.setVisible(false);
		
		// Get the intent, verify the action and get the query
		entry.onActionViewCollapsed();
		
		listResults.clear();
		googleMap.clear();
		
		// default result display is list
		if (type == DisplayType.MAP) {
			((Home)getActivity()).toggleSearchDisplayType();
		}
		
		progress.setVisibility(View.VISIBLE);
		
		if (type == DisplayType.LIST){
			listResults.refresh();
		} else {
			// update map with results
			fetcher.fetchItems(null, handler, true);
		}
	}

	@Override
	public void dataServiceConnected(boolean connected) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getTitle() {
		// 
		return "Search";
	}
	
	@Override
	public String getFooter() {
		//
		if (!Utils.isEmpty(currentResults)) {
			//
			return currentTotal + " result" + (currentTotal != 1 ? "s" : "");
		} else {
			return null;
		}
	}

	@Override
	public void refresh() {
		// 
		if (listResults.isEmpty()) {
			entry.setIconified(false);
			entry.requestFocus();
		}
		
		setDisplayType(getDisplayType());
		
		filterView.refresh();
	}
	
	public DisplayType getDisplayType() {
		return type;
	}
	
	public void setDisplayType(DisplayType type) {
		//
		//FragmentManager manager = getActivity().getSupportFragmentManager();
		//FragmentTransaction transaction = manager.beginTransaction();
		this.type = type;
		
		if (type == DisplayType.LIST) {
			//transaction.hide(mapResults).show(listResults);
			mapResults.getView().setVisibility(View.GONE);
			
			if (listResults.isEmpty()) {
				showPreviousSearches(true);
			} else {
				showPreviousSearches(false);
				listResults.setVisibility(View.VISIBLE);
			}
		} else {
			
			if (verifyGogoplayServices()) {
				//
				// close search
				entry.onActionViewCollapsed();
				
				showPreviousSearches(false);
				listResults.setVisibility(View.GONE);
				mapResults.getView().setVisibility(View.VISIBLE);
				mapResults.getView().requestFocus();
				
				setCurrentLocation();
			}
		}
		
		displayItems(currentResults, false);
		
		//transaction.commit();
	}
	
	private void setCurrentLocation() {
        // Enabling MyLocation Layer of Google Map
        googleMap.setMyLocationEnabled(true);

        // Getting Current Location
        Location location = locUtil.getCurrentLocation();

        if(location!=null){
            onLocationChanged(location);
        }
        //locUtil.requestLocationUpdates(20000, 100, this);
	}
	
	private boolean verifyGogoplayServices() {
		//
		int res = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
		
		if (res != ConnectionResult.SUCCESS) {
			// show error message
			GooglePlayServicesUtil.getErrorDialog(res, getActivity(), 0).show();
		}
		
		return res == ConnectionResult.SUCCESS;
	}

	public DisplayType toggleDisplayType() {
		//
		if (type == DisplayType.LIST) {
			type = DisplayType.MAP;
		} else {
			type = DisplayType.LIST;
		}
		
		setDisplayType(type);
		
		return type;
	}
	
	@Override
	public void onHiddenChanged(boolean hidden) {
		// TODO Auto-generated method stub
		super.onHiddenChanged(hidden);
		
		//if (entry != null)
		//	entry.setIconified(true);
		
		Log.i(getTag(), "Hidden changed! " + hidden);
		
		/*int vis = hidden ? View.GONE : View.VISIBLE;
		
		if (mapResults != null)
			mapResults.getView().setVisibility(vis);*/
	}

	@Override
    public void onLocationChanged(Location location) {
 
        //TextView tvLocation = (TextView) findViewById(R.id.tv_location);
 
        // Getting latitude of the current location
        double latitude = location.getLatitude();
 
        // Getting longitude of the current location
        double longitude = location.getLongitude();
 
        // Creating a LatLng object for the current location
        LatLng latLng = new LatLng(latitude, longitude);
 
        // Showing the current location in Google Map
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, ZOOM));
 
        // Zoom in the Google Map
        //googleMap.animateCamera(CameraUpdateFactory.zoomTo(5));
 
        // Setting latitude and longitude in the TextView tv_location
        //tvLocation.setText("Latitude:" +  latitude  + ", Longitude:"+ longitude );
 
    }
	
	@Override
    public void onProviderDisabled(String provider) {
        // TODO Auto-generated method stub
    }
 
    @Override
    public void onProviderEnabled(String provider) {
        // TODO Auto-generated method stub
    }
 
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub
    }
    
    public void onNavShowing(boolean showing) {
    	//
    	if (type == DisplayType.MAP) {
    		mapResults.getView().setVisibility(showing ? View.GONE : View.VISIBLE);
    	}
    }
    
    public static class SearchProducts extends Products {
    	//
    	private ShopFragment parent;
    	
    	protected String getEmptyTitle() {
    		//
    		return getString(R.string.emptySearchTitle);
    	}
    	
    	public void setParent(ShopFragment parent) {
			// 
    		this.parent = parent;
		}
    	
    	public boolean isFragmentHidden() {
    		return parent.isHidden();
    	}

		protected String getEmptyText() {
    		//
    		return getString(R.string.emptySearch);
    	}
    	
    	protected Drawable getEmptyIcon() {
    		//
    		return getResources().getDrawable(R.drawable.ic_menu_search);
    	}
    	
    	protected void onEmptyMessageClicked() {
    		// Do nothing
    	}
    	
    	public boolean isFeedDisplay() {
    		return false;
    	}
    	
    	public boolean isSortingRequired() {
    		return false;
    	}
    }
    
    private void delete(long itemId) {
    	// delete item
    	ContentResolver cr = getActivity().getContentResolver();
    	cr.delete(SearchContentProvider.CONTENT_URI.buildUpon().appendPath(String.valueOf(itemId)).build(), null, null);
    }

	@Override
	public android.support.v4.content.Loader<Cursor> onCreateLoader(int id,
			Bundle args) {
		// 
		String[] projection = { SearchEntryHelper.SEARCH_ID_COLUMN, SearchEntryHelper.SEARCH_TEXT_COLUMN };
	    CursorLoader cursorLoader = new CursorLoader(getActivity(),
	        SearchContentProvider.CONTENT_URI, projection, null, null, null);
	    return cursorLoader;
	}

	@Override
	public void onLoadFinished(android.support.v4.content.Loader<Cursor> loader,
			Cursor data) {
		//
		previousAdapter.swapCursor(data);
	}

	@Override
	public void onLoaderReset(android.support.v4.content.Loader<Cursor> loader) {
		// 
		previousAdapter.swapCursor(null);
	}
	
	/*@Override
	public void close() {
		// 
		super.close();
		
		InputMethodManager imm = (InputMethodManager) getActivity()
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        if (imm != null) {
            imm.hideSoftInputFromWindow(entry.getWindowToken(), 0);
        }
	}*/
}
