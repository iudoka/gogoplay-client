package com.gogoplay;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.widget.CursorAdapter;
import android.text.SpannableStringBuilder;
import android.text.style.SuperscriptSpan;
import android.text.style.TextAppearanceSpan;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.gogoplay.data.FilterHelper;
import com.gogoplay.data.SearchFilterContentProvider;
import com.gogoplay.pref.Sorter;
import com.gogoplay.ui.ShopApplication;
import com.gogoplay.ui.ShopFragment;
import com.gogoplay.ui.widget.RangeSeekBar;
import com.gogoplay.ui.widget.RangeSeekBar.OnRangeSeekBarChangeListener;
import com.gogoplay.util.LocationUtil;
import com.gogoplay.util.Utils;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.Required;

public class SearchFilter extends ShopFragment implements LoaderCallbacks<Cursor>,  ValidationListener {
	//
	//private static final String TAG = "SearchFilter";
	private static final int MIN_PRICE = 0;
	private static final int MAX_PRICE = 1000;
	private static final int SORTER_REQUEST = 111;
	
	// FILTER LIST VIEW
	private ViewGroup filterListContainer;
	private View newFilterBtn;
	private ListView filterList;
	private CursorAdapter filterAdapter;
	
	// ADD/EDIT FILTER VIEW
	private Validator validator;
	private View closeAddFilter;
	private ScrollView filter;
	@Required(order = 1)
	private EditText name;
	private Spinner age;
	private Spinner gender;
	private TextView location;
	private Sorter sorter; // location
	private AutoCompleteTextView brand;
	private View clearLocation;
	private TextView priceMin;
	private TextView priceMax;
	private RangeSeekBar<Integer> priceBar;
	private Spinner category;
	private Spinner condition;
	private Button addFilter;
	private Button addApplyFilter;
	private com.gogoplay.data.SearchFilter currentFilter;
	private com.gogoplay.data.SearchFilter selectedFilter;
	
	// Filter view container
	private Search filterParent;
	private boolean applyRequested;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent,
			Bundle savedInstanceState) {
		//
		return inflater.inflate(R.layout.filter, parent, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		//
		super.onViewCreated(view, savedInstanceState);

		createUI(view, savedInstanceState);
		setupListeners();
	}
	
	@Override
	public void onResume() {
		// 
		super.onResume();
		//refresh();
	}

	private void createUI(View view, Bundle savedInstanceState) {
		//
		validator = new Validator(this);
		validator.setValidationListener(this);
		
		filterListContainer = (ViewGroup) view.findViewById(R.id.filterListContainer);
		newFilterBtn = view.findViewById(R.id.newFilterBtn);
		filterList = (ListView) view.findViewById(R.id.filterList);
		
		filterAdapter = new CursorAdapter(getActivity(), null, true) 
		{
			private LayoutInflater inflater;

			private LayoutInflater getInflater(Context context) {
				//
				if (inflater == null) {
					//
					inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				}
				
				return inflater;
			}
			
			/* ... */
			@Override
			public View newView(Context context, Cursor cursor, ViewGroup parent) {
				//
				return getInflater(context).inflate(R.layout.filter_item, parent, false);
			}

			@Override
			public void bindView(View view, Context context, Cursor cursor) {
				//
				ImageView selected = (ImageView) view.findViewById(R.id.selected);
				TextView name = (TextView) view.findViewById(R.id.name1);
				TextView name2 = (TextView) view.findViewById(R.id.name2);
				TextView descr = (TextView) view.findViewById(R.id.description);
				ImageView edit = (ImageView) view.findViewById(R.id.editFilter);
				ImageView delete = (ImageView) view.findViewById(R.id.removeFilter);
				
				final com.gogoplay.data.SearchFilter filter = readFilter(cursor);
				
				selected.setVisibility(filter.isSelected() ? View.VISIBLE : View.INVISIBLE);
				String fName = cursor.getString(cursor.getColumnIndex(FilterHelper.FILTER_NAME_COLUMN));
				
				name.setText(fName.substring(0,1));
				StringBuilder fNameBuilder = new StringBuilder(fName.substring(1).trim());
				int nameLength = fNameBuilder.length();
				
				StringBuilder d1 = new StringBuilder();
				// Gender
				if (!filter.getGender().contains("All")) {
					d1.append("   " + filter.getGender());
				}
				// Age
				if (!filter.getAge().contains("All")) {
					
					d1.append(" (");
					
					if (filter.getAge().contains("Big")) {
						//
						d1.append("15+yrs");
					} else {
						d1.append(filter.getAge());
					}
					
					d1.append(")");
				}
				
				fNameBuilder.append(d1.toString());
				String fName2 = fNameBuilder.toString().trim();
				SpannableStringBuilder ssb = new SpannableStringBuilder(fName2);
				
				ssb.setSpan(new UnderlineSpan(), 0, nameLength, 0);
				ssb.setSpan(new TextAppearanceSpan(null, 0, 10, null, null), nameLength, fName2.length(), 0);
				ssb.setSpan(new SuperscriptSpan(), nameLength, fName2.length(), 0);
				
				name2.setText(ssb);
				
				d1 = new StringBuilder();
				// Gender
				/*if (!filter.getGender().contains("All")) {
					d1.append(filter.getGender() + "s");
					
					//if (filter.getAge().contains("All")) {
						//
					//	d1.append("s");
					//}
					
					d1.append(" ");
				}
				// Age
				if (!filter.getAge().contains("All")) {
					
					if (!filter.getGender().contains("All")) {
						d1.append("(");
					}
					
					if (filter.getAge().contains("Big")) {
						//
						d1.append("15+yrs");
					} else {
						d1.append(filter.getAge());
					}
					
					if (!filter.getGender().contains("All")) {
						d1.append(") ");
					} else {
						d1.append(", ");
					}
				}*/
				
				// Condition
				if (!filter.getCondition().contains("Any")) {
					//
					d1.append("'" + filter.getCondition() + "' ");
				}
				
				// Category
				if (!filter.getCategory().contains("Any") &&
					!filter.getCategory().contains("Other")) {
					//
					d1.append(filter.getCategory() + " ");
				} else {// if (filter.getCondition().contains("Any")) {
					//
					d1.append("Items ");
				}
				
				if (filter.getPriceMin() > 0) {
					//
					d1.append(getString(R.string.from_price) + " " + Utils.formatPrice(filter.getPriceMin()) + " ");
				}
				
				if (filter.getPriceMax() < MAX_PRICE) {
					//
					if (filter.getPriceMin() > 0) {
						//
						d1.append(getString(R.string.to_price));
					} else {
						//
						d1.append(getString(R.string.less_than));
					}
					
					d1.append(" " + Utils.formatPrice(filter.getPriceMax()) + " ");
				}

				// Location
				if (filter.getLocation() != null && filter.getLocation().getLocation() != null) {
					//
					d1.append(getString(R.string.near) + " " + filter.getLocation().getLocation().getExtras().getString(LocationUtil.EXTRA_ZIPCODE) + " ");
				}
				
				String descrTxt = d1.toString().trim();
				
				if (descrTxt.startsWith("Items")) {
					descrTxt = "All " + descrTxt;
				}
				
				descr.setText(descrTxt);
				
				edit.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// 
						setCurrentFilter(filter);
						goToFilterView();
					}
				});
				
				delete.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// 
						delete(filter.getId());
						notifyDataSetChanged();
					}
				});
				
				view.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// 
						selectedFilter = filter;
						filterParent.setCurrentFilter(selectedFilter);
						filterAdapter.notifyDataSetChanged();
					}
				});
			}
		};
		
		filterList.setAdapter(filterAdapter);
		loadFilters();
		
		// ADD/EDIT FIlTER VIEW
		closeAddFilter = (View) view.findViewById(R.id.closeAddFilter);
		filter = (ScrollView) view.findViewById(R.id.scroller);
		name = (EditText) view.findViewById(R.id.name);
		age = (Spinner) view.findViewById(R.id.age);
		gender = (Spinner) view.findViewById(R.id.gender);
		
		brand = (AutoCompleteTextView) view.findViewById(R.id.brand);
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                R.layout.list_item, R.id.item, getResources().getStringArray(R.array.brand_array));
		brand.setAdapter(adapter);
		
		location = (TextView) view.findViewById(R.id.location);
		clearLocation = (View) view.findViewById(R.id.clearLocation);
		setLocation(null);
		priceMin = (TextView) view.findViewById(R.id.priceMin);
		priceMin.setText(getString(R.string.from_price) + " " + Utils.formatPrice(MIN_PRICE));
		priceMax = (TextView) view.findViewById(R.id.priceMax);
		priceMax.setText(getString(R.string.to_price) + " " + Utils.formatPrice(MAX_PRICE) + "+");
		
		// create RangeSeekBar as Integer range between 10 and 1000
		priceBar = new RangeSeekBar<Integer>(MIN_PRICE, MAX_PRICE, view.getContext());
		priceBar.setOnRangeSeekBarChangeListener(new OnRangeSeekBarChangeListener<Integer>() {
			@Override
			public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {
				// handle changed range values
				//Log.i(TAG, "User selected new range values: MIN=" + minValue + ", MAX=" + maxValue);
				priceMin.setText(getString(R.string.from_price) + " " + Utils.formatPrice(minValue));
				priceMax.setText(getString(R.string.to_price) + " " + Utils.formatPrice(maxValue) + (maxValue == MAX_PRICE ? "+" : ""));
			}
		});

		// add RangeSeekBar to pre-defined layout
		ViewGroup layout = (ViewGroup) view.findViewById(R.id.priceLayout);
		layout.addView(priceBar);
		
		category = (Spinner) view.findViewById(R.id.category);
		adapter = new ArrayAdapter<String>(getActivity(), 
				R.layout.spinner_item, getResources().getStringArray(R.array.category_filter));
		adapter.setDropDownViewResource(R.layout.spinner_drop_item);
		category.setAdapter(adapter);
		
		condition = (Spinner) view.findViewById(R.id.condition);
		
		adapter = new ArrayAdapter<String>(getActivity(), 
				R.layout.spinner_item, getResources().getStringArray(R.array.condition_filter));
		adapter.setDropDownViewResource(R.layout.spinner_drop_item);
		condition.setAdapter(adapter);
		
		addFilter = (Button) view.findViewById(R.id.addFilter);
		addApplyFilter = (Button) view.findViewById(R.id.addApplyFilter);
	}

	
	private void setCurrentFilter(com.gogoplay.data.SearchFilter filter) {
		//
		currentFilter = filter;
		
		populateFilterView(currentFilter);
	}
	
	private void clearFilterView() {
		populateFilterView(new com.gogoplay.data.SearchFilter());
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void populateFilterView(com.gogoplay.data.SearchFilter filter) {
		//
		name.setText(Utils.emptyIfNull(filter.getName()));
		ArrayAdapter myAdap;
		
		if (!Utils.isEmpty(filter.getAge())) {
			myAdap = (ArrayAdapter) age.getAdapter(); //cast to an ArrayAdapter
			age.setSelection(myAdap.getPosition(filter.getAge()));
		} else {
			age.setSelection(0);
		}
		
		if (!Utils.isEmpty(filter.getGender())) {
			myAdap = (ArrayAdapter) gender.getAdapter();
			gender.setSelection(myAdap.getPosition(filter.getGender()));
		} else {
			gender.setSelection(0);
		}
		
		if (!Utils.isEmpty(filter.getBrand())) {
			brand.setText(Utils.emptyIfNull(filter.getBrand()));
		} else {
			brand.setText("");
		}
		
		setLocation(filter.getLocation());
		
		if (filter.getId() <= 0) {
			priceBar.setSelectedMinValue(0);
			priceBar.setSelectedMaxValue(MAX_PRICE);
		} else {
			priceBar.setSelectedMinValue((int)filter.getPriceMin());
			priceBar.setSelectedMaxValue((int)filter.getPriceMax());
		}
		
		int minValue = priceBar.getSelectedMinValue();
		int maxValue = priceBar.getSelectedMaxValue();
		
		priceMin.setText(getString(R.string.from_price) + " " + Utils.formatPrice(minValue));
		priceMax.setText(getString(R.string.to_price) + " " + Utils.formatPrice(maxValue) + (maxValue == MAX_PRICE ? "+" : ""));
		
		if (!Utils.isEmpty(filter.getCategory())) {
			myAdap = (ArrayAdapter) category.getAdapter();
			category.setSelection(myAdap.getPosition(filter.getCategory()));
		} else {
			category.setSelection(0);
		}
		
		if (!Utils.isEmpty(filter.getCondition())) {
			myAdap = (ArrayAdapter) condition.getAdapter();
			condition.setSelection(myAdap.getPosition(filter.getCondition()));
		} else {
			condition.setSelection(0);
		}
	}
	
	private void setupListeners() {
		//
		location.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 
				showSortLocationDialog();
			}
		});

		clearLocation.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 
				setLocation(null);
			}
		});
		
		addFilter.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 
				applyRequested = false;
				validator.validate();
			}
		});
		
		addApplyFilter.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 
				applyRequested = true;
				validator.validate();
			}
		});
		
		newFilterBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 
				setCurrentFilter(new com.gogoplay.data.SearchFilter());
				goToFilterView();
			}
		});
		
		closeAddFilter.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 
				currentFilter = null;
				goToFilterList();
			}
		});
	}
	
	private void loadFilters() {
		//
	    getLoaderManager().initLoader(0, null, this);
	}
	
	private void storeFilter(boolean apply) {
		//
		com.gogoplay.data.SearchFilter filter = populateFilter();
		
		if (apply) {
			filter.setSelected(apply);
		}
		

		ContentValues values = new ContentValues();
		
		values.put(FilterHelper.FILTER_NAME_COLUMN, filter.getName()); 
		values.put(FilterHelper.FILTER_AGE_COLUMN, filter.getAge());
		values.put(FilterHelper.FILTER_GENDER_COLUMN, filter.getGender());
		values.put(FilterHelper.FILTER_BRAND_COLUMN, filter.getBrand());
		values.put(FilterHelper.FILTER_LOCATION_COLUMN, filter.getLocation() == null ? null : Sorter.toJson(filter.getLocation()));
		values.put(FilterHelper.FILTER_PRICE_MIN_COLUMN, filter.getPriceMin());
		values.put(FilterHelper.FILTER_PRICE_MAX_COLUMN, filter.getPriceMax());
		values.put(FilterHelper.FILTER_CATEGORY_COLUMN, filter.getCategory());
		values.put(FilterHelper.FILTER_CONDITION_COLUMN, filter.getCondition());
		values.put(FilterHelper.FILTER_SELECTED_COLUMN, filter.isSelected() ? 1 : 0);
		
		if (filter.getId() > 0) {
			// update
			Uri filterUri = SearchFilterContentProvider.CONTENT_URI.buildUpon().appendPath(filter.getId() + "").build();
			
		    getActivity().getContentResolver().update(filterUri, values, null, null);
		} else {
			// insert
		    Uri newId = getActivity().getContentResolver().insert(SearchFilterContentProvider.CONTENT_URI, values);
		    
		    String id = newId.getLastPathSegment();
		    filter.setId(Long.valueOf(id));
		}
	}
	
	private void goToFilterView() {
		//
		name.setError(null);
		onClose(); // hides keyboard if showing
		filterListContainer.setVisibility(View.GONE);
		filter.setVisibility(View.VISIBLE);
		
		closeAddFilter.setVisibility(filterList.getCount() > 0 ? View.VISIBLE : View.GONE);
	}
	
	private void goToFilterList() {
		//
		// clear filter view
		clearFilterView();
		onClose(); // hides keyboard if showing
		
		filter.setVisibility(View.GONE);
		filterListContainer.setVisibility(View.VISIBLE);
	}
	
	private com.gogoplay.data.SearchFilter populateFilter() {
		//
		if (currentFilter == null) {
			currentFilter = new com.gogoplay.data.SearchFilter();
		}
		
		currentFilter.setName(name.getText().toString().trim());
		currentFilter.setAge(age.getSelectedItem().toString());
		currentFilter.setGender(gender.getSelectedItem().toString());
		currentFilter.setBrand(Utils.emptyIfNull(brand.getText()).toString());
		currentFilter.setLocation(sorter);
		currentFilter.setPriceMin(priceBar.getSelectedMinValue());
		currentFilter.setPriceMax(priceBar.getSelectedMaxValue());
		currentFilter.setCategory(category.getSelectedItem().toString());
		currentFilter.setCondition(condition.getSelectedItem().toString());
		
		return currentFilter;
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		//
		if (requestCode == SORTER_REQUEST) {
			//
			if (data != null) {
				Sorter sorter = (Sorter) data.getExtras().get(LocationDialog.EXTRA_SORTER);
				
				setLocation(sorter);
			}
		}
		
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	private void showSortLocationDialog() {
		//
		Intent intent = new Intent(getActivity(), LocationDialog.class);
		intent.putExtra(LocationDialog.EXTRA_SORTER, sorter);
		intent.putExtra(LocationDialog.EXTRA_RADIUS, true);
		
		startActivityForResult(intent, SORTER_REQUEST);
	}
	
	private void setLocation(Sorter newLocation) {
		//
		sorter = newLocation;
		
		if (newLocation == null) { 
			location.setText(getString(R.string.search_all));
		} else {
			StringBuilder text = new StringBuilder();
			
			if (newLocation.useCurrentLocation) {
				text.append(getString(R.string.current_location));
			} else {
				if (newLocation.getLocation() != null) {
					text.append(newLocation.getLocation().getExtras().getString(LocationUtil.EXTRA_ZIPCODE));
				}
			}
			
			text.append(" ~ " + newLocation.radius + getString(R.string.miles_abbrev));
			
			location.setText(text.toString());
		}
		
		clearLocation.setVisibility(newLocation == null ? View.GONE : View.VISIBLE);
	}

	@Override
	public void dataServiceConnected(boolean connected) {
		// TODO Auto-generated method stub

	}

	@Override
	public void refresh() {
		// 
		boolean showList = filterList.getCount() > 0 && currentFilter == null;
		
		filterListContainer.setVisibility(showList ? View.VISIBLE : View.GONE);
		filter.setVisibility(!showList ? View.VISIBLE : View.GONE);
		closeAddFilter.setVisibility(filterList.getCount() > 0 ? View.VISIBLE : View.GONE);
	}
	
	public void setFilterParent(Search search) {
		// 
		filterParent = search;
	}

	private boolean delete(long filterId) {
    	// delete filter
    	ContentResolver cr = getActivity().getContentResolver();
    	int deleted = cr.delete(SearchFilterContentProvider.CONTENT_URI.buildUpon().appendPath(String.valueOf(filterId)).build(), null, null);
    	
    	return deleted > 0;
    }

	@Override
	public android.support.v4.content.Loader<Cursor> onCreateLoader(int id,
			Bundle args) {
		// 
		String[] projection = { FilterHelper.FILTER_ID_COLUMN,
				FilterHelper.FILTER_NAME_COLUMN,
				FilterHelper.FILTER_AGE_COLUMN,
				FilterHelper.FILTER_GENDER_COLUMN,
				FilterHelper.FILTER_BRAND_COLUMN,
				FilterHelper.FILTER_LOCATION_COLUMN,
				FilterHelper.FILTER_PRICE_MIN_COLUMN,
				FilterHelper.FILTER_PRICE_MAX_COLUMN,
				FilterHelper.FILTER_CATEGORY_COLUMN,
				FilterHelper.FILTER_CONDITION_COLUMN,
				FilterHelper.FILTER_SELECTED_COLUMN };
	    CursorLoader cursorLoader = new CursorLoader(getActivity(),
	        SearchFilterContentProvider.CONTENT_URI, projection, null, null, null);
	    return cursorLoader;
	}

	@Override
	public void onLoadFinished(android.support.v4.content.Loader<Cursor> loader,
			Cursor data) {
		//
		filterAdapter.swapCursor(data);
		refresh();
	}

	@Override
	public void onLoaderReset(android.support.v4.content.Loader<Cursor> loader) {
		// 
		filterAdapter.swapCursor(null);
	}
	
	private com.gogoplay.data.SearchFilter readFilter(Cursor cursor) {
		//
		com.gogoplay.data.SearchFilter filter = new com.gogoplay.data.SearchFilter();
		
		filter.setId(cursor.getLong(cursor.getColumnIndex(FilterHelper.FILTER_ID_COLUMN)));
		filter.setName(cursor.getString(cursor.getColumnIndex(FilterHelper.FILTER_NAME_COLUMN)));
		//filter.setSelected(cursor.getInt(cursor.getColumnIndex(FilterHelper.FILTER_SELECTED_COLUMN)) > 0);
		
		if (selectedFilter != null) {
			//
			filter.setSelected(selectedFilter.getId() == filter.getId());
		}
		
		filter.setAge(cursor.getString(cursor.getColumnIndex(FilterHelper.FILTER_AGE_COLUMN)));
		filter.setGender(cursor.getString(cursor.getColumnIndex(FilterHelper.FILTER_GENDER_COLUMN)));
		filter.setBrand(cursor.getString(cursor.getColumnIndex(FilterHelper.FILTER_BRAND_COLUMN)));
		filter.setCategory(cursor.getString(cursor.getColumnIndex(FilterHelper.FILTER_CATEGORY_COLUMN)));
		filter.setCondition(cursor.getString(cursor.getColumnIndex(FilterHelper.FILTER_CONDITION_COLUMN)));
		filter.setPriceMin(cursor.getInt(cursor.getColumnIndex(FilterHelper.FILTER_PRICE_MIN_COLUMN)));
		filter.setPriceMax(cursor.getInt(cursor.getColumnIndex(FilterHelper.FILTER_PRICE_MAX_COLUMN)));
		
		String locJson = cursor.getString(cursor.getColumnIndex(FilterHelper.FILTER_LOCATION_COLUMN));
		
		if (!Utils.isEmpty(locJson)) {
			filter.setLocation(Sorter.fromJson(locJson));
		}
		
		return filter;
	}

	@Override
	public void onValidationSucceeded() {
		// 
		name.setError(null);
		storeFilter(applyRequested);
		currentFilter = null;
		
		if (applyRequested) {
			selectedFilter = currentFilter;
			filterParent.setCurrentFilter(selectedFilter);
		}
		
		goToFilterList();
	}

	@Override
	public void onValidationFailed(View failedView, Rule<?> failedRule) {
		//
		String message = failedRule.getFailureMessage();

		if (failedView instanceof EditText) {
			failedView.requestFocus();
			((EditText) failedView).setError(message);
		} else {
			ShopApplication.showMessage(this.getActivity(), message);
		}
	}
}
