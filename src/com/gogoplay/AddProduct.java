/**
 * 
 */
package com.gogoplay;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.gogoplay.data.Image;
import com.gogoplay.data.Item;
import com.gogoplay.data.Item.Gender;
import com.gogoplay.pref.Sorter;
import com.gogoplay.ui.ShopApplication;
import com.gogoplay.ui.ShopFragment;
import com.gogoplay.ui.widget.ImageSelector;
import com.gogoplay.ui.widget.LocationEditText;
import com.gogoplay.ui.widget.ProgressDisplay;
import com.gogoplay.ui.widget.ToggleButton;
import com.gogoplay.util.LocationUtil;
import com.gogoplay.util.Utils;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.NumberRule;
import com.mobsandgeeks.saripaar.annotation.NumberRule.NumberType;
import com.mobsandgeeks.saripaar.annotation.Required;

/**
 * @author iudoka
 * 
 */
public class AddProduct extends ShopFragment implements ValidationListener {
	//
	private static final NumberFormat priceFormat = NumberFormat.getInstance();
	
	static {
		//
		priceFormat.setMaximumFractionDigits(2);
		priceFormat.setMinimumFractionDigits(2);
	}
	
	/*private static String twitterPkg = "com.twitter.android";
	private static String twitterName = "Twitter";
	private static String facebookPkg = "com.facebook.katana";
	private static String facebookName = "Facebook";*/
	
	private View root;
	private Validator validator;
	private LocationUtil locationUtil;
	private Handler handler;
	private String currentLocation;
	private Location geoLoc;

	private ProgressDisplay progress;

	private ScrollView scroller;
	private ToggleButton photoSource;

	private ImageSelector selector1;
	private ImageSelector selector2;
	private ImageSelector selector3;
	private ImageSelector selector4;
	private ImageSelector selector5;
	private ImageSelector selector6;

	@Required(order = 1)
	private Spinner category;
	@Required(order = 3)
	private Spinner condition;
	//@Required(order = 5)
	//@Email(order = 6, messageResId = R.string.invalidEmail)
	//private EditText email;
	@Required(order = 7)
	private EditText title;
	@Required(order = 8)
	@NumberRule(order = 9, type = NumberType.DOUBLE, gt = 0)
	private EditText price;
	@Required(order = 10)
	@NumberRule(order = 11, type = NumberType.INTEGER, gt = 0)
	private EditText quantity;
	@Required(order = 12)
	private LocationEditText location;

	private Spinner age;
	private Spinner gender;
	private AutoCompleteTextView brand;
	private EditText tags;
	private EditText description;

	private ViewGroup shareOn;
	/*private ViewGroup twitterShare;
	private View twitterToggle;
	private CompoundButton shareOnTwitter;
	private ViewGroup facebookShare;
	private View facebookToggle;
	private CompoundButton shareOnFacebook;*/
	private CompoundButton shareBtn;

	private Button saveButton;
	
	private boolean initialized = false;
	private boolean grabImage = false;
	private boolean showCameraOnStart = false;
	
	private boolean editMode = false;
	
	private Item item;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		//
		super.onCreate(savedInstanceState);
		Log.i(getTag(), "Just created Add Product Fragment " + this);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		//
		return inflater.inflate(R.layout.add_product, container, false);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		if (checkAuthentication()) {
			//
		} else {
			//
			initialized = false;
		}
		
		createUI();
		setupListeners();
	}
	
	private boolean checkAuthentication() {
		//
		if (isDataServiceConnected()) {
			//
			if (getDataService().checkAuthentication(getActivity())) {
				//
				return true;
			} else {
				//
				return false;
			}
		}
		
		return false;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (savedInstanceState != null)
			return;
	}

	protected void createUI() {
		//
		//super.createUI();
		View v = getView();
		
		root = getView().findViewById(R.id.root);
		validator = new Validator(this);
		validator.setValidationListener(this);
		
		scroller = (ScrollView) v.findViewById(R.id.scroller);
		
		// Get the location manager
		locationUtil = new LocationUtil(this.getActivity());
		handler = new UploadHandler();
		FragmentManager fm = getActivity().getSupportFragmentManager();

		progress = (ProgressDisplay) v.findViewById(R.id.progress);
		progress.showText(true);
		progress.setVisibility(View.GONE);
		photoSource = (ToggleButton) v.findViewById(R.id.photoSource);
		/*takePhoto = (ToggleButton) v.findViewById(R.id.takePhoto);
		takePhoto.setChecked(true);
		takePhoto.requestFocus();
		choosePhoto = (ToggleButton) v.findViewById(R.id.choosePhoto);
		choosePhoto.setChecked(false);

		takePhoto.setWidth((int) takePhoto.getPaint().measureText(
				choosePhoto.getText() + "")
				+ takePhoto.getPaddingLeft() + takePhoto.getPaddingRight());*/

		selector1 = (ImageSelector) fm.findFragmentById(R.id.imgSelector1);
		selector2 = (ImageSelector) fm.findFragmentById(R.id.imgSelector2);
		selector3 = (ImageSelector) fm.findFragmentById(R.id.imgSelector3);
		selector4 = (ImageSelector) fm.findFragmentById(R.id.imgSelector4);
		selector5 = (ImageSelector) fm.findFragmentById(R.id.imgSelector5);
		selector6 = (ImageSelector) fm.findFragmentById(R.id.imgSelector6);

		//email = (EditText) v.findViewById(R.id.email);
		title = (EditText) v.findViewById(R.id.title);
		price = (EditText) v.findViewById(R.id.price);
		quantity = (EditText) v.findViewById(R.id.quantity);
		location = (LocationEditText) v.findViewById(R.id.location);
		location.setLocationUtil(locationUtil);

		condition = (Spinner) v.findViewById(R.id.condition);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), 
				R.layout.spinner_item, getResources().getStringArray(R.array.condition_array));
		adapter.setDropDownViewResource(R.layout.spinner_drop_item);
		condition.setAdapter(adapter);
		
		category = (Spinner) v.findViewById(R.id.category);
		adapter = new ArrayAdapter<String>(getActivity(), 
				R.layout.spinner_item, getResources().getStringArray(R.array.category_array));
		adapter.setDropDownViewResource(R.layout.spinner_drop_item);
		category.setAdapter(adapter);
		
		age = (Spinner) v.findViewById(R.id.age);
		gender = (Spinner) v.findViewById(R.id.gender);
		brand = (AutoCompleteTextView) v.findViewById(R.id.brand);
		
		adapter = new ArrayAdapter<String>(getActivity(),
                R.layout.list_item, R.id.item, getResources().getStringArray(R.array.brand_array));
		brand.setAdapter(adapter);
		
		tags = (EditText) v.findViewById(R.id.tag);
		description = (EditText) v.findViewById(R.id.description);

		shareOn = (ViewGroup) v.findViewById(R.id.shareOn);
		
		/*twitterShare = (ViewGroup) v.findViewById(R.id.twitterShareContainer);
		twitterToggle = v.findViewById(R.id.twitter);
		shareOnTwitter = (CompoundButton) v.findViewById(R.id.shareOnTwitter);
		facebookShare = (ViewGroup) v.findViewById(R.id.facebookShareContainer);
		facebookToggle = v.findViewById(R.id.facebook);
		shareOnFacebook = (CompoundButton) v.findViewById(R.id.shareOnFacebook);*/
		shareBtn = (CompoundButton) v.findViewById(R.id.shareListing);

		saveButton = (Button) v.findViewById(R.id.add_product);
		
		ImageSelector.setUseCamera(photoSource.isButton1Selected());
	}

	@Override
	public void onResume() {
		//
		super.onResume();
		
		refresh();
	};

	@SuppressLint("HandlerLeak")
	private void loadLocation() {
		//
		/*locationUtil.getCurrentCityAndZip(new Handler() {
			@Override
			public void handleMessage(Message msg) {
				String loc = msg.getData().getString("location");
				double lat = msg.getData().getDouble("lat");
				double lon = msg.getData().getDouble("lon");
				
				location.setText(loc);
				
				// Store for use later
				currentLocation = loc;
				geoLoc = new Location("");
				geoLoc.setLatitude(lat);
				geoLoc.setLongitude(lon);
			}
		});*/
	}

	private void setupListeners() {
		//
		photoSource.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ImageSelector.setUseCamera(photoSource.isButton1Selected());
			}
		});

		/*twitterToggle.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				shareOnTwitter.setChecked(!shareOnTwitter.isChecked());
			}
		});

		facebookToggle.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				shareOnFacebook.setChecked(!shareOnFacebook.isChecked());
			}
		});*/

		saveButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//
				validator.validate();
			}
		});
		
		// Add custom validation rules
		Rule<Spinner> rule = new Rule<Spinner>(getString(R.string.select_category)) {

			@Override
			public boolean isValid(Spinner view) {
				//
				boolean valid = view.getSelectedItemPosition() > 0;
				
				if (!valid) {
					//
					scroller.post(new Runnable() {
						public void run() {
							category.requestFocusFromTouch();
						}
					});
				}
				
				return valid;
			}
		};
		validator.put(category, rule);

		rule = new Rule<Spinner>(getString(R.string.select_condition)) {

			@Override
			public boolean isValid(Spinner view) {
				//
				boolean valid = view.getSelectedItemPosition() > 0;
				
				if (!valid) {
					//
					scroller.post(new Runnable() {
						public void run() {
							condition.requestFocusFromTouch();
						}
					});
				}
				
				return valid;
			}
		};
		validator.put(condition, rule);

		Rule<View> ruleImg = new Rule<View>(getString(R.string.select_picture)) {

			@Override
			public boolean isValid(View view) {
				//
				boolean valid = true;
				
				if (!editMode) {
					if (selector1.getImageUri() == null &&
						selector2.getImageUri() == null &&
						selector3.getImageUri() == null &&
						selector4.getImageUri() == null &&
						selector5.getImageUri() == null &&
						selector6.getImageUri() == null) {
						//
						valid = false;
					}
				} else {
					//
					ImageSelector.State selected = ImageSelector.State.IMAGE_SELECTED;
					
					if (selector1.getState() != selected &&
						selector2.getState() != selected &&
						selector3.getState() != selected &&
						selector4.getState() != selected &&
						selector5.getState() != selected &&
						selector6.getState() != selected) {
						//
						valid = false;
					}
				}

				if (!valid) {
					//
					photoSource.requestFocus();
					scroller.scrollTo(0, 0);
				}
				
				return valid;
			}
		};
		validator.put(selector1.getView(), ruleImg);
	}
	
	private void setSpinner(Spinner spinner, String value, int optionsResId) {
		//
		if (!Utils.isEmpty(value)) {
			//
			String[] values = getResources().getStringArray(optionsResId);
			int index = -1;
			
			int curr = 0;
			for (String val : values) {
				//
				if (val.equalsIgnoreCase(value)) {
					//
					index = curr;
					break;
				}
				
				curr++;
			}
			
			if (index < 0) {
				index = 0;
			}
			
			spinner.setSelection(index);
		} else {
			spinner.setSelection(0);
		}
	}
	
	public void editItem(Item item) {
		//
		editMode = true;
		
		try {
			//
			this.item = item;
			
			price.setText(priceFormat.format(item.getPrice()));
			title.setText(Utils.emptyIfNull(item.getTitle()));
			//email.setText(Utils.emptyIfNull(item.getEmail()));

			setSpinner(category, item.getCategory(), R.array.category_array);
			setSpinner(condition, item.getCondition(), R.array.condition_array);
			
			description.setText(Utils.emptyIfNull(item.getDetail()));
			quantity.setText(item.getQuantity() + "");

			setSpinner(age, item.getAge(), R.array.age_array);
			
			Gender sel = item.getGender();
			int selIndex = 0;
			
			if (sel == Gender.M){
				selIndex = 1;
			} else if (sel == Gender.F){
				selIndex = 2;
			}
			gender.setSelection(selIndex);
			
			brand.setText(Utils.emptyIfNull(item.getBrand()));

			String tagList = null;
			
			if (!Utils.isEmpty(item.getTags())) {
				StringBuilder tagBuilder = new StringBuilder();
				
				for (String aTag : item.getTags()) {
					tagBuilder.append(aTag + ", ");
				}
				
				tagList = tagBuilder.substring(0, tagBuilder.length() - 2);
			}
			
			tags.setText(Utils.emptyIfNull(tagList));
			
			location.setText(item.getAddress());

			List<Image> images = item.getImages();

			if (!Utils.isEmpty(images)) {
				//
				int size = images.size();
				
				if (size > 0)
					selector1.setImage(getDataService(), images.get(0));
			
				if (size > 1)
					selector2.setImage(getDataService(), images.get(1));
				
				if (size > 2)
					selector3.setImage(getDataService(), images.get(2));
				
				if (size > 3)
					selector4.setImage(getDataService(), images.get(3));
				
				if (size > 4)
					selector5.setImage(getDataService(), images.get(4));
				
				if (size > 5)
					selector6.setImage(getDataService(), images.get(5));
			}

			shareOn.setVisibility(View.GONE);
			saveButton.setText(getString(R.string.save_product));
		} catch (Exception e) {
			Log.e(getTag(), "Unable to load item info!", e);
		}
		
		hideProgress();
	}

	private void uploadItem() {

		if (item == null) {
			item = new Item();
		}

		try {
			//
			item.setPrice(Double.parseDouble(price.getText().toString()));
			item.setTitle(title.getText().toString());
			//item.setEmail(email.getText().toString());
			item.setCondition(condition.getSelectedItem().toString());
			item.setCategory(category.getSelectedItem().toString());
			item.setDetail(description.getText().toString());
			item.setQuantity(Integer.parseInt(quantity.getText().toString()));

			item.setAge(age.getSelectedItem().toString());
			
			Gender sel = Gender.A;
			
			if (gender.getSelectedItem().toString().contains("Boy")) {
				sel = Gender.M;
			} else if (gender.getSelectedItem().toString().contains("Girl")) {
				sel = Gender.F;
			}
			
			item.setGender(sel);
			item.setBrand(brand.getText().toString());

			List<String> tagList = null;
			String tag = tags.getText().toString();

			if (tag.contains(" ") || tag.contains(",")) {
				String[] split = tags.getText().toString().replace(",", "")
						.split(" ");

				if (split != null) {
					tagList = Arrays.asList(split);
				}
			} else {
				tagList = new ArrayList<String>();
				tagList.add(tag);
			}

			item.setTags(tagList);

			String addrs = location.getText().toString();
			double[] loc = null;

			if (!Utils.isEmpty(addrs)) {
				//
				geoLoc = location.getLocation();

				if (geoLoc != null) {
					currentLocation = geoLoc.getExtras().getString(
							LocationUtil.EXTRA_CITY_AND_STATE_CODE);

					if (addrs.equalsIgnoreCase(currentLocation)) {
						//
						loc = new double[] { geoLoc.getLatitude(),
								geoLoc.getLongitude() };
					} else {
						// figure out location based on what user typed in
						Location locat = locationUtil.getLocation(addrs);

						if (locat != null) {
							loc = new double[] { locat.getLatitude(),
									locat.getLongitude() };
						}
					}
				}
			}

			item.setAddress(addrs);
			item.setLocation(loc);

			List<Image> images = new ArrayList<Image>();

			getImage(images, selector1);
			getImage(images, selector2);
			getImage(images, selector3);
			getImage(images, selector4);
			getImage(images, selector5);
			getImage(images, selector6);

			item.setImages(images);

			/*List<String> shareOn = new ArrayList<String>();

			if (shareOnTwitter.isChecked()) { shareOn.add("twitter"); }
			
			if (shareOnFacebook.isChecked()) { shareOn.add("facebook"); }

			item.setShareOn(shareOn);*/

			displayProgress(editMode ? getString(R.string.saving) : getString(R.string.uploading));
			
			if (editMode) {
				//
				getDataService().editItem(this.getActivity(), item, handler);
			} else {
				getDataService().uploadItem(this.getActivity(), item, handler);
			}
		} catch (Exception e) {
			ShopApplication.showMessage(this.getActivity(),
					getString(R.string.upload_error));
			Log.e(getTag(), "Unable to populate user intpu into Item object!",
					e);
		}
	}

	public void displayProgress(CharSequence message) {
		//
		progress.setVisibility(View.VISIBLE);
		progress.setText(message);
	}

	public void hideProgress() {
		progress.setVisibility(View.GONE);
	}

	private void getImage(List<Image> images, ImageSelector selector) {
		//

		if (selector.getImageUri() != null) {
			//
			Uri uri = selector.getImageUri();
			String path = getRealPathFromURI(this.getActivity(), uri);
			String id = uri.getLastPathSegment();
			Image image = new Image(id, id, path);
			images.add(image);
		} else if (selector.getSelectedImage() != null) {
			// Working with an image that has already been uploaded
			Image image = new Image(null, null, null, selector.getSelectedImage());
			images.add(image);
		}
	}

	public String getRealPathFromURI(Context context, Uri contentUri) {
		//
		if (contentUri.toString().startsWith("file://")) {
			return contentUri.toString().replace("file://", "");
		}

		Cursor cursor = null;
		try {
			String[] proj = { MediaStore.Images.Media.DATA };
			cursor = context.getContentResolver().query(contentUri, proj, null,
					null, null);
			int column_index = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}

	public String getViewTitle() {
		return getString(R.string.app_name);
	}

	@Override
	public void dataServiceConnected(boolean connected) {
		//
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		
		ShopApplication.unbindDrawables(root);
	}

	@SuppressLint("HandlerLeak")
	private class UploadHandler extends Handler {
		//
		@Override
		public void handleMessage(Message msg) {
			boolean success = msg.getData().getBoolean("upload");

			if (!success) {
				Toast.makeText(AddProduct.this.getActivity(), getString(R.string.itemSaveError),
						Toast.LENGTH_LONG).show();
				Log.e(getTag(), "Unable to upload item to server!");
			} else {
				// Display success messages
				Toast.makeText(AddProduct.this.getActivity(), editMode ? getString(R.string.itemSaved) : getString(R.string.itemAdded),
						Toast.LENGTH_LONG).show();
				
				// share if selected
				/*boolean shareTwitter = shareOnTwitter.isChecked();
				boolean shareFacebook = shareOnFacebook.isChecked();
				
				if (shareTwitter || shareFacebook) {*/
				
				boolean share = shareBtn.isChecked();
				
				if (share) {
					//
					String id = msg.getData().getString("id");
					item.setId(id);
					
					Utils.shareProduct(getActivity(), getActivity(), item, getUser());
				}
				//ShopApplication.goHome(getActivity());
				if (!editMode) {
					// clear and go home
					clear();
					((Home)getActivity()).goHome(true);
				} else {
					// clear and go back
					//clear();
					getActivity().onBackPressed();
				}
			}
			hideProgress();
		}
	}

	@Override
	public void onValidationSucceeded() {
		//			
		uploadItem();
	}

	@Override
	public void onValidationFailed(View failedView, Rule<?> failedRule) {
		//
		String message = failedRule.getFailureMessage();

		if (failedView instanceof EditText) {
			failedView.requestFocus();
			((EditText) failedView).setError(message);
		} else {
			ShopApplication.showMessage(this.getActivity(), message);
		}
	}
	
	private void updateScreen() {
		//
		if (progress != null) {

			/*takePhoto.setWidth((int) takePhoto.getPaint().measureText(
					choosePhoto.getText() + "")
					+ takePhoto.getPaddingLeft() + takePhoto.getPaddingRight());*/
			
			// dynamically load categories and conditions for spinners
			/*List<String> list = new ArrayList<String>();
			list.add("Category");
			ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
					android.R.layout.simple_spinner_item, list);
			category.setAdapter(dataAdapter);
	
			list = new ArrayList<String>();
			list.add("Condition");
			
			getResources().getStringArray(R.string.sex_);
			
			dataAdapter = new ArrayAdapter<String>(getActivity(),
					android.R.layout.simple_spinner_item, list);
			condition.setAdapter(dataAdapter);*/
			
			initialized = true;
			clear();
		}
	}
	
	public void clear() {
		//
		editMode = false;
		
		if (initialized) {
			//takePhoto.setChecked(true);
			//takePhoto.requestFocus();
			//choosePhoto.setChecked(false);
			photoSource.selectButton1();
			ImageSelector.setUseCamera(true);
			
			condition.setSelection(0);
			category.setSelection(0);
			
			selector1.clearSelection();
			selector1.cleanTempDirectory();
			selector2.clearSelection();
			selector3.clearSelection();
			selector4.clearSelection();
			selector5.clearSelection();
			selector6.clearSelection();
			
			progress.setVisibility(View.GONE);
			ImageSelector.setUseCamera(true);
	
			String empty = "";
			/*String emailTxt = empty;
			
			if (isDataServiceConnected()) {
				//
				User user = getDataService().getUser(getActivity());
				
				if (user != null) {
					//
					emailTxt = user.getEmail();
				}
			}*/
			
			//email.setText(emailTxt);
			title.setText(empty);
			price.setText(empty);
			quantity.setText(empty);
			age.setSelection(0);
			gender.setSelection(0);
			brand.setText(empty);
			tags.setText(empty);
			description.setText(empty);
			
			List<String> shareApps = getShareApps();
			
			shareOn.setVisibility(!Utils.isEmpty(shareApps) ? View.VISIBLE : View.INVISIBLE);
			shareBtn.setChecked(false);
			
			/*boolean hasTwitter = listContains("twitter", shareApps);
			boolean hasFacebook = listContains("facebook", shareApps); 
			
			if (hasTwitter || hasFacebook) {
				shareOn.setVisibility(View.VISIBLE);
				facebookShare.setVisibility(hasFacebook ? View.VISIBLE : View.GONE);
				twitterShare.setVisibility(hasTwitter ? View.VISIBLE : View.GONE);
				shareOnFacebook.setChecked(false);
				shareOnTwitter.setChecked(false);
			} else {
				//
				shareOn.setVisibility(View.INVISIBLE);
			}*/
			
			saveButton.setText(getString(R.string.add_product));
			
			//loadLocation();
			
			if (isDataServiceConnected()) {
				//
				Sorter sorter = getDataService().getSorter(getActivity());
				
				if (sorter != null && sorter.location != null) {
					//
					location.setLocation(sorter.location);
				} else {
					location.setCurrentLocation();
				}
			}
			
			scroller.postDelayed(new Runnable() {
			    @Override
			    public void run() {
			    	
			    	if (scroller.getScrollY() > 0) {
			    		scroller.fullScroll(ScrollView.FOCUS_UP);
			    		//photoSource.requestFocus();
			    	}

					if (grabImage) {
						grabImage = false;
						//selector1.grabImage();
					}
			    }
			}, 600);
		}
	}
	
	/*private boolean listContains(String substring, List<String> list) {
		//
		for (String str : list) {
			//
			if (str.contains(substring)) {
				return true;
			}
		}
		
		return false;
	}*/
	
	private List<String> getShareApps() {
		//
		ArrayList<String> apps = new ArrayList<String>();
		PackageManager pm = getActivity().getPackageManager();
	    Intent sendIntent = new Intent(Intent.ACTION_SEND);    
	    sendIntent.setType("text/plain");
	 
	    // Pick specific intents you want!
	    List<ResolveInfo> resInfo = pm.queryIntentActivities(sendIntent, 0);      
	    for (int i = 0; i < resInfo.size(); i++) {
	        // Extract the label, append it, and repackage it in a LabeledIntent
	        ResolveInfo ri = resInfo.get(i);
	        String packageName = ri.activityInfo.packageName;
	        
	        /*if (packageName.contains("twitter")) {
	        	twitterPkg = packageName;
	        	twitterName = ri.activityInfo.name;
	        } else if (packageName.contains("facebook")) {
	        	facebookPkg = packageName;
	        	facebookName = ri.activityInfo.name;
	        }*/
	        
	        apps.add(packageName);
	    }
	    
	    return apps;
	}
	
	@Override
	public void refresh() {
		// 
		if (!initialized) {
			updateScreen();
		}
	}
	
	@Override
	public void onHiddenChanged(boolean hidden) {
		// 
		super.onHiddenChanged(hidden);
		
		clear();
		
		if (showCameraOnStart) {
			//
			showCameraOnStart = false;
			selector1.grabImage();
		}
	}
	
	@Override
	public String getTitle() {
		return "Add Listing";//getString(R.string.add_product_title);
	}
	
	public boolean canShow() {
		//
		boolean show = false;
		
		if (checkAuthentication()) {
			show = true;
			
			grabImage = true;
		} else {
			//
			initialized = false;
		}
		
		return show;
	}

	public void setShowCameraOnStart() {
		// 
		showCameraOnStart = true;
	}
}
