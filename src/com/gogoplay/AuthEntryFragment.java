package com.gogoplay;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.gogoplay.R;
import com.gogoplay.data.User;
import com.gogoplay.data.User.UserType;
import com.gogoplay.ui.ShopApplication;
import com.gogoplay.ui.ShopFragment;
import com.gogoplay.ui.widget.FacebookLoginButton;
import com.gogoplay.ui.widget.ProgressDisplay;
import com.gogoplay.ui.widget.FacebookLoginButton.Mode;
import com.gogoplay.util.Utils;

import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Required;

public class AuthEntryFragment extends ShopFragment implements ValidationListener {
	//
	//private View root;
	private Validator validator;
	private Handler handler;
	
	@Required(order = 1)
	@Email(order = 2, messageResId = R.string.invalidEmail)
	private TextView email;
	private ViewGroup passwordField;
	@Required(order = 3)
	private TextView password;
	private ViewGroup fNameField;
	@Required(order = 4)
	private TextView fName;
	private ViewGroup lNameField;
	@Required(order = 4)
	private TextView lName;
	private ViewGroup sexField;
	private Spinner sex;
	private TextView modeBtn;
	private TextView forgotPwdBtn;
	private Button signupBtn;
	
	private ProgressDisplay progress;
	
	private User user;
	
	private boolean forUpdate = false;
	
	@Override
	public void dataServiceConnected(boolean connected) {
		//
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	 @Override
	 public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
		 //
		 View v = inflater.inflate(R.layout.auth_entry_fragment, parent, false);
		 createUI(v);
		 setupListeners();
		 return v;
	 }
	
	private void createUI(View v) {
		//
		//root = v.findViewById(R.id.root);
		validator = new Validator(this);
		validator.setValidationListener(this);
		handler = new SignupHandler();
		
		email = (TextView) v.findViewById(R.id.email);
		passwordField = (ViewGroup) v.findViewById(R.id.passwordField);
		password = (TextView) v.findViewById(R.id.password);
		fNameField = (ViewGroup) v.findViewById(R.id.fNameField);
		fName = (TextView) v.findViewById(R.id.fName);
		lNameField = (ViewGroup) v.findViewById(R.id.lNameField);
		lName = (TextView) v.findViewById(R.id.lName);
		sexField = (ViewGroup) v.findViewById(R.id.sexField);
		sex = (Spinner) v.findViewById(R.id.sex);
		modeBtn = (TextView) v.findViewById(R.id.modeBtn);
		forgotPwdBtn = (TextView) v.findViewById(R.id.forgotPwdBtn);
		signupBtn = (Button) v.findViewById(R.id.signUpUser);
		
		progress = (ProgressDisplay) v.findViewById(R.id.progress);
		progress.setVisibility(View.GONE);
		
		setMode(FacebookLoginButton.mode);
	}
	
	private void toggleMode() {
		//
		Mode nextMode = Mode.Signup;
		
		if (FacebookLoginButton.mode == Mode.Signup) {
			nextMode = Mode.Login;
		}
		
		FacebookLoginButton.setMode(nextMode);
		setMode(nextMode);
	}
	
	protected void setMode(Mode mode) {

		if (fNameField != null) {
			String btnLabel = getString(R.string.add_user);
			String nextModeLabel = getString(R.string.login_user);
			
			int visibility = View.VISIBLE;
			
			if (forUpdate) {
				//
				btnLabel = getString(R.string.update_user);
			} else if (mode == Mode.Login) {
				//
				btnLabel = getString(R.string.login_user);
				nextModeLabel = getString(R.string.add_user);
				visibility = View.GONE;
			}
			
			// toggle extra fields
			fNameField.setVisibility(visibility);
			lNameField.setVisibility(visibility);
			sexField.setVisibility(visibility);
			
			modeBtn.setText(nextModeLabel);
			
			signupBtn.setText(btnLabel);
		}
	}
	
	private void setupListeners() {
		//
		signupBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// sign up or log user in
				validator.validate();
			}
		});
		
		modeBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				toggleMode();
			}
		});
	}
	
	@Override
	public void onResume() {
		//
		super.onResume();
		
		// clear password
		password.setText("");
		setMode(FacebookLoginButton.mode);
		//Intent intent = new Intent(this, LoginActivity.class);

		//startActivityForResult(intent, 1);
	};
	
	@Override
	public void onStart() {
		// 
		super.onStart();
		
		//setMode(FacebookLoginButton.mode);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		//
		super.onActivityResult(requestCode, resultCode, data);
	};

	//@Override
	protected void serviceConnected() {
		// TODO Auto-generated method stub
		
	}

	//@Override
	public String getViewTitle() {
		return getString(R.string.app_name);
	}
	
	private void signupOrLoginUser() {
		//
		if (isDataServiceConnected()) {
			if (user == null) {
				user = new User();
			}
			
			user.setEmail(Utils.nullEmptyString(email.getText().toString()));
			user.setPassword(Utils.nullEmptyString(password.getText().toString()));
			
			progress.setVisibility(View.VISIBLE);
			if (FacebookLoginButton.mode == Mode.Signup) {
				user.setFirstName(Utils.nullEmptyString(fName.getText().toString()));
				user.setLastName(Utils.nullEmptyString(lName.getText().toString()));
				user.setSex(sex.getSelectedItem().toString());
				
				getDataService().signUp(getActivity(), user, handler);
			} else {
				getDataService().logIn(getActivity(), user.getEmail(), user.getPassword(), handler);
			}
		}
	}
	
	@SuppressLint("HandlerLeak")
	private class SignupHandler extends Handler {
		//
		@Override
		public void handleMessage(Message msg) {
			progress.setVisibility(View.GONE);
			boolean success = msg.getData().getBoolean("success");
			
			if (success) {
				//ShopApplication.goHome(getActivity());
				((LoginScreen)getActivity()).onBackPressed();
			} else {
				StringBuilder errorMsg = new StringBuilder(getString(R.string.loginError));
				
				if (FacebookLoginButton.mode == Mode.Signup) {
					errorMsg = new StringBuilder(getString(R.string.signupError));
				}
				
				if (!Utils.isEmpty(msg.getData().getString("msg"))) {
					errorMsg.append(msg.getData().getString("msg"));
				}
				
				ShopApplication.showMessage(getActivity(), errorMsg.toString());
			}
		}
	}
	
	@Override
	public void onValidationSucceeded() {
		// call service to add user
		signupOrLoginUser();
	}
	
	@Override
	public void onValidationFailed(View failedView, Rule<?> failedRule) {
		//
		String message = failedRule.getFailureMessage();

		if (failedView instanceof EditText) {
			failedView.requestFocus();
			((EditText) failedView).setError(message);
		} else {
			ShopApplication.showMessage(getActivity(), message);
		}
	}
	
	public void setUser(User userInfo) {
		setUser(userInfo, false);
	}
	
	public void setUser(User userInfo, boolean forUpdate) {
		//
		this.forUpdate = forUpdate;
		
		this.user = userInfo;
		String emailTxt = "";
		String passwordTxt = "";
		String firstNameTxt = "";
		String lastNameTxt = "";
		String sexTxt = "female";
		int pwdVis = View.VISIBLE;
		
		if (user != null) {
			emailTxt = user.getEmail();
			passwordTxt = user.getPassword();
			firstNameTxt = user.getFirstName();
			lastNameTxt = user.getLastName();
			sexTxt = user.getSex();
			
			if (user.getType() == UserType.facebook || forUpdate) {
				pwdVis = View.GONE;
			}
		}
		
		email.setText(emailTxt);
		password.setText(passwordTxt);
		passwordField.setVisibility(pwdVis);
		fName.setText(firstNameTxt);
		lName.setText(lastNameTxt);
		sex.setSelection("female".equalsIgnoreCase(sexTxt) ? 1 : 0);
		
		String btnText = forUpdate ? getResources().getString(R.string.update_user) : getResources().getString(R.string.add_user);
		
		signupBtn.setText(btnText);
		
		int showLogin = (!forUpdate) ? View.VISIBLE : View.GONE;
		
		forgotPwdBtn.setVisibility(showLogin);
		modeBtn.setVisibility(showLogin);
		
		if (forUpdate) {
			//
			
		}
	}
	
	public void refresh() {
		//
		Log.i(getTag(), "Refreshing!");
	}
	
	@Override
	public String getTitle() {
		// TODO Auto-generated method stub
		return "";
	}
}
