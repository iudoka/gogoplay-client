/**
 * 
 */
package com.gogoplay;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.gogoplay.data.Comment;
import com.gogoplay.data.Image;
import com.gogoplay.data.Item;
import com.gogoplay.data.User;
import com.gogoplay.ui.ProductImageAdapter;
import com.gogoplay.ui.ShopActionBarActivity;
import com.gogoplay.ui.ShopActivity;
import com.gogoplay.ui.ShopApplication;
import com.gogoplay.ui.widget.Avatar;
import com.gogoplay.ui.widget.CirclePageIndicator;
import com.gogoplay.ui.widget.CommentSection;
import com.gogoplay.ui.widget.ProgressDisplay;
import com.gogoplay.ui.widget.TextEntryFragment;
import com.gogoplay.util.LocationUtil;
import com.gogoplay.util.Utils;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;

/**
 * @author iudoka
 * 
 */
public class ProductDetail extends ShopActionBarActivity implements CommentSection.CommentListener {
	//
	private static final NumberFormat format = NumberFormat.getCurrencyInstance();
	
	public final static String ACTION_DETAILS = "Details";
	private int PAGER_IMG_SPACING;
	
	private LocationUtil locationUtil;
	private PullToRefreshScrollView scroller;
	private ProgressDisplay progress;
	private Item item;
	private View root;
	private Avatar sellerAvatar;
	private ViewGroup likeBtn;
	private ImageView likeBtnIcon;
	private TextView likeBtnText;
	private ViewGroup shareBtn;
	private ViewGroup commentBtn;
	private View productCommentsBtn;
	private TextView likeCount;
	private TextView commentCount;
	private ViewPager imageList;
	//private ProductListAdapter listAdapter;
	private ProductImageAdapter listAdapter;
	private ViewGroup brandArea;
	private TextView brand;
	private TextView age;
	private TextView description;
	private TextView title;
	private TextView price;
	private TextView quantity;
	private TextView location;
	private TextView tags;
	
	private ViewGroup btnLayout;
	private Button buyProduct;
	private Button tradeProduct;
	
	private CommentSection commentSection;
	private ViewGroup locationMapContainer;
	private ImageView locationMap;
	private TextView navigate;
	private Handler handler;
	private ItemHandler itemHandler;

	private MenuItem watchItemBtn;

	private CirclePageIndicator indicator;
	//private MenuItem reportItemBtn;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.product_details);

		createUI();
		setupListeners();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.product_details, menu);

		boolean retVal = super.onCreateOptionsMenu(menu);
		
		watchItemBtn = menu.findItem(R.id.watch);
		
		if (item != null) {
			//
			boolean isUsersItem = Utils.isSameUser(item.getSeller(), getUser());
			
			menu.findItem(R.id.report).setVisible(!isUsersItem);
			watchItemBtn.setVisible(!isUsersItem);
			watchItemBtn.setTitle(item.isWatched() ? R.string.unwatch : R.string.add_watch);
		}
		
		return retVal;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem menuItem) {
		//
		switch (menuItem.getItemId()) {
			case R.id.refresh:
				refresh();
			break;
			case R.id.report:
				if (isDataServiceConnected()) {
					//
					getDataService().reportItem(ProductDetail.this, item, handler);
				}
				break;
			case R.id.watch:
				if (isDataServiceConnected()) {
					//
					getDataService().watchItem(ProductDetail.this, item, handler);
				}
				break;
			default:
				break;
		}
		
		return super.onOptionsItemSelected(menuItem);
	}
	
	@Override
	public boolean onKeyUp(int keycode, KeyEvent e) {
	    switch(keycode) {
	        case KeyEvent.KEYCODE_MENU:
	            if (getMenu() !=null) {
	                getMenu().performIdentifierAction(R.id.action_overflow, 0);
	            }
	    }
	 
	    return super.onKeyUp(keycode, e);
	}
	
	public void shareProduct(Item item, User user) {
		//
		Utils.shareProduct(this, this, item, user);
	}

	@SuppressLint("HandlerLeak")
	protected void createUI() {
		//super.createUI();
		
		PAGER_IMG_SPACING = getResources().getDimensionPixelSize(R.dimen.product_img_spacing);
		
		//
		ActionBar actionBar = getSupportActionBar();
	    actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		root = findViewById(R.id.root);
		scroller = (PullToRefreshScrollView) findViewById(R.id.scroller);
		// Get the location manager
		locationUtil = new LocationUtil(this);
		
		imageList = (ViewPager) findViewById(R.id.productImageGallery);
		
		//listAdapter = new ProductListAdapter(this);
		listAdapter = new ProductImageAdapter(getSupportFragmentManager(), this, imageList, this);
		imageList.setAdapter(listAdapter);
		imageList.setOnPageChangeListener(listAdapter);
		
		// Set current item to the middle page so we can fling to both
		// directions left and right
		imageList.setCurrentItem(0);
		
		// Necessary or the pager will only have one extra page to show
		// make this at least however many pages you can see
		imageList.setOffscreenPageLimit(3);
		
		// Set margin for pages as a negative number, so a part of next and 
		// previous pages will be showed
	    imageList.setPageMargin(getImageSpacing());
		
		indicator = (CirclePageIndicator)findViewById(R.id.indicator);
		indicator.setViewPager(imageList);
		
		likeBtn = (ViewGroup) findViewById(R.id.likeBtn);
		likeBtnIcon = (ImageView) findViewById(R.id.likeBtnIcon);
		likeBtnText = (TextView) findViewById(R.id.likeBtnText);
		commentBtn = (ViewGroup) findViewById(R.id.commentBtn);
		productCommentsBtn = (View) findViewById(R.id.productComments);
		shareBtn = (ViewGroup) findViewById(R.id.shareBtn);
		
		likeCount = (TextView) findViewById(R.id.likeCount);
		commentCount = (TextView) findViewById(R.id.commentCount);
		
		brandArea = (ViewGroup) findViewById(R.id.brandAge);
		//ageArea = (ViewGroup) findViewById(R.id.ageContainer);
		brand = (TextView) findViewById(R.id.brand);
		age = (TextView) findViewById(R.id.age);
		title = (TextView) findViewById(R.id.title);
		price = (TextView) findViewById(R.id.price);
		quantity = (TextView) findViewById(R.id.quantity);
		description = (TextView) findViewById(R.id.description);
		location = (TextView) findViewById(R.id.location);
		tags = (TextView) findViewById(R.id.tags);
		
		btnLayout = (ViewGroup) findViewById(R.id.btnLayout);
		buyProduct = (Button) findViewById(R.id.buyProduct);
		tradeProduct = (Button) findViewById(R.id.tradeProduct);
		
		progress = (ProgressDisplay) findViewById(R.id.progress);
		
		sellerAvatar = (Avatar) findViewById(R.id.sellerAvatar);
		sellerAvatar.requestFocus();
		
		locationMapContainer = (ViewGroup) findViewById(R.id.locationMapContainer);
		locationMap = (ImageView) findViewById(R.id.locationMap);
		navigate = (TextView) findViewById(R.id.navigate);
		
		FragmentManager fm = getSupportFragmentManager();
		
		commentSection = (CommentSection) fm.findFragmentById(R.id.comments);
	}
	
	private int getImageSpacing() {
		//
		return (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) ? 2 * PAGER_IMG_SPACING : PAGER_IMG_SPACING;
	}

	@SuppressLint("HandlerLeak")
	private void setupListeners() {
		//
		commentSection.setCommentListener(this);
		
		handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				// 
				//populate();
				if (msg.getData() != null && msg.getData().getBoolean("success")) {
					refresh();
				}
			}
		};
		
		itemHandler = new ItemHandler();
		
		likeBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//
				if (isDataServiceConnected() && getDataService().checkAuthentication(ProductDetail.this)) {
					//
					progress.setVisibility(View.VISIBLE);
					getDataService().likeItem(ProductDetail.this, item, new Handler() {
						@Override
						public void handleMessage(Message msg) {
							//
							refresh();
						}
					});
				}
			}
		});
		
		productCommentsBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 
				//scroller.scrollTo(0, commentSection.getListBottom());
				scroller.post(new Runnable() {
					public void run() {
						View view = commentSection.getView();
						//view.setFocusableInTouchMode(true);
						view.requestFocusFromTouch();
						//view.requestRectangleOnScreen(viewRectangle);
					}
				});
			}
		});
		
		commentBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 
				 new Handler().post(new Runnable() {
			            @Override
			            public void run() {
			                //
			            	if (isDataServiceConnected() && getDataService().checkAuthentication(ProductDetail.this)) {
			            		// popup entry dlg
			            		Intent intent = new Intent(ProductDetail.this, TextEntryFragment.class);
			            		intent.putExtra(TextEntryFragment.EXTRA_TITLE, getString(R.string.comment));
			            		intent.putExtra(TextEntryFragment.EXTRA_MAX_CHARS, getResources().getInteger(R.integer.max_comment_length));
			            		
			            		startActivityForResult(intent, 0);
			            	}
			            }
			        });
			    }
			
		});
		
		shareBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 
				shareProduct(ProductDetail.this.item, getDataService().getUser(ProductDetail.this));
			}
		});
		
		navigate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 
				goToNavigation();
			}
		});
		
		scroller.setOnRefreshListener(new OnRefreshListener<ScrollView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ScrollView> refreshView) {
				// 
				refresh();
			}
		});
		
		buyProduct.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 
				if (getDataService().checkAuthentication(ProductDetail.this)) {
					//
					/*if (item.getPrice() == 0) {
						//
						ShopApplication.showConfirmation(ProductDetail.this, "Agree to Buy Free Item", "Do you agree to purchase this free item?", getString(R.string.yesCaps), getString(R.string.noCaps), new Handler() {
							//
							@Override
							public void handleMessage(Message msg) {
								// 
								if (msg.getData().getBoolean(ShopApplication.CONFIRM_MSG)) {
									// 
									getDataService().buyItem(ProductDetail.this, item, 0, handler);
								}
							}
						});
					} else*/ {
						ShopApplication.buyProduct(ProductDetail.this, item);
					}
				}
			}
		});
		
		tradeProduct.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 
				if (getDataService().checkAuthentication(ProductDetail.this)) {
					//
					ShopApplication.tradeProduct(ProductDetail.this, item);
				}
			}
		});
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// 
		if (data != null && resultCode == ShopActivity.RESULT_OK) {
			//
			String text = data.getExtras().getString(TextEntryFragment.EXTRA_TEXT);
			
			Comment comment = new Comment("", new Date(), getDataService().getUser(this), text, 0, null);
			commentAdded(comment);
		}
	}
	
	@Override
	public void commentAdded(Comment comment) {
		// 
		if (isDataServiceConnected()) {
			//
			item.addComment(comment);
			item.setCommentCount(item.getCommentCount() + 1);
			
			getDataService().addComment(this, comment, item, handler);
			
			displayItem(item);
		}
	}
	
	@Override
	public void commentRemoved(final Comment comment) {
		// 
		if (isDataServiceConnected()) {
			//
			progress.setVisibility(View.VISIBLE);
			getDataService().removeComment(this, comment, item, handler);
			
			if (item.getComments().remove(comment)) {
				//
				displayItem(item);
			}
		}
	}
	
	@Override
	protected void onStop() {
		super.onStop();
	}
	
	@Override
	public void onBackPressed() {
		//
		//super.onBackPressed();
		
		Intent data = new Intent(ACTION_DETAILS);
		data.putExtra("item", item);
		
		if (getParent() == null) {
		    setResult(RESULT_OK, data);
		} else {
		    getParent().setResult(RESULT_OK, data);
		}
		
		Intent intent = getIntent();
		
		if (intent != null) {
			final String action = intent.getAction();
		    
		    if (Intent.ACTION_VIEW.equals(action)) {
			    // Go back to home
			    Intent i = new Intent(this, Home.class);
		        startActivity(i);
		        
		        finish();
		    } else {
		    	super.onBackPressed();
		    }
		} else {
			super.onBackPressed();
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		long start = System.currentTimeMillis();
		ShopApplication.unbindDrawables(root);
		Log.i(getTag(), "Took " + (System.currentTimeMillis() - start) + " milliseconds to finish unbinding!");
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		if (item == null) {
			populate();
		}
		
		//sellerAvatar.requestFocus();
	}

	@Override
	protected void dataServiceConnected(boolean connected) {
		if (connected && item == null) {
			populate();
		}
	}
	
	protected void populate() {
		//
		if (isDataServiceConnected()) {
			//
			btnLayout.setVisibility(View.GONE);
			scroller.setVisibility(View.INVISIBLE);
			progress.setVisibility(View.VISIBLE);
			final Intent intent = getIntent();
		    final String action = intent.getAction();
		    String itemId = null;
		    Item item = null;
		    
		    if (Intent.ACTION_VIEW.equals(action)) {
		        final List<String> segments = intent.getData().getPathSegments();
		        if (segments.size() > 1) {
		            itemId = segments.get(segments.size() - 1);
		        }
		    } else {
		    	//
		    	Object param = intent.getExtras().get("params");
		    	
		    	if (param instanceof Item) {
		    		item = (Item) param;
		    	} else if (param instanceof String) {
		    		itemId = (String) param;
		    	}
		    }
			
		    if (itemId != null) {
		    	Log.i(getTag(), "Item details: " + itemId + " item: " + item);
		    	getDataService().getItem(this, itemId, null, itemHandler);
		    }
		    else if (item != null) {
		    	this.item = item;
		    	displayItem(item);
		    }
		}
	}
	
	protected void refresh() {
		//
		if (item != null) {
			progress.setVisibility(View.VISIBLE);
			Log.i(getTag(), "Item details: " + item);
			getDataService().getItem(this, item.getId(), null, itemHandler);
		} else {
			progress.setVisibility(View.GONE);
		}
	}

	private void displayItem(Item item) {
		//
		if (item == null) {
			//
			//showMessage("Unable to load Item");
			progress.setVisibility(View.GONE);
			onBackPressed();
			return;
		}
		
		scroller.setVisibility(View.VISIBLE);
		List<Image> images = item.getImages();
		
		Log.i(getTag(), "Item details: " + item);
		
		if (!Utils.isEmpty(images)) {
			// load main image
			//listAdapter.setImages(images);
			listAdapter.setItem(item);
			
			imageList.setCurrentItem(0);
		}
		
		likeBtn.setVisibility(View.VISIBLE);
		commentBtn.setVisibility(View.VISIBLE);
		
		if (Utils.isSameUser(getUser(), item.getSeller())) {
			// prevent self liking/commenting
			likeBtn.setVisibility(View.GONE);
			commentBtn.setVisibility(View.GONE);
		}
		
		if (item.isLiked()) {
			// show different icon
			likeBtnText.setText(getString(R.string.liked));
			likeBtnIcon.setImageResource(R.drawable.ic_heart_blue);
		} else {
			likeBtnText.setText(getString(R.string.like));
			likeBtnIcon.setImageResource(R.drawable.ic_heart_red);
		}
		
		likeCount.setText(item.getLikeCount() + "");
		commentCount.setText(item.getCommentCount() + "");
		
		if (Utils.isEmpty(item.getBrand()) && Utils.isEmpty(item.getAge())) {
			//
			brandArea.setVisibility(View.GONE);
		} else {
			brandArea.setVisibility(View.VISIBLE);
		}
		
		/*if (Utils.isEmpty(item.getAge())) {
			//
			ageArea.setVisibility(View.GONE);
		} else {
			ageArea.setVisibility(View.VISIBLE);
		}*/
		
		title.setText(getTitleText());
		brand.setText(Utils.emptyIfNull(item.getBrand()));
		age.setText(Utils.isEmpty(item.getAge()) ? getResources().getStringArray(R.array.age_array)[0] : item.getAge());
		price.setText(format.format(item.getPrice()));
		quantity.setText(item.getQuantity() + " remaining");
		description.setText(Utils.emptyIfNull(item.getDescription()));
		
		if (Utils.isEmpty(item.getAddress())) {
			loadLocation();
		} else {
			location.setText(item.getAddress());
		}
		
		// load location map if location available
		showLocationMap(item.getLocation() != null);
		
		tags.setText(parseTags(item.getTags()));
		commentSection.setItem(item);
		commentSection.setComments(new ArrayList<Comment>(item.getComments()));
		
		if (item.getSeller() != null) {
			sellerAvatar.setUser(item.getSeller(), getDataService());
		}
		
		if (watchItemBtn != null) {
			watchItemBtn.setTitle(item.isWatched() ? R.string.unwatch : R.string.add_watch);
		}
		
		if (isDataServiceConnected()) {
			//
			boolean showBuyOptions = true;
			String userId = "";
			User user = getUser();
			
			if (user != null) {
				userId = Utils.emptyIfNull(user.getId());
			}
			
			if (item.getSeller() != null &&
				userId.equalsIgnoreCase(Utils.nullEmptyString(item.getSeller().getId()))) {
				//
				showBuyOptions = false;
			}
			
			btnLayout.setVisibility(showBuyOptions ? View.VISIBLE : View.GONE);
		} else {
			btnLayout.setVisibility(View.VISIBLE);
		}
		
		// TODO: REMOVE THIS WHEN DONE!
		//btnLayout.setVisibility(View.VISIBLE);
		
		getSupportActionBar().setTitle(item.getTitle());
		progress.setVisibility(View.GONE);
	}

	@SuppressLint("HandlerLeak")
	private void loadLocation() {
		//
		if (item.getLocation() != null) {
			//
			Handler handler = new Handler() {
				//
				@Override
				public void handleMessage(Message msg) {
					boolean found = msg.getData().getBoolean("success");
					String loc = msg.getData().getString("location");
					
					if (!found) {
						loc = getString(R.string.unknown);
						//showLocationMap(false);
					} else {
						//
						//showLocationMap(true);
					}
					
					location.setText(loc);
				}
			};
			
			new LoadLocationTask(item.getLocation()[0], item.getLocation()[1], handler).execute();
			
		} else {
			location.setText(getString(R.string.unknown));
			locationMapContainer.setVisibility(View.GONE);
		}
	}
	
	private class LoadLocationTask extends AsyncTask<Void, Void, Location> {
		//
		private Handler callback;
		private double lat;
		private double lon;
		
		public LoadLocationTask(double lat, double lon, Handler callback) {
			this.lat = lat;
			this.lon = lon;
			this.callback = callback;
		}
		
		@Override
		protected Location doInBackground(Void... params) {
			// 
			return locationUtil.getLocation(lat, lon);
		}
		
		@Override
		protected void onPostExecute(Location result) {
			// 
			Message m = new Message();
			Bundle b = new Bundle();
			b.putBoolean("success", result != null);
			
			if (result != null) {
				b.putString("location", result.getExtras().getString(LocationUtil.EXTRA_CITY_AND_STATE_CODE));
			}

			m.setData(b);

			callback.sendMessage(m);
		}
	}
	
	private void showLocationMap(boolean show) {
		//
		if (show) {
			if (isDataServiceConnected()) {
				// no internet connectivity so cannot load map
				show = getDataService().checkConnectivity(this);
			} else {
				show = false;
			}
		}
		
		if (show) {
			//
			String image = getMapImage();
			loadThumbnail(image, locationMap);
		}
		
		locationMapContainer.setVisibility(show ? View.VISIBLE : View.GONE);
	}
	
	private String getMapImage() {
		//
		DisplayMetrics screenSize = ShopApplication.getScreenSize();
		int w = screenSize.widthPixels;
		int h = getResources().getDimensionPixelSize(R.dimen.location_map_height);
		String label = !Utils.isEmpty(item.getTitle()) ? item.getTitle().charAt(0) + "" : "";
		
		
		return "http://maps.googleapis.com/maps/api/staticmap?size=" + 
				w + "x" + h + "&zoom=12&center=" + item.getLocation()[0] + "," + 
				item.getLocation()[1] + "&format=png&style=feature:road.highway&markers=color:blue%7Clabel:" + 
				Utils.toUpperCase(label) + "%7C" + item.getLocation()[0] + "," + item.getLocation()[1];
	}
	
	private void goToNavigation() {
		//
		String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?&daddr=%f,%f (%s)", item.getLocation()[0], item.getLocation()[1], item.getTitle());
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        try
        {
            startActivity(intent);
        }
        catch(ActivityNotFoundException ex)
        {
            try
            {
                Intent unrestrictedIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(unrestrictedIntent);
            }
            catch(ActivityNotFoundException innerEx)
            {
                Toast.makeText(this, "Please install a maps application", Toast.LENGTH_LONG).show();
            }
        }
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
	    super.onConfigurationChanged(newConfig);
	    
	    // reload static map
	    if (item.getLocation() != null) {
	    	showLocationMap(true);
	    }
	    
	    if (item != null) {
		    // reload view pager
		    imageList.setAdapter(listAdapter);
		    
		    imageList.setPageMargin(getImageSpacing());
	    }
	}

	private String getTitleText() {
		StringBuilder title = new StringBuilder(Utils.emptyIfNull(item.getTitle()));
		
		if (!Utils.isEmpty(item.getCondition())) {
			title.append(" (" + item.getCondition() + ")");
		}
		
		return title.toString();
	}

	private String parseTags(List<String> tagList) {
		//
		StringBuilder tag = new StringBuilder();
		
		if (tagList != null && !tagList.isEmpty()) {
			boolean addComma = false;
			
			for (String t : tagList) {
				//
				if (addComma) {
					tag.append(", ");
				}
				tag.append(t);
				addComma = true;
			}
		} else {
			tag.append(getString(R.string.none));
		}
		
		if (Utils.isEmpty(tag.toString())) {
			return getString(R.string.none);
		}
		
		return tag.toString();
	}
	
	public class ProductListAdapter extends PagerAdapter {
		//
		private List<Image> images;
	    private Context mContext;

	    public ProductListAdapter(Context context) {
	        this.mContext = context;
	    }
	    
	    public void setImages(List<Image> images) {
			//
			this.images = images;
			super.notifyDataSetChanged();
		}

	    // As per docs, you may use views as key objects directly 
	    // if they aren't too complex
	    @Override
	    public Object instantiateItem(ViewGroup container, final int position) {
	        LayoutInflater inflater = LayoutInflater.from(mContext);
	        View view = inflater.inflate(R.layout.product_details_img, null);
	        container.addView(view);
	        
	        final String img = images.get(position).getUri();
			ImageView imgView = (ImageView) view.findViewById(R.id.itemImage);
			
			imgView.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					//
					if (item.getImages() != null && position < item.getImages().size()) {
						//
						Intent intent = new Intent(ProductDetail.this, FullScreenPreview.class);
						intent.putExtra(FullScreenPreview.s_EXTRA_IMAGE_GALLERY, new ArrayList<Image>(item.getImages()));
						intent.putExtra(FullScreenPreview.s_EXTRA_START_INDEX, position);
						
						startActivity(intent);
					}
				}
			});
			
			loadThumbnail(img, imgView);
	        
	        return view;
	    }

	    @Override
	    public void destroyItem(ViewGroup container, int position, Object object) {
	        container.removeView((View) object);
	    }

	    @Override
	    public int getCount() {
	        return images == null ? 0 : images.size();
	    }

	    @Override
	    public boolean isViewFromObject(View view, Object object) {
	        return view == object;
	    }

	    // Important: page takes all available width by default,
	    // so let's override this method to fit as many images as we can in a single screen
	    @Override
	    public float getPageWidth(int position) {
	        //
	    	if (images != null && images.size() > 1) {
	    		return (float) 1f/getMaxImages();
	    	} else {
	    		return 1f;
	    	}
	    }
	    
	    private int getMaxImages() {
			// calculate width
			final DisplayMetrics screenSize = new DisplayMetrics();
			getWindowManager().getDefaultDisplay().getMetrics(screenSize);

			final int img_size = getResources().getDimensionPixelSize(R.dimen.details_thumbs_size);

			int screenWidth = screenSize.widthPixels;

			int swFactor = Math.round(screenWidth / img_size);

			if (swFactor < 1) {
				swFactor = 1;
			}
			
			return swFactor;
		}
	}

	@Override
	public String getViewTitle() {
		return "";//getString(R.string.app_name);
	}
	
	@Override
	public String getViewFooter() {
		// 
		return null;
	}

	@SuppressLint("HandlerLeak")
	private class ItemHandler extends Handler {
		//
		@Override
		public void handleMessage(Message msg) {
			//
			scroller.onRefreshComplete();
			item = (Item) msg.getData().get("item");
			displayItem(item);
			//progress.setVisibility(View.GONE);
		}
	}
	
	@Override
	public String getTag() {
		return "ProductDetailView";
	}

	public void popupClosed() {
		// 
		//btnLayout.setVisibility(View.VISIBLE);
	}
	
	
}
