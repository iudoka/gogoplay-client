package com.gogoplay;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.facebook.AppEventsLogger;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionLoginBehavior;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.gogoplay.data.User;
import com.gogoplay.ui.ShopActionBarActivity;
import com.gogoplay.ui.ShopApplication;
import com.gogoplay.ui.widget.FacebookLoginButton;
import com.gogoplay.util.Utils;

public class LoginScreen extends ShopActionBarActivity {
	//
	private AuthSelectFragment selectFragment;
	private AuthEntryFragment  entryFragment;
	private boolean 		   isShowingEntry;
	
	private Session currentSession;
    private Session.StatusCallback sessionStatusCallback;
	
	@Override
	protected void dataServiceConnected(boolean connected) {
		//
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_screen);

		restoreFragments(savedInstanceState);
		
		sessionStatusCallback = new Session.StatusCallback() {
            @Override
            public void call(Session session, SessionState state, Exception exception) {
                onSessionStateChange(session, state, exception);
            }
        };
        
        if (savedInstanceState != null) {
        	//
            currentSession = Session.restoreSession(
                    this,
                    null,
                    sessionStatusCallback,
                    savedInstanceState);
        }
        
        showSelectScreen();
	}
	
	@Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        
        FragmentManager manager = getSupportFragmentManager();
        manager.putFragment(outState, AuthSelectFragment.class.getName(), selectFragment);
        manager.putFragment(outState, AuthEntryFragment.class.getName(), entryFragment);

        Session.saveSession(currentSession, outState);
	}
	
	@Override
    public void onBackPressed() {
        if (isShowingEntry) {
            // This back is from the entry fragment
            showSelectScreen();
            return;
        } else {
            // Allow the user to back out of the app as well.
        	// only allow to go back if user has authenticated
        	// to prevent circular navigation
        	if (!ShopApplication.getGoBackHome() && isDataServiceConnected()) {
        		//
        		if (getDataService().isAuthenticated(this)) {
        			super.onBackPressed();
        			return;
        		}
        	}
        }
        
        ShopApplication.setGoBackHome(false);
        ShopApplication.goHome(this);
    }
	
	private void restoreFragments(Bundle savedInstanceState) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        if (savedInstanceState != null) {
        	selectFragment = (AuthSelectFragment) manager.getFragment(savedInstanceState, AuthSelectFragment.class.getName());
        	entryFragment = (AuthEntryFragment) manager.getFragment(savedInstanceState, AuthEntryFragment.class.getName());
        }
        
        if (selectFragment == null) {
        	selectFragment = new AuthSelectFragment();
        	transaction.add(R.id.root, selectFragment, AuthSelectFragment.class.getName());
        }
        
        if (entryFragment == null) {
        	entryFragment = new AuthEntryFragment();
        	transaction.add(R.id.root, entryFragment, AuthEntryFragment.class.getName());
        }
        
        transaction.commit();
	}
	
	public void showSelectScreen() {
		isShowingEntry = false;
		
		selectFragment.setMode(FacebookLoginButton.mode);
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
	        transaction.hide(entryFragment)
	                .show(selectFragment)
	                .commit();
	}
	
	public void showEntryScreen() {
		isShowingEntry = true;
		
		entryFragment.setMode(FacebookLoginButton.mode);
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.hide(selectFragment)
                .show(entryFragment)
                .commit();
	}
	
	private void onSessionStateChange(Session session, SessionState state, Exception exception) {
        if (session != currentSession) {
            return;
        }

        if (state.isOpened()) {
            // Log in just happened.
            fetchUserInfo();
        } else if (state.isClosed()) {
            // Log out just happened. Update the UI.
            updateFragments(null);
        }
    }
	
	private void fetchUserInfo() {
        if (currentSession != null && currentSession.isOpened()) {
            Request request = Request.newMeRequest(currentSession, new Request.GraphUserCallback() {
                @Override
                public void onCompleted(GraphUser me, Response response) {
                    if (response.getRequest().getSession() == currentSession) {
                    	updateFragments(me);
                    }
                }
            });
            request.executeAsync();
        }
    }
	
	private void updateFragments(GraphUser fbUser) {
		//
		User user = new User(fbUser, currentSession.getAccessToken());
		
		if (validate(user)) {
			//FlurryAgent.setAge(fbUser.get)
			selectFragment.setUser(user);
		} else {
			entryFragment.setUser(user);
			showEntryScreen();
		}
	}
	
	private boolean validate(User user) {
		//
		boolean valid = true;
		
		if (Utils.isEmpty(user.getId()) ||
			Utils.isEmpty(user.getToken()) ||
			Utils.isEmpty(user.getEmail())) {
			//
			valid = false;
		}
		
		return valid;
	}
	
	@Override
	protected void onResume() {
		//
		super.onResume();
		//Intent intent = new Intent(this, LoginActivity.class);

		//startActivityForResult(intent, 1);
		if (currentSession != null) {
            currentSession.addCallback(sessionStatusCallback);
        }
		
		// Call the 'activateApp' method to log an app event for use in analytics and advertising reporting.  Do so in
        // the onResume methods of the primary Activities that an app may be launched into.
        AppEventsLogger.activateApp(this);
	}
	
	@Override
	protected void onPause() {
		//
		super.onPause();
		
		if (currentSession != null) {
            currentSession.removeCallback(sessionStatusCallback);
        }
	}
	
	@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (currentSession != null) {
            currentSession.onActivityResult(this, requestCode, resultCode, data);
        }
    }
	
	//@Override
	public String getTag() {
		// TODO Auto-generated method stub
		return LoginScreen.class.getSimpleName();
	}

	//@Override
	protected void serviceConnected() {
		// TODO Auto-generated method stub
		
	}
	
	public void goBack() {
		super.onBackPressed();
	}

	//@Override
	public String getViewTitle() {
		return getString(R.string.app_name);
	}
	
	@Override
	public String getViewFooter() {
		// 
		return null;
	}
	
	public void startFBSession() {
		//
		currentSession = new Session.Builder(this).build();
		currentSession.addCallback(sessionStatusCallback);
		
		Session.OpenRequest openRequest = new Session.OpenRequest(this);
		openRequest.setLoginBehavior(SessionLoginBehavior.SSO_WITH_FALLBACK);
		openRequest.setRequestCode(Session.DEFAULT_AUTHORIZE_ACTIVITY_CODE);
		
		ArrayList<String> permissions = new ArrayList<String>();
		permissions.add("email");
		
		openRequest.setPermissions(permissions);
		
		currentSession.openForRead(openRequest);
	}
}
