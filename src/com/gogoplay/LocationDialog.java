package com.gogoplay;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.gogoplay.pref.Sorter;
import com.gogoplay.pref.Sorter.SortType;
import com.gogoplay.ui.ShopActionBarActivity;
import com.gogoplay.ui.ShopApplication;
import com.gogoplay.ui.widget.ProgressDisplay;
import com.gogoplay.ui.widget.ShopEditText;
import com.gogoplay.util.LocationUtil;
import com.gogoplay.util.Utils;

public class LocationDialog extends ShopActionBarActivity {
	//
	private String TAG = "LocationGrabber";
	
	public static final String EXTRA_SORTER = "SORTER";
	public static final String EXTRA_RADIUS = "RADIUS";

	private static final int DEFAULT_RADIUS_INDEX = 6;
	
	private static final String DEFAULT_MAP_ZIP = "Brooklyn+Bridge,New+York,NY";

	private LocationUtil util;
	private Location location;
	
	//private ImageView wallpaper;
	private View closeBtn;
	private TextView locationText;
	private ViewGroup currentLocBtn;
	private ShopEditText zipcodeEntry;
	private ViewGroup radiusArea;
	private Spinner radius;
	private ProgressDisplay progress;
	
	private boolean useCurrentLocation;
	
	private Sorter sorter;

	private Button doneBtn;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// remove title

		setContentView(R.layout.location_dialog);

		createUI();
		setupListeners();
	}
	
	@Override
	protected void onResume() {
		// 
		super.onResume();
		
		Intent intent = getIntent();
		
		Sorter init = null;
		
		if (intent.getExtras() != null) {
			if (intent.getExtras().containsKey(EXTRA_SORTER)) {
				//
				init = (Sorter) intent.getExtras().get(EXTRA_SORTER);
			}
			
			if (intent.getExtras().containsKey(EXTRA_RADIUS)) {
				//
				showRadius(intent.getExtras().getBoolean(EXTRA_RADIUS));
			} else {
				showRadius(false);
			}
		}
		
		setSorter(init);
	}
	
	public void setSorter(Sorter sorter) {
		//
		this.sorter = sorter;
		
		if (sorter != null) {
			location = sorter.location;
			
			if (location != null) {
				//
				locationText.setText(location.getExtras().getString(LocationUtil.EXTRA_CITY_AND_STATE_CODE));
				zipcodeEntry.setText(location.getExtras().getString(LocationUtil.EXTRA_ZIPCODE));
			} else {
				setSorter(null);
				return;
			}
			
			setRadius(sorter.radius);
		} else {
			// use default values
			location = util.getCurrentLocation();
			
			String locText = null;
			String zipcodeStr = null;
			
			if (location != null) {
				locText = location.getExtras().getString(LocationUtil.EXTRA_CITY_AND_STATE_CODE);
				zipcodeStr = location.getExtras().getString(LocationUtil.EXTRA_ZIPCODE);
				
				if (Utils.isEmpty(zipcodeStr)) {
					zipcodeStr = util.getCurrentZipcode();
					location.getExtras().putString(LocationUtil.EXTRA_ZIPCODE, zipcodeStr);
				}
			}
			
			if (!Utils.isEmpty(zipcodeStr)) {
				//
				locationText.setText(Utils.emptyIfNull(locText));
				zipcodeEntry.setText(Utils.emptyIfNull(zipcodeStr));
			} else {
				//
				locationText.setText("");
				zipcodeEntry.setText("");
			}
			
			setRadius(Sorter.DEFAULT_RADIUS);
			
			doneBtn.setVisibility(!Utils.isEmpty(zipcodeEntry.getText().toString()) ? View.VISIBLE : View.GONE);
		}
	}
	
	private void setRadius(int theRadius) {
		// 
		int index = DEFAULT_RADIUS_INDEX;
		
		for (int i= 0; i < radius.getAdapter().getCount(); i++) {
			int r = parseInt(radius.getAdapter().getItem(i));
			
			if (r == theRadius) {
				index = i;
				break;
			}
		}
		
		radius.setSelection(index);
	}

	protected void createUI() {
		//
		util = new LocationUtil(LocationDialog.this);
		
		//wallpaper = (ImageView) findViewById(R.id.wallpaper);
		closeBtn = (View) findViewById(R.id.closeBtn);
		doneBtn = (Button) findViewById(R.id.doneBtn);
		currentLocBtn = (ViewGroup) findViewById(R.id.currentLocBtn);
		locationText = (TextView) findViewById(R.id.locationText);
		zipcodeEntry = (ShopEditText) findViewById(R.id.zipcodeEntry);
		// Make sure keyboard pops up with done button
		zipcodeEntry.setImeOptions(EditorInfo.IME_ACTION_DONE);
		
		//zipBtn = (View) findViewById(R.id.zipBtn);
		radiusArea = (ViewGroup) findViewById(R.id.radiusArea);
		radius = (Spinner) findViewById(R.id.radius);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.radius_array));
		adapter.setDropDownViewResource(R.layout.spinner_drop_item);
		radius.setAdapter(adapter);
		radius.setSelection(4);
		progress = (ProgressDisplay) findViewById(R.id.progress);
	}
	
	private void setupListeners() {
		
		closeBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 
				zipcodeEntry.hideKeyboard();
				setResult(RESULT_CANCELED);
				finish();
			}
		});
		
		doneBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//
				storeSorter();
				
				Intent data = new Intent(EXTRA_SORTER);
				data.putExtra(EXTRA_SORTER, sorter);
				
				setResult(RESULT_OK, data);
				finish();
			}
		});
		
		currentLocBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 
				useCurrentLocation = false;
				location = util.getCurrentLocation(true, getString(R.string.turn_on_location), getString(R.string.enterZipcode));
				String zip = null;
				String loc = null;
				
				if (location != null) {
					zip = location.getExtras().getString(LocationUtil.EXTRA_ZIPCODE);
					loc = location.getExtras().getString(LocationUtil.EXTRA_CITY_AND_STATE_CODE);
				}
				
				if (!Utils.isEmpty(zip)) {
					// Show save btn
					useCurrentLocation = true;
					doneBtn.setVisibility(View.VISIBLE);
				} else {
					// Hide save button
					//Toast toast = Toast.makeText(LocationDialog.this, getString(R.string.unable_to_find_location), Toast.LENGTH_LONG);
					//toast.show();
					
					doneBtn.setVisibility(View.GONE);
				}
				
				zipcodeEntry.setText(Utils.emptyIfNull(zip));
				locationText.setText(Utils.emptyIfNull(loc));
			}
		});
		
		zipcodeEntry.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				progress.setVisibility(View.VISIBLE);

				String message = "Please enter a valid zipcode";
				String zip = zipcodeEntry.getText().toString();
				
				if (Utils.isEmpty(zip) || zip.length() < 5) {
					// invalid zip code
					location = null;
				} else {
					//
					LocationUtil util = new LocationUtil(LocationDialog.this);
					location = util.getLocation(zip);
				}

				progress.setVisibility(View.GONE);
				
				if (location == null) {
					//
					Toast toast = Toast.makeText(LocationDialog.this, message, Toast.LENGTH_LONG);
					toast.show();
					
					doneBtn.setVisibility(View.GONE);
				} else {
					//
					locationText.setText(location.getExtras().getString(LocationUtil.EXTRA_CITY_AND_STATE_CODE));
					doneBtn.setVisibility(View.VISIBLE);
					updateMapLocation(zipcodeEntry.getText().toString());
				}
				return false;
			}
		});
	}
	
	@Override
	public void finish() {
		// hide soft input keyboard
		onClose();
		super.finish();
	}
	
	private void updateMapLocation(String zipcode) {
		//
		//loadThumbnail(getMapImageUri(zipcode), wallpaper);
	}
	
	private void showRadius(boolean show) {
		radiusArea.setVisibility(show ? View.VISIBLE : View.GONE);
	}
	
	private String getMapImageUri(String zipcode) {
		//
		int scale = 2;
		DisplayMetrics screenSize = ShopApplication.getScreenSize();
		int w = screenSize.widthPixels/scale;
		int h = screenSize.heightPixels/scale;
		
		
		return "http://maps.googleapis.com/maps/api/staticmap?size=" + 
				w + "x" + h + "&zoom=12&center=" + zipcode;// + "&format=png&style=feature:road.highway";
	}
	
	private void storeSorter() {
		//
		sorter = new Sorter();
		
		sorter.type = SortType.distance;
		sorter.useCurrentLocation = useCurrentLocation;
		sorter.location = location;
		sorter.radius = parseInt(radius.getSelectedItem());
	}
	
	private int parseInt(Object selection) {
		String val = selection.toString();
		val = val.replace("miles", "");
		
		return Integer.parseInt(val.trim());
	}
	
	@Override
	public String getTag() {
		// 
		return TAG;
	}

	@Override
	public String getViewTitle() {
		// 
		return "";
	}

	@Override
	public String getViewFooter() {
		// 
		return null;
	}

	@Override
	protected void dataServiceConnected(boolean connected) {
		// TODO Auto-generated method stub
		String zip = DEFAULT_MAP_ZIP;
		
		if (location != null) {
			//
			zip = location.getExtras().getString(LocationUtil.EXTRA_ZIPCODE);
		}
		
		//loadThumbnail(getMapImageUri(zip), wallpaper);
	}
}
