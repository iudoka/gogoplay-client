package com.gogoplay;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.gogoplay.ui.AccountAdapter;
import com.gogoplay.ui.ShopActionBarActivity;
import com.gogoplay.ui.ShopActivity;
import com.gogoplay.ui.ShopApplication;
import com.gogoplay.ui.ShopFragment;
import com.gogoplay.ui.widget.ListItem;
import com.gogoplay.ui.widget.ListItem.Type;
import com.gogoplay.ui.widget.ProgressDisplay;
import com.gogoplay.ui.widget.TextEntryFragment;

public class Account extends ShopFragment {
	public static final int RATE_REQUEST = 1911;
	
	public static final String LOGOUT = "Logout";
	public static final String ORDER_HISTORY = "My Listings";
	public static final String RATE = "Love";
	public static final String SETTINGS = "Settings";
	public static final String PROFILE = "Profile";
	public static final String ABOUT = "How It Works";
	public static final String HELP = "Help";
	public static final String INVITE = "Invite Friends";
	//
	private ViewGroup root;
	private ProgressDisplay progress;
	private ListView list;
	private AccountAdapter adapter;
	private ArrayList<ListItem> navItems;
	private Handler logoutHandler;
	private Handler rateHandler;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		//
		return inflater.inflate(R.layout.account, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (savedInstanceState != null)
			return;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		if (checkAuthentication()) {
			//
		}

		createUI();
		setupListeners();
	}
	
	@Override
    public void onStart() {
        super.onStart();
        
        //checkAuthentication();
        if (list != null)
        	list.requestFocus();
    }
	
	private boolean checkAuthentication() {
		//
		if (isDataServiceConnected()) {
			//
			ShopApplication.setGoBackHome(true);
			if (getDataService().checkAuthentication(getActivity())) {
				//
				ShopApplication.setGoBackHome(false);
				return true;
			} else {
				//
				return false;
			}
		}
		
		return false;
	}
	
	@Override
	public void onResume() {
		super.onResume();
	}
	
	private void attachDataService() {
		//
	}

    @Override
    public void onStop() {
        super.onStop();
    }
    
    @Override
    public void onPause() {
    	super.onPause();
    }

	@Override
	public void onDestroy() {
		//
		super.onDestroy();
		
		ShopApplication.unbindDrawables(root);
	}

	private void createUI() {
		//
		root = (ViewGroup)getView().findViewById(R.id.root);
		list = (ListView)getView().findViewById(R.id.list);
		
		navItems = new ArrayList<ListItem>();
		
		navItems.add(new ListItem(PROFILE, -1, -1, Type.Navigation, null));
		navItems.add(new ListItem(SETTINGS, -1, -1, Type.Navigation, null));
		navItems.add(new ListItem(getString(R.string.app_name) + " " + HELP, -1, -1, Type.Navigation, null));
		navItems.add(new ListItem(INVITE, -1, -1, Type.Navigation, null));
		navItems.add(new ListItem(ORDER_HISTORY, -1, -1, Type.Navigation, null));
		navItems.add(new ListItem(RATE + " " + getString(R.string.app_name) + "?", -1, -1, Type.Action, null));
		navItems.add(new ListItem(ABOUT, -1, -1, Type.Navigation, null));
		//navItems.add(new ListItem(LOGOUT, -1, -1, Type.Action, null));
		
		
		adapter = new AccountAdapter((ShopActionBarActivity)getActivity(), getActivity(), navItems);
		
		list.setAdapter(adapter);
		list.setOnItemClickListener(new AccountItemClickListener());
		list.setSelector(getResources().getDrawable(android.R.drawable.screen_background_light));
	}
	
	private class AccountItemClickListener implements
			ListView.OnItemClickListener {
		@Override
		@SuppressWarnings("rawtypes")
		public void onItemClick(AdapterView parent, View view,
				final int position, long id) {
			
			ListItem item = navItems.get(position);
			
			if (item != null) {
				if (item.getType() == Type.Action) {
					if (LOGOUT.equalsIgnoreCase(item.getTitle())) {
						//
						logout();
					} else if (item.getTitle().startsWith(RATE)) {
						//
						String appName = getString(R.string.app_name);
						String title = getString(R.string.rate) + " " + appName;
						String msg = getString(R.string.rate_app).replace("%%", appName);
						ShopApplication.showConfirmation(getActivity(), title, msg, getString(R.string.Yes_ex), getString(R.string.no), rateHandler);
					}
				} else if (item.getType() == Type.Navigation) {
					//
					 if (PROFILE.equalsIgnoreCase(item.getTitle())) {
						 //
						if (isDataServiceConnected()) {
							ShopApplication.goToProfile(getActivity(), getDataService().getUser(getActivity()));
						}
					} else if (ORDER_HISTORY.equalsIgnoreCase(item.getTitle())) {
						 //
						if (isDataServiceConnected()) {
							ShopApplication.goToOrderHistory(getActivity(), getDataService().getUser(getActivity()));
						}
					} else if (SETTINGS.equalsIgnoreCase(item.getTitle())) {
						//
						ShopApplication.goToSettings(getActivity());
					}
				}
			}
		}
	}
	
	public void logout() {
		//
		if (isDataServiceConnected()) {
			ShopApplication.showConfirmation(getActivity(), LOGOUT, getString(R.string.logout_confirm), getString(R.string.logout_user), getString(R.string.cancel), logoutHandler);
		}
	}
	
	@SuppressLint("HandlerLeak")
	private void setupListeners() {
		//
		logoutHandler = new Handler() {
			
			@Override
			public void handleMessage(Message msg) {
				// 
				if (msg.getData().getBoolean(ShopApplication.CONFIRM_MSG)) {
					//
					getDataService().logOut(getActivity());
					((Home)getActivity()).goHome(true);
					//ShopApplication.goHome(getActivity());
				}
			}
		};
		
		rateHandler = new Handler() {
			
			@Override
			public void handleMessage(Message msg) {
				// 
				if (msg.getData().getBoolean(ShopApplication.CONFIRM_MSG)) {
					// Rate the app
					final String appPackageName = getActivity().getPackageName();
					
					try { 
					    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
					} catch (android.content.ActivityNotFoundException anfe) {
					    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
					}
				} else {
					// Find out why
					Intent intent = new Intent(getActivity(), TextEntryFragment.class);
            		intent.putExtra(TextEntryFragment.EXTRA_TITLE, getString(R.string.rateUs));
            		intent.putExtra(TextEntryFragment.EXTRA_SEND_TEXT, getString(R.string.send));
            		intent.putExtra(TextEntryFragment.EXTRA_HINT, getString(R.string.rateReasonHint));
            		intent.putExtra(TextEntryFragment.EXTRA_MAX_CHARS, getResources().getInteger(R.integer.max_comment_length));
            		
            		startActivityForResult(intent, RATE_REQUEST);
				}
			}
		};
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// 
		if (requestCode == RATE_REQUEST) {
			//
			if (data != null && resultCode == ShopActivity.RESULT_OK) {
				//
				String rateReason = data.getExtras().getString(TextEntryFragment.EXTRA_TEXT);
			}
		} else {
			super.onActivityResult(requestCode, resultCode, data);
		}
	}
	
	@Override
	public void refresh() {
		//
		//checkAuthentication();
	}

	@Override
	public void dataServiceConnected(boolean connected) {
		//
		if (connected) {
			attachDataService();
		}
	}
	
	@Override
	public String getTitle() {
		return "Account";//getString(R.string.sellers_list_title);
	}
	
	public boolean canShow() {
		//
		return checkAuthentication();
	}
}
