package com.gogoplay;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;

public class WishList extends Products {
	//
	private ProductFetcher fetcher;
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// 
		super.onViewCreated(view, savedInstanceState);
		
		fetcher = new ProductFetcher() {
			
			@Override
			public void fetchItems(String lastItemId, Handler handler, boolean refresh) {
				// 
				WishList.this.fetchItems(lastItemId, handler, refresh);
			}
			
			public boolean canFetch() {
				boolean fetch = true;
				
				if (isDataServiceConnected()) {
					fetch = getDataService().checkAuthentication(getActivity());
				}
				
				return fetch;
			}
		};
		setProductFetcher(fetcher);
	}
	
	/**
	 * Override fetch Items to load the specific
	 * products the current view wants to display
	 */
	private void fetchItems(String lastItemId, Handler handler, boolean refresh) {
		//
		Log.i(getTag(), "Loading items" + (refresh ? " for refresh" : ""));
		getDataService().getWatchedItems(getActivity(), lastItemId, handler, refresh);
	}
	
	public String getTitle() {
		return "Watch List";
	}
	
	protected String getEmptyTitle() {
		//
		return getString(R.string.emptyWishListTitle);
	}
	
	protected String getEmptyText() {
		//
		return getString(R.string.emptyWishList);
	}
	
	protected String getNoResultsTitle() {
		return getEmptyTitle();
	}
	
	protected String getNoResultsText() {
		return getEmptyText();
	}
	
	protected Drawable getEmptyIcon() {
		//
		return getResources().getDrawable(R.drawable.ic_heart_red);
	}
	
	protected void onEmptyMessageClicked() {
		// Do nothing
	}
	
	private boolean checkAuthentication() {
		if (isDataServiceConnected()) {
			return getDataService().checkAuthentication(getActivity());
		}
		
		return true;
	}
	
	public void refresh() {
		//
		//if (checkAuthentication()) {
			super.refresh();
		//}
	}
	
	public boolean canShow() {
		//
		return checkAuthentication();
	}
	
	@Override
	public boolean isFeedDisplay() {
		// 
		return false;
	}
	
	@Override
	public void update() {
		//
		refresh();
	}
	
	public boolean isSortingRequired() {
		return false;
	}
	
	@Override
	public boolean isFragmentHidden() {
		boolean hidden = super.isFragmentHidden();
		return hidden;
	}
}
