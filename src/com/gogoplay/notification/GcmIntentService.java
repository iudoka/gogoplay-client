package com.gogoplay.notification;

import java.util.Set;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;

import com.gogoplay.data.Item;
import com.gogoplay.data.Notification;
import com.gogoplay.data.Notification.Action;
import com.gogoplay.data.server.ShopClient;
import com.gogoplay.util.Utils;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class GcmIntentService extends IntentService {
	
	private static final String TAG = "GcmIntentService";
	private static final String s_MSG_TYPE = "msg";
	private static final String s_META_DATA = "meta";
	private static final String s_MSG_LIKE_TYPE = "like";
	private static final String s_MSG_COMMENT_TYPE = "comment";
	private static final String s_MSG_EMAIL_TYPE = "message";
	private static final String s_MSG_WATCH_TYPE = "watch";
	private static final String s_MSG_FOLLOW_TYPE = "follow";

	public GcmIntentService() {
		super("GcmIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		// The getMessageType() intent parameter must be the intent you received
		// in your BroadcastReceiver.
		String messageType = gcm.getMessageType(intent);

		if (!extras.isEmpty()) { // has effect of unparcelling Bundle
			/*
			 * Filter messages based on message type. Since it is likely that
			 * GCM will be extended in the future with new message types, just
			 * ignore any message types you're not interested in, or that you
			 * don't recognize.
			 */
			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR
					.equals(messageType)) {
				//sendNotification("Send error", extras.toString(), null);
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED
					.equals(messageType)) {
				//sendNotification("Deleted messages on server: "
				//		, extras.toString(), null);
				// If it's a regular GCM message, do some work.
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE
					.equals(messageType)) {
				Log.i(TAG, "Completed work @ " + SystemClock.elapsedRealtime());
				
				Set<String> keySet = extras.keySet();
				Notification not = null;
				String message = null;
				String itemId = null;
				
				for (String key : keySet) {
					//
					if (s_MSG_TYPE.equalsIgnoreCase(key)) {
						// dequeue notification
						message = extras.getString(key);
						not = new Notification();
						not.setNotification(message);
						not.setDateReceived(null);
					} else if (s_META_DATA.equalsIgnoreCase(key)) {
						// actual push notification
						// parse meta data
						JsonElement jsonItem = new JsonParser().parse(extras.getString(key));
						JsonObject obj = jsonItem.getAsJsonObject();
						Action action = null;
						String actionKey = null;
						
						if (Utils.has(obj, s_MSG_LIKE_TYPE)) {
							//
							action = Action.like;
							actionKey = s_MSG_LIKE_TYPE;
						} else if (Utils.has(obj, s_MSG_COMMENT_TYPE)) {
							//
							action = Action.comment;
							actionKey = s_MSG_COMMENT_TYPE;
						} else if (Utils.has(obj, s_MSG_EMAIL_TYPE)) {
							//
							action = Action.message;
							actionKey = s_MSG_EMAIL_TYPE;
						} else if (Utils.has(obj, s_MSG_WATCH_TYPE)) {
							action = Action.watch;
							actionKey = s_MSG_WATCH_TYPE;
						} else if (Utils.has(obj, s_MSG_FOLLOW_TYPE)) {
							action = Action.follow;
							actionKey = s_MSG_FOLLOW_TYPE;
						}
						
						if (action != null) {
							JsonObject itemObj = obj.get(actionKey).getAsJsonObject().getAsJsonObject("item");
							
							// parse item
							Item item = ShopClient.parseItem(itemObj, null);
							
							if (item != null) {
								itemId = item.getId();
							}
							
							not = new Notification();
							not.setAction(action);
							not.setItemId(itemId);
							not.setDateReceived(null);
							not.setNotification("");
							not.setFrom(null); // TODO: fix this and save notification
						}
					}
				}
				
				if (not != null) {
					// Post notification of received message.
					sendNotification(not);
				}
			}
		}
		// Release the wake lock provided by the WakefulBroadcastReceiver.
		GcmBroadcastReceiver.completeWakefulIntent(intent);
	}

	private void sendNotification(Notification note) {
		PushNotificationHelper.displayNotification(this, note);
	}
}
