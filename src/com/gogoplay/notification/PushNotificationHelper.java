package com.gogoplay.notification;

import java.io.IOException;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.gogoplay.Home;
import com.gogoplay.ProductDetail;
import com.gogoplay.R;
import com.gogoplay.data.DataService;
import com.gogoplay.data.Notification;
import com.gogoplay.data.User;
import com.gogoplay.data.User.PushToken;
import com.gogoplay.data.User.PushToken.DeviceType;
import com.gogoplay.data.server.ServerUtils;
import com.gogoplay.ui.ShopActionBarActivity;
import com.gogoplay.ui.ShopApplication;
import com.gogoplay.util.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

@SuppressLint("HandlerLeak")
public class PushNotificationHelper {
	//
	private static final String TAG = "PushNotificationHelper";
	public static final int NOTIFICATION_ID = 1;

	public static final String EXTRA_MESSAGE = "message";
	public static final String PROPERTY_REG_ID = "registration_id";
	private static final String PROPERTY_APP_VERSION = "appVersion";
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

	/**
	 * Substitute you own sender ID here. This is the project number you got
	 * from the API Console, as described in "Getting Started."
	 */
	String SENDER_ID = "631941922119";

	private GoogleCloudMessaging gcm;
	private Context context;
	private ShopActionBarActivity activity;

	private String deviceRegistrationId;
	
	public PushNotificationHelper(Context context, ShopActionBarActivity shopActionBarActivity) {
		//
		this.context = context;
		this.activity = shopActionBarActivity;
	}
	
	public void register(Handler handler) {
		register(handler, false);
	}

	public void register(Handler handler, boolean force) {
		//
		// Check device for Play Services APK. If check succeeds, proceed with
		// GCM registration.
		if (checkPlayServices()) {
			//gcm = GoogleCloudMessaging.getInstance(context);
			deviceRegistrationId = getRegistrationId();

			if (force || Utils.isEmpty(deviceRegistrationId)) {
				registerInBackground(handler, deviceRegistrationId, force);
			} else {
				//
				Message m = new Message();
	            Bundle b = new Bundle();
	            b.putString("registrationId", deviceRegistrationId);
	            m.setData(b);
	            
	            handler.sendMessage(m);
			}
		} else {
			Log.i(TAG, "No valid Google Play Services APK found.");
		}
	}
	
	public String getDeviceRegistrationId() {
		//
		return deviceRegistrationId;
	}

	/**
	 * Gets the current registration ID for application on GCM service.
	 * <p>
	 * If result is empty, the app needs to register.
	 * 
	 * @return registration ID, or empty string if there is no existing
	 *         registration ID.
	 */
	private String getRegistrationId() {
		final SharedPreferences prefs = getGCMPreferences();
		String registrationId = prefs.getString(PROPERTY_REG_ID, "");
		if (registrationId.isEmpty()) {
			Log.i(TAG, "Registration not found.");
			return "";
		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION,
				Integer.MIN_VALUE);
		int currentVersion = getAppVersion(context);
		if (registeredVersion != currentVersion) {
			Log.i(TAG, "App version changed.");
			return "";
		}
		return registrationId;
	}

	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	private static Integer getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			Log.e(TAG, "Could not get package name: " + e);
		}

		return null;
	}

	/**
	 * @return Application's {@code SharedPreferences}.
	 */
	private SharedPreferences getGCMPreferences() {
		// This sample app persists the registration ID in shared preferences,
		// but how you store the regID in your app is up to you.
		return context.getSharedPreferences(activity.getClass().getSimpleName(), Context.MODE_PRIVATE);
	}

	/**
	 * Check the device to make sure it has the Google Play Services APK. If it
	 * doesn't, display a dialog that allows users to download the APK from the
	 * Google Play Store or enable it in the device's system settings.
	 */
	public boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, activity,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Log.i(TAG, "This device is not supported.");
				// activity.finish();
			}
			return false;
		}
		return true;
	}
	
	/**
	 * Registers the application with GCM servers asynchronously.
	 * <p>
	 * Stores the registration ID and app versionCode in the application's
	 * shared preferences.
	 */
	private void registerInBackground(final Handler handler, final String oldDeviceId, boolean forcePush) {
	    //
	    new RegistrationTask(handler, oldDeviceId, forcePush).execute();
	}
	
	private class RegistrationTask extends AsyncTask<Void, Void, String> {
		//
		private String oldDeviceId;
		private Handler handler;
		private boolean forcePush;

		public RegistrationTask(Handler handler, String oldDeviceId, boolean forcePush) {
			this.handler = handler;
			this.oldDeviceId = oldDeviceId;
			this.forcePush = forcePush;
		}

		@Override
		protected String doInBackground(Void... params) {
			String msg = "";
			try {
				if (gcm == null) {
					gcm = GoogleCloudMessaging.getInstance(context);
				}
				deviceRegistrationId = gcm.register(SENDER_ID);
				//msg = "Device registered, registration ID="
				//		+ deviceRegistrationId;

				boolean pushToServer = forcePush || !Utils.equals(oldDeviceId, deviceRegistrationId);

				if (pushToServer) {
					// You should send the registration ID to your server over
					// HTTP,
					// so it can use GCM/HTTP or CCS to send messages to your
					// app.
					// The request to your server should be authenticated if
					// your app
					// is using accounts.
					sendRegistrationIdToBackend(deviceRegistrationId, handler);
				}

				// Persist the regID - no need to register again.
				storeRegistrationId(deviceRegistrationId);
			} catch (IOException ex) {
				msg = "Error :" + ex.getMessage();
				// If there is an error, don't just keep trying to register.
				// Require the user to click a button again, or perform
				// exponential back-off.
			}

			Log.i(TAG, msg);
			return deviceRegistrationId;
		}

		@Override
		protected void onPostExecute(String regId) {
			Message m = new Message();
			Bundle b = new Bundle();
			b.putString("registrationId", regId);
			m.setData(b);

			handler.sendMessage(m);
		}
	}
	
	public void sendRegistrationIdToBackend(String regId, Handler handler) {
		// 
		// send to server
		if (activity.isDataServiceConnected() && activity.getDataService().getUser(activity) != null) {
			//
			PushToken token = new PushToken();

			token.setDeviceType(DeviceType.android);
			token.setDeviceId(ShopApplication.getDeviceId(context));//activity.getDataService().getUser(activity).getEmail());
			token.setDeviceToken(regId);
			
			activity.getDataService().sendPushToken(context, token, handler);
		}
	}

	/**
	 * Stores the registration ID and app versionCode in the application's
	 * {@code SharedPreferences}.
	 *
	 * @param context application's context.
	 * @param regId registration ID
	 */
	private void storeRegistrationId(String regId) {
		//
	    final SharedPreferences prefs = getGCMPreferences();
	    int appVersion = getAppVersion(context);
	    Log.i(TAG, "Saving regId " + regId + " on app version " + appVersion);
	    SharedPreferences.Editor editor = prefs.edit();
	    editor.putString(PROPERTY_REG_ID, regId);
	    editor.putInt(PROPERTY_APP_VERSION, appVersion);
	    editor.commit();
	}
	
	// Put the message into a notification and post it.
	public static void displayNotification(Context context, Notification note) {
		//
		Log.i(TAG, "Received notification: " + note);
		NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		String title = context.getString(R.string.notificationTitle);
		String msg = Utils.emptyIfNull(note.getNotification());
		String itemId = note.getItemId();

		Intent productIntent = null;
			
		if (!Utils.isEmpty(itemId)) {
			//
			productIntent = new Intent(context, ProductDetail.class);
			Uri data = Uri.parse(ServerUtils.getServerUrl() + ServerUtils.buildItemUrl(null, "") + "/" + itemId);
			productIntent.setAction(Intent.ACTION_VIEW);
			productIntent.setData(data);
			//productIntent.putExtra("params", itemId);
		} else {
			//
			productIntent = new Intent(context, Home.class);
			productIntent.setAction(Intent.ACTION_SYNC);
		}
		
		// Display a notification message on the phone is a user is currently logged on
		User user = DataService.getCurrentUser(context);
		
		if (user != null) {
			//
			PendingIntent contentIntent = PendingIntent.getActivity(context, 0, productIntent, 0);
			
			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
					context).setSmallIcon(R.drawable.ic_menu_notifications)
					.setContentTitle(title)
					.setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
					.setContentText(msg);
	
			mBuilder.setContentIntent(contentIntent);
			mBuilder.setAutoCancel(true);
			mNotificationManager.notify(msg, NOTIFICATION_ID, mBuilder.build());
		}		
		
		//
		//note.setUniqueId(user.getId());
		//note.store(context);
	}
}
