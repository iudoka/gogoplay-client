package com.gogoplay;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ScrollView;
import android.widget.TextView;

import com.gogoplay.data.Item;
import com.gogoplay.data.User;
import com.gogoplay.ui.ShopActionBarActivity;
import com.gogoplay.ui.ShopApplication;
import com.gogoplay.ui.widget.NonScrollableGridView;
import com.gogoplay.ui.widget.ProfileHeader;
import com.gogoplay.ui.widget.ProgressDisplay;
import com.gogoplay.ui.widget.RecyclingImageView;
import com.gogoplay.util.Utils;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;

@SuppressLint("HandlerLeak")
public class ProfileScreen extends ShopActionBarActivity implements OnRefreshListener<ScrollView>, OnLastItemVisibleListener {
	//
	private User user;
	
	private ProgressDisplay progress;
	private ProductAdapter adapter;
	private PullToRefreshScrollView scroller;
	private NonScrollableGridView grid;
	private int currentOrientation;
	private Item lastItem;
	
	private ProfileHeader profileHeader;
	private ResultHandler handler;
	private boolean waitingForDataService = true;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profile);
		
		createUI();
		setupListeners();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.profile, menu);

		return super.onCreateOptionsMenu(menu);
	}
	
	
	private void createUI() {
		//
		//root = (ViewGroup) findViewById(R.id.root);
		progress = (ProgressDisplay) findViewById(R.id.progress);
		handler = new ResultHandler();
		profileHeader = new ProfileHeader(this, null, 0);
		
		adapter = new ProductAdapter(this, this, null);
		
		((ViewGroup) findViewById(R.id.headerLayout)).addView(profileHeader);
		scroller = (PullToRefreshScrollView) findViewById(R.id.scroller);
		scroller.setOnRefreshListener(this);
		grid = (NonScrollableGridView) findViewById(R.id.list);
		grid.setAdapter(adapter);
	}
	
	private void load(boolean refresh, String lastItemId) {
		//
		showMessage("Loading...");
		getDataService().getUserItems(ProfileScreen.this, lastItemId, getUserFromIntent().getId(), handler, refresh);
	}
	
	@SuppressLint("HandlerLeak")
	private class ResultHandler extends Handler {
		//
		@SuppressWarnings("unchecked")
		@Override
		public void handleMessage(Message msg) {
			//
			ArrayList<Item> items = (ArrayList<Item>) msg.getData().get("items");
			boolean refresh = msg.getData().getBoolean("refresh");
			String lastItemId = msg.getData().getString("lastItemId");
			
			// Set profile info
    		User u = getUserFromIntent();
    		int size = items != null ? items.size() : 0;
    		
    		profileHeader.setUser(u, size);
    		
    		displayItems(items, lastItemId, refresh);
			//adapter.notifyDataSetChanged();
    		
    		profileHeader.requestFocus();
    		profileHeader.requestFocusFromTouch();
		}
		
		private void displayItems(List<Item> items, String lastItemId, boolean refresh) {
			//
			grid.setNumColumns((int)adapter.getNumColumns());
			
			if (!Utils.isEmpty(items)) {
				//
				lastItem = items.get(items.size() - 1);
					
				if (refresh) {
					// refresh data
					adapter.refresh(items);
					
				} else if (lastItemId == null) {
					//
					adapter.refresh(items);
				}
				else {
					adapter.append(items);
				}
			} else {
				//
				lastItem = null;
				
				if (refresh) {
					//
					// refresh data
					clear();
				}
			}
			
			hideMessage();
		}
	}
	
	public void clear() {
		//
		if (adapter != null) {
			adapter.refresh(new ArrayList<Item>());
		}
	}
	
	private void setupListeners() {
		// 
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem menuItem) {
		//
		switch (menuItem.getItemId()) {
			case R.id.edit:
				edit();
				break;
			case R.id.refresh:
				refresh();
			break;
			case R.id.report:
				if (isDataServiceConnected()) {
					//
					showMessage("");
					getDataService().reportUser(this, user, new Handler() {
						@Override
						public void handleMessage(Message msg) {
							// 
							hideMessage();
							if (msg.getData().getBoolean("success")) {
								//
								
							}
						}
					});
				}
				break;
			default:
				break;
		}
		
		return super.onOptionsItemSelected(menuItem);
	}
	
	public void hideMessage() {
		progress.setVisibility(View.GONE);
		scroller.onRefreshComplete();
	}
	
	public void showMessage(String msg) {
		//
		progress.setVisibility(View.VISIBLE);
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// 
		boolean currUser = isCurrentUser();
		
		menu.findItem(R.id.edit).setVisible(currUser);
		menu.findItem(R.id.action_overflow).setVisible(!currUser);
		menu.findItem(R.id.report).setVisible(!currUser);
		
		return super.onPrepareOptionsMenu(menu);
	}
	
	@Override
	public boolean onKeyUp(int keycode, KeyEvent e) {
	    switch(keycode) {
	        case KeyEvent.KEYCODE_MENU:
	            if (getMenu() !=null) {
	                getMenu().performIdentifierAction(R.id.action_overflow, 0);
	            }
	    }
	 
	    return super.onKeyUp(keycode, e);
	}
	
	private void edit() {
		// TODO - edit current user's info
	}
	
	private void refresh() {
		//
		load(true, null);
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// 
		super.onConfigurationChanged(newConfig);
		
		if (newConfig.orientation != currentOrientation) {
			//
			currentOrientation = newConfig.orientation;
			//scroller.setVisibility(View.GONE);
			redraw();
			//refresh();
		}
	}
	
	public void redraw() {
		//
		grid.setNumColumns((int) adapter.getNumColumns());
		adapter.redraw();
	}

	@Override
	protected void onResume() {
		// 
		super.onResume();
		
		if (isDataServiceConnected()) {
			waitingForDataService = false;
			refresh();
		} else {
			//
			waitingForDataService = true;
		}
	}
	
	private User getUserFromIntent() {
		//
		if (user == null) {
			//
			Intent i = getIntent();
			user = (User) i.getExtras().get("params");
		}
		
		return user;
	}
	
	@Override
	public String getTag() {
		// 
		return ProfileScreen.class.getName();
	}

	@Override
	public String getViewTitle() {
		// 
		return "Profile";
	}
	
	@Override
	public String getViewFooter() {
		// 
		return null;
	}

	@Override
	protected void dataServiceConnected(boolean connected) {
		// 
		if (connected) {
			//
			profileHeader.setDataService(getDataService());
			
			if (waitingForDataService) {
				//
				refresh();
			}
		}
	}
	
	private boolean isCurrentUser() {
		//
		if (isDataServiceConnected()) {
			User appUser = getDataService().getUser(this);
			
			if (appUser != null) {
				//
				return Utils.equals(appUser.getId(), getUserFromIntent().getId());
			}
		}
		
		return false;
	}
	
	static class ImageViewHolder {
		//
		ViewGroup root;
		RecyclingImageView imgView;
		ViewGroup itemDescLayout;
		TextView nameView;
		TextView descView;
		TextView priceView;
		TextView condView;
		View likes;
		ImageView likeIcon;
	}
	
	private class ProductAdapter extends BaseAdapter {
		//
		private static final String TAG = "MyListingsAdapter";

		private FragmentActivity context;
		private ProfileScreen view;
		private List<Item> items = Collections.synchronizedList(new ArrayList<Item>());

		public ProductAdapter(ProfileScreen view, FragmentActivity context, List<Item> items) {
			super();
			this.view = view;
			this.context = context;

			if (items != null) {
				this.items.addAll(items);
			}

			init();
		}

		@SuppressLint("HandlerLeak")
		private void init() {
			//
		}
		
		private int getResId() {
			return R.layout.product;
		}
		
		public void redraw() {
			//
			super.notifyDataSetChanged();
		}

		@SuppressLint("HandlerLeak")
		public View getView(final int position, View convertView, ViewGroup parent) {

			View v = convertView;
			// 
			if (v == null) {
				LayoutInflater vi = (LayoutInflater) getSystemService(
						Context.LAYOUT_INFLATER_SERVICE);
				v = vi.inflate(getResId(), parent, false);
			} else {
				//
				//v.forceLayout();
			}

			final Item item = (Item) getItem(position);

			if (item != null) {
				//
				initImageDisplay(item, v);
			}

			return v;
		}
		
		private void initImageDisplay(final Item item, final View v) {
			// 
			ImageViewHolder viewHolder;
			
			if (v.getTag() != null && v.getTag() instanceof ImageViewHolder) {
				viewHolder = (ImageViewHolder)v.getTag();
			} else {
				//
				viewHolder = new ImageViewHolder();
				
				viewHolder.root = (ViewGroup) v.findViewById(R.id.root);
				viewHolder.itemDescLayout = (ViewGroup) v.findViewById(R.id.itemDescLayout);
				viewHolder.imgView = (RecyclingImageView) v.findViewById(R.id.itemImage);
				viewHolder.nameView = (TextView) v.findViewById(R.id.itemTitle);
				viewHolder.descView = (TextView) v.findViewById(R.id.itemDesc);
				viewHolder.priceView = (TextView) v.findViewById(R.id.itemPrice);
				viewHolder.condView = (TextView) v.findViewById(R.id.itemCondition);
				viewHolder.likes = (View) v.findViewById(R.id.likeLayout);
				viewHolder.likeIcon = (ImageView) v.findViewById(R.id.likeIcon);
				
				v.setTag(viewHolder);
			}
			
			setSize(viewHolder.root);
			
			viewHolder.descView.setVisibility(View.GONE);
			viewHolder.condView.setVisibility(View.GONE);
			viewHolder.likes.setVisibility(View.GONE);
			viewHolder.likeIcon.setVisibility(View.GONE);
			
			if (!Utils.isEmpty(item.getImages())) {
				// Fetch and set bitmap for item
				String image = item.getImages().get(0).getUri();
				
				view.loadThumbnail(image, viewHolder.imgView);
			} else {
				// no images
				viewHolder.imgView.setScaleType(ScaleType.CENTER);
				viewHolder.imgView.setImageResource(R.drawable.loading);
			}
			
			CharSequence title = Utils.isEmpty(item.getTitle()) ? item.getDescription() : item.getTitle();
			viewHolder.nameView.setText(Utils.emptyIfNull(title));
			
			viewHolder.priceView.setText(Utils.formatPrice(context, item.getPrice()));

			v.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					//
					//view.displayProductDetails(item);
					ShopApplication.goToProductDetails(context, item.getId());
				}
			});
		}
		
		private void setSize(ViewGroup root) {
			// 
			LayoutParams params = root.getLayoutParams();
			
			// calculate width
			final DisplayMetrics screenSize = new DisplayMetrics();
			context.getWindowManager().getDefaultDisplay().getMetrics(screenSize);
			
			double screenWidth = screenSize.widthPixels;//context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT ? screenSize.widthPixels : screenSize.heightPixels;
			
			double swFactor = getNumColumns();
			
			params.width = (int) (screenWidth/swFactor);
			params.height = params.width;
			
			root.setLayoutParams(params);
		}
		
		public double getNumColumns() {
			// calculate width
			final DisplayMetrics screenSize = new DisplayMetrics();
			context.getWindowManager().getDefaultDisplay().getMetrics(screenSize);

			double screenWidth = screenSize.widthPixels;
			double swFactor = 1;

			double prod_pref_size = 0;
			
			prod_pref_size = context.getResources().getDimensionPixelSize(R.dimen.product_img_size);

			swFactor = Math.ceil(screenWidth / prod_pref_size);
			
			if (swFactor < 1) {
				swFactor = 1;
			}
			
			return (int) swFactor;
		}

		@Override
		public int getCount() {
			return items.size();
		}

		@Override
		public Object getItem(int position) {
			Object item = null;
			
			try {
				item = items.get(position);
			} catch (Exception ex) {
				Log.e(TAG, "Unable to get item", ex);
			}
			
			return item;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		/**
		 * Append these items to the end of the list
		 * 
		 * @param items
		 *            - items to append
		 */
		public void append(List<Item> items) {
			if (items != null) {
				this.items.addAll(items);
				super.notifyDataSetChanged();
			}
		}
		
		public void refresh(List<Item> items) {
			//
			this.items.clear();
			append(items);
		}
	}
	
	private void loadNextPage() {
		// load the next page of products
		
		if (isDataServiceConnected() && lastItem != null) {
			// make sure reload is only called once as needed
			showMessage("Loading next page...");
			
			String bottom = lastItem.getId();
			
			load(false, bottom);
			lastItem = null;
		}
	}
	
	@Override
	public void onLastItemVisible() {
		// 
		loadNextPage();
	}

	@Override
	public void onRefresh(PullToRefreshBase<ScrollView> refreshView) {
		// 
		refresh();
	}
}
